#ifndef _VIS_FEATS_TACTILE_IMG_
#define _VIS_FEATS_TACTILE_IMG_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <math.h>
#include <dynamic_reconfigure/server.h>

#include <tactile_servo_config/img_proc_paramsConfig.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/Image_weiss.h"
#include "tactile_servo_msgs/PlanFeats.h"

// #include "tactile_image_processing.hpp"

// for smooth transitions
#include "tactile_servo_msgs/COCtoZMP.h"

class VisFeats
{
public:
  
  VisFeats(int sensors_number);
  ~VisFeats();
  void show_edge_feats();
  void show_point_feats();
  void show_two_point_feats();
  void show_feats();
  
  // for smooth coc2zmp
  ros::Subscriber num_pixels_weight_gain_sub;
  double smooth_;
  void num_pixels_weight_gain_cb (const tactile_servo_msgs::COCtoZMP& msg);
  
private:
  ros::NodeHandle nh;
  void get_sensor_params(int& tactile_sensor_num);
  void ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist,
			  int& tactile_sensor_num); 
  
  int rows_, cols_, step_;
  double size_x_, size_y_, tactel_area_;
  
  std::string tact_img_top_name_, tact_img_top_name_12bit_;
  std::string fb_feats_control_top_name_, fb_feats_plan_top_name_, 
  des_feats_top_name_;
  
  int noise_threshold_, coc_threshold_, area_threshold_,
  contact_points_n_;
  double pres_from_image_, force_scale_;
  
  int scaling_factor_;
  
  float total_pressure_, contact_orientation_;
  
  
  ros::Subscriber des_feats_sub;
  ros::Subscriber fb_feats_sub;
  ros::Subscriber fb_plan_sub;
  ros::Subscriber fb_feats_pca_sub;
  
  void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
  void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
  
  void cb_plan_sub(const tactile_servo_msgs::PlanFeatsConstPtr& msg_);
  
  float copx_des,
  copy_des,
  force_des,
    cocx_des,
    cocy_des,
    orientz_des;
  float copx_fb,
  copy_fb,
  force_fb,
    cocx_fb,
    cocy_fb,
    orientz_fb;
  
  cv::Point2d cop_viz, coc_viz;
  cv::Point2d cop_viz_des, coc_viz_des;
  
  
  void sizes2pixels(float& x, float& y, cv::Point2d& ret_point);
  
  
  
  void draw_circle(cv::Mat& img, cv::Point2d& center,
		   int r, int g, int b);
  
  void draw_gunsight(cv::Mat& img, cv::Point2d& center,
		     int r, int g, int b);
  
  void draw_line(cv::Mat& img, cv::Point2d& center, 
		 float orient, int r, int g, int b);
  
  void draw_squares(cv::Mat& img, cv::Point2d& center,
		    int r, int g, int b);
  
  image_transport::ImageTransport it_viz;
  image_transport::Subscriber image_sub_viz;
  void image_cb_viz(const sensor_msgs::ImageConstPtr& msg);
  cv::Mat src_img_;
  
  void show_windows(cv::Mat& img_show_feats);
  
  std::string type_contact_; 
  int num_contacts_;
  
  void apply_pca_viz(std::vector<cv::Point> &pts, 
		     int contour_num, 
		     int tot_num_contours);
  cv::Point coc_one_viz_, coc_two_viz_;
  
  
  void cb_dynamic_reconf(tactile_servo_config::img_proc_paramsConfig &config, uint32_t level);
  dynamic_reconfigure::Server<tactile_servo_config::img_proc_paramsConfig> change_improc_param_server;
  dynamic_reconfigure::Server<tactile_servo_config::img_proc_paramsConfig> :: CallbackType f;
  
  cv::Point2d cop, coc;
  std::string sensor_frame_name_urdf_ ;
  
  cv::Mat bin_img_;
  cv::Point2d coc_one_, coc_two_;
  double area_contact_one_, area_contact_two_;
  
  std::string window_name_feats; 
  std::string window_name_bin; 
  
  

};
#endif