/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 2 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @brief  This is a library class for tactile image processing
 */

#include "tactile_image/vis_feats_tactile_image.hpp"
// #include "tactile_image/tactile_image_processing.hpp"

VisFeats::VisFeats(int sensors_number): it_viz(nh)
{
  
  
  get_sensor_params(sensors_number);
  image_sub_viz = it_viz.subscribe("/"+tact_img_top_name_, 1,
				   &VisFeats::image_cb_viz, this);
  
  des_feats_sub = nh.subscribe(des_feats_top_name_, 1,  &VisFeats::cb_des_feats_sub,this);
  fb_feats_sub = nh.subscribe(fb_feats_control_top_name_, 1,  &VisFeats::cb_fb_feats_sub,this);
  
  fb_plan_sub = nh.subscribe(fb_feats_plan_top_name_, 1,  &VisFeats::cb_plan_sub,this);
  
  f = boost::bind(&VisFeats::cb_dynamic_reconf, this, _1, _2);
  change_improc_param_server.setCallback(f);
  
  
  copx_des = 0;
  copy_des = 0;
  force_des =0;
  cocx_des=0;
  cocy_des =0;
  orientz_des=0;
  copx_fb=0;
  copy_fb=0;
  force_fb=0;
  cocx_fb=0;
  cocy_fb=0;
  orientz_fb=0;
  
  scaling_factor_ = 30;
  coc_threshold_ = 5;
  
  cop_viz.x = 0;
  cop_viz_des.x = 0;
  cop_viz.y = 0;
  cop_viz_des.y = 0;
  
  window_name_feats = "I&F";
  window_name_bin= "b&w";
  cv::namedWindow(window_name_feats, cv::WINDOW_NORMAL);
  cv::namedWindow(window_name_bin, cv::WINDOW_NORMAL);
//   cv::namedWindow("test", cv::WINDOW_NORMAL);
// 
//   cv::namedWindow("testColor", cv::WINDOW_NORMAL);

  //   cv::namedWindow("not_filtered", WINDOW_NORMAL);
  cv::startWindowThread();
  
//       ROS_FATAL_STREAM("initialized = ");
  
  //coc2zmp smooth
    num_pixels_weight_gain_sub = nh.subscribe("/coc_zmp_smooth_trans", 2, &VisFeats::num_pixels_weight_gain_cb, this);
  smooth_ = 1;


}

VisFeats::~VisFeats()
{
  cv::destroyAllWindows();
}
void VisFeats::num_pixels_weight_gain_cb(const tactile_servo_msgs::COCtoZMP& msg)
{
  smooth_ = msg.coc_to_zmp_smooth;
}

void VisFeats::image_cb_viz(const sensor_msgs::ImageConstPtr& msg)
{
  uchar img[rows_][cols_];
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  
  int c = 0;
  total_pressure_ = 0;
  contact_points_n_ = 0;
  for(int i=0; i<rows_; ++i) {
    for(int j=0; j<cols_; j++) {
      // make it visible
      img[i][j]=cv_ptr->image.data[c]*3;
      total_pressure_ += img[i][j];
      c = c + 1;
      if (img[i][j] >= coc_threshold_)
      {
	contact_points_n_ = contact_points_n_ + 1;
      }
    }
  }
  src_img_ = cv::Mat(rows_, cols_, CV_8UC1, img);
  show_feats();
  
//   cv::Mat rsz_src_test;
//   cv::resize(src_img_, rsz_src_test, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
//   cv::imshow("test", rsz_src_test);
  

}

void VisFeats::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
//     ROS_FATAL_STREAM("cb_des_feats_sub = " << msg_->control_features.size() );

  if (msg_->control_features.size() == 1)
  {
    copx_des = msg_->control_features[0].centerpressure_x + size_x_/2.0;
    copy_des = msg_->control_features[0].centerpressure_y + size_y_ / 2.0;
    force_des = msg_->control_features[0].contactForce;


//     cocx_des = msg_->control_features[0].centerContact_x + size_x_/2.0;
//     cocy_des = msg_->control_features[0].centerContact_y + size_y_ / 2.0;
    
    // temporary switch in order to visualize features for control force and 
    // orientation
    cocx_des = msg_->control_features[0].centerpressure_x + size_x_/2.0;
    cocy_des = msg_->control_features[0].centerpressure_y + size_y_ / 2.0;
    copx_des = msg_->control_features[0].centerContact_x + size_x_/2.0;
    copy_des = msg_->control_features[0].centerContact_y + size_y_ / 2.0;
    
    orientz_des = msg_->control_features[0].contactOrientation;
    
    sizes2pixels(copx_des, copy_des, cop_viz_des);
    sizes2pixels(cocx_des, cocy_des, coc_viz_des);
  }
}

void VisFeats::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
//       ROS_FATAL_STREAM("cb_fb_feats_sub = " << msg_->control_features.size() );

  if (msg_->control_features.size() == 1)
  {
    // in kuka the center is in the center add the half
    copx_fb = msg_->control_features[0].centerpressure_x + size_x_/2.0;
    copy_fb = msg_->control_features[0].centerpressure_y + size_y_ / 2.0;
    force_fb = msg_->control_features[0].contactForce;
    
//     cocx_fb = msg_->control_features[0].centerContact_x + size_x_/2.0 ;
//     cocy_fb = msg_->control_features[0].centerContact_y + size_y_/2.0;
    
    // temporary switch in order to visualize features for control force and 
    // orientation
    cocx_fb = msg_->control_features[0].centerpressure_x + size_x_/2.0;
    cocy_fb = msg_->control_features[0].centerpressure_y + size_y_ / 2.0;
    copx_fb = msg_->control_features[0].centerContact_x + size_x_/2.0;
    copy_fb = msg_->control_features[0].centerContact_y + size_y_ / 2.0;
    
    orientz_fb = msg_->control_features[0].contactOrientation;
    
    sizes2pixels(copx_fb, copy_fb, cop_viz);
    sizes2pixels(cocx_fb, cocy_fb, coc_viz);
    
  }
}

void VisFeats::cb_plan_sub(const tactile_servo_msgs::PlanFeatsConstPtr& msg_)
{
//   ROS_FATAL_STREAM("contact_type = " << msg_->TypeContact  );
  
  type_contact_ = msg_->TypeContact;
  num_contacts_ = msg_->numContours;
  
}

void VisFeats::show_feats()
{
  // TODO function to show eigen vectors  void VisFeats::recognize(){}
  // tmp function to recognize edge
   smooth_ = 1;
  if (smooth_ == 0)
  {
    type_contact_ = "edge";
  }
   if (smooth_ == 1)
  {
    type_contact_ = "point";
  }
  
//   type_contact_ = "edge";
  if (type_contact_ == "edge")
  {
//         show_point_feats();
        show_edge_feats();
  }
  if (type_contact_ == "point")
  {
//     show_edge_feats();
        show_point_feats();
  }
  if (type_contact_ == "two_point")
  {
//         show_point_feats();
    show_edge_feats();
    //     show_two_point_feats();
  }
  
}


void VisFeats::show_point_feats()
{
  cv::Mat rsz_cop;
  cv::resize(src_img_, rsz_cop, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  cv::Mat rsz_color_cop;
  cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
  //fb cop
  draw_circle(rsz_color_cop, cop_viz, 255, 0, 0);
  
  /////////
  // coc //
  /////////
  
  draw_gunsight(rsz_color_cop, coc_viz, 255, 0, 0);
  
  
  //des cop
  // temprary removed to show COP and COC only
//   draw_circle(rsz_color_cop, cop_viz_des, 0, 255, 0);
  
//   draw_gunsight(rsz_color_cop, coc_viz_des, 0, 255, 0);
    // beatiful contour
  cv::Mat bin_img;
  cv::Mat img_cp = src_img_ ;

  cv::threshold(src_img_, bin_img, coc_threshold_, 255, CV_THRESH_TOZERO);
   
  cv::Mat rsz;
  cv::resize(bin_img, rsz, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
 
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  
  cv::findContours(rsz, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
  
   
  if (contours.size() > 0)
  {
    for (size_t i = 0; i <contours.size(); ++i)
    {
        if (i == 0)
      {
// 	cv::drawContours(rsz_color_cop, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
	apply_pca_viz(contours.at(i), i, contours.size() );
	
	std::vector<cv::Point> pts = contours.at(i);
	
	cv::Mat data_pts = cv::Mat(pts.size(), 2, CV_64FC1);
	for (int ind = 0; ind < data_pts.rows; ++ind)
	{
	  data_pts.at<double>(ind, 0) = pts.at(ind).x;
	  data_pts.at<double>(ind, 1) = pts.at(ind).y;
	}
	cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
	
	cv::Point2d pos = cv::Point2d(pca_analysis.mean.at<double>(0, 0),
				      pca_analysis.mean.at<double>(0, 1));
	
	std::vector<cv::Point2d> eigen_vecs(2);
	std::vector<double> eigen_val(2);
	for (int ind = 0; ind < 2; ++ind)
	{
	  eigen_vecs.at(ind) = cv::Point2d(pca_analysis.eigenvectors.at<double>(ind, 0),
					   pca_analysis.eigenvectors.at<double>(ind, 1));
	  eigen_val.at(ind) = pca_analysis.eigenvalues.at<double>(0, ind);
	}
	
	cv::Point2d eig_one, eig_two;
	double k_scale = 0.0002;
	eig_one.x = k_scale*eigen_val.at(0)*eigen_vecs.at(0).x;
	eig_one.y = k_scale*eigen_val.at(0)*eigen_vecs.at(0).y;
	eig_two.x = k_scale*eigen_val.at(1)*eigen_vecs.at(1).x;
	eig_two.y = k_scale*eigen_val.at(1)*eigen_vecs.at(1).y;
	cv::Point2d p1 = pos +
	0.02 * cv::Point2d(
	  (eigen_vecs[0].x * eigen_val[0]),
			   (eigen_vecs[0].y * eigen_val[0]));
	cv::Point2d p2 = pos -
	0.02 * cv::Point2d(
	  (eigen_vecs[1].x * eigen_val[1]),
			   (eigen_vecs[1].y * eigen_val[1] ));
// 	cv::line(rsz_color_cop, pos, p1, CV_RGB(0, 255, 0), 3, 8);
// 	cv::line(rsz_color_cop, pos, p2, CV_RGB(0, 155, 0), 3, 8);
	
	  
      }
    }
  }
  show_windows(rsz_color_cop);
  
}



void VisFeats::show_edge_feats()
{
  cv::Mat rsz_cop;
ROS_DEBUG_STREAM("resize test1");

  cv::resize(src_img_, rsz_cop, cv::Size(),
	     scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
//   ROS_FATAL_STREAM("resize test2");
  cv::Mat rsz_color_cop;
  cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
//   ROS_FATAL_STREAM("resize test2");
//   cv::imshow( "testColor", rsz_color_cop );
//   ROS_FATAL_STREAM("resize test3");
  
  /////////////////
  //////  fb  /////
  /////////////////
  /////////
  // cop //
  /////////
  cv::Point2d cop_viz_reverse;
  cop_viz_reverse.x = cop_viz.y;
  cop_viz_reverse.y = cop_viz.x;
  
  draw_circle(rsz_color_cop, cop_viz, 255, 0, 0);
  //   cv::circle(rsz_color_cop, scaling_factor_*cop_viz, 3, CV_RGB(255, 0, 0), 3);
  /////////
  // coc //
  /////////
  
  cv::Point2d coc_viz_reverse;
  coc_viz_reverse.x = coc_viz.y;
  coc_viz_reverse.y = coc_viz.x;
  draw_gunsight(rsz_color_cop, coc_viz, 255, 0, 0);
  /*
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, scaling_factor_*(coc_viz + 0.1) ,
   *    CV_RGB(255, 0, 0), 3,8);
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, scaling_factor_*(coc_viz - 0.1) ,
   *    CV_RGB(255, 0, 0), 3,8);
   * 
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
   *    cv::Point2d(scaling_factor_*(coc_viz.x - 0.1) ,scaling_factor_*(coc_viz.y + 0.1) ),
   *    CV_RGB(255, 0, 0), 3,8);
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
   *    cv::Point2d(scaling_factor_*(coc_viz.x + 0.1) ,scaling_factor_*(coc_viz.y - 0.1) ),
   *    CV_RGB(255, 0, 0), 3,8);*/
  ////////////
  // orient //
  ////////////
  draw_line(rsz_color_cop, coc_viz, orientz_fb, 255, 0, 0);
  /*
   *   float y_temp_val_to_show_orient = std::tan(orientz_fb);
   *   cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz + 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);
   *   cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz - 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);*/
  
  //////////////////
  //////  des  /////
  //////////////////
  //cop
  
  cv::Point2d cop_viz_reverse_des;
  cop_viz_reverse_des.x = cop_viz_des.y;
  cop_viz_reverse_des.y = cop_viz_des.x;
  
  // temporary remove to show fb feats only
  draw_circle(rsz_color_cop, cop_viz_des, 0, 255, 0);
  
  //   cv::circle(rsz_color_cop, scaling_factor_*cop_viz_des, 3, CV_RGB(255, 0, 0), 3);
  //coc
  cv::Point2d coc_viz_reverse_des;
  coc_viz_reverse_des.x = coc_viz_des.y;
  coc_viz_reverse_des.y = coc_viz_des.x;
  // temporary remove to show fb feats only
  draw_gunsight(rsz_color_cop, coc_viz_des, 0, 255, 0);
  
  /*cv::line(rsz_color_cop, scaling_factor_*coc_viz_des, scaling_factor_*(coc_viz_des + 0.1) ,
   *    CV_RGB(255, 0, 0), 3,8);
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz_des, scaling_factor_*(coc_viz_des - 0.1) ,
   *    CV_RGB(255, 0, 0), 3,8);
   * 
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
   *    cv::Point2d(scaling_factor_*(coc_viz.x - 0.1) ,scaling_factor_*(coc_viz.y + 0.1) ),
   *    CV_RGB(255, 0, 0), 3,8);
   * cv::line(rsz_color_cop, scaling_factor_*coc_viz, 
   *    cv::Point2d(scaling_factor_*(coc_viz.x + 0.1) ,scaling_factor_*(coc_viz.y - 0.1) ),
   *    CV_RGB(255, 0, 0), 3,8);
   */ 
  // oreint
  
  // temporary remove to show fb feats only
  draw_line(rsz_color_cop, coc_viz_des, orientz_des, 0, 255, 0);
  
  /*  float y_temp_val_to_show_orient = std::tan(orientz_fb);
   * cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz + 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);
   * //  cv::line(rsz_color_cop, scaling_factor_*cop_viz, (scaling_factor_*cop_viz - 2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, scaling_factor_)) , CV_RGB(255, 0, 0), 3,8);
   */
  
  // beatiful contour
  cv::Mat bin_img;
  cv::Mat img_cp = src_img_ ;

  cv::threshold(src_img_, bin_img, coc_threshold_, 255, CV_THRESH_TOZERO);
   
  cv::Mat rsz;
  cv::resize(bin_img, rsz, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
 
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  
  cv::findContours(rsz, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
  
   
  if (contours.size() > 0)
  {
    for (size_t i = 0; i <contours.size(); ++i)
    {
        if (i == 0)
      {
// 	cv::drawContours(rsz_color_cop, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
	apply_pca_viz(contours.at(i), i, contours.size() );
	
	std::vector<cv::Point> pts = contours.at(i);
	
	cv::Mat data_pts = cv::Mat(pts.size(), 2, CV_64FC1);
	for (int ind = 0; ind < data_pts.rows; ++ind)
	{
	  data_pts.at<double>(ind, 0) = pts.at(ind).x;
	  data_pts.at<double>(ind, 1) = pts.at(ind).y;
	}
	cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
	
	cv::Point2d pos = cv::Point2d(pca_analysis.mean.at<double>(0, 0),
				      pca_analysis.mean.at<double>(0, 1));
	
	std::vector<cv::Point2d> eigen_vecs(2);
	std::vector<double> eigen_val(2);
	for (int ind = 0; ind < 2; ++ind)
	{
	  eigen_vecs.at(ind) = cv::Point2d(pca_analysis.eigenvectors.at<double>(ind, 0),
					   pca_analysis.eigenvectors.at<double>(ind, 1));
	  eigen_val.at(ind) = pca_analysis.eigenvalues.at<double>(0, ind);
	}
	
	cv::Point2d eig_one, eig_two;
	double k_scale = 0.0002;
	eig_one.x = k_scale*eigen_val.at(0)*eigen_vecs.at(0).x;
	eig_one.y = k_scale*eigen_val.at(0)*eigen_vecs.at(0).y;
	eig_two.x = k_scale*eigen_val.at(1)*eigen_vecs.at(1).x;
	eig_two.y = k_scale*eigen_val.at(1)*eigen_vecs.at(1).y;
	cv::Point2d p1 = pos +
	0.02 * cv::Point2d(
	  (eigen_vecs[0].x * eigen_val[0]),
			   (eigen_vecs[0].y * eigen_val[0]));
	cv::Point2d p2 = pos -
	0.02 * cv::Point2d(
	  (eigen_vecs[1].x * eigen_val[1]),
			   (eigen_vecs[1].y * eigen_val[1] ));
// 	cv::line(rsz_color_cop, pos, p1, CV_RGB(0, 255, 0), 3, 8);
// 	cv::line(rsz_color_cop, pos, p2, CV_RGB(0, 155, 0), 3, 8);
	
	  
      }
    }
  }
  
  show_windows(rsz_color_cop);
}

void VisFeats::show_two_point_feats()
{
  cv::Mat bin_img;
  cv::Mat img_cp = src_img_ ;

  cv::threshold(src_img_, bin_img, coc_threshold_, 255, CV_THRESH_TOZERO);
   
  cv::Mat rsz;
  cv::resize(bin_img, rsz, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
 
 
  
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  
  cv::findContours(rsz, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
  
  cv::Mat rsz_cop;
  cv::resize(img_cp, rsz_cop, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  
  
  cv::Mat rsz_color_cop;
  cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
  
  
  if (contours.size() > 0)
  {
    for (size_t i = 0; i <contours.size(); ++i)
    {
      cv::drawContours(rsz_color_cop, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
      apply_pca_viz(contours.at(i), i, contours.size() );
      
      if (i == 1)
      {
	line(rsz_color_cop, coc_one_viz_, coc_two_viz_, CV_RGB(255, 0, 0), 3,8); // line btw two points
	cv::circle(rsz_color_cop, coc_one_viz_, 3, CV_RGB(255, 0, 0), 3); // center of pressure
	cv::circle(rsz_color_cop, coc_two_viz_, 3, CV_RGB(255, 0, 0), 3); // center of pressure
	
      }
    }
  }
  show_windows(rsz_color_cop);
}

void VisFeats::apply_pca_viz(std::vector<cv::Point> &pts, 
			     int contour_num, 
			     int tot_num_contours)
{
  cv::Mat data_pts = cv::Mat(pts.size(), 2, CV_64FC1);
  for (int ind = 0; ind < data_pts.rows; ++ind)
  {
    data_pts.at<double>(ind, 0) = pts.at(ind).x;
    data_pts.at<double>(ind, 1) = pts.at(ind).y;
  }
  cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
  
  cv::Point2d pos = cv::Point2d(pca_analysis.mean.at<double>(0, 0),
				pca_analysis.mean.at<double>(0, 1));
  
  std::vector<cv::Point2d> eigen_vecs(2);
  std::vector<double> eigen_val(2);
  for (int ind = 0; ind < 2; ++ind)
  {
    eigen_vecs.at(ind) = cv::Point2d(pca_analysis.eigenvectors.at<double>(ind, 0),
				     pca_analysis.eigenvectors.at<double>(ind, 1));
    eigen_val.at(ind) = pca_analysis.eigenvalues.at<double>(0, ind);
  }
  
  
  if (tot_num_contours == 2)
  {
    if (contour_num == 0)
    {
      coc_one_viz_ = pos;
    }
    else if (contour_num == 1)
    {
      coc_two_viz_ = pos;
    }
  }
}


void VisFeats::show_windows(cv::Mat& img_show_feats)
{
  //display image/ 
  cv::imshow( window_name_feats, img_show_feats );
  //    cv::resizeWindow("TI&F", cols*scaling_factor, rows*scaling_factor);
  
  //display image binary
  cv::Mat rsz_coc;
  cv::Mat bin_img_viz;
  cv::threshold(src_img_, bin_img_viz, coc_threshold_, 255, CV_THRESH_BINARY);
  cv::resize(bin_img_viz, rsz_coc, cv::Size(), scaling_factor_, scaling_factor_,  cv::INTER_NEAREST);
  cv::imshow(window_name_bin, rsz_coc);
}

void VisFeats::draw_circle(cv::Mat& img, cv::Point2d& center,
			   int r, int g, int b)
{
  cv::circle(img, scaling_factor_*center, 3, CV_RGB(r, g, b), 3);
}

void VisFeats::draw_gunsight(cv::Mat& img, cv::Point2d& center,
			     int r, int g, int b)
{
  double gun = 0.25;
  cv::line(img, scaling_factor_*center, scaling_factor_* cv::Point2d(center.x + gun,center.y + gun) ,
	   CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, scaling_factor_*cv::Point2d(center.x - gun,center.y - gun) ,
	   CV_RGB(r, g, b), 3,8);
  
  cv::line(img, scaling_factor_*center, 
	   cv::Point2d(scaling_factor_*(center.x - gun) ,scaling_factor_*(center.y + gun) ),
	   CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, 
	   cv::Point2d(scaling_factor_*(center.x + gun) ,scaling_factor_*(center.y - gun) ),
	   CV_RGB(r, g, b), 3,8);
}

void VisFeats::draw_line(cv::Mat& img, cv::Point2d& center, float orient,
			 int r, int g, int b)
{
  float y_temp_val_to_show_orient = std::tan(orient);
  cv::line(img, scaling_factor_*center, 
	   (scaling_factor_*center +  
	   2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, 
			   scaling_factor_)) ,
	   CV_RGB(r, g, b), 3,8);
  cv::line(img, scaling_factor_*center, 
	   (scaling_factor_*center - 
	   2 * cv::Point2d(scaling_factor_*y_temp_val_to_show_orient, 
			   scaling_factor_)) , 
	   CV_RGB(r, g, b), 3,8);
}

void VisFeats::draw_squares(cv::Mat& img, cv::Point2d& center,
			    int r, int g, int b)
{
  /*
   *   
   *   //coc rect
        cv::Rect rect1;
   //     rect1.x = scaling_factor_*(coc_viz.x-0.5);
   //     rect1.y = scaling_factor_*(coc_viz.y-0.5);
   //     rect1.width = scaling_factor_*0.5;
   //     rect1.height = scaling_factor_*0.5;
   //     cv::Rect rect2;
   //     rect2.x = scaling_factor_*(coc_viz.x);
   //     rect2.y = scaling_factor_*coc_viz.y;
   //     rect2.width = scaling_factor_*0.5;
   //     rect2.height = scaling_factor_*0.5;
   //     cv::Rect rect3;
   //     rect3.x = scaling_factor_*(coc_viz.x-0.5);
   //     rect3.y = scaling_factor_*(coc_viz.y);
   //     rect3.width = scaling_factor_*0.5;
   //     rect3.height = scaling_factor_*0.5;
   //     cv::Rect rect4;
   //     rect4.x = scaling_factor_*(coc_viz.x);
   //     rect4.y = scaling_factor_*(coc_viz.y-0.5);
   //     rect4.width = scaling_factor_*0.5;
   //     rect4.height = scaling_factor_*0.5;
   //     cv::rectangle(rsz_color_cop, rect1,  CV_RGB(255, 0, 0),3, 3);
   //     cv::rectangle(rsz_color_cop, rect2,  CV_RGB(255, 0, 0),3, 3);
   //     cv::rectangle(rsz_color_cop, rect3,  CV_RGB(255, 0, 0),3, 3);
   //     cv::rectangle(rsz_color_cop, rect4,  CV_RGB(255, 0, 0),3, 3);
   */  
}

void VisFeats::sizes2pixels(float& x, float& y, cv::Point2d& ret_point)
{
  /*
   * float copx = size_x_ - (float)((float)size_x_/
   * ((float)rows_-1)) * ((float) (cop.y));
   * 
   * float copy = size_y_ - (float)((float)size_y_/
   * ((float)cols_-1)) * ((float) (cop.x));
   * 
   * float cocx = size_x_ - (float)((float)size_x_/
   * ((float)rows_-1)) * ((float) (coc.y));
   * 
   * float cocy = size_y_ - (float)((float)size_y_/
   * ((float)cols_-1)) * ((float) (coc.x));
   */
  //   cop_viz.y = (size_x_ - copx_fb) * (rows_ - 1)/size_x_;
  //   cop_viz.x = (size_y_ - copy_fb) * (cols_ - 1)/size_y_;
  //   
  //   coc_viz.y = (size_x_ - cocx_fb) * (rows_ - 1)/size_x_;
  //   coc_viz.x = (size_y_ - cocy_fb) * (cols_ - 1)/size_y_;
  
  ret_point.x  = (size_y_ - y) * (cols_ - 0)/size_y_;
  ret_point.y  = (size_x_ - x) * ( rows_ - 0)/size_x_;
} 



void VisFeats::get_sensor_params(int& tactile_sensor_num){
  /*
   *   if (gazebo_bumper_topic_ == "cea/ffj3"){
   *       sensor_count = 1;
}*/
  std::string sensor_name;
  XmlRpc::XmlRpcValue my_list;
  //get tactile sensor's parameters from param server
  nh.getParam("/tactile_arrays_param", my_list);
  
  ckeck_param_fields(my_list, tactile_sensor_num);
  
  tact_img_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_image_topic_name"]);
  tact_img_top_name_12bit_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_pressure_topic_name"]);
  
  // get topic names of control features for a given sensor
  fb_feats_control_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_control_topic_name"]);
  fb_feats_plan_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_planning_topic_name"]);
  des_feats_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["des_feats_topic_name"]);
  
  rows_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][0]["cells_x"]);
  cols_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
  step_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
  size_x_=static_cast<double> (my_list[tactile_sensor_num]["size"][0]["x"]);
  size_y_=static_cast<double> (my_list[tactile_sensor_num]["size"][1]["y"]);
  sensor_frame_name_urdf_ =  static_cast<std::string> (my_list[tactile_sensor_num]["frame_name_urdf"]);
  tactel_area_ = static_cast<double> (my_list[tactile_sensor_num]["tactel_area"]);
}

void VisFeats::ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist, int& tactile_sensor_num)
{
  ROS_ASSERT_MSG(xmlrpclist.getType() == XmlRpc::XmlRpcValue::TypeArray, "check param /tactile_arrays_param");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "control_topic_name string  ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test planning_topic_name ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["des_feats_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "desired_topic_name");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("dimensions"), "Test dimensions");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("frame_id_header"), "Test frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["tactel_area"].getType() == XmlRpc::XmlRpcValue::TypeDouble, "Test tactel_area");
}

void VisFeats::cb_dynamic_reconf(tactile_servo_config::img_proc_paramsConfig &config, uint32_t level){
  noise_threshold_ = config.noise_threshold;
  coc_threshold_ = config.coc_threshold;
  area_threshold_ = config.improc_area_thresh;
  scaling_factor_ = config.improc_scaling_factor;
  force_scale_ = config.force_scale;
  pres_from_image_ = config.pres_from_image;
  ROS_INFO("noise_threshold:%d", (int)noise_threshold_);
  ROS_INFO("coc_threshold:%d", (int)coc_threshold_);
}