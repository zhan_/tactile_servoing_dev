/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include "tactile_image/tactile_image_processing_kuka.hpp"
#include <ros/ros.h>
#include <cmath>

ImageToolsKuka::ImageToolsKuka( int sensors_number):it(nh)
{
  get_sensor_params(sensors_number);
  // dynamic reconfigure
  f = boost::bind(&ImageToolsKuka::cb_dynamic_reconf, this, _1, _2);
  change_improc_param_server.setCallback(f);
  
  fb_feats_pub = nh.advertise<tactile_servo_msgs::ContsFeats>(fb_feats_control_top_name_, 1);
  fb_feat_set.header.frame_id = sensor_frame_name_urdf_;
  
  fb_feats_pub_pca = nh.advertise<tactile_servo_msgs::ContsFeats>("fb_feats_pca", 1);
  fb_feat_set_pca.header.frame_id = sensor_frame_name_urdf_;
  
  fb_plan_pub = nh.advertise<tactile_servo_msgs::PlanFeats>(fb_feats_plan_top_name_, 1);
  fb_feat_plan_set.header.frame_id = sensor_frame_name_urdf_;
  
  start_tactile = nh.advertise<std_msgs::Bool>("/is_contact", 2);
  force_estim_pub = nh.advertise<std_msgs::Float64>("/force_area", 2);

  
  image_sub = it.subscribe("/"+tact_img_top_name_, 1,
			   &ImageToolsKuka::image_cb, this);
//   image_sub_orient = it_orient.subscribe("/"+"tactile_image_high_gain", 2,
// 			   &ImageToolsKuka::image_cb_orient, this);
  // tactile image with high dynamic range 16 bit
  // image transport not used
  //   image_sub = it_force.subscribe("/"+"ros_tactile_image_weiss", 2,
  // 			   &ImageToolsKuka::image_cb_force, this);
  
  image_sub_force  = nh.subscribe("/ros_tactile_image_weiss",2,
				  &ImageToolsKuka::image_cb_force,this);
  
  isContact = 0;
  filter_length_ = 15.0;
  force_old_ = 0.0;
  contact_orientation_old_ = 0.0;
  pca_orient_ = 0;
  zmpx_old_ = 0.0;
  zmpy_old_ = 0.0;
  force_ = 0.0;
  zmpx_ = 0.0;
  zmpy_ = 0.0;
  cocx_ = 0;
  cocy_ = 0;
  cocx_old_ = cocy_old_ = 0;
  copx_ = copy_ = copx_old_ = copy_old_ = 0;
  force_from_area_ = 20000;
  force_res = 0;
  
  num_pixels_weight_gain = nh.advertise<tactile_servo_msgs::COCtoZMP>("/coc_zmp_smooth_trans_testing", 2);
  coc2zmp.header.frame_id = "link_ati";
  coc2zmp.coc_to_zmp_smooth = 0;
  coc2zmp.num_pixels_contact = 0;
  coc2zmp_begin_num_pixels_ = 12;
  coc2zmp_end_num_pixels_ = 30;
  coc2zmp_pixels2smoothtrans_ = 0.025;
}

ImageToolsKuka::~ImageToolsKuka(){}

void ImageToolsKuka::get_sensor_params(int& tactile_sensor_num){
    /*
    if (gazebo_bumper_topic_ == "cea/ffj3"){
        sensor_count = 1;
    }*/
    std::string sensor_name;
    XmlRpc::XmlRpcValue my_list;
    //get tactile sensor's parameters from param server
    nh.getParam("/tactile_arrays_param", my_list);
    
    ckeck_param_fields(my_list, tactile_sensor_num);
    
    tact_img_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_image_topic_name"]);
    tact_img_top_name_12bit_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_pressure_topic_name"]);

    // get topic names of control features for a given sensor
    fb_feats_control_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_control_topic_name"]);
    fb_feats_plan_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_planning_topic_name"]);
    des_feats_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["des_feats_topic_name"]);
   
    rows_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][0]["cells_x"]);
    cols_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
    step_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
    size_x_=static_cast<double> (my_list[tactile_sensor_num]["size"][0]["x"]);
    size_y_=static_cast<double> (my_list[tactile_sensor_num]["size"][1]["y"]);
    sensor_frame_name_urdf_ =  static_cast<std::string> (my_list[tactile_sensor_num]["frame_name_urdf"]);
    tactel_area_ = static_cast<double> (my_list[tactile_sensor_num]["tactel_area"]);
}

void ImageToolsKuka::ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist, int& tactile_sensor_num)
{
  ROS_ASSERT_MSG(xmlrpclist.getType() == XmlRpc::XmlRpcValue::TypeArray, "check param /tactile_arrays_param");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "control_topic_name string  ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test planning_topic_name ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["des_feats_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "desired_topic_name");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("dimensions"), "Test dimensions");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("frame_id_header"), "Test frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["tactel_area"].getType() == XmlRpc::XmlRpcValue::TypeDouble, "Test tactel_area");
}

void ImageToolsKuka::cb_dynamic_reconf(tactile_servo_config::img_proc_paramsConfig &config, uint32_t level){
    noise_threshold_ = config.noise_threshold;
    coc_threshold_ = config.coc_threshold;
    area_threshold_ = config.improc_area_thresh;
    scaling_factor_ = config.improc_scaling_factor;
    force_scale_ = config.force_scale;
    pres_from_image_ = config.pres_from_image;
    torque_from_image_x_ = config.torque_from_image_x;
    torque_from_image_y_ = config.torque_from_image_y;
    compare_eigens_ = config.compare_eigens;
    filter_length_ = config.filter_length;
    force_from_area_ = config.force_from_area;
    coc2zmp_begin_num_pixels_ = config.coc2zmp_begin_num_pixels;
    coc2zmp_end_num_pixels_ = config.coc2zmp_end_num_pixels;
    coc2zmp_pixels2smoothtrans_ = config.coc2zmp_pixels2smoothtrans;


    ROS_INFO("set");
//     ROS_INFO("coc_threshold:%d", (int)coc_threshold_);
}


// publish fb_feats_pca plan_feats


void ImageToolsKuka::image_cb(const sensor_msgs::ImageConstPtr& msg)
{
    uchar img[rows_][cols_];
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
    isContact = 0;

    int c = 0;
    total_pressure_ = 0;
    contact_points_n_ = 0;
    for(int i=0; i<rows_; ++i) {
        for(int j=0; j<cols_; j++) {
                img[i][j]=cv_ptr->image.data[c];
                total_pressure_ += img[i][j];
		c = c + 1;
		if (img[i][j] >= coc_threshold_)
		{
		  contact_points_n_ = contact_points_n_ + 1;
		  isContact = isContact + 1;
		}
        }
    }
    
    estim_force_area.data = force_from_area_*contact_points_n_*tactel_area_;
    estim_force_area.data = estim_force_area_old_.data*(1.0 - (float)sample_factor_) + (float) sample_factor_ * estim_force_area.data;
    estim_force_area_old_.data = estim_force_area.data;
    
    // SMOOTH TRANSITION FROM COC TO ZMP WHILE ROTATING
    coc2zmp.num_pixels_contact = contact_points_n_;
    
    if (coc2zmp.num_pixels_contact <= coc2zmp_begin_num_pixels_)
    {
      coc2zmp.coc_to_zmp_smooth = 1;      
    }
    // REMOVE THIS FOR WITHOUT SMOOTH TRANSITION
    else if ((coc2zmp.num_pixels_contact > coc2zmp_begin_num_pixels_)
      &&(coc2zmp.num_pixels_contact <= coc2zmp_end_num_pixels_))
    {
      coc2zmp.coc_to_zmp_smooth = 1.0 - 
      ( (float)coc2zmp.num_pixels_contact*1.0 / (float)coc2zmp_end_num_pixels_  );
      if (coc2zmp.coc_to_zmp_smooth < 0){ coc2zmp.coc_to_zmp_smooth = 0;}
    }
    if (coc2zmp.num_pixels_contact > coc2zmp_end_num_pixels_)
    {
      coc2zmp.coc_to_zmp_smooth = 0.0;
    }
    coc2zmp.header.stamp = ros::Time::now();
    
    
    cv::Mat src_img = cv::Mat(rows_, cols_, CV_8UC1, img);
    
    cv::threshold(src_img, bin_img_, coc_threshold_, 255, CV_THRESH_BINARY);
//     image moments
    
    img_proc_moments (src_img);
    img_proc_pca (src_img);
    // to delete
    // ROS_DEBUG_STREAM("fb_feat_set=" << fb_one_cont_feats.centerpressure_x);
    
    std_msgs::Float64 send_force_from_pressure_instead_area;
    
    if (fb_feat_set.control_features.size() == 1) 
    {
      // to delete
      ROS_DEBUG_STREAM("fb_feat_set.control_features=" << 
      fb_feat_set.control_features.at(0).centerpressure_x);
      
      fb_feats_pub.publish(fb_feat_set);
      send_force_from_pressure_instead_area.data = force_res;
      force_estim_pub.publish(send_force_from_pressure_instead_area);
      fb_feat_set.control_features.clear();
    }
    
    // publish yes no contact for switching from traj to servo
    start_tactile.publish(switch_from_traj_to_tactile);
    
    // smooth transitions from coc to zmp (rotate around point untill edge
    num_pixels_weight_gain.publish(coc2zmp);
    
    if (fb_feat_set_pca.control_features.size() == 1) 
    {
      // to delete
      ROS_DEBUG_STREAM("fb_feat_set_pca.control_features=" << 
      fb_feat_set_pca.control_features.at(0).centerContact_x);
      
      fb_feats_pub_pca.publish(fb_feat_set_pca);
      fb_feat_set_pca.control_features.clear();
    }
}

// cop_
// coc_
// force <- force from 16bit
// zmp <- weighted sum from 16 bit
// orient <- from pca
void ImageToolsKuka::img_proc_moments(cv::Mat& input_img)
{
  // get moments
  cv::Mat no_noise;
  cv::threshold(input_img, no_noise, noise_threshold_, 255, CV_THRESH_TOZERO);
  cv::Moments m = moments(no_noise,false);
  cop.x= (m.m10/m.m00);
  cop.y= (m.m01/m.m00);
  //to delete    
  ROS_DEBUG_STREAM("cop.x=" << cop.x);
  ROS_DEBUG_STREAM("cop.y = " << cop.y);
  
  //   cv::threshold(input_img, bin_img_, coc_threshold_, 255, CV_THRESH_BINARY);
  cv::Moments m2 = moments(bin_img_,true);
  coc.x= m2.m10/m2.m00;
  coc.y= m2.m01/m2.m00;
  
  //to delete    
  ROS_DEBUG_STREAM("coc.x=" << coc.x);
  ROS_DEBUG_STREAM("coc.y = " << coc.y);
  // contact_orientation  = 1.57-0.5*std::atan2(m2.mu11,(m2.mu20-m2.mu02));
  contact_orientation_  = (float)0.5*std::atan2(m2.mu11,(m2.mu02- m2.mu20));
  
  copx_ = size_x_ - (float)((float)size_x_/
  ((float)rows_-1)) * ((float) (cop.y));
  if (copx_ != copx_)
  {
    copx_ = copx_old_;
  }
  else
  {
    copx_old_ = copx_;
  }
  
  copy_ = size_y_ - (float)((float)size_y_/
  ((float)cols_-1)) * ((float) (cop.x));
    if (copy_ != copy_)
  {
    copy_ = copy_old_;
  }
  else
  {
    copy_old_ = copy_;
  }
  //to delete    
  ROS_DEBUG_STREAM("copx=" << copx_);
  ROS_DEBUG_STREAM("copy=" << copy_);
  
  cocx_ = size_x_ - (float)((float)size_x_/
  ((float)rows_-1)) * ((float) (coc.y));
  if (cocx_ != cocx_)
  {
    cocx_ = cocx_old_;
//     ROS_FATAL_STREAM("if nan cocx_=" << cocx_);

  }
  else
  {
    cocx_old_ = cocx_;
  }
//   ROS_FATAL_STREAM("cocx_=" << cocx_);

  cocy_ = size_y_ - (float)((float)size_y_/
  ((float)cols_-1)) * ((float) (coc.x));
    if (cocy_ != cocy_)
  {
    cocy_ = cocy_old_;
  }
  else
  {
    cocy_old_ = cocy_;
  }
  /* old way
//   float zmp_x = copx - cocx_;
//   ROS_DEBUG_STREAM("zmp_x_pure=" << zmp_x);
//   zmp_x = zmp_x * (float)torque_from_image_x_;
//   ROS_DEBUG_STREAM("zmp_x_=" << zmp_x);
// 
//   float zmp_y = copy - cocy_;
//   ROS_DEBUG_STREAM("zmp_y_pure=" << zmp_y);
//   zmp_y = zmp_y * (float)torque_from_image_y_;
//   ROS_DEBUG_STREAM("zmp_y_=" << zmp_y);
*/
  /** how to force
   * 
   *  I(x,y) - tactile image
   *  I_max = 255
   *  I_min = 1
   *  F_max = 1.8 N
   *  F_min = 0.2 N
   *255^
   *   |             255-1    
   *   | tg(alpha) = -----
   *   |             1.8 - 0.2
   *  1|_______>
   *    0.2    1.8 N
   * tg(alpha) = 158.75 
   * in the above we measured force
   * 
   * convert it to pressure
   * P = force / area
   * area = around 2,683281573*10^-6 (=PI*R^2)
   * P_max = 1.8N / 2,683281573*10^-6 = 250 kPa
   * P_min = 0
   * I_max = lets assume around 250
   * I_min = 0
   * pres_from_image_ = 250k/250 = 1000 times
   * is the scaling between image
   * value I and pressure value P
   * then for each value of I the pressure P = I*1000  
   * area = 3.4*3.4 = 11.56 * 10^-6    
   * force = total_pressure_*contact_points_n_*area
   * force = I*1000*contact_points_n_*area
   
  
  ROS_DEBUG_STREAM("total_pressure_=" << total_pressure_);
  ROS_DEBUG_STREAM("pres_from_image_=" << pres_from_image_);
  ROS_DEBUG_STREAM("contact_points_n_=" << contact_points_n_);
  ROS_DEBUG_STREAM("tactel_area_=" << tactel_area_);
/*
//   float force = (float)total_pressure_*pres_from_image_*
//   contact_points_n_*tactel_area_;
//   ROS_DEBUG_STREAM("force_=" << force);
  */
  
  fb_feat_set.header.stamp = ros::Time::now();
  // instead of servoing cop in line follow I need coc
  fb_one_cont_feats.centerpressure_x =cocx_ - size_x_/ 2.0;
  fb_one_cont_feats.centerpressure_y =cocy_ - size_y_ / 2.0;
  
  
  /* filter data
  // fb_one_cont_feats.contactForce = force;
  // use force 16 bit range
  // filter
//   sample_factor_ = 1.0 / filter_length_;
//   float force_res = (1.0 - (float)sample_factor_) * force_old_;
//   force_res = force_res + sample_factor_ * force_;
  */
  
  
   force_res = low_pass(force_, force_old_);
    ROS_DEBUG_STREAM("force_res=" << force_res);

  force_old_ = force_res;
//   fb_one_cont_feats.contactForce = force_res;
  fb_one_cont_feats.contactForce = estim_force_area_old_.data;
  
  // according to new jacobian we use COC for translational motions
  //  now coc is cop
  fb_one_cont_feats.centerContact_x = copx_- size_x_/ 2.0;
  fb_one_cont_feats.centerContact_y = copy_- size_y_/ 2.0;
  
  // use orientation from pca pca_analysis
  float orient_res = low_pass(pca_orient_, contact_orientation_old_);
  contact_orientation_old_ = orient_res;
  fb_one_cont_feats.contactOrientation = orient_res;
  // we set orientation from very high sensitive image
  // well it was not good idea. I will increase sensitivity to the original 
  // ros_tactile_image and calculate contact orientation, number of contours 
  // cop and coc from this topic;
  // force and zmp will be calculated from the 16bit message
  //   fb_one_cont_feats.contactOrientation = orient_from_sensitive_img_;
//   fb_one_cont_feats.zmp_x = zmp_x;
  // use force 16 bit range
  
//   ROS_FATAL_STREAM ("zmpx_old_" << zmpx_old_);
//   ROS_FATAL_STREAM ("zmpx_" << zmpx_);
  //   float zmpx_res = low_pass(zmpx_, zmpx_old_);
//   ROS_FATAL_STREAM ("zmpx_" << zmpx_);
  
  zmpx_ = copx_ - cocx_;
  ROS_DEBUG_STREAM("zmp_x_pure=" << zmpx_);
  zmpx_ = zmpx_ * (float)torque_from_image_x_;
  ROS_DEBUG_STREAM("zmpx_=" << zmpx_);
//   ROS_FATAL_STREAM("zmpx_=" << zmpx_);
  zmpy_ = copy_ - cocy_;
  ROS_DEBUG_STREAM("zmp_y_pure=" << zmpy_);
  zmpy_ = zmpy_ * (float)torque_from_image_y_;
  ROS_DEBUG_STREAM("zmpy_=" << zmpy_);
  

  float zmpx_res = zmpx_old_*(1.0 - (float)sample_factor_) + (float) sample_factor_ * zmpx_;
//   ROS_FATAL_STREAM ("zmpx_res" << zmpx_res);
  zmpx_old_ = zmpx_res;
  fb_one_cont_feats.zmp_x = zmpx_res;

  //   fb_one_cont_feats.zmp_y = zmp_y;
  // use force 16 bit range
  float zmpy_res = low_pass(zmpy_, zmpy_old_);
  zmpy_old_ = zmpy_res;
  fb_one_cont_feats.zmp_y = zmpy_res;
  fb_feat_set.control_features.push_back(fb_one_cont_feats);
  
}
//http://blog.thomnichols.org/2011/08/smoothing-sensor-data-with-a-low-pass-filter
float ImageToolsKuka::low_pass(float& in_new, float& in_old)
{
//       ROS_DEBUG_STREAM("in_new=" << in_new);
//       ROS_DEBUG_STREAM("in_old=" << in_old);

  sample_factor_ = 1.0 / filter_length_;
//         ROS_DEBUG_STREAM("sample_factor_=" << sample_factor_);
//         ROS_DEBUG_STREAM("filter_length_=" << filter_length_);

  float filter_out;
  /*
   * the same as 
   * res = old + ALPHA*(new - old);
   * */
  
  filter_out = in_old*(1.0 - (float)sample_factor_) + (float) sample_factor_ * in_new;
//           ROS_DEBUG_STREAM("filter_out=" << filter_out);

  return filter_out;
}

// plan feats
void ImageToolsKuka::img_proc_pca(cv::Mat& input_img)
{
  // Find all objects of interest
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;
  
  cv::findContours(bin_img_, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0,0));
  int tot_num_conts = contours.size();
  fb_feat_plan_set.numContours = tot_num_conts;
  

  if (tot_num_conts > 0)
  {

    for (size_t i = 0; i <contours.size(); ++i)
    {
      apply_pca(contours.at(i), i, tot_num_conts);
    }
    switch_from_traj_to_tactile.data= true;
  }
  else
  {
    switch_from_traj_to_tactile.data = false;
  }
  
  float weighted_y, weighted_x;
  weighted_center(input_img, weighted_x, weighted_y);
  
  fb_one_cont_feats_pca.centerpressure_x =weighted_x - size_x_ / 2.0 ;
  fb_one_cont_feats_pca.centerpressure_y =weighted_y - size_y_ / 2.0;
  
  
  fb_one_cont_feats_pca.zmp_x = weighted_x - fb_one_cont_feats_pca.centerContact_x;
  fb_one_cont_feats_pca.zmp_y = weighted_y - fb_one_cont_feats_pca.centerContact_y;
  
  fb_feat_set_pca.header.stamp = ros::Time::now();
  fb_feat_set_pca.control_features.push_back(fb_one_cont_feats_pca);
}

void ImageToolsKuka::apply_pca(std::vector<cv::Point> &pts, 
			   int contour_num, 
			   int tot_num_contours)
{
  cv::Mat data_pts = cv::Mat(pts.size(), 2, CV_64FC1);
  for (int ind = 0; ind < data_pts.rows; ++ind)
  {
    data_pts.at<double>(ind, 0) = pts.at(ind).x;
    data_pts.at<double>(ind, 1) = pts.at(ind).y;
  }
  cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
  
  cv::Point2d pos = cv::Point2d(pca_analysis.mean.at<double>(0, 0),
			    pca_analysis.mean.at<double>(0, 1));
  
  std::vector<cv::Point2d> eigen_vecs(2);
  std::vector<double> eigen_val(2);
  eigen_vecs_help_.clear();
  eigen_val_help_.clear();
  
  for (int ind = 0; ind < 2; ++ind)
  {
    eigen_vecs.at(ind) = cv::Point2d(pca_analysis.eigenvectors.at<double>(ind, 0),
				pca_analysis.eigenvectors.at<double>(ind, 1));
    eigen_val.at(ind) = pca_analysis.eigenvalues.at<double>(0, ind);
  }
  
  eigen_vecs_help_ = eigen_vecs;
  eigen_val_help_ = eigen_val;
  
  if (tot_num_contours == 2)
  {
    fb_feat_plan_set.TypeContact = "two_point";
    if (contour_num == 0)
    {
      coc_one_ = pos;
      area_contact_one_= cv::contourArea( pts ,false);
    }
    else if (contour_num == 1)
    {
      coc_two_ = pos;
      area_contact_two_= cv::contourArea( pts ,false);
      
      double area_contact_total = area_contact_two_+ area_contact_one_;
      float force_area = (float)total_pressure_*pres_from_image_*
      area_contact_total*tactel_area_;
      fb_one_cont_feats_pca.contactForce = force_area;
    }
    
    double pca_orient;
    if (coc_two_.y > coc_one_.y)
    {
      pca_orient = std::atan2(coc_two_.x - coc_one_.x, coc_two_.y - coc_one_.y);
          pca_orient_ = (float)pca_orient;

    }
    else
    {
      pca_orient = std::atan2(coc_one_.x - coc_two_.x, coc_one_.y - coc_two_.y);
      pca_orient_ = (float)pca_orient;
    }
    
    fb_one_cont_feats_pca.contactOrientation = pca_orient;
    if (std::abs(pca_orientation_old_ - fb_one_cont_feats_pca.contactOrientation) > 0.15)
    {
//       ROS_FATAL_STREAM("pca_orient two point - one point = " 
//       << std::abs(pca_orientation_old_ -
//       fb_one_cont_feats_pca.contactOrientation));
      
    }
    pca_orientation_old_ = fb_one_cont_feats_pca.contactOrientation;
    
  }
  else if ( ( isinf(eigen_val.at(0) == 1 ) ) || (isinf(eigen_val.at(1) == 1) ) ) 
  {
    fb_feat_plan_set.TypeContact = "point";
        float cocx_pca = size_x_ - (float)((float)size_x_/
    ((float)rows_-1)) * ((float) (pos.y));
    float cocy_pca = size_y_ - (float)((float)size_y_/
    ((float)cols_-1)) * ((float) (pos.x));
    
    fb_one_cont_feats_pca.centerContact_x = cocx_pca;
    fb_one_cont_feats_pca.centerContact_y = cocy_pca;
    
        
    double area_contact = cv::contourArea( pts ,false);
    float force_area = (float)total_pressure_*pres_from_image_*
    area_contact*tactel_area_;
    fb_one_cont_feats_pca.contactForce = force_area;

  }
  else if (std::abs(eigen_val.at(0) / eigen_val.at(1)) > compare_eigens_)
  {
   
    
    float cocx_pca = size_x_ - (float)((float)size_x_/
    ((float)rows_-1)) * ((float) (pos.y));
    float cocy_pca = size_y_ - (float)((float)size_y_/
    ((float)cols_-1)) * ((float) (pos.x));
    
    fb_one_cont_feats_pca.centerContact_x = cocx_pca;
    fb_one_cont_feats_pca.centerContact_y = cocy_pca;
    
        
    double area_contact = cv::contourArea( pts ,false);
    float force_area = (float)total_pressure_*pres_from_image_*
    area_contact*tactel_area_;
    fb_one_cont_feats_pca.contactForce = force_area;
    
    float borders = 0.0025;
    if ((cocx_ <= borders ) || (cocx_ >= size_x_ - borders) || (cocy_ <= borders) || (cocy_ >= size_y_ - borders) )
    {
      fb_feat_plan_set.TypeContact = "point";
    }
    else
    {
   
      fb_feat_plan_set.TypeContact = "edge";
      double pca_orient = std::atan2(eigen_vecs.at(0).x, eigen_vecs.at(0).y);
      pca_orient_ = float (pca_orient);
      fb_one_cont_feats_pca.contactOrientation = (float)pca_orient;
      pca_orientation_old_ = fb_one_cont_feats_pca.contactOrientation;
      
    }
  }
  else
  {
    fb_feat_plan_set.TypeContact = "point";
    float cocx_pca = size_x_ - (float)((float)size_x_/
    ((float)rows_-1)) * ((float) (pos.y));
    float cocy_pca = size_y_ - (float)((float)size_y_/
    ((float)cols_-1)) * ((float) (pos.x));
    fb_one_cont_feats_pca.centerContact_x = cocx_pca;
    fb_one_cont_feats_pca.centerContact_y = cocy_pca;
    
    double area_contact = cv::contourArea( pts ,false);
    float force_area = (float)total_pressure_*pres_from_image_*
    area_contact*tactel_area_;
    fb_one_cont_feats_pca.contactForce = force_area;
  }
}

void ImageToolsKuka::image_cb_force(const tactile_servo_msgs::Image_weiss& msg_weiss)
{
    int col_weiss = msg_weiss.width;
    int rows_weiss = msg_weiss.height;
    uchar img_weiss[rows_][cols_];
    
    int counter_wess =0;
    int contact_points_n = 0;
    float total_pressure = 0;
    max_intensity_ = 0;
    for(int i=0;  i<rows_weiss; ++i)
    {
      for(int j=0; j<col_weiss; j++) 
      {
	img_weiss[i][j]=msg_weiss.data[counter_wess];
	++counter_wess;
	
	if (img_weiss[i][j] >= coc_threshold_)
	{
	  
	  contact_points_n = contact_points_n + 1;
	  total_pressure = total_pressure + img_weiss[i][j];
	  if (img_weiss[i][j] > max_intensity_)
	  {
	    max_intensity_ = img_weiss[i][j];
	  }
	  
	}
      }
    }
    
    cv::Mat weiss_pressure = cv::Mat(rows_, cols_, CV_16U, img_weiss);
    /* it is wrong
//     weighted_center(weiss_pressure, weighted_x_16bit, weighted_y_16bit);
    
//     int total_sum_m00 = 0;
//   int sum_x_m10 = 0;
//   int sum_y_m01 = 0;
// int tmp;
//   for(int r = 0; r < weiss_pressure.rows; ++r)
//   {
//     for(int c = 0; c < weiss_pressure.cols; ++c)
//     {
//             ROS_FATAL_STREAM ("r" << r);
//             ROS_FATAL_STREAM ("c" << c);
// 
//        tmp = (int)weiss_pressure.at<uchar>(r, c);
//       ROS_FATAL_STREAM ("(int)weiss_pressure.at<uchar>(r, c)" << tmp);
//       total_sum_m00 = total_sum_m00 + (int)weiss_pressure.at<uchar>(r, c);
//       sum_x_m10 = sum_x_m10 + (int)weiss_pressure.at<uchar>(r, c) * (r+1);
//       sum_y_m01 = sum_y_m01 + (int)weiss_pressure.at<uchar>(r, c) * (c+1);
//     }
//   }
//   
//   ROS_FATAL_STREAM ("sum_x_m10" << sum_x_m10);
//       ROS_FATAL_STREAM ("sum_y_m01" << sum_y_m01);
//       ROS_FATAL_STREAM ("total_sum_m00" << sum_y_m01);
// 
//   float weigted_center_x = (float)sum_x_m10 / (float)total_sum_m00;
//   float weigted_center_y = (float)sum_y_m01 / (float)total_sum_m00;
//   
//   weighted_x_16bit_ = size_x_ - (float)((float)size_x_/
//   ((float)rows_-1)) * ((float) (weigted_center_y));
//   weighted_y_16bit_ = size_y_ - (float)((float)size_y_/
//   ((float)cols_-1)) * ((float) (weigted_center_x));
//   
//   ROS_FATAL_STREAM("weighted_x_16bit_"<<weighted_x_16bit_);
//   ROS_FATAL_STREAM("cocx_"<<cocx_);
  
  
//   zmpx_ = weighted_x_16bit_ - cocx_;
//   ROS_DEBUG_STREAM("zmp_x_pure=" << zmpx_);
//   zmpx_ = zmpx_ * (float)torque_from_image_x_;
//   ROS_DEBUG_STREAM("zmpx_=" << zmpx_);
//   ROS_FATAL_STREAM("zmpx_=" << zmpx_);
//   zmpy_ = weighted_y_16bit_ - cocy_;
//   ROS_DEBUG_STREAM("zmp_y_pure=" << zmpy_);
//   zmpy_ = zmpy_ * (float)torque_from_image_y_;
//   ROS_DEBUG_STREAM("zmpy_=" << zmpy_);
  */
  force_ = (float)total_pressure*pres_from_image_*contact_points_n*tactel_area_;
  ROS_DEBUG_STREAM("force_=" << force_);
  
//   estim_force_area.data = pres_from_image_*contact_points_n*tactel_area_;
  
  //if point contact is very small TODO compare this with above
  //force_ = (float)max_intensity_*pres_from_image_*1.0*tactel_area_;
  //ROS_DEBUG_STREAM("force_=" << force_);
}

void ImageToolsKuka::weighted_center(cv::Mat& input_img, float& weighted_x, float& weighted_y)
{
  int total_sum_m00 = 0;
  int sum_x_m10 = 0;
  int sum_y_m01 = 0;

  for(int r = 0; r < input_img.rows; ++r)
  {
    for(int c = 0; c < input_img.cols; ++c)
    {
      total_sum_m00 = total_sum_m00 + (int)input_img.at<uchar>(r, c);
      sum_x_m10 = sum_x_m10 + (int)input_img.at<uchar>(r, c) * (r+1);
      sum_y_m01 = sum_y_m01 + (int)input_img.at<uchar>(r, c) * (c+1);
    }
  }
  float weigted_center_x = (float)sum_x_m10 / (float)total_sum_m00;
  float weigted_center_y = (float)sum_y_m01 / (float)total_sum_m00;
  
  weighted_x = size_x_ - (float)((float)size_x_/
    ((float)rows_-1)) * ((float) (weigted_center_y));
  weighted_y = size_y_ - (float)((float)size_y_/
    ((float)cols_-1)) * ((float) (weigted_center_x));
}



void ImageToolsKuka::publish_features()
{
    if (isContact == 0)
    {
      fb_feat_plan_set.numContours = 0;
    }
    else
    {
      isContact = 0;
    }
  
    fb_feat_plan_set.header.frame_id = "weiss_plan";
    fb_feat_plan_set.header.stamp = ros::Time::now();
    
    fb_plan_pub.publish(fb_feat_plan_set);
}

ImageHelpToolsKuka::ImageHelpToolsKuka(ImageToolsKuka& img_tools_obj_ref):
img_tools_obj_ref_(img_tools_obj_ref)
{
  pub_eigens = nh_eigen.advertise<tactile_servo_msgs::compare_eigens>("/compare_eigens", 2);
//   parallel_loop();
}

ImageHelpToolsKuka::~ImageHelpToolsKuka()
{
//   ros::shutdown();
};


void ImageHelpToolsKuka::send_eigens()
{
  tactile_servo_msgs::compare_eigens eigens;
  if (img_tools_obj_ref_.eigen_val_help_.size() > 0)
  {
    eigens.val_eigen1 = (float)img_tools_obj_ref_.eigen_val_help_.at(0);
    eigens.vec_eigen1_x = (float)img_tools_obj_ref_.eigen_vecs_help_.at(0).x;
    eigens.vec_eigen1_y = (float)img_tools_obj_ref_.eigen_vecs_help_.at(0).y;

    eigens.val_eigen2 = (float)img_tools_obj_ref_.eigen_val_help_.at(1);
    eigens.vec_eigen2_x = (float)img_tools_obj_ref_.eigen_vecs_help_.at(1).x;
    eigens.vec_eigen2_y = (float)img_tools_obj_ref_.eigen_vecs_help_.at(1).y;
    
    pub_eigens.publish(eigens);
  }
}
/* did not work
void ImageHelpToolsKuka::parallel_loop()
{
  ros::Rate loop_rate(1);
  ROS_INFO("send eigen vals node in parallel");
  
  while( ros::ok() )
  {
    send_eigens();
    ros::spinOnce();
    loop_rate.sleep();
    ROS_INFO("sent eigen");
  }
}
*/



