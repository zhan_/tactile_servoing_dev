/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to extract features 
 * requires parameters on ros param server /tactile_array....
 */
#include "tactile_image/vis_feats_tactile_image.hpp"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "viz_feats_tact_image_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(50);
    ROS_INFO("viz_feats_tact_image_node");
    VisFeats vis(0);
    
//     ROS_DEBUG_STREAM("testing log levels");
    
    while( ros::ok() )
    {
//       vis.show_feats();
      ros::spinOnce();
      loop_rate.sleep(); 
    }
    
}