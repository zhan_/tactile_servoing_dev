/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to extract features 
 * requires parameters on ros param server /tactile_array....
 */
#include "tactile_image/tactile_image_processing.hpp"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "tact_image_process_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(120);
    ROS_INFO("tact_image_process_node");
    ImageTools extractor(0);
    ImageHelpTools eigens(extractor);
    
    while( ros::ok() )
    {
      ros::spinOnce();
      extractor.publish_features();
      eigens.send_eigens();

      loop_rate.sleep(); 
    }
    
}
    