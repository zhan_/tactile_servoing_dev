/*
 * fake feedback features to test jacobian
 * */
#include <ros/ros.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"


tactile_servo_msgs::ContsFeats feat_set;
tactile_servo_msgs::OneContFeats one_cont_feats;

//world_tactile_contact.point.x = msg_.states[0].contact_positions[i].x;


ros::Publisher feats_pub;

float copx, copy, force, cocx, cocy, orientz;
float     zmp_x,  zmp_y;


int main(int argc, char** argv)
{
    ROS_INFO("Publish fake fetures node");
    ros::init(argc, argv, "pub_fake_features");
    ros::NodeHandle n;
    copx = 0.020;
    copy = 0.0125;
    force = 1;
    cocx = 0.025;
    cocy = 0.0125;
    orientz = 0.0;

    feats_pub = n.advertise<tactile_servo_msgs::ContsFeats>("fb_feats", 1);
    
    ros::spinOnce();
    
    ros::Rate loop_rate(200);												// Loop at 100Hz until the node is shut down
    while (ros::ok())
    {
      //       ROS_INFO("working");
      feat_set.header.stamp = ros::Time::now();
      feat_set.header.frame_id = "smth";
      feat_set.header.stamp = ros::Time::now();
      one_cont_feats.centerContact_x = cocx;
      one_cont_feats.centerContact_y = cocy;
      one_cont_feats.contactForce = force;
      one_cont_feats.centerpressure_x = copx;
      one_cont_feats.centerpressure_y = copy;
      one_cont_feats.contactOrientation = orientz;
      one_cont_feats.zmp_x = zmp_x;
      one_cont_feats.zmp_y = zmp_y;
      feat_set.control_features.push_back(one_cont_feats);
      //       ROS_INFO(feat_set.header.frame_id.c_str());
      //       ROS_INFO(feat_set.control_features[0].centerContact_x);
      //       std::cout<<" number of contacts "<<feat_set.control_features.size()<<std::endl;
      //       std::cout<<"frame nanme = "<<feat_set.header.frame_id<<std::endl;
      //       std::cout<<"feat_set.control_features[0].centerContact_x = "<<feat_set.control_features[0].centerContact_x<<std::endl;
      //       std::cout<<"feat_set.control_features[0].contactOrientation = "<<feat_set.control_features[0].contactOrientation<<std::endl;
      feats_pub.publish(feat_set);
      feat_set.control_features.clear();
      ros::spinOnce();
      // do this
      loop_rate.sleep();			 										// wait until it is time for another interaction
    }
    
    return 0;

}
