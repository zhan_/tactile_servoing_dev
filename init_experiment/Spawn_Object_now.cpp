#include <cstdlib>
#include "ros/ros.h"
#include <string>
#include <sstream>
#include <math.h>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "dy_proprioception/HandPositioning.h"
#include "proprioception/AdaptiveGrasp.h"
#include "std_msgs/Float64.h"
#include "gazebo_msgs/SpawnModel.h"
#include "gazebo_msgs/DeleteModel.h"

#include "dy_proprioception/dynamic_image.h"


bool spawnObject(ros::NodeHandle* nh_ptr,const std::string object_name, const std::string object_path){

  //Service
  ros::ServiceClient spawnModelClient = nh_ptr->serviceClient<gazebo_msgs::SpawnModel> ("/gazebo/spawn_sdf_model");
  gazebo_msgs::SpawnModel model;


  //Spawning information
  std::string objectPath = object_path + object_name + "0" + ".sdf";
  char buff[1000];
    //ROS_FATAL_STREAM("object path =" << objectPath);
  std::string globalObjectPath = getcwd(buff, 1000);
  globalObjectPath = globalObjectPath + "/" + objectPath;
  std::cout << globalObjectPath << std::endl;
  std::ifstream file(globalObjectPath.c_str());
  std::string line;

  std::string position_temp;  //this will contain the position in y-axis as a string


  int line_counter=0;
  while(!file.eof()) // Parse the contents of the given sdf in a string
    {
        //ROS_FATAL_STREAM("in the while.");
      std::getline(file,line);
      model.request.model_xml+=line;

      //Line 10 has the scale which is used to know the position of the y.
      if(line_counter==10)
        {
              std::size_t found=line.find("<scale>");
              int char_counter=7;
              while(!std::isspace(line.at(found+char_counter)))
                {
                  position_temp.push_back(line.at(found+char_counter));
                  char_counter++;
                }
        }

      line_counter++;

    }

  file.close();


  //Variables of the coordinates of the pose are created.
  float coord_x, coord_y, coord_z, coord_w;
  float magnitud;
  float theta;

  //filling up msg "model" with the pose.
  model.request.reference_frame = "forearm";

  model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-0.025;
  model.request.initial_pose.position.z=0.35;

  if(object_name=="sphere")
    {
      model.request.initial_pose.position.x=0;
      coord_x=1;
      coord_y=1;
      coord_z=1;
      coord_w=0; //std::cos(0.5*theta);
    }


  if(object_name=="cylinder")
    {
      model.request.initial_pose.position.x=0;
      model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-0.025;
      coord_x=0;
      coord_y=1;
      coord_z=0;
      coord_w=1; //std::cos(0.5*theta);
    }


  if(object_name=="cone")
    {
      model.request.initial_pose.position.x=-0.054256;
      model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-0.020;
      coord_x=-::atof(position_temp.c_str())/2;
      coord_y=0.15;
      coord_z=0;
      coord_w=1; //std::cos(0.5*theta);

    }

  magnitud=std::sqrt(std::pow(coord_x,2)+std::pow(coord_y,2)+std::pow(coord_z,2));
  if(object_name=="torus")
    {
      std::cout << "in torus" << std::endl;
      model.request.reference_frame = "world";
      model.request.initial_pose.position.x=0;
      model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-std::pow(::atof(position_temp.c_str()),2)-0.030;
      coord_x=0;
      coord_y=0;
      coord_z=0;
      coord_w=0; //std::cos(0.5*theta);
      magnitud=1;

    }

  model.request.initial_pose.orientation.x=coord_x/magnitud;
  model.request.initial_pose.orientation.y=coord_y/magnitud;
  model.request.initial_pose.orientation.z=coord_z/magnitud;
  model.request.initial_pose.orientation.w=coord_w;

  ROS_FATAL_STREAM("MODEL=" << model.request.initial_pose);
    std::ostringstream stm;
    model.request.model_name = "object";
    //ROS_FATAL_STREAM(" object spawned.");
    if (!spawnModelClient.call(model))
      {
        ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
      }
    return model.response.success;
}


void deleteObject(ros::NodeHandle* nh_ptr)
{
  ros::ServiceClient deleteModelClient = nh_ptr->serviceClient<gazebo_msgs::DeleteModel>("/gazebo/delete_model");
  gazebo_msgs::DeleteModel deleteModel;
  deleteModel.request.model_name = "object";
  if (!deleteModelClient.call(deleteModel))
    ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "Object_Now");
    ros::NodeHandle nh;




    //Loading the parameters that have the list of objects to be loaded

    std::string PARAM_NAME = "object_to_be_spawn" ;
    //PARAM_NAME = "object_to_be_spawn" ;
    std::vector<std::string> object;
    bool ok =ros::param::get(PARAM_NAME,
                             object);
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter "<< PARAM_NAME) ;
        exit(1);
    }

    PARAM_NAME = "path" ;
    //PARAM_NAME = "object_to_be_spawn" ;
    std::vector<std::string> object_path;
    ok =ros::param::get(PARAM_NAME,
                             object_path);
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter "<< PARAM_NAME) ;
        exit(1);
    }
    deleteObject(&nh);
    spawnObject(&nh,object.at(0),object_path.at(2));

  return 0;
}


