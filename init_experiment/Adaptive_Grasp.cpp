#include "dy_proprioception/Adaptive_Grasp.h"
#include "dy_proprioception/adaptive_grasp_plugin.h"

///////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::Adaptive_Grasp
/// \param hand_in   This is the initializer, true=real_hand, false=simulation_hand
///
/// Notice that the topics change depending on the hand. and the plugin is called
/// in case of working with the real hand.
///
Adaptive_Grasp::Adaptive_Grasp(bool hand_in) : somatosensory()
{
    hand=hand_in;
    Init_Publishers();
    stop=true;

    start_time=false;


    const std::string PARAM_NAME = "joint_initial_pos_AG" ;
    bool ok =ros::param::get(PARAM_NAME,
                             joint_initial_pos_AG);
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter "<< PARAM_NAME) ;
        exit(1);
    }

    Init_Flags();
    if(hand)
    {
        ROS_FATAL_STREAM( "In simulation") ;
        std::string finger[5]={"ff","mf","rf","lf","th"};
        std::string phalange[3]={"distal","middle","proximal"};
        Contacts(finger, phalange);

    }else{
        ROS_FATAL_STREAM( "In real") ;
        std::string finger[5]={"ff","mf","rf","lf","th"};
        std::string phalange[3]={"distal_real","middle_real","proximal_real"};
        Contacts(finger, phalange);
        Adaptive_Grasp_plugin Plugin;
    }

}


////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::Init_Publishers
///
/// The parameters are obtained from "dy_proprioception/param/dy_proprioception_params.yaml"
/// These publishes to the controllers
///
/// TODO: see if the controllers have the same name on the real hand.
///
void Adaptive_Grasp::Init_Publishers()
{
    const std::string PARAM_NAME = "joint_names" ;
    std::vector<std::string> jointnames;
    bool ok =ros::param::get(PARAM_NAME,
                             jointnames) ;
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter"<< PARAM_NAME) ;
        exit(1);
    }

    for(unsigned int j=0;j<jointnames.size();j++)
    {
        std::string topic_name="/"+jointnames.at(j)+"_position_controller/command";
        std::cout << topic_name << std::endl;
        PubPointer.push_back( nh_ptr.advertise<std_msgs::Float64>(topic_name, 1));
    };
}

////////////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::Init_Flags
///
/// This initialize the flags for the whole code to run.
/// It is executed each time the service is called.
///
/// It initializes the commands according to those used by the
/// service that puts the hand in the initial position.
///
void Adaptive_Grasp::Init_Flags(){

    for(int unsigned i=0;i<12;i++)
    {
            flag[i]=false;
            touch_seq[i]="No";
            touch_seq2[i]=1000;
    };

    touch_index=0;
    for(int unsigned i=0;i<5;i++)
    {
            proximal[i]=0;
            distal[i]=0;
            command_proximal[i] = joint_initial_pos_AG.at(i*2);
            command_distal[i]   = joint_initial_pos_AG.at(i*2+1);
    };
    command_proximal[4] = joint_initial_pos_AG.at(12);
    command_distal[4]   = joint_initial_pos_AG.at(9);
    proximal_stop=false;
    distal_stop=false;

}


//////////////////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::Perform_Grasp
/// \param req
/// \param res
/// \return
///
///
/// this is the callback of the service. It first goes into a while
///  where the hand is closed and then it sends the messages.
bool Adaptive_Grasp::Perform_Grasp(proprioception::AdaptiveGrasp::Request &req, proprioception::AdaptiveGrasp::Response &res){

    stop=false;
    Init_Flags();

    if(start_time)
      {
        setup_time_msg();
      }

    while(!proximal_stop || !distal_stop)
    {
        ros::spinOnce();
    }

    Time_Haptics.finished=true;
    stop=true;


    for(int unsigned k=0;k<12;k++)
    {
        if(!touch_seq[k].empty())
        {
            res.touch_seq[k]=touch_seq[k];
            res.touch_seq2[k]=touch_seq2[k];
        }else{
            res.touch_seq[k]="No";
            res.touch_seq2[k]=10000;
            std::cout << "empty" << std::endl;
        }
    }


    res.finished=true;

    return true;
}

////////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::register_data
/// \param id
///
void Adaptive_Grasp::register_data(int id)
{
      if(touch_index==0)
      {
       seq_init=ros::Time::now().toSec();
       seq_diff=0;

      }else{
       seq_diff=ros::Time::now().toSec()-seq_init;
      }
      std::stringstream ss;
      ss << seq_diff;
      std::string str = ss.str();
      touch_seq[id]=str;
      touch_seq2[id]=seq_diff;
      touch_index++;

}

void Adaptive_Grasp::Start_Time()
{
    PubTime = nh_ptr.advertise<dy_proprioception::Contact_Time>("/time_of_contact", 1, true);
    start_time=true;
}
/////////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::time_msg_update
///
/// This updates the message of the times suited for the
/// dynamic image.
///
void Adaptive_Grasp::time_msg_update()
{
  Time_Haptics.header.stamp=ros::Time::now();
  for(unsigned int i=0;i<12;i++)
    {
      if(!flag[i])
        {
          //Time_Haptics.sequence[i]=true;
          Time_Haptics.touch_time[i]=ros::Time::now().toSec()-Time_Haptics.start_time;
        }else{
          Time_Haptics.sequence[i]=true;
        }
    }

  //ROS_FATAL_STREAM( "publishing to time topic");

}

/////////////////////////////////////////////////////////////////
/// \brief Adaptive_Grasp::callback_contacts
/// \param msg          info coming from the topics
/// \param id           id of the flag position for each part of the finger
/// \param flag_index   this tells to which finger msg belongs to.
///
///
void Adaptive_Grasp::callback_contacts(const gazebo_msgs::ContactsStateConstPtr& msg, int id, int flag_index)
{
  //ROS_FATAL_STREAM( "is is calling");
    if(!stop & flag_index!=4)
    {
        //ROS_FATAL_STREAM( "is is calling");

        if(!proximal_stop && proximal[flag_index]==0)
        {
            //ROS_FATAL_STREAM( "trying to close proximals");
            command.data=command_proximal[flag_index];
            PubPointer[2*flag_index+1].publish(command);
            command_proximal[flag_index]=command_proximal[flag_index]+0.00008;
            if(command_proximal[flag_index]>1.5)
            {
                proximal[flag_index]=1;
            }
        }

        if(proximal[flag_index]==1 && !distal_stop && distal[flag_index]==0)
        {
            //ROS_FATAL_STREAM( "trying to close distals");
            command.data=command_distal[flag_index];
            this->PubPointer[2*flag_index].publish(command);
            command_distal[flag_index]=command_distal[flag_index]+0.00008;
            if(command_distal[flag_index]>2)
            {
                distal[flag_index]=1;
            }
        }


        if(!msg->states.empty())
        {


            if(id==3*flag_index && !flag[id])
            {
                //ROS_FATAL_STREAM( "flag index:" << flag_index);
                //ROS_FATAL_STREAM( "in contact distal:" << id);
                proximal[flag_index]=1;
                distal[flag_index]=1;
                flag[id]=true;
                register_data(id);

            }

            if(id>3*flag_index && !flag[id])
            {
                //ROS_FATAL_STREAM( "flag index:" << flag_index);
                //ROS_FATAL_STREAM( "in contact proximal:" << id);
                //ROS_FATAL_STREAM( "in contact proximal");
                proximal[flag_index]=1;
                flag[id]=true;
                register_data(id);
            }


            int count_p=0;
            for(int unsigned i=0;i<4;i++)
            {
                if(proximal[i]==1)
                {
                    count_p++;
                }
            }

            if(count_p==4)
            {
                proximal_stop=true;
            }

            int count_d=0;
            for(int unsigned i=0;i<4;i++)
            {
                if(distal[i]==1)
                {
                    count_d++;
                }
            }

            if(count_d==4)
            {
                //std::cout << "stop distal" << std::endl;
                distal_stop=true;
            }

    }

 }
    if(!stop & flag_index==4)
    {
        //ROS_FATAL_STREAM( "traying to thumb"<< id);
        if(proximal[flag_index]==0)
        {
            //ROS_FATAL_STREAM( "traying to close thumb");
            command.data=command_proximal[flag_index];
            this->PubPointer[12].publish(command);
            command_proximal[flag_index]=command_proximal[flag_index]+0.00008;
            if(command_proximal[flag_index]>50*3.14/180)
            {
                proximal[flag_index]=1;
            }
        }
        if(distal[flag_index]==0)
        {
            //ROS_FATAL_STREAM( "traying to thumb");
            command.data=command_proximal[flag_index];
            this->PubPointer[9].publish(command);
            command_proximal[flag_index]=command_proximal[flag_index]+0.00004;
            if(command_proximal[flag_index]>30*3.14/180)
            {
                proximal[flag_index]=1;
            }
        }

        if(!msg->states.empty())
        {
            //ROS_FATAL_STREAM( "contact to thumb");
            if(id==3*flag_index && !flag[id])
            {
                proximal[flag_index]=1;
                distal[flag_index]=1;
            }

            if(id>3*flag_index && !flag[id])
            {
                proximal[flag_index]=1;
                flag[id]=true;
            }


        }
    }
    ////  Here, the
    if(!stop & start_time)
      {
        Time_Haptics.finger=flag_index;
        time_msg_update();

      }else{
        Time_Haptics.start=false;
      }

    Time_Haptics.header.stamp=ros::Time::now();
    PubTime.publish(Time_Haptics);

}

