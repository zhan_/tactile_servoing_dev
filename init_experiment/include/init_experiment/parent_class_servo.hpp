#ifndef _PARENT_CLASS_SERVO_HPP_
#define _PARENT_CLASS_SERVO_HPP_

#include "ros/ros.h"

class ParentClassServo
{
  public:
    ParentClassServo();
    ~ParentClassServo();
      
  protected:
    ros::NodeHandle nh_ptr;
};    
#endif // _PARENT_CLASS_SERVO_HPP_

      
