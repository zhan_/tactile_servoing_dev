#ifndef _POSE_INIT_SERVICE_HPP_
#define _POSE_INIT_SERVICE_HPP_

#include "tactile_servo_srvs/pose_arm_init.h"

class PosArmInit
{
public:
//   ros::NodeHandle* nh_ptr
  PosArmInit();
  ~PosArmInit();
  ros::NodeHandle nh_;
  
  ros::ServiceServer service;
  ros::ServiceServer service2;
  ros::ServiceServer service3;

  bool move(tactile_servo_srvs::pose_arm_init::Request &req,
	    tactile_servo_srvs::pose_arm_init::Response &res);
  bool move2(tactile_servo_srvs::pose_arm_init::Request &req,
	    tactile_servo_srvs::pose_arm_init::Response &res);
  bool move3(tactile_servo_srvs::pose_arm_init::Request &req,
	    tactile_servo_srvs::pose_arm_init::Response &res);

  void print();

  // start movement service
  bool move_init;
  std::vector<std::string>  arm_joint_names_initialization;
  std::vector<std::string> name_controller_type;
  std::vector<float>  joint_vals_arm_initial,
		      joint_vals_arm_initial2;
  std::vector<ros::Publisher> Publishers;

  std::vector<std::string>  hand_joint_names_initialization;
  std::vector<float>  joint_vals_hand_initial;
  std::vector<ros::Publisher> Publishers_fingers;

};

#endif


