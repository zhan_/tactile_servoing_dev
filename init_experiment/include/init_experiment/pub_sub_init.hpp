#ifndef _INIT_PUB_SUB_HPP_
#define _INIT_PUB_SUB_HPP_

#include "init_experiment/parent_class_servo.hpp"

// ros includes
// #include "ros/ros.h" already added in parent class

class InitPubSub : ParentClassServo
{
  public:
    InitPubSub();
    ~InitPubSub();
    init_SubsPubs();
 
  protected:
    bool is_real_;
    ros::Subscriber hand_joints_sub;
    ros::Subscriber arm_joints_sub;
    std::vector<ros::Publisher> hand_pub;
    std::vector<ros::Publisher> arm_pub;
    

  private:

  
};

#endif _INIT_PUB_SUB_HPP_


