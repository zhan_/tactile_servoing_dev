#ifndef ADAPTIVE_GRASP_H
#define ADAPTIVE_GRASP_H


#include "dy_proprioception/somatosensory.h"

#include <string>
#include <sstream>
#include <math.h>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "control_msgs/JointControllerState.h"
#include "gazebo_msgs/ContactsState.h"
#include "proprioception/AdaptiveGrasp.h"
#include "sensor_msgs/JointState.h"
#include "dy_proprioception/Contact_Time.h"


class Adaptive_Grasp : somatosensory
{

    protected:

        double seq_init;
        double seq_diff;
        bool flag[15];
        bool stop;
        int proximal[5];
        bool proximal_stop;
        int distal[5];
        bool distal_stop;
        bool flag_thumb[3];


        std::vector<ros::Subscriber> contacts;
        std::vector<ros::Publisher> PubPointer;

        std::vector<float> joint_initial_pos_AG;

        std::string touch_seq[12];
        float touch_seq2[12];
        int touch_index;
        int touch;

        std_msgs::Float64 command;
        float command_proximal[5];
        float command_distal[5];
        int move_seq[3];
        void register_data(int id);

        void time_msg_update();
        ros::Publisher PubTime;
        bool start_time;


    public:

        void Start_Time();
        void Init_Publishers();
        void Init_Flags();
        bool Perform_Grasp(proprioception::AdaptiveGrasp::Request &req, proprioception::AdaptiveGrasp::Response &res);
        void callback_contacts(const gazebo_msgs::ContactsStateConstPtr& msg, int id, int a);

        Adaptive_Grasp (bool hand_in);
        Adaptive_Grasp (bool hand_in, bool dynamics);

};

#endif // ADAPTIVE_GRASP_H
