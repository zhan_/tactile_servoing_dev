#include "dy_proprioception/somatosensory.h"

#include <ros/ros.h>
#include <string>
#include <math.h>

#include "sensor_msgs/JointState.h"
#include "sensor_msgs/Image.h"
#include "geometry_msgs/WrenchStamped.h"
#include "gazebo_msgs/ContactsState.h"



///////////////////////////////////////////////////
/// \brief somatosensory::Joints
///
/// initializes the subscriber to joint angles.
///
/// TODO: make it general so that other classes derivated from
/// this ones can implement different callback
///
void somatosensory::Joints()
{
    std::string topic_name;
    if(hand)
    {
        topic_name="/sh/joint_states";     ///// working with real hand
    }else{
        topic_name="/joint_states";
    }

    joints_sub = nh_ptr.subscribe(topic_name, 10, & somatosensory::callback_joints, this);

}


///////////////////////////////////////////////////
/// \brief somatosensory::Contacts
/// \param finger   This is an array containing the names of the fingers.
/// \param phalange This is an array containing the names of the phalanges.
///
/// initializes the subscribers to know when the fingers are in contact
///
///

void somatosensory::Contacts(std::string finger[],
                             std::string phalange[])
{

    int fing_index=0;
    for(unsigned int i=0; i< 5; i++)
     {
         for(unsigned int k=0; k<3 ; k++)
         {
             std::string topicname="/contacts/"+finger[i]+"/"+phalange[k];
             contacts.push_back(nh_ptr.subscribe<gazebo_msgs::ContactsState>(topicname, 10, boost::bind( &somatosensory::callback_contacts, this, _1, fing_index, i) ) );
             fing_index++;
         };
     };



}

///////////////////////////////////////////////////////
/// \brief somatosensory::Ati_Nano
///
///This initializes the subscribers to the Ati_nano.
///
void somatosensory::Ati_Nano()
{

    contacts.push_back(nh_ptr.subscribe<geometry_msgs::WrenchStamped>("/nano17ft/finger1", 10,  boost::bind( &somatosensory::callback_Ati_nano, this, _1, 4) ) );
    contacts.push_back(nh_ptr.subscribe<geometry_msgs::WrenchStamped>("/nano17ft/finger2", 10,  boost::bind( &somatosensory::callback_Ati_nano, this, _1, 0) ) );
    contacts.push_back(nh_ptr.subscribe<geometry_msgs::WrenchStamped>("/nano17ft/finger3", 10,  boost::bind( &somatosensory::callback_Ati_nano, this, _1, 1) ) );
    contacts.push_back(nh_ptr.subscribe<geometry_msgs::WrenchStamped>("/nano17ft/finger4", 10,  boost::bind( &somatosensory::callback_Ati_nano, this, _1, 2) ) );
    contacts.push_back(nh_ptr.subscribe<geometry_msgs::WrenchStamped>("/nano17ft/finger5", 10,  boost::bind( &somatosensory::callback_Ati_nano, this, _1, 3) ) );
}

////////////////////////////////////////////////////////
/// \brief somatosensory::CEA
///
/// This subscribes to the CEA sensors' topic.
///
void somatosensory::CEA()
{
    contacts.push_back(nh_ptr.subscribe("/ros_tactile_image", 10, &somatosensory::callback_cea, this));
}

////////////////////////////////////////////////////////
/// \brief somatosensory::callback_joints
/// \param msg
///
/// this is the callback of the /joint_states topic. It does not do anything
/// because it is virtual and each other class will define what to do in it.
///
void somatosensory::callback_joints(const sensor_msgs::JointStateConstPtr& msg)
{
    std::cout << "In Function callback_joints from base class" << std::endl;
}

/////////////////////////////////////////////////////////
/// \brief somatosensory::callback_contacts
/// \param msg           message coming from the topic
/// \param fing_index    the finger index to later be used as index of a flag variable
/// \param i             this tells us to which finger corresponds the incoming msg.
///
/// This callback uses the contacts of the fingers from the simulation hand to stop the
/// phalange once it has touched the object.
///
/// TODO: define if this is used for several executables, ifnot, define the code here.
///
void somatosensory::callback_contacts(const gazebo_msgs::ContactsStateConstPtr& msg, int fing_index, int i)
{
  ROS_FATAL_STREAM( "service for adaptive grasp called");
    std::cout << "In Function callback_contacts from base class" << std::endl;
}



/////////////////////////////////////////////////////////
/// \brief somatosensory::callback_Ati_nano
/// \param msg    message coming from the topic
/// \param id     tell to which Ati_nano corresponds the incoming msg
///
/// This receives the Ati_nano msgs.
///
/// TODO: - Implement code to stop movement.
///       - use dynamic reconfiguration to change the threshold
///
void somatosensory::callback_Ati_nano(const geometry_msgs::WrenchStampedConstPtr& msg, int id)
{
    std::cout << "In Function callback_Ati_nano from base class" << std::endl;
}

/////////////////////////////////////////////////////////
/// \brief callback_cea
/// \param msg  data coming from the topic
///
/// This subscribe to the topic of the CEA sensors, which are already stamped.
///
/// TODO: improve the code
///       use dynamic recongifuration to change the threshold.
///
void somatosensory::callback_cea(const sensor_msgs::ImagePtr& msg)
{/*
    for(unsigned int i=0;i<(msg->height*msg->width);i++)
    {
        if(msg->data[i]>35)
        {
            if(msg->header.frame_id=="Index_finger_sensor")
            {
                flag[1]=true;
            }
            if(msg->header.frame_id=="Middle_finger_sensor")
            {
                flag[8]=true;
            }
            if(msg->header.frame_id=="PallmIndex_finger_sensor")
            {
                flag[11]=true;
            }
            if(msg->header.frame_id=="Thumb_finger_sensor")
            {
                flag[17]=true;
            }
        }
    }
*/
}


////////////////////////////////////////////////////////////
/// \brief somatosensory::setup_time_msg
///
/// This function sets the information of the time sequence.
void somatosensory::setup_time_msg()
{
  Time_Haptics.start=true;
  Time_Haptics.finished=false;
  Time_Haptics.start_time=ros::Time::now().toSec();
  for(unsigned int i=0;i<12;i++)
  {
      Time_Haptics.sequence[i]=false;
      Time_Haptics.touch_time[i]=0;
  }

}


