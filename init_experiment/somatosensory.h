#ifndef SOMATOSENSORY_H
#define SOMATOSENSORY_H


#include "ros/ros.h"
#include "boost/shared_ptr.hpp"
#include "sensor_msgs/JointState.h"
#include "sensor_msgs/Image.h"
#include "geometry_msgs/WrenchStamped.h"
#include "gazebo_msgs/ContactsState.h"

#include "dy_proprioception/Contact_Time.h"

class somatosensory
{
  public:
      somatosensory() {}
      ~somatosensory() {}

      void Joints();
      void Contacts(std::string finger[], std::string phalange[]);
      void Ati_Nano();
      void CEA();

      virtual void callback_joints(const sensor_msgs::JointStateConstPtr& msg);
      virtual void callback_contacts(const gazebo_msgs::ContactsStateConstPtr& msg, int fing_index, int i);
      virtual void callback_Ati_nano(const geometry_msgs::WrenchStampedConstPtr& msg, int id);
      virtual void callback_cea(const sensor_msgs::ImagePtr& msg);
      void setup_time_msg();
      dy_proprioception::Contact_Time Time_Haptics;


  protected:

     std::vector<ros::Subscriber> contacts_Ati;
     ros::Subscriber joints_sub;
     std::vector<ros::Subscriber> contacts;
     ros::NodeHandle nh_ptr;
     bool hand;
};




#endif // SOMATOSENSORY_H
