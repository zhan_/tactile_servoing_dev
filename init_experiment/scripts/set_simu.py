#!/usr/bin/env python 
import rospy
from gazebo_msgs.srv import SpawnModel
from geometry_msgs.msg import Pose


rospy.init_node('insert_object',log_level=rospy.INFO)

initial_pose = Pose()
initial_pose.position.x = 0.697
initial_pose.position.y = -0.0862
initial_pose.position.z = 1.202
initial_pose.orientation.z = 3.14/2
initial_pose.orientation.x = 3.14/2
initial_pose.orientation.y = 3.14/2

f = open('/home/zhan/ros/indigo/tactile_servo_ws/src/Gazebo_objects/model.sdf','r')
#f = open('/home/alex/.gazebo/models/cylinder3/model.sdf','r')
sdff = f.read()

rospy.wait_for_service('gazebo/spawn_sdf_model')
spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel)
spawn_model_prox("some_robo_name", sdff, "robotos_name_space", initial_pose, "world")
