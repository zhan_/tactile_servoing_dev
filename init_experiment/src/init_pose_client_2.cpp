#include <ros/ros.h>

#include "tactile_servo_srvs/pose_arm_init.h"

ros::ServiceClient init_pose_client;

tactile_servo_srvs::pose_arm_init init_pose_client_srv;
tactile_servo_srvs::pose_arm_init::Request  init_pose_client_req;
tactile_servo_srvs::pose_arm_init::Response init_pose_client_res;

int move();

int boolet_proof = 0;
int move(){
  init_pose_client_req.init = 0;
  if(init_pose_client.call(init_pose_client_srv))
  {
    ROS_FATAL_STREAM ( "initialization =" << init_pose_client_srv.response.initialized);
    boolet_proof = 1;
    return 0;
    
  }
  else
  {
    ROS_ERROR ("Failed to call initialization");
    boolet_proof = 1;
    return 1;
    
  }
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "init_pose_client_node");
  ros::NodeHandle n;
  ros::service::waitForService("move_to_init_2");
  ROS_INFO("connected service provider");

  init_pose_client = n.serviceClient<tactile_servo_srvs::pose_arm_init>("move_to_init_2");
  if (boolet_proof == 0)
  {
    move();
  }
  
}
