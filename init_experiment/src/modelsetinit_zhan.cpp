#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "control_msgs/JointControllerState.h"
#include <string>
#include <sstream>
#include <boost/functional/hash.hpp>
#include <boost/bind.hpp>
#include <ros/message_event.h>
#include <math.h>
//#include "comunication/InitPoseVerify.h"
#include <gazebo_msgs/SpawnModel.h>
#include <gazebo_msgs/ContactsState.h>
#include <ros/callback_queue.h>
#include <fstream>

std::string jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                            "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                            "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                              "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};
//float joint_init_pos[24]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.8, 1, 0.6, 0, 0, 0};
float joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -1.17,       1,        0.6,           0,           0,            0};
/// data to initialize arm and hand
/*
name: ['ElbowJRotate', 'ElbowJSwing', 'FFJ1', 'FFJ2', 'FFJ3', 'FFJ4',
        'LFJ1', 'LFJ2', 'LFJ3', 'LFJ4', 'LFJ5', 'MFJ1',
        'MFJ2', 'MFJ3', 'MFJ4', 'RFJ1', 'RFJ2', 'RFJ3',
        'RFJ4', 'ShoulderJRotate', 'ShoulderJSwing', 'THJ1', 'THJ2', 'THJ3',
        'THJ4', 'THJ5', 'WRJ1', 'WRJ2', 'weiss_connect']
position: [-0.783999052588058, 1.5699978824383027, 0.027289721780177523, -0.00018476314973003838, 0.00014116803490615837, -0.02440769887690486,
        0.0008528141812513823, 0.0031270344456393318, 0.0022660315233888895, 0.004294951844630468, 0.01065638193396623, 0.0009213004291144244,
        0.0014468314090096968, 0.036795570983375825, -0.007636183326337864, 0.6182712061775684, 1.3011372278975193, 0.021725873388346528,
        0.006817640268253378, 1.0025179948162233, -3.3852941969669814e-06, 0.03340804264171027, 0.03237937899326404, -0.04752542995490394,
        8.55145306815075e-05, -0.0860631726252441, -1.617502206041621e-05, 9.358684236460135e-07, -8.123979267082859e-08]
*/
// data to initialize rrbot

/*name: ['joint1', 'joint2', 'joint3']
position: [1.5888606628106814, 0.007837277070499304, -8.180181785277796e-07]
*/



class Command_pub{
    public:
        std::vector<ros::Publisher> PubPointer;
        void publi_init(ros::NodeHandle* n);
     }pub_command;

void Command_pub::publi_init(ros::NodeHandle* n){
    for(int i=0;i<24;i++){
        std::string topicname="/"+jointnames[i]+"_position_controller/command";
        this->PubPointer.push_back( n->advertise<std_msgs::Float64>(topicname, 1));
    }
}

class State_listener{
public:
	//float init;
	std::vector<ros::Subscriber> errors;
    bool flag[24]; // publisher will stop publishing to exact topic when actial position and desired position within an error
    bool flag2; // publisher will stop publishing when all flags (flag[24])
	float  error_current;
	float  error_max[24];
	float  error_max2;
    void print();
	void callback(const control_msgs::JointControllerStateConstPtr &msg, int i);
    State_listener (bool flag_state, float  error_max_init) : flag2(flag_state), error_max2(error_max_init) {};
	void init_flags();
	//~State_listener () {delete errors};
}state_watch (false,0);

void State_listener::print(){
    std::cout << "flag2"<< flag2 << std::endl;
}

void State_listener::init_flags(){
	for(int i=0;i<24;i++){
			flag[i]=flag2;
			error_max[i]=error_max2;
		};
}


/*this callback gets information about the error of the controllers and when
the maximun error reaches 0.05, it sets the flag to one to stop the program */
void State_listener::callback(const control_msgs::JointControllerStateConstPtr &msg, int id){
    /*this part watches the errors of all the controllers, and when the maximun
      * error reaches 0.005 or lower, it sets the flag to 1 and the program stops */
    error_current=(msg->process_value);
    //std::cout<< "current_error" << error_current << std::endl;
    error_max[id]=std::fabs(error_current-joint_init_pos[id]);
    //sets flag to 1 and stops the program.
	
    if(error_max[id]<0.05){
		flag[id]=true;
        std::cout << "id=" << id << std::endl;
        std::cout << "flag_subs=" << flag[id] << std::endl;
	}
	
	int count=0;
	for(int i=0;i<24;i++){
		if(flag[i]==true){
			count++;
            std::cout << "count=" << i << std::endl;
		}	
	}
	
	if(count==23){
        flag2=true;
	//std::cout << "flag_2=" << flag2 << std::endl;
    }

	 ros::Time::now();
}
 
  //Spawn an object
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
void spawnObject(ros::NodeHandle* nh_ptr){
  ros::ServiceClient spawnModelClient = nh_ptr->serviceClient<gazebo_msgs::SpawnModel> ("/gazebo/spawn_sdf_model");
  gazebo_msgs::SpawnModel model;
  std::string objectPath = "src/Gazebo_objects/model.sdf";
  //std::string objectPath = "src/comunication/sphere.sdf";
  //std::string objectPath = "src/comunication/cone.sdf";

  char buff[1000];
  std::string globalObjectPath = getcwd(buff, 1000);
  globalObjectPath = globalObjectPath + "/" + objectPath;
  std::ifstream file(globalObjectPath.c_str());
  std::string line;

  while(!file.eof()) // Parse the contents of the given sdf in a string
    {
      std::getline(file,line);
      model.request.model_xml+=line;
    }
  file.close();


  model.request.reference_frame = "world";
  model.request.initial_pose.position.x=0.681;
  model.request.initial_pose.position.y=-0.0862;
  model.request.initial_pose.position.z=1.02;
  model.request.initial_pose.orientation.x=0;
  model.request.initial_pose.orientation.y=0;
  model.request.initial_pose.orientation.z=1;
  model.request.initial_pose.orientation.z=0;

  std::ostringstream stm;
  //stm << model_name << id;
  model.request.model_name = "cylinder";

  if (!spawnModelClient.call(model))
    ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
}

int initialize_simu(ros::NodeHandle* nh_ptr){
	
	//ros::Time begin=ros::Time::now();
	//ros::Duration five_seconds(10.0);
	
	//Command_pub pub_command;
	state_watch.init_flags();
	
	
	// initialization of publishers
	pub_command.publi_init(nh_ptr);
	
	
	
	//definition of the command
	std_msgs::Float64 command;
	
	//Publication of the commands and flag surveillance
	//ros::Rate r(10);
	//std::cout << state_watch.flag2 << std::endl;
	int i=1;
	while(state_watch.flag2==false){	
		
		if(!state_watch.flag[i]){

		command.data=joint_init_pos[i];
		
		pub_command.PubPointer[i].publish(command);


		}
		i++;
		if(i>23)
		i=0;
		
		ros::spinOnce();
		


	}

 return 0;

}


int main(int argc, char **argv)
{
	state_watch.init_flags();
	ros::init(argc, argv, "pose_init_verify_server");
	ros::NodeHandle n;

	for(int j=0;j<24;j++){
		std::string topicname="/"+jointnames[j]+"_position_controller/state";
		
		//the following line shows how to call a callback from a class with more than one input
		 state_watch.errors.push_back( n.subscribe<control_msgs::JointControllerState>(topicname, 1, boost::bind(&State_listener::callback, &state_watch, _1, j) ));
		};
	
	
	//initialize simulation	
	initialize_simu(&n);
	
	//spawn model
    spawnObject(&n);
	
	//perform grasp;
    //perform_grasp(&n);
	
	
  return 0;
}
