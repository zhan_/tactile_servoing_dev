#include <cstdlib>
#include "ros/ros.h"
#include <string>
#include <sstream>
#include <math.h>
#include <fstream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "std_msgs/Float64.h"
#include "gazebo_msgs/SpawnModel.h"
#include "gazebo_msgs/DeleteModel.h"

  float coord_x, coord_y, coord_z, coord_w;


bool spawnObject(ros::NodeHandle* nh_ptr,const std::string object_name){

  //Service
  ros::ServiceClient spawnModelClient = nh_ptr->serviceClient<gazebo_msgs::SpawnModel> ("/gazebo/spawn_sdf_model");
  gazebo_msgs::SpawnModel model;


  //Spawning information
  std::string objectPath = "objects/" + object_name + ".sdf";
  char buff[1000];
    //ROS_FATAL_STREAM("object path =" << objectPath);
  std::string globalObjectPath = getcwd(buff, 1000);
  globalObjectPath = globalObjectPath + "/" + objectPath;
  std::cout << globalObjectPath << std::endl;
  std::ifstream file(globalObjectPath.c_str());
  std::string line;

  std::string position_temp;  //this will contain the position in y-axis as a string


  int line_counter=0;
  while(!file.eof()) // Parse the contents of the given sdf in a string
    {
        //ROS_FATAL_STREAM("in the while.");
      std::getline(file,line);
      model.request.model_xml+=line;

      //Line 10 has the scale which is used to know the position of the y.
      if(line_counter==10)
        {
              std::size_t found=line.find("<scale>");
              int char_counter=7;
              while(!std::isspace(line.at(found+char_counter)))
                {
                  position_temp.push_back(line.at(found+char_counter));
                  char_counter++;
                }
        }

      line_counter++;

    }

  file.close();
  ROS_FATAL_STREAM("I read file");

  //Variables of the coordinates of the pose are created.
//   float coord_x, coord_y, coord_z, coord_w;
  float magnitud;
  float theta;

  //filling up msg "model" with the pose.
//   model.request.reference_frame = "forearm";
  model.request.reference_frame = "weiss";

  model.request.initial_pose.position.y=0.05; //- ::atof(position_temp.c_str())-0.025;
  model.request.initial_pose.position.z=0.035;
  
  if(object_name=="bar")
  {
    model.request.initial_pose.position.x=0.025;
    model.request.initial_pose.position.y= 0.0125;//- ::atof(position_temp.c_str())-0.025;
    model.request.initial_pose.position.z=0.015+0.025/2.0;
  
  }
    if(object_name=="corner")
  {
    model.request.initial_pose.position.x=0.025;
    model.request.initial_pose.position.y= 0.0125+0.075;//- ::atof(position_temp.c_str())-0.025;
    model.request.initial_pose.position.z=0.015+0.025/2.0;
  
  }
  
      if(object_name=="corner_2")
  {
    model.request.initial_pose.position.x=0.025;
    model.request.initial_pose.position.y= 0.0125+0.075;//- ::atof(position_temp.c_str())-0.025;
    model.request.initial_pose.position.z=0.015+0.025/2.0;
  
  }
  
  if(object_name=="cylinder_servo")
  {
    model.request.initial_pose.position.x=0.02;
    model.request.initial_pose.position.y= 0.035;//- ::atof(position_temp.c_str())-0.025;
    model.request.initial_pose.position.z=0.03;

  }
  
    if(object_name=="sphere")
    {
    model.request.initial_pose.position.x=0.02;
    model.request.initial_pose.position.y= 0.02;//- ::atof(position_temp.c_str())-0.025;
    model.request.initial_pose.position.z=0.02;
    }
    

  if(object_name=="cone")
    {
      model.request.initial_pose.position.x=-0.054256;
      model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-0.020;
      coord_x=-::atof(position_temp.c_str())/2;
      coord_y=0.15;
      coord_z=0;
      coord_w=1; //std::cos(0.5*theta);

    }

  magnitud=std::sqrt(std::pow(coord_x,2)+std::pow(coord_y,2)+std::pow(coord_z,2));
  if(object_name=="torus")
    {
      std::cout << "in torus" << std::endl;
      model.request.reference_frame = "world";
      model.request.initial_pose.position.x=0;
      model.request.initial_pose.position.y=- ::atof(position_temp.c_str())-std::pow(::atof(position_temp.c_str()),2)-0.030;
      coord_x=0;
      coord_y=0;
      coord_z=0;
      coord_w=0; //std::cos(0.5*theta);
      magnitud=1;

    }

    
    
//   model.request.initial_pose.orientation.x=coord_x/magnitud;
//   model.request.initial_pose.orientation.y=coord_y/magnitud;
//   model.request.initial_pose.orientation.z=coord_z/magnitud;
//   model.request.initial_pose.orientation.w=coord_w;

// transform rotations from Euler to quartenions  
  float c1 = cos(coord_x/2);
  ROS_FATAL_STREAM("c1=" <<c1);

  float s1 = sin(coord_x/2);
  float c2 = cos(coord_y/2);
  float s2 = sin(coord_y/2);  
  float c3 = cos(coord_z/2);
  float s3 = sin(coord_z/2);
  float c1c2 = c1*c2;
  ROS_FATAL_STREAM("c1c2=" << c1c2);
  float s1s2 = s1*s2;
  float ww =c1c2*c3 - s1s2*s3;
  float wx =c1c2*s3 + s1s2*c3;
  float wy =s1*c2*c3 + c1*s2*s3;
  float wz =c1*s2*c3 - s1*c2*s3;
  ROS_FATAL_STREAM("wz=" << wz);

  model.request.initial_pose.orientation.x=wx;
  model.request.initial_pose.orientation.y=wy;
  model.request.initial_pose.orientation.z=wz;
  model.request.initial_pose.orientation.w=ww;

  ROS_FATAL_STREAM("MODEL=" << model.request.initial_pose);
    std::ostringstream stm;
    model.request.model_name = "object";
    //ROS_FATAL_STREAM(" object spawned.");
    if (!spawnModelClient.call(model))
      {
        ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
      }
    return model.response.success;
}


void deleteObject(ros::NodeHandle* nh_ptr)
{
  ros::ServiceClient deleteModelClient = nh_ptr->serviceClient<gazebo_msgs::DeleteModel>("/gazebo/delete_model");
  gazebo_msgs::DeleteModel deleteModel;
  deleteModel.request.model_name = "object";
  if (!deleteModelClient.call(deleteModel))
    ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
}

void select_orientations()
{
    std::cout << "Please enter orientation : ";
    std::cout << "x"<<std::endl; 
    std::cin >> coord_x;
    std::cout << "orient x =  "<< coord_x<< std::endl;

    std::cout << "Please enter orientation : ";
    std::cout << " y"<<std::endl; 
    std::cin >> coord_y;
    
    std::cout << "Please enter orientation : ";
    std::cout << " z"<<std::endl; 
    std::cin >> coord_z;
}


int main(int argc, char **argv)
{

    ros::init(argc, argv, "Object_Now");
    ros::NodeHandle nh;




    //Loading the parameters that have the list of objects to be loaded

    std::string PARAM_NAME = "object_to_be_spawn" ;
    //PARAM_NAME = "object_to_be_spawn" ;
    std::vector<std::string> object;
    bool ok =ros::param::get(PARAM_NAME,
                             object);
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter "<< PARAM_NAME) ;
        exit(1);
    }
    
    coord_x = 1.57;
    coord_y = 1.57;
    coord_z = 0;

//     select_orientations();
    
    
    int contact;
    int flag = 0; int check=1;
    //Input data from keyboard
  do {
	std::cout << "Please chose an object : ";
	std::cout << "delete, cylinder, bar, point"<<std::endl; 
	std::cin >> contact;
	/*if (contact == "offsetx"|| contact == "offsety") ++flag;
	if (contact == "orientation"|| contact == "push") ++flag;*/
	if (contact == 5){
	      deleteObject(&nh);
	}
	
	if ((contact >= 0)&(contact <= 4)) {
	  deleteObject(&nh);
	  spawnObject(&nh,object.at(contact));  
	}
	if (contact>5) break;
	std::cout << "Invalid Input: " << contact <<"\n";
  }while( check );

   
//     deleteObject(&nh);
//     spawnObject(&nh,object.at(0));

  return 0;
}


