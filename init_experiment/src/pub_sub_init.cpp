#include "init_experiment/pub_sub_init.hpp"

#include <string>


InitPubSub::InitPubSub() : ParentClassServo()
{
  const std::string PARAM_NAME = "is_real_hand" ;
  
  bool ok =ros::param::get(PARAM_NAME,
			   is_real_);
  if(!ok)
  {
    ROS_FATAL_STREAM( "Couldn't get parameter "<< PARAM_NAME) ;
    exit(1);
  }
  init_SubsPubs();
}
InitPubSub::init_SubsPubs()
{

  if(is_real_)
  {
    ROS_INFO("REAL HAND");
    const std::string PARAM_NAME = "name_controller_type";
    
  }
  else
  {
    ROS_INFO("SIMULATED HAND");
  }

}
/*
protected:
    bool is_real;
    ros::Subscriber hand_joints_sub;
    ros::Subscriber arm_joints_sub;
    std::vector<ros::Publisher> hand_pub;
    std::vector<ros::Publisher> arm_pub;


controller_type = "_position_controller";
*/