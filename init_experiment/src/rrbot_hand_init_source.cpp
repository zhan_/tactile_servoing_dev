#include "rrbot_hand_init.h"
#include <string>
#include <ros/ros.h>
#include <std_msgs/Float64.h>

#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
Command_pub::Command_pub(){

}
Command_pub::~Command_pub(){

}
void Command_pub::publi_init(ros::NodeHandle* n, std::vector<std::string>& robot_jointnames_, std::string& controller_type_ ){
    for(int i=0;i<robot_jointnames_.size();i++){
        std::string topicname="/"+robot_jointnames_.at(i)+controller_type_+"/command";
        this->PubPointer.push_back( n->advertise<std_msgs::Float64>(topicname, 1));
    }
}



State_listener::State_listener(
        bool flag_state_,
        float  error_max_init_,
        const std::vector<bool>& flag_pub_joint_robot_):
            flag_pub_robot(flag_state_),
            error_max_robot(error_max_init_),
            allow_pub_joint(flag_pub_joint_robot_){
                error_joint_val = std::vector<float>(flag_pub_joint_robot_.size(),9999);
                std::cout << "Object of State_listener is being created" << std::endl;
                debugger = 0;


            }
State_listener::~State_listener(){

}

void State_listener::print(){
    std::cout << "flag_pub_robot"<< flag_pub_robot << std::endl;
}

void State_listener::init_flags(){
    for(int i=0;i<allow_pub_joint.size();i++){
            allow_pub_joint.at(i)=flag_pub_robot;
            //error_max_joint[i]=error_max_robot;
        };
}


/*this callback gets information about the error of the controllers and when
the maximun error reaches 0.05, it sets the flag to one to stop the program */
void State_listener::callback(const control_msgs::JointControllerStateConstPtr &msg, int id, const std::vector<float>&rrbot_joint_init_pos_vec_){
    /*this part watches the errors of all the controllers, and when the maximun
      * error reaches max error or lower, it sets the flag to 1 and the program stops */
    current_joint_val=(msg->process_value);
    std::cout<<"Process_value"<<msg->process_value<<std::endl;
    //std::cout<< "current_joint_val" << current_joint_val << std::endl;
    error_joint_val.at(id) = std::fabs(current_joint_val-rrbot_joint_init_pos_vec_.at(id));
    std::cout<<"error_joint = "<<error_joint_val.at(id)<<"for joint"<<id<<std::endl;

    //sets flag to 1 and stops the program.

    if(error_joint_val.at(id)<error_max_robot){
        allow_pub_joint.at(id)=true;
        std::cout << "id=" << id << std::endl;
        std::cout << "flag_subs=" << allow_pub_joint.at(id) << std::endl;
    }

    int count=0;
    for(int i=0;i<allow_pub_joint.size();i++){
        if(allow_pub_joint.at(i)==true){
            count++;
            std::cout << "count=" << i << std::endl;
        }
    }

    if(count==(allow_pub_joint.size())){
        flag_pub_robot=true; // all joints riched the values
    std::cout << "flag_pub_robot=" << flag_pub_robot << std::endl;
    }

}

void State_listener::callback_weiss_listen(const gazebo_msgs::ContactsState& msg_)
{
  //publish the message
  //std_msgs::Float64 command;
    std::cout<<"is_weiss in callback of weiss image"<<std::endl;

  if(msg_.states.size()>0){
     is_weiss_in_contact = true;
     std::cout<<"is_weiss _in_contact = "<<is_weiss_in_contact<<std::endl;

  }
  std::cout<<"size of msg"<<msg_.states.size()<<std::endl;
  debugger = debugger + 1;
  std::cout<<"debugger"<<debugger<<std::endl;



  return;
}
