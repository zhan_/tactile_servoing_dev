#ifndef RRBOT_HAND_INIT_H
#define RRBOT_HAND_INIT_H
#include <control_msgs/JointControllerState.h>
#include <string>
#include <ros/ros.h>
#include <std_msgs/Float64.h>

#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"

class Command_pub{
    public:
        Command_pub();
        std::vector<ros::Publisher> PubPointer;
        void publi_init(ros::NodeHandle* n, std::vector<std::string>& robot_jointnames_, std::string& controller_type_ );
        ~Command_pub();
     };



class State_listener{
public:
    std::vector<ros::Subscriber> joint_vals;
    std::vector<bool> allow_pub_joint;
    bool flag_pub_robot; // = flag_state_; // publisher will stop publishing when all flags (flag[24])
    float current_joint_val;
    std::vector<float> error_joint_val;
    float error_max_robot;// = error_max_init_;
    int debugger;
    bool is_weiss_in_contact;
    ros::Subscriber weiss_listen; // listen contacts
    void print();
    void init_flags();
    void callback(const control_msgs::JointControllerStateConstPtr &msg, int id, const std::vector<float>&rrbot_joint_init_pos_vec_);
    void callback_weiss_listen(const gazebo_msgs::ContactsState& msg_); // listen contacts
    State_listener (
            bool flag_state_,
            float  error_max_init_,
            const std::vector<bool>& flag_pub_joint_robot_);
    ~State_listener();
    /// it is the same as assigning flag2 = flag_state_, error_max2 = error_max_init_. It is helpful
    /// when the type you pass in is big. Note - the sequence of variables to instanciate should
    /// be in right order as in the class declaration
};//state_watch (false,0.01);

#endif // RRBOT_HAND_INIT_H
