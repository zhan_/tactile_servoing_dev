#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>
#include <control_msgs/JointControllerState.h>
#include <string>
#include <math.h>
#include "rrbot_hand_init.h"


#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"

// begin to servo
#include "tactile_servo_srvs/start_servo_controller.h"

int NUM_JOINTS_RRBOT = 3;
int NUM_JOINTS_HAND = 24;
//rrbot/joint1_position_controller/command

std::string controller_type = "_position_controller";
std::string rrbot_jointnames[3]={"rrbot/joint1", "rrbot/joint2", "rrbot/joint3"};
std::vector<std::string> rrbot_jointnames_vec(rrbot_jointnames, rrbot_jointnames+3);

std::string hand_jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                            "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                            "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                              "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};
std::vector<std::string> hand_jointnames_vec(hand_jointnames, hand_jointnames+24);

//float joint_init_pos[24]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.8, 1, 0.6, 0, 0, 0};
float hand_joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -1.17,       1,        0.6,           0,           0,            0};
float hand_joint_init_pos_begin[24]={0,           0,          0,           0,           0,            0,
                                0,           0,          0,           0,           0,            0,
                                0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                                -0.78,       1.57,        0.0,           -0.7,           0.02,            0};
float joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -0.78,       1.57,        0.0,           -0.05,           0.02,            0};

float rrbot_joint_init_pos[3]={1.57, 0, 1.57 };
std::vector<float> rrbot_joint_init_pos_vec(rrbot_joint_init_pos, rrbot_joint_init_pos + 3);
std::vector<float> hand_joint_init_pos_vec(hand_joint_init_pos_begin, hand_joint_init_pos_begin + 24);


std::vector<bool> flag_rrbot_init(3); // vector of false to allow publish rrbot
std::vector<bool> flag_hand_init(24); // vector of false to allow publish hand



/*
name: ['ElbowJRotate', 'ElbowJSwing', 'FFJ1', 'FFJ2', 'FFJ3', 'FFJ4',
        'LFJ1', 'LFJ2', 'LFJ3', 'LFJ4', 'LFJ5', 'MFJ1',
        'MFJ2', 'MFJ3', 'MFJ4', 'RFJ1', 'RFJ2', 'RFJ3',
        'RFJ4', 'ShoulderJRotate', 'ShoulderJSwing', 'THJ1', 'THJ2', 'THJ3',
        'THJ4', 'THJ5', 'WRJ1', 'WRJ2', 'weiss_connect']
position: [-0.783999052588058, 1.5699978824383027, 0.027289721780177523, -0.00018476314973003838, 0.00014116803490615837, -0.02440769887690486,
        0.0008528141812513823, 0.0031270344456393318, 0.0022660315233888895, 0.004294951844630468, 0.01065638193396623, 0.0009213004291144244,
        0.0014468314090096968, 0.036795570983375825, -0.007636183326337864, 0.6182712061775684, 1.3011372278975193, 0.021725873388346528,
        0.006817640268253378, 1.0025179948162233, -3.3852941969669814e-06, 0.03340804264171027, 0.03237937899326404, -0.04752542995490394,
        8.55145306815075e-05, -0.0860631726252441, -1.617502206041621e-05, 9.358684236460135e-07, -8.123979267082859e-08]
*/ // data to initialize arm and hand
/*name: ['joint1', 'joint2', 'joint3'] // data to initialize rrbot
position: [1.5888606628106814, 0.007837277070499304, -8.180181785277796e-07]
*/


Command_pub pub_command;
Command_pub pub_command_hand;

//pub_command.publi_init(nh_ptr);
//pub_command.PubPointer[i].publish(command);

State_listener state_watch (false,0.05,flag_rrbot_init);
State_listener state_watch_hand (false,0.2,flag_hand_init);

int initialize_simu(ros::NodeHandle* nh_ptr){
	//ros::Time begin=ros::Time::now();
	//ros::Duration five_seconds(10.0);
	
	//Command_pub pub_command;
	state_watch.init_flags();

	// initialization of publishers
    pub_command.publi_init(nh_ptr,rrbot_jointnames_vec,controller_type);
    pub_command_hand.publi_init(nh_ptr,hand_jointnames_vec,controller_type);
	//definition of the command
	std_msgs::Float64 command;
	
	//Publication of the commands and flag surveillance
	//ros::Rate r(10);
	//std::cout << state_watch.flag2 << std::endl;
    int i=0;
    while(state_watch.flag_pub_robot==false){
		
        if(!state_watch.allow_pub_joint[i]){

        command.data=rrbot_joint_init_pos[i];
        std::cout << "int i = " << i << std::endl;
        std::cout << "Command=" << command.data << std::endl;
		
		pub_command.PubPointer[i].publish(command);


		}
		i++;
        if(i>2){
            i=0;
        }
		ros::spinOnce();
	}
    i=0;
    state_watch_hand.init_flags();
    while(state_watch_hand.flag_pub_robot==false){

        if(!state_watch_hand.allow_pub_joint[i]){

        command.data=hand_joint_init_pos_begin[i];
        std::cout << "int i = " << i << std::endl;
        std::cout << "Command=" << command.data << std::endl;

        pub_command_hand.PubPointer[i].publish(command);


        }
        i++;
        if(i>23){
            i=0;
        }
        ros::spinOnce();
    }
    state_watch_hand.flag_pub_robot = false;
    state_watch_hand.allow_pub_joint.flip();
    std::cout<< "HAND MOVED"<< std::endl;

    std::cout << "state_watch_hand.flag_pub_robot " << state_watch_hand.flag_pub_robot << std::endl;
    std::cout << "state_watch_hand.allow_pub_joint " << state_watch_hand.allow_pub_joint[0] << std::endl;
    std::cout << "state_watch_hand.allow_pub_joint Shoulder rot " << state_watch_hand.allow_pub_joint[21] << std::endl;

    state_watch_hand.init_flags();
    std::cout << "state_watch_hand.allow_pub_joint Shoulder rot " << state_watch_hand.allow_pub_joint[21] << std::endl;

    std::cout<< "hand moved outwards"<< std::endl;
    std::cout<< "SHoulder rotate"<< joint_init_pos[21]<< std::endl;
    state_watch_hand.is_weiss_in_contact = false;
    // move hand toward rrbot until contact
    //ros::Duration duration(1./24.);

    ros::spinOnce();
    command.data = hand_joint_init_pos_begin[21] + 0.6;
    std::cout<< "isWeissInCOntactinside"<< state_watch_hand.is_weiss_in_contact<< std::endl;
    float command_temp = 0.004;
    while (state_watch_hand.is_weiss_in_contact == false){
        ros::spinOnce();
        ros::Duration(0.5).sleep();
        ros::spinOnce();

        command.data = command.data + command_temp;// hand_joint_init_pos_begin[21] + i*0.05;
        pub_command_hand.PubPointer[21].publish(command);
        std::cout<< "isWeissInCOntactinside"<< state_watch_hand.is_weiss_in_contact<< std::endl;
        std::cout<< "isRotateSHoulder"<< command.data<< std::endl;
        //duration.sleep();



    }



//    i=0;
//    while(state_watch_hand.flag_pub_robot==false){

//        if(!state_watch_hand.allow_pub_joint[i]){

//        command.data=joint_init_pos[i];
//        std::cout << "int i = " << i << std::endl;
//        std::cout << "Command=" << command.data << std::endl;

//        pub_command_hand.PubPointer[i].publish(command);

//        if (i==21) {
//            std::cout<< "SHoulder rotate command"<< command.data << std::endl;

//            std::cout<< "SHoulder rotate"<< joint_init_pos[21]<< std::endl;
//        }



//        }
//        i++;
//        if(i>23){
//            i=0;
//        }
//        ros::spinOnce();
//    }
 return 0;

}




int main(int argc, char **argv)
{


    state_watch.init_flags();
    state_watch_hand.init_flags();
    ros::init(argc, argv, "pose_init_");
	ros::NodeHandle n;


    for(int j=0;j<NUM_JOINTS_RRBOT;j++){
        std::string topicname="/"+rrbot_jointnames[j]+"_position_controller/state";
		
		//the following line shows how to call a callback from a class with more than one input
         state_watch.joint_vals.push_back(
                     n.subscribe<control_msgs::JointControllerState>(
                         topicname, 1,
                         boost::bind(&State_listener::callback, &state_watch, _1, j,
                                     rrbot_joint_init_pos_vec) ));

    };
    for(int j=0;j<NUM_JOINTS_HAND;j++){
        std::string topicname2="/"+hand_jointnames[j]+"_position_controller/state";

        //the following line shows how to call a callback from a class with more than one input
         state_watch_hand.joint_vals.push_back(
                     n.subscribe<control_msgs::JointControllerState>(
                         topicname2, 1,
                         boost::bind(&State_listener::callback, &state_watch_hand, _1, j,
                                     hand_joint_init_pos_vec) ));

    };
	
   state_watch_hand.weiss_listen = n.subscribe("/contacts/weiss",1000, &State_listener::callback_weiss_listen, &state_watch_hand);

	
	//initialize simulation	
	initialize_simu(&n);


    ros::ServiceClient client = n.serviceClient<tactile_servo_srvs::start_servo_controller>("start_servo_controller_service");
    tactile_servo_srvs::start_servo_controller srv;
    srv.request.begin_controller = true;
    if (client.call(srv))
    {
        ROS_INFO("begin servo");
    }
    else
    {
        ROS_ERROR("Failed");
    }
	//spawn model
    //spawnObject(&n);
	
	//perform grasp;
    //perform_grasp(&n);
	
	
  return 0;
}
