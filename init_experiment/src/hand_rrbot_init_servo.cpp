#include <ros/ros.h>
#include <string>
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>
#include <std_msgs/Float64MultiArray.h>


//rrbot/joint1_position_controller/command
std::string controller_type = "_position_controller";

// rrbot topics to publish
std::string rrbot_jointnames[3]={"rrbot/joint1", "rrbot/joint2", "rrbot/joint3"};
std::string hand_jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                                 "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                                 "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                                 "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};

float joint_init_pos_begin[24]={0,           0,          0,           0,           0,            0,
                                0,           0,          0,           0,           0,            0,
                                0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                                -0.78,       1.57,        0.0,           -1,           0,            0};
float joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -0.78,       1.57,        0.0,           0,           0,            0};
float rrbot_joint_init_pos[3]={1.57, 0, 0 };

// publisher class
class Command_pub{
public:
    //std::vector<ros::Publisher> PubPointer_hand;
    std::vector<ros::Publisher> PubPointer_rrbot;
    //void publi_init_hand(ros::NodeHandle* n);
    void publi_init_rrbot(ros::NodeHandle* n);
}pub_command;

//void Command_pub::publi_init_hand(ros::NodeHandle* n){
//    for(int i=0;i<24;i++){
//        std::string hand_topicnames="/"+hand_jointnames[i]+"_position_controller/command";
//        this->PubPointer_hand.push_back( n->advertise<std_msgs::Float64>(hand_topicnames, 1));
//    }
//}

void Command_pub::publi_init_rrbot(ros::NodeHandle* n){
    for(int i=0;i<3;i++){
        std::string rrbot_topicnames="/"+rrbot_jointnames[i]+"_position_controller/command";
        this->PubPointer_rrbot.push_back( n->advertise<std_msgs::Float64>(rrbot_topicnames, 1));
    }
}


/*
int initialize_simu(ros::NodeHandle* nh_ptr){
    pub_command.publi_init_hand(nh_ptr);
    std::cout<<"pub hand inited"<<std::endl;
    pub_command.publi_init_rrbot(nh_ptr);
    std::cout<<"pub rrbot inited"<<std::endl;

    std_msgs::Float64 command;

    // 1 publish rrbot joints
    for (int i=0; i++; i<3){
        std::cout<<"int i = "<< i <<std::endl;

        command.data=rrbot_joint_init_pos[i];
        std::cout<<"commanded val"<< command.data <<std::endl;

        pub_command.PubPointer_rrbot[i].publish(command);
        ros::spinOnce();
    }


   /* while(state_watch.flag2==false){

        if(!state_watch.flag[i]){



        pub_command.PubPointer[i].publish(command);


        }
        i++;
        if(i>23)
        i=0;

        ros::spinOnce();

    }

 return 0;

}
*/

int main(int argc, char** argv)
{
    int rate_of_loop = 100;
    //init the ros node
    ros::init(argc, argv, "initialize simulation");
    ros::NodeHandle node;
//    initialize_simu(&node);
  //  pub_command.publi_init_hand(&node);
    std::cout<<"pub hand inited"<<std::endl;
    pub_command.publi_init_rrbot(&node);
    std::cout<<"pub rrbot inited"<<std::endl;

    std_msgs::Float64 command;
    ros::spinOnce();
    // 1 publish rrbot joints
    for (int i=0; i++; i<3){
        std::cout<<"int i = "<< i <<std::endl;

        command.data=rrbot_joint_init_pos[i];
        std::cout<<"commanded val"<< command.data <<std::endl;

        pub_command.PubPointer_rrbot[i].publish(command);
        ros::spinOnce();
    }

    //pub_command.publi_init(&node);
//    ros::Rate loop_rate(rate_of_loop);

//    while( ros::ok() ){

//        ros::spinOnce();
//        loop_rate.sleep();

//    }
    return 0;
}
