#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "control_msgs/JointControllerState.h"
#include <string>
#include <sstream>
#include <boost/functional/hash.hpp>
#include <boost/bind.hpp>
#include <ros/message_event.h>
#include <math.h>
//#include "comunication/InitPoseVerify.h"
#include <gazebo_msgs/SpawnModel.h>
#include <gazebo_msgs/ContactsState.h>
#include <ros/callback_queue.h>
#include <fstream>

std::string jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4","sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3","sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5","sa_er","sa_es","sa_ss","sa_sr","sh_wrj1","sh_wrj2"};
//float joint_init_pos[24]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.8, 1, 0.6, 0, 0, 0};
float joint_init_pos[24]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180, -2.36, 1, 0.6, -0.5, 0, 0};


/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
 
 class Command_pub{
public:
	std::vector<ros::Publisher> PubPointer;
	 void publi_init(ros::NodeHandle* n);
 }pub_command;
 
 void Command_pub::publi_init(ros::NodeHandle* n){
	 for(int i=0;i<24;i++){
		std::string topicname="/"+jointnames[i]+"_position_controller/command";
		this->PubPointer.push_back( n->advertise<std_msgs::Float64>(topicname, 1));
	}
 }




 //This class is meant to control when the program stops (see while in main)
class State_listener{
public:
	//float init;
	std::vector<ros::Subscriber> errors;
	bool flag[24];
	bool flag2;
	float  error_current;
	float  error_max[24];
	float  error_max2;
	void callback(const control_msgs::JointControllerStateConstPtr &msg, int i);
	State_listener (bool flag_state, float  error_max_init) : flag2(flag_state), error_max2(error_max_init) {};
	void init_flags();
	//~State_listener () {delete errors};
}state_watch (false,0);


void State_listener::init_flags(){
	for(int i=0;i<24;i++){
			flag[i]=flag2;
			error_max[i]=error_max2;
		};
}


/*this callback gets information about the error of the controllers and when
the maximun error reaches 0.05, it sets the flag to one to stop the program */
 void State_listener::callback(const control_msgs::JointControllerStateConstPtr &msg, int id){
	//std::cout<< "current_error" << std::fabs(error_current) << std::endl;
	 
	 /*this part watches the errors of all the controllers, and when the maximun
	  * error reaches 0.005 or lower, it sets the flag to 1 and the program stops */

	 error_current=(msg->process_value);
	  //std::cout<< "current_error" << error_current << std::endl;
	  error_max[id]=std::fabs(error_current-joint_init_pos[id]);

    //sets flag to 1 and stops the program.

	
	if(error_max[id]<0.005){
		flag[id]=true;
		//std::cout << "id=" << id << std::endl;
		//std::cout << "flag_subs=" << flag[id] << std::endl;
	}
	
	int count=0;
	for(int i=0;i<24;i++){
		if(flag[i]==true){
			count++;
			//std::cout << "count=" << i << std::endl;
		}	
	}
	
	if(count==23){
	flag2=true;
	//std::cout << "flag_2=" << flag2 << std::endl;
};

	 ros::Time::now();
 }
 
  //Spawn an object
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
void spawnObject(ros::NodeHandle* nh_ptr){
  ros::ServiceClient spawnModelClient = nh_ptr->serviceClient<gazebo_msgs::SpawnModel> ("/gazebo/spawn_sdf_model");
  gazebo_msgs::SpawnModel model;
  std::string objectPath = "src/comunication/model.sdf";
  //std::string objectPath = "src/comunication/sphere.sdf";
  //std::string objectPath = "src/comunication/cone.sdf";

  char buff[1000];
  std::string globalObjectPath = getcwd(buff, 1000);
  globalObjectPath = globalObjectPath + "/" + objectPath;
  std::ifstream file(globalObjectPath.c_str());
  std::string line;

  while(!file.eof()) // Parse the contents of the given sdf in a string
    {
      std::getline(file,line);
      model.request.model_xml+=line;
    }
  file.close();


  model.request.reference_frame = "world";
  model.request.initial_pose.position.x=0.681;
  model.request.initial_pose.position.y=-0.0862;
  model.request.initial_pose.position.z=1.02;
  model.request.initial_pose.orientation.x=0;
  model.request.initial_pose.orientation.y=0;
  model.request.initial_pose.orientation.z=1;
  model.request.initial_pose.orientation.z=0;

  std::ostringstream stm;
  //stm << model_name << id;
  model.request.model_name = "cylinder";

  if (!spawnModelClient.call(model))
    ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
}

int initialize_simu(ros::NodeHandle* nh_ptr){
	
	//ros::Time begin=ros::Time::now();
	//ros::Duration five_seconds(10.0);
	
	//Command_pub pub_command;
	state_watch.init_flags();
	
	
	// initialization of publishers
	pub_command.publi_init(nh_ptr);
	
	
	
	//definition of the command
	std_msgs::Float64 command;
	
	//Publication of the commands and flag surveillance
	//ros::Rate r(10);
	//std::cout << state_watch.flag2 << std::endl;
	int i=1;
	while(state_watch.flag2==false){	
		
		if(!state_watch.flag[i]){

		command.data=joint_init_pos[i];
		
		pub_command.PubPointer[i].publish(command);


		}
		i++;
		if(i>23)
		i=0;
		
		ros::spinOnce();
		


	}

 return 0;

}



class Contact_listener{
public:
    std::vector<ros::Subscriber> is_contact_sub;
    bool flag[12];
    bool flag_val;
    bool flag_arm;
    bool flag_thumb;
    void callback(const gazebo_msgs::ContactsStateConstPtr& msg, int id);
    Contact_listener (bool flag_state) : flag_val(flag_state) {};
    void init_flags();
    //~State_listener () {delete errors};
}iscontact(false);

void Contact_listener::init_flags(){
    for(int i=0;i<12;i++){
            flag[i]=flag_val;
			
        };
        flag_arm=flag_val;
        flag_thumb=flag_val;
}
 
void Contact_listener::callback(const gazebo_msgs::ContactsStateConstPtr& msg, int id){
    //std::cout<< "current_error" << std::fabs(error_current) << std::endl;
	 
     /*this part watches the errors of all the controllers, and when the maximun
      * error reaches 0.005 or lower, it sets the flag to 1 and the program stops */
    //if there isn't contact
                //std::cout<< "the id is " << id << std::endl;
     if(msg->states.empty())
     {

     }else{
        if(id<12)
        {

            flag[id]=true;
            flag_arm=true;
			
        }else
        {
            if(id<16){
                flag_arm=true;
            }else{
                flag_thumb=true;
            }
            //std::cout<< id << std::endl;

        }
			
    };
	

	 
 }
 
void perform_grasp(ros::NodeHandle* nh_ptr){
	
	
    std::cout<< "dentro del grasp" << std::endl;
    int finger_proximal[4]={1,4,8,11};
    int finger_distal[4]={0,3,7,10};
    std::string fingerprefix[5]={"ff","lf","mf","rf"};
    std::string fingerpart[3]={"distal","middle","proximal"};
    iscontact.init_flags();
    float command_proximal[4]={0,0,0,0};
    float command_distal[4]={0,0,0,0};
    bool flag_mono[12]={true,true,true,true,true,true,true,true,true,true,true,true};
    int fing_index=0;
    pub_command.publi_init(nh_ptr);
    //initilization of subscribers
     for(unsigned int i=0; i< 4; i++)
     {
         for(unsigned int k=0; k<3 ; k++)
         {
             std::string topicname="/contacts/"+fingerprefix[i]+"/"+fingerpart[k];
             iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>(topicname, 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, fing_index) ) );
             fing_index++;
         };
     };
	

    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/lf/knuckle", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 13) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/lf/metacarpal", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 14) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/palm", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 15) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/th/base", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 16) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/th/hub", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 17) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/th/distal", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 18) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/th/middle", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 19) ) );
    iscontact.is_contact_sub.push_back(nh_ptr->subscribe<gazebo_msgs::ContactsState>("/contacts/th/middle", 1000, boost::bind(&Contact_listener::callback, &iscontact, _1, 20) ) );
    std_msgs::Float64 command;
	
    //Move arm^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    std::cout<< "moving arm" << std::endl;
    float command_arm=-0.5;
    float count_time_arm=0;
    while(!iscontact.flag_arm)
    {
        command.data=command_arm;
        pub_command.PubPointer[21].publish(command);
        if(command_arm<0.01)
        {
        command_arm=command_arm+0.00001;
        }else{
            count_time_arm=count_time_arm+0.01;
            std::cout<< count_time_arm << std::endl;
            if(count_time_arm>10){
                iscontact.flag_arm=true;
            }
        }

        //std::cout<< command_arm << std::endl;

		
        ros::spinOnce();
    }
   std::cout<< "finish moving arm" << std::endl;
    //Move thumb^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    float command_thumb=-60*3.14/180;
    while(!iscontact.flag_thumb)
    {
        //std::cout << "the command of thumb is " << command_thumb*180/3.14 << std::endl;
        command.data=command_thumb;
        pub_command.PubPointer[17].publish(command);
        command_thumb=command_thumb+0.000001;
        ros::spinOnce();
        if(command_thumb>55*3.14/180)
        {
            iscontact.flag_thumb=true;
        }
    }
    std::cout<< "fuera del thumb" << std::endl;
    //Move proximals^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	
    bool flag_proximal=true;

    //std::cout<< "dentro del grasp" << std::endl;
    while(flag_proximal)
    {
        int count_proximal=0;
        for(int j=0;j<4;j++)
        {
            if(!iscontact.flag[3*j+2])  //it is multiplied by so it reads only the proximal
            {
                command.data=command_proximal[j];
                pub_command.PubPointer[finger_proximal[j]].publish(command);
                command_proximal[j]=command_proximal[j]+0.00001;
                std::cout << "the command proximal of " << 3*j+2 << "is " << command_proximal[j] << std::endl;
                if(command_proximal[j]>1.5 || iscontact.flag[3*j] || iscontact.flag[3*j+1])
                {
                    std::cout << "cambio de status" << std::endl;
                    iscontact.flag[3*j+2]=true;
                }
            }else{
                count_proximal++;
				
				
            }
        }
        if(count_proximal>3)
        {
            flag_proximal=false;
        }
		
        ros::spinOnce();
    }
    std::cout<< "fuera del prximal" << std::endl;
    
    bool flag_distal=true;
    //std::cout << "Now the distals" << std::endl;
    while(flag_distal)
    {
        int count_distal=0;
        for(int j=0;j<4;j++)
        {
            if((!iscontact.flag[3*j] && !iscontact.flag[3*j+1]) || !iscontact.flag[3*j]) //it is multiplied by so it reads only the proximal
            {
                //std::cout << "is in contact" << std::endl;
                command.data=command_distal[j];
                pub_command.PubPointer[finger_distal[j]].publish(command);
                command_distal[j]=command_distal[j]+0.00001;
                //std::cout << "the command of distal " << 3*j << "is " << command_distal[j] << std::endl;
                if(command_distal[j]>2)
                {
                    iscontact.flag[3*j+1]=true;
                    iscontact.flag[3*j]=true;
                }
            }else{
                count_distal++;
				
            }
        }
        if(count_distal>3)
        {
            flag_distal=false;
        }
		
        ros::spinOnce();
    }
    std::cout<< "fuera del distal" << std::endl;
}





int main(int argc, char **argv)
{
	state_watch.init_flags();
	ros::init(argc, argv, "pose_init_verify_server");
	ros::NodeHandle n;

	for(int j=0;j<24;j++){
		std::string topicname="/"+jointnames[j]+"_position_controller/state";
		
		//the following line shows how to call a callback from a class with more than one input
		 state_watch.errors.push_back( n.subscribe<control_msgs::JointControllerState>(topicname, 1, boost::bind(&State_listener::callback, &state_watch, _1, j) ));
		};
	
	
	//initialize simulation	
	initialize_simu(&n);
	
	//spawn model
	//spawnObject(&n);
	
	//perform grasp;
    //perform_grasp(&n);
	
	
  return 0;
}
