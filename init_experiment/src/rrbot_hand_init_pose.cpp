#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "control_msgs/JointControllerState.h"
#include <string>
#include <sstream>
#include <boost/functional/hash.hpp>
#include <boost/bind.hpp>
#include <ros/message_event.h>
#include <math.h>
//#include "comunication/InitPoseVerify.h"
#include <gazebo_msgs/SpawnModel.h>
#include <gazebo_msgs/ContactsState.h>
#include <ros/callback_queue.h>
#include <fstream>

std::string jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                            "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                            "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                              "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};
//float joint_init_pos[24]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.8, 1, 0.6, 0, 0, 0};
float joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -1.17,       1,        0.6,           0,           0,            0};

/// data to initialize arm and hand
/*

name: ['ElbowJRotate', 'ElbowJSwing', 'FFJ1', 'FFJ2', 'FFJ3', 'FFJ4',
 'LFJ1', 'LFJ2', 'LFJ3', 'LFJ4', 'LFJ5', 'MFJ1', 'MFJ2', 'MFJ3', 'MFJ4',
'RFJ1', 'RFJ2', 'RFJ3', 'RFJ4', 'ShoulderJRotate', 'ShoulderJSwing', 'THJ1',
 'THJ2', 'THJ3', 'THJ4', 'THJ5', 'WRJ1', 'WRJ2', 'weiss_connect']
position: [-0.7839909063476904, 1.5699641297818339, 0.24515326523732028,
 0.2443036569658359, 0.7502231888166095, 0.0174518212254009, 0.5417644228131016,
9.018932072812902e-05, 0.837489803729702, 0.03490513592644717, 0.017454060003539773,
0.2642749692594064, 0.24112360392970622, 0.7855917414335281, 0.017451844923419557,
0.271589749350043, 0.2527738566089166, 0.7851298754506235, -0.017451725196095502,
-0.042448774108561516, -1.3136711931593936e-06, 0.10471876674746383, 0.12217525796379203,
 -0.19197231405694026, 0.2617751170933955, 0.17458388978425532, -0.03611439395025684,
 -5.93913653981204e-05, -4.669523383071805e-06]


*/

float joint_init_pos[24]={0,           0,          0,           0,           0,            0,
                          0,           0,          0,           0,           0,            0,
                          0, 30*3.14/180, 8*3.14/180, -7*3.14/180, 70*3.14/180, -60*3.14/180,
                          -1.17,       1,        0.6,           0,           0,            0};

// data to initialize rrbot

/*
name: ['joint1', 'joint2', 'joint3']
position: [1.5886360451890393, 0.0072987118712211085, -3.1725118281755726e-05]
*/



class Command_pub{
    public:
        std::vector<ros::Publisher> PubPointer;
        void publi_init(ros::NodeHandle* n);
     }pub_command;

void Command_pub::publi_init(ros::NodeHandle* n){
    for(int i=0;i<24;i++){
        std::string topicname="/"+jointnames[i]+"_position_controller/command";
        this->PubPointer.push_back( n->advertise<std_msgs::Float64>(topicname, 1));
    }
}

class State_listener{
public:
	//float init;
	std::vector<ros::Subscriber> errors;
    bool flag[24]; // publisher will stop publishing to exact topic when actial position and desired position within an error
    bool flag2; // publisher will stop publishing when all flags (flag[24])
	float  error_current;
	float  error_max[24];
	float  error_max2;
    void print();
	void callback(const control_msgs::JointControllerStateConstPtr &msg, int i);
    State_listener (bool flag_state, float  error_max_init) : flag2(flag_state), error_max2(error_max_init) {};
	void init_flags();
	//~State_listener () {delete errors};
}state_watch (false,0);

void State_listener::print(){
    std::cout << "flag2"<< flag2 << std::endl;
}

void State_listener::init_flags(){
	for(int i=0;i<24;i++){
			flag[i]=flag2;
			error_max[i]=error_max2;
		};
}


/*this callback gets information about the error of the controllers and when
the maximun error reaches 0.05, it sets the flag to one to stop the program */
void State_listener::callback(const control_msgs::JointControllerStateConstPtr &msg, int id){
    /*this part watches the errors of all the controllers, and when the maximun
      * error reaches 0.005 or lower, it sets the flag to 1 and the program stops */
    error_current=(msg->process_value);
    //std::cout<< "current_error" << error_current << std::endl;
    error_max[id]=std::fabs(error_current-joint_init_pos[id]);
    //sets flag to 1 and stops the program.
	
    if(error_max[id]<0.05){
		flag[id]=true;
        std::cout << "id=" << id << std::endl;
        std::cout << "flag_subs=" << flag[id] << std::endl;
	}
	
	int count=0;
	for(int i=0;i<24;i++){
		if(flag[i]==true){
			count++;
            std::cout << "count=" << i << std::endl;
		}	
	}
	
	if(count==23){
        flag2=true;
	//std::cout << "flag_2=" << flag2 << std::endl;
    }

	 ros::Time::now();
}
 
  //Spawn an object
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
void spawnObject(ros::NodeHandle* nh_ptr){
  ros::ServiceClient spawnModelClient = nh_ptr->serviceClient<gazebo_msgs::SpawnModel> ("/gazebo/spawn_sdf_model");
  gazebo_msgs::SpawnModel model;
  std::string objectPath = "src/Gazebo_objects/model.sdf";
  //std::string objectPath = "src/comunication/sphere.sdf";
  //std::string objectPath = "src/comunication/cone.sdf";

  char buff[1000];
  std::string globalObjectPath = getcwd(buff, 1000);
  globalObjectPath = globalObjectPath + "/" + objectPath;
  std::ifstream file(globalObjectPath.c_str());
  std::string line;

  while(!file.eof()) // Parse the contents of the given sdf in a string
    {
      std::getline(file,line);
      model.request.model_xml+=line;
    }
  file.close();


  model.request.reference_frame = "world";
  model.request.initial_pose.position.x=0.681;
  model.request.initial_pose.position.y=-0.0862;
  model.request.initial_pose.position.z=1.02;
  model.request.initial_pose.orientation.x=0;
  model.request.initial_pose.orientation.y=0;
  model.request.initial_pose.orientation.z=1;
  model.request.initial_pose.orientation.z=0;

  std::ostringstream stm;
  //stm << model_name << id;
  model.request.model_name = "cylinder";

  if (!spawnModelClient.call(model))
    ROS_ERROR("reinitializeObject : Failed to call service for setmodelstate ");
}

int initialize_simu(ros::NodeHandle* nh_ptr){
	
	//ros::Time begin=ros::Time::now();
	//ros::Duration five_seconds(10.0);
	
	//Command_pub pub_command;
	state_watch.init_flags();
	
	
	// initialization of publishers
	pub_command.publi_init(nh_ptr);
	
	
	
	//definition of the command
	std_msgs::Float64 command;
	
	//Publication of the commands and flag surveillance
	//ros::Rate r(10);
	//std::cout << state_watch.flag2 << std::endl;
	int i=1;
	while(state_watch.flag2==false){	
		
		if(!state_watch.flag[i]){

		command.data=joint_init_pos[i];
		
		pub_command.PubPointer[i].publish(command);


		}
		i++;
		if(i>23)
		i=0;
		
		ros::spinOnce();
		


	}

 return 0;

}


int main(int argc, char **argv)
{
	state_watch.init_flags();
	ros::init(argc, argv, "pose_init_verify_server");
	ros::NodeHandle n;

	for(int j=0;j<24;j++){
		std::string topicname="/"+jointnames[j]+"_position_controller/state";
		
		//the following line shows how to call a callback from a class with more than one input
		 state_watch.errors.push_back( n.subscribe<control_msgs::JointControllerState>(topicname, 1, boost::bind(&State_listener::callback, &state_watch, _1, j) ));
		};
	
	
	//initialize simulation	
	initialize_simu(&n);
	
	//spawn model
    spawnObject(&n);
	
	//perform grasp;
    //perform_grasp(&n);
	
	
  return 0;
}
