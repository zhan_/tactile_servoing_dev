#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include <string>
#include <sstream>
#include <ros/message_event.h>
#include <math.h>
#include <fstream>
#include <iostream>
#include "namesp.h"

// usestok1.cpp -- using the Stock class

#include "stock.h"
int main()
{
    using std::cout;
    using std::ios_base;
    cout.precision(2); // #.## format
    cout.setf(ios_base::fixed, ios_base::floatfield);// #.## format
    cout.setf(ios_base::showpoint); // #.## format
    cout << "Using constructors to create new objects\n";
    Stock stock1("NanoSmart", 12, 20.0); // syntax 1
    stock1.show();
    Stock stock2 = Stock ("Boffo Objects", 2, 2.0); // syntax 2
    stock2.show();
    cout << "Assigning stock1 to stock2:\n";
    stock2 = stock1;
    cout << "Listing stock1 and stock2:\n";
    stock1.show();
    stock2.show();
    cout << "Using a constructor to reset an object\n";
    stock1 = Stock("Nifty Foods", 10, 50.0); // temp object
    cout << "Revised stock1:\n";
    stock1.show();
    cout << "Done\n";
    return 0;
}
