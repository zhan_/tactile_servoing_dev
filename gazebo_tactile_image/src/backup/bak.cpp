/**
  * Node:tactile_image_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	Subscribe to collision message from gazebo simualation
  * 	and publish the ros fotmated tactile image
  * Topic: ros_tactile_image
  * Topic Msg Type: http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
 */
#include "ros/ros.h"
#include <ros/console.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"

#include <iostream>
#include <string>
#include <vector>

/// These headers will allow us to convert to ros type image
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h> 

/// These are the gazebo headers
#include "std_msgs/Header.h"
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

/// These headers are used to perform tf transformations
#include <tf/transform_listener.h>
#include <geometry_msgs/Pose.h>
#include "tf2_ros/buffer.h"
#include <tf2_ros/transform_listener.h>
//#include <tf2_ros/transform_listener.h>


#include <boost/scoped_ptr.hpp>
#include <kdl/chain.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/frames.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>

#include <kdl/chainfksolver.hpp>
#include <kdl/frames_io.hpp>
#include <stdio.h>

#include "sensor_msgs/JointState.h"

#include <cmath>    


using namespace std;
using namespace gazebo_msgs;
using namespace KDL;


/// Variables of position for matching tactile image 
double x; double y; double z;

/// Variables to store forces 
double fz;         				 //the normal forces 

/// Form fixed tactel multidimensional array 
int tactel_array[72] = {}; //unsigned 32-bit int 

/// counters and flags
unsigned int counter = 0; unsigned int flag = 0;

/// Create globally messages to be published
sensor_msgs::Image ros_image;
const float MPI = 3.141516;

/// Get joints
float q[5] = {};

/// Position in world frame
float Wpos_Frame[3] = {};
float Temp_Frame[3] = {};

/// Create the frame that will contain the results in cartesian space
KDL::Frame cartpos; 

/// Link poses wrt world frame
float xl[5] = {} ; float yl[5] = {} ; float zl[5] = {} ;

/// Store contact posoition with respect to tactile sensor 
float tactel_pose_x, tactel_pose_y, tactel_pose_z ;


void matchWorld () {
	
		/// Store the data into an array		
			if((y < 25)  && (y >= 8)) { //if((y < -32)  && (y >= -50))
					if((x > 1)  && (x <= 15)) tactel_array[0] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[1] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[2] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[3] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[4] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[5] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[6] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[7] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[8] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[9] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[10] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[11] = fz;	
				}
			if((y < 8)  && (y >= -2)) { //if((y < -22)  && (y >= -32))
					if((x > 1)  && (x <= 15)) tactel_array[12] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[13] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[14] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[15] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[16] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[17] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[18] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[19] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[20] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[21] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[22] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[23] = fz;
				}
			if((y < -2)  && (y >= -12)) { //if((y < -12)  && (y >= -22)) 
					if((x > 1)  && (x <= 15)) tactel_array[24] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[25] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[26] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[27] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[28] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[29] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[30] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[31] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[32] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[33] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[34] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[35] = fz;
				}
			if((y < -12)  && (y >= -22)) { //if((y < -2)  && (y >= -12))
					if((x > 1)  && (x <= 15)) tactel_array[36] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[37] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[38] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[39] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[40] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[41] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[42] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[43] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[44] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[45] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[46] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[47] = fz;
				}
			if((y < -22)  && (y >= -32)) { //if((y < 8)  && (y >= -2))
					if((x > 1)  && (x <= 15)) tactel_array[48] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[49] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[50] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[51] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[52] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[53] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[54] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[55] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[56] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[57] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[58] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[59] = fz;
				}
			if((y < -32)  && (y >= -50)) { //if((y < 25)  && (y >= 8))
					if((x > 1)  && (x <= 15)) tactel_array[60] = fz;											
					if((x > 15)  && (x <= 20)) tactel_array[61] = fz;		
					if((x > 20)  && (x <= 25)) tactel_array[62] = fz;
					if((x > 25)  && (x <= 30)) tactel_array[63] = fz;				
					if((x > 30)  && (x <= 35)) tactel_array[64] = fz;		
					if((x > 35)  && (x <= 40)) tactel_array[65] = fz;
					if((x > 40)  && (x <= 45)) tactel_array[66] = fz;
					if((x > 45)  && (x <= 50)) tactel_array[67] = fz;
					if((x > 50)  && (x <= 55)) tactel_array[68] = fz;
					if((x > 55)  && (x <= 60)) tactel_array[69] = fz;
					if((x > 60)  && (x <= 65)) tactel_array[70] = fz;
					if((x > 65)  && (x <= 80)) tactel_array[71] = fz;
				}
    
	
	}
	
	
	
	
void matchTactel () {
	
	/// long 6 * 6
	/// Accomodate this data according to sensor colocation and dimensions, current data is in [mm]
	const int dminTactel_x = -30, dmaxTactel_x = 30, dminTactel_y = -30, dmaxTactel_y = 30;    
	
	const int tactel_noRows = 18, tactel_noCols = 4;
	
	float dTactels_x=0, dTactels_y=0;
	
	int ifx=0;
	
	dTactels_x = (abs(dminTactel_x) + dmaxTactel_x) / tactel_noCols;	// 15
	dTactels_y = (abs(dminTactel_y) + dmaxTactel_y) / tactel_noRows; 	// 3.3
	
	
	if(  (tactel_pose_y > dminTactel_y)  && ( tactel_pose_y <= ( dminTactel_y + dTactels_y ) )  ) {										
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[0] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[1] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[2] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[3] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y +   dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 2*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[4] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[5] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[6] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[7] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 2*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 3*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[8] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[9] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[10] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[11] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 3*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 4*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[12] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[13] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[14] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[15] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 4*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 5*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[16] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[17] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[18] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[19] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 5*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 6*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[20] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[21] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[22] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[23] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 6*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 7*dTactels_y))) {				
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[24] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[25] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[26] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[27] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 7*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 8*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[28] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[29] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[30] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[31] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 8*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 9*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[32] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[33] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[34] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[35] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 9*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 10*dTactels_y))) {		
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[36] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[37] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[38] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[39] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 10*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 11*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[40] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[41] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[42] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[43] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 11*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 12*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[44] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[45] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[46] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[47] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 12*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 13*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[48] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[49] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[50] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[51] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 13*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 14*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[52] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[53] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[54] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[55] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 14*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 15*dTactels_y))) {		
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[56] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[57] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[58] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[59] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 15*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 16*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[60] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[61] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[62] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[63] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 16*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 17*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[64] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[65] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[66] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[67] = fz; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 17*dTactels_y))  && (tactel_pose_y <= dmaxTactel_y)) { 
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel_array[68] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel_array[69] = fz; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel_array[70] = fz; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel_array[71] = fz; 
	}



	}
	
	
	
	
	
/// Hear joints state, store and turn on flag 1 
void joints_stateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	if(flag){
	q[0]  = 0; 	q[1]  = 0; q[2] = msg->position[0]; 	 
	q[3] = msg->position[1]; q[4]  = 0;

	//ROS_INFO("I heard the joint states... ");
	}
} 





/// broadcast all link states in world frame	
void extract_links_poseCallback(const gazebo_msgs::LinkStates::ConstPtr& msg) 
{
	//if(flag){
	ROS_INFO("Looking for links ... ");  
	
	//cout<<"Links: "<<endl;

	for( int i=0; i<msg->name.size() ; i++ ) 
	{	
		string name = msg->name[i];
		
	/**	
		cout<<name<<": "<<"[ "<<msg->pose[i].position.x * 1000<<","
							  <<msg->pose[i].position.y * 1000<<","
							  <<msg->pose[i].position.z * 1000<<" ]"<<" ; "<<endl;  	
	*/    
	    xl[i] = msg->pose[i].position.x * 1000 ;
		yl[i] = msg->pose[i].position.y * 1000 ;
		zl[i] = msg->pose[i].position.z * 1000 ;
	}
	
	/// Show for debugging purposes
	/**
	cout<<endl<<"X: [";
	for ( int i=0; i<(msg->name.size()) ; i++ ) cout<<x[i]<<"  ";
	cout<<"]"<<endl<<"Y: [";
	for ( int i=0; i<(msg->name.size()) ; i++ ) cout<<y[i]<<"  ";
	cout<<"]"<<endl<<"Z: [";
	for ( int i=0; i<(msg->name.size()) ; i++ ) cout<<z[i]<<"  ";
	cout<<"]"<<endl;
	*/
	
	
	/// contact frame is Wpos_Frame
    Wpos_Frame[0] = x; Wpos_Frame[1] = y; Wpos_Frame[2] = z;
	
	//cout<<"Sensor frame: "<< endl << "[ " << xl[4] << " , " << yl[4] << " , " << zl[4] << " ]"<<endl; 
	//for (int i = 0 ; i < 3 ; i++) Temp_Frame[i] = Wpos_Frame[i] - cartpos.p[i];
	//cout << "Sensor Position:" << endl;
	//for (int i = 0 ; i < 3 ; i++) cout << Temp_Frame[i] << " ";
	

	//tactel_pose_x = xl[4] - Wpos_Frame[0];
	//tactel_pose_y = yl[4] - Wpos_Frame[1];
	//tactel_pose_z = zl[4] - Wpos_Frame[2];
		/// Show for debugging purposes
	/**
	cout<<"Sensor pose: "<< endl << "[ " << tactel_pose_x  <<" , "
										 << tactel_pose_y  <<" , "
										 << tactel_pose_z  << " ]"<<endl; 
	
	*/
	
	/**
	/// Create the robot chain
	KDL::Chain chain;
	chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(xl[0],yl[0],zl[0]))));
	chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(xl[1],yl[1],zl[1]))));
	chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(xl[2],yl[2],zl[2]))));
	chain.addSegment(Segment(Joint(Joint::RotX),Frame(Vector(xl[3],yl[3],zl[3]))));
	chain.addSegment(Segment(Joint(Joint::RotZ),Frame(Vector(xl[4],yl[4],zl[4]))));	
		
	/// Create solver based on kinematic chain
    ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);
    
    /// Create joint array
    unsigned int nj = chain.getNrOfJoints();
    KDL::JntArray jointpositions = JntArray(nj);
    
    /// Get joint positions
    for(unsigned int i=0;i<nj;i++)  jointpositions(i)=q[i];
 
	/// Calculate forward position kinematics
    bool kinematics_status;
    kinematics_status = fksolver.JntToCart(jointpositions,cartpos);
    
    if(kinematics_status>=0){
       // cout << "Cartisian Position:" << endl << cartpos.p <<endl;
        //cout << "Cartisian Orientation:" << endl << cartpos.M <<endl;
    }else{
		cout << "I couldnt create FK" << endl;
    }
 */
   // }
	
}




	
/**
 * Callback function that execute each time a new collision is detected
 */

void extract_imageCallback(const gazebo_msgs::ContactsState::ConstPtr& msg)
{
	//ROS_INFO("Looking for contacts ... ");  
	//if (msg->states.size()>1) {
	    /// sweep for the total geom pairs in contacts        
		for(int i=0;i<msg->states.size(); i++) {
			 
			counter ++;													// initialize counter	
			
			/// GET TACTILE INFORMATION 
			string frame = msg->header.frame_id;  			  			// Get the sensor frame
			string collision1 = msg->states[i].collision1_name;  		// Get the collision object 1
			string collision2 = msg->states[i].collision2_name;  		// Get the collision object 2
						
			ros::Time ts = ros::Time::now();							// Get ros time
			
			uint32_t seq = msg->header.seq;								// Get sequence
			
			//msg->header.stamp = ros::Time::now(); 		
			//uint32_t tg = msg->header.stamp.sec;	// Get the stamp in seconds		
			//contact_depth=msg->states[i].depths;    // Get depth
					
			geometry_msgs::Vector3 positions; 		  // Get the positions from collision topic 
			positions.x = msg->states[i].contact_positions[i].x;	
			positions.y = msg->states[i].contact_positions[i].y;
			positions.z = msg->states[i].contact_positions[i].z;
			x = positions.x*1000; y = positions.y*1000; z = positions.z*1000;
			
			/// contact frame is Wpos_Frame
			Wpos_Frame[0] = x; Wpos_Frame[1] = y; Wpos_Frame[2] = z;
			
			geometry_msgs::Vector3 forces;								// Get the normal forces[i] from collision topic
			forces.z = msg->states[i].wrenches[i].force.z;
			fz = forces.z*100;
			
			//cout<<" Contact points gazebo world frame: "<<endl;
			//cout<<"[ "<<x<<" , "<<y<<" , "<<z<<" ]"<<endl;
			
			//matchWorld (); 												//match tactel position against the world reference frame

			tactel_pose_x = xl[4] -x;
			tactel_pose_y = yl[4] - y;
			tactel_pose_z = zl[4] - z;
					
			matchTactel ();												//match tactel position against the sensor frame		
    	
		// after 100 interactions in order to have more stable data of contact	
		if(counter >= 100) {
			counter=0;
			std::cout<<"\n";
			ROS_INFO("Contacts detected ... ");   
			cout<<"Contact between  "<<collision1<<"  and  "<<collision2<<endl;
			cout<<"Frame:     "<<frame<<endl;
			cout<<"Seq:       "<<seq<<endl;
			cout<<"Time ros:  "<<ts<<endl;	
			//std::cout<<"Time gaz:  "<<tg<<std::endl;	
			
	
	//	cout<<"Sensor pose: "<< endl << "[ " << tactel_pose_x  <<" , "
		//								 << tactel_pose_y  <<" , "
			//							 << tactel_pose_z  << " ]"<<endl; 
	
		// filter data and fill in tactile array message for publishing the topic 
		int array_size = sizeof(tactel_array)/sizeof(int);
		
		for(int j=0; j<(array_size); j++) ros_image.data.push_back(tactel_array[j]); //fill array when high pass filtered
				
		//Fill message data in ros image format using std_msgs
		ros_image.height=18; 						//number of rows
		ros_image.width=4; 							//number of columns
		ros_image.step=4; 							//number of columns (3) * number of channels (1) * size of data type(1) 
		ros_image.encoding="8UC1";   				//conversions (mono8: CV_8UC1, grayscale image) and in OpenCv CvMat: "8UC1"	
		ros_image.is_bigendian=0;       			//false default		
		ros_image.header.frame_id = frame;			//frame where is attached the sensor in simulation urdf
		//ros_image.header.stamp = ts;   			//gazebo simulation time
		ros_image.header.stamp = ros::Time::now();  //ros time
		
		for(int j=0; j<72; j++) tactel_array[j]=0;  // empty vector
		flag = 1;									//now  allow to publish	
				
		} // close counter

        } // close for to sweep contacts
        
       // } // close if contact
    
    } //close callback function







 /**********************************************************************
 * Main
 ***********************************************************************/  
   
int main(int argc, char **argv)
{
  /// Initialize Ros systen 
  ros::init(argc, argv, "tactile_image_extractor");
  
  /// Create node
  ros::NodeHandle n;
    
  /// Create the suscriber to the topic collisions
  ros::Subscriber subCol = n.subscribe("/collisions", 1000, extract_imageCallback);
  ros::spinOnce();
  
  /// Create the suscriber to the topic of gazebo links states
  ros::Subscriber subLink = n.subscribe("/gazebo/link_states", 1000, extract_links_poseCallback);
  ros::spinOnce();
   
  /// Create the suscriber to the joint angles position, velocity and effort
  //ros::Subscriber sub_joints_state = n.subscribe("/tilt_pan/joint_states", 1000, joints_stateCallback);
  //ros::spinOnce();
  
  /// Create the publisher of the topic: tactile_image
  ros::Publisher pub_image = n.advertise<sensor_msgs::Image>("ros_tactile_image", 100);
  ros::Publisher pub_tfs = n.advertise<geometry_msgs::Pose>("tf_sensor", 100);
  
  /// Loop at 100Hz until the node is shut down
  ros::Rate loop_rate(50);

  
  /// Main loop

  while (ros::ok())
  {

    
	int array_size = sizeof(ros_image.data)/sizeof(int);
	if(flag==1 && array_size<73){
		flag=0;
		pub_image.publish(ros_image);
		ros_image.data.clear();
		ROS_INFO("Tactile image published ... ");
		
	}

	// Do this
    ros::spinOnce();
    
    //wait until it is time for another interaction
    loop_rate.sleep();

    			
  }
  return 0;
}
