/**
 * @author Galo MALDONADO, galo_xav@hotmail.com
 * @date 2014-04-16
 * @file tactile_image_extractor.cpp
 * 
* Copyright Handle Project  
* Version: 1.1
* 
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*  
 * @brief : This node subscribes to collision message and to link states in gazebo simulation 
 *          to extract a virtual contact image and publish as ros fotmated tactile topic ros_tactile_image
 *          Topic Msg Type: http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
 */
 
#include "virtualPixelPPS6x4.hpp"
 
#include "ros/ros.h"
#include <ros/console.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

/// These headers will allow us to convert to ros type image
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h> 

/// These are the gazebo headers
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

#include "sensor_msgs/JointState.h"


using namespace std;
using namespace gazebo_msgs;
using namespace ros;

/// Initialize the tactile sensor data
const unsigned int NoRows = 6; const unsigned int NoCols = 4;
const unsigned int tactelsNumber = NoRows * NoCols;
int tactel_array_center[tactelsNumber] = {};
int tactel_array_left[tactelsNumber] = {};
int tactel_array_right[tactelsNumber] = {};
   
int array_size = 0;
int tactel=0;
int NoSensors=1; 

/// Accomodate this data according to sensor colocation and dimensions, current data is in [mm] for actual PPS 6x4
const float dminTactel_y = -8.5, dmaxTactel_y = 8.5, dminTactel_x = -10, dmaxTactel_x = 10; 

/// Variables of position for matching tactile image 
double x; double y; double z;

/// Variables to store forces 
double fz;         				                                        //the normal forces 

/// flag for allow publish just when detect contact
bool flag = false; unsigned int counter = 0;

/// Create globally messages to be published
sensor_msgs::Image ros_image_center_sensor;
sensor_msgs::Image ros_image_left_sensor;
sensor_msgs::Image ros_image_right_sensor;

/// Store contact posoition with respect to tactile sensor 
float tactel_pose_x, tactel_pose_y, tactel_pose_z ;

/// Max Links and array of frame names
const int maxLinks = 500;												// set up a fixed size for a string array
string linksNames[maxLinks] = {} ;										// create array of strings for store the links names
int linksNo = 0; 														// number of links in simulation environment, reset to zero
const unsigned int maxContactIter = 300;      								// for contact loop 
/// Link poses wrt world frame		
float xl[maxLinks] = {} ; float yl[maxLinks] = {} ; float zl[maxLinks] = {} ;

float offset_x_center_sensor, offset_y_center_sensor, offset_z_center_sensor;   
float offset_x_right_sensor, offset_y_right_sensor, offset_z_right_sensor; 
float offset_x_left_sensor, offset_y_left_sensor, offset_z_left_sensor; 
float x_center_sensor, y_center_sensor, z_center_sensor; 
float x_left_sensor, y_left_sensor, z_left_sensor; 
float x_right_sensor, y_right_sensor, z_right_sensor; 
float x_center_frame, y_center_frame, z_center_frame;

string frame;  			  			    
string collision1;  		   
string collision2;  		    
ros::Time ts;							   
uint32_t seq;		

bool gotLeftOffset = false;
bool gotCenterOffset = false;
bool gotRightOffset = false;
bool got_Center = false;
bool contact;
bool msg_right_sensor, msg_center_sensor, msg_left_sensor  ;

class Extractor {
    public:
    //    Extractor();
        bool init();
		

    private:
        ros::NodeHandle n, nh_private;  
        void getImage();
        void fillMsg();
        
        
        
};

// destructor Extractor() {
/******************************************************************************************
 * Callback function that execute peridically to extract link states in world frame
 ******************************************************************************************/	
				
void extract_links_poseCallback(const gazebo_msgs::LinkStates::ConstPtr& msg) 
{

if (!got_Center && !gotRightOffset) {
	if (!gotLeftOffset && !gotCenterOffset) {
	
	ROS_INFO("Looking for links poses ... ");  
	linksNo = 0;
	
	for( int i=0; i<msg->name.size() ; i++ ) 							/// get link poses
	{	
		linksNames[i] = msg->name[i]; 
	    xl[i] = msg->pose[i].position.x * 1000 ;
		yl[i] = msg->pose[i].position.y * 1000 ;
		zl[i] = msg->pose[i].position.z * 1000 ;
		linksNo ++;
		
		if (linksNames[i] == "Tilt_pan::center_sensor") { 
			x_center_frame = xl[i]; y_center_frame = yl[i]; z_center_frame = zl[i];  
			cout<<endl<< "CENTER FRAME : [ " << x_center_frame << " , " << y_center_frame << " , " << z_center_frame << " ]" << endl << endl;
			got_Center = true;
		}
	}
	
	
	
	
	
	if(got_Center) {	
		for(unsigned int j = 0; j < msg->name.size(); j++) {  /// compute offsets
		
		if (linksNames[j] == "Tilt_pan::right_sensor") {
				x_right_sensor = xl[j]; y_right_sensor = yl[j]; z_right_sensor = zl[j];  
				gotRightOffset = true;

		
		}

		
		if (linksNames[j] == "Tilt_pan::center_sensor") {
				x_center_sensor = xl[j]; y_center_sensor = yl[j]; z_center_sensor = zl[j];  
				gotCenterOffset = true;

			}
			
				
		
		if (linksNames[j] == "Tilt_pan::left_sensor") {
				x_left_sensor = xl[j]; y_left_sensor = yl[j]; z_left_sensor = zl[j];  
				gotLeftOffset = true;				

		}
	
		}
	
	}
	
	else {
		ROS_ERROR("Could not get reference frame for contact sensors");
	}
	
	
	
	}
	
	}
}
	
/********************************************************************************************
 * Callback functions that execute each time a new collision is detected
 *******************************************************************************************/

void extract_imageCallback(const gazebo_msgs::ContactsState::ConstPtr& msg)
{
	//ROS_INFO("Looking for contacts ... ");  
    /// sweep for the total geom pairs in contacts        
    for(int i=0;i<msg->states.size(); i++) {			 
   
        				    								// initialize counter	
			
        /// GET TACTILE INFORMATION 
        frame = msg->header.frame_id;  			  			    		// Get the sensor frame name in contact
        collision1 = msg->states[i].collision1_name;  		    		// Get the collision object 1
        collision2 = msg->states[i].collision2_name;  		    		// Get the collision object 2
        ts = ros::Time::now();							    			// Get ros time
        seq = msg->header.seq;								    		// Get sequence
                
        geometry_msgs::Vector3 positions; 	    	                    // Get the positions from collision topic 
        positions.x = msg->states[i].contact_positions[i].x;	
        positions.y = msg->states[i].contact_positions[i].y;
        positions.z = msg->states[i].contact_positions[i].z;
        x = positions.x*1000; y = positions.y*1000; z = positions.z*1000;
			
        geometry_msgs::Vector3 forces;				    				// Get the normal forces[i] from collision topic
        forces.z = msg->states[i].wrenches[i].force.z;
        fz = forces.z*100;		
        
        contact=true;
		Extractor k;
		if (k.init()<0) {
			ROS_ERROR("Could not initialize extractor");
		}

    }                                                                   // close for to sweep contacts
    
}  

/// Fill message data in ros image format using std_msgs
/// for PPS6x4
bool Extractor::init() {
	
	//ROS_INFO("EXTRACTOR "); 
	
//    if(contact && fz > 0) {
		
	//	cout<<endl<<endl;
	//	ROS_INFO("Contacts detected ... ");   
	//	cout<<"Contact between  "<<collision1<<"  and  "<<collision2<<endl;
	//	cout<<"Frame:     "<<frame<<endl;
	//	cout<<"Seq:       "<<seq<<endl;
	//	cout<<"Time ros:  "<<ts<<endl;
		
		getImage (); 
		counter ++;	
		/// after n interactions in order to have more stable data of contact from simulation		
		if(counter >=  maxContactIter) fillMsg ();
		
				
//	}                                                               // close counter
	 
}


void Extractor::getImage () { /// get tactels information from contact image
		/// Pose => compute difference between sensor frame and contact world gazebo frame
		//cout << "Pos: " << x << " , " << y << " , " << z << endl;

			if (frame == "tactel_center") {
				tactel_pose_x = 0; tactel_pose_y = 0; tactel_pose_z  = 0;
				tactel_pose_x = x_center_sensor - x; tactel_pose_y = y_center_sensor - y; tactel_pose_z = z_center_sensor - z;
			//	cout << "tactel center pose: [ " << tactel_pose_x << " , " << tactel_pose_y << " ]" << endl;
				tactel = virtualPixelPPS6x4(tactel_pose_y, tactel_pose_x, NoRows, NoCols, dminTactel_x, dmaxTactel_x, dminTactel_y, dmaxTactel_y, offset_x_center_sensor, offset_y_center_sensor);
			//	cout << "No : " << tactel << endl;
				tactel_array_center [tactel] = fz; 
			}
			
			if (frame == "tactel_left") {	
				tactel_pose_x = 0; tactel_pose_y = 0; tactel_pose_z  = 0;	
				tactel_pose_x = x_left_sensor - x; tactel_pose_y = y_left_sensor - y; tactel_pose_z = z_left_sensor - z;
			//	cout << "tactel left pose:   [ " << tactel_pose_x << " , " << tactel_pose_y << " ]" << endl;
				tactel = virtualPixelPPS6x4(tactel_pose_y, tactel_pose_x, NoRows, NoCols, dminTactel_x, dmaxTactel_x, dminTactel_y, dmaxTactel_y, offset_x_left_sensor, offset_y_left_sensor);
			//	cout << "No : " << tactel << endl;
				tactel_array_left [tactel] = fz;
				
			}
			
			if (frame == "tactel_right") {
				tactel_pose_x = 0; tactel_pose_y = 0; tactel_pose_z  = 0;
				tactel_pose_x = x_right_sensor - x; tactel_pose_y = y_right_sensor - y; tactel_pose_z = z_right_sensor - z;
			//	cout << "tactel right pose:  [ " << tactel_pose_x << " , " << tactel_pose_y << " ]" << endl;
				tactel = virtualPixelPPS6x4(tactel_pose_y, tactel_pose_x, NoRows, NoCols, dminTactel_x, dmaxTactel_x, dminTactel_y, dmaxTactel_y, offset_x_right_sensor, offset_y_right_sensor);
			//	cout  << "No : " << tactel << endl;
				tactel_array_right [tactel] = fz;
			}

}


void Extractor::fillMsg () /// after 100 interactions in order to have more stable data of contact	
{ 				
				cout << "counter: " << counter << endl;
				counter=0;
				int c = 0;
				
				ROS_INFO("Filling messages ... ");

				
				/// Fill and reset arrays
				array_size = sizeof(tactel_array_right)/sizeof(int);
				for(int j=0; j<(array_size); j++) ros_image_right_sensor.data.push_back(tactel_array_right[j]);		
				
				cout << "TACTEL RIGHT" << endl;
				for(int i = 0; i < 6; i++){
					for(int j = 0; j < 4; j++) {
						cout << tactel_array_right[c] << "      ";
						++c;
					}
					cout << endl;
				}
				for(int j=0; j<array_size; j++) tactel_array_right[j]=0;
				
			    if(ros_image_right_sensor.data.size()>0) 
				{	
					ros_image_right_sensor.height=NoRows; 					    // number of rows
					ros_image_right_sensor.width=NoCols; 						// number of columns
					ros_image_right_sensor.step=NoCols; 						// number of columns (3) * number of channels (1) * size of data type(1)
					ros_image_right_sensor.encoding="8UC1";   				    // conversions (mono8: CV_8UC1, grayscale image) and in OpenCv CvMat: "8UC1"
					ros_image_right_sensor.is_bigendian=0;       			    // false default
					ros_image_right_sensor.header.frame_id = frame;			    // frame where is attached the sensor in simulation urdf
					ros_image_right_sensor.header.stamp = ros::Time::now();     // ros time
					msg_right_sensor = true;
				}
			    
			       
			    
			    
			    
			    
			    c = 0;
				array_size = sizeof(tactel_array_center)/sizeof(int);
				for(int j=0; j<(array_size); j++) ros_image_center_sensor.data.push_back(tactel_array_center[j]);	
				cout << "TACTEL CENTER" << endl;
				for(int i = 0; i < 6; i++){
					for(int j = 0; j < 4; j++) {
						 cout << tactel_array_center[c] << "       ";
						 ++c;
					}
					cout << endl;
				}	
				for(int j=0; j<array_size; j++) tactel_array_center[j]=0;  
				
				if(ros_image_center_sensor.data.size()>0) 
				{	
					ros_image_center_sensor.height=NoRows; 						// number of rows
					ros_image_center_sensor.width=NoCols; 						// number of columns
					ros_image_center_sensor.step=NoCols; 						// number of columns (3) * number of channels (1) * size of data type(1) 
					ros_image_center_sensor.encoding="8UC1";   				    // conversions (mono8: CV_8UC1, grayscale image) and in OpenCv CvMat: "8UC1"
					ros_image_center_sensor.is_bigendian=0;       			    // false default
					ros_image_center_sensor.header.frame_id = frame;			// frame where is attached the sensor in simulation urdf
					ros_image_center_sensor.header.stamp = ros::Time::now();    // ros time
					msg_center_sensor = true;
				}
				
				
				
				
				
				c = 0;
				array_size = sizeof(tactel_array_left)/sizeof(int);
				for(int j=0; j<(array_size); j++) ros_image_left_sensor.data.push_back(tactel_array_left[j]);
				cout << "TACTEL LEFT" << endl;
				for(int i = 0; i < 6; i++){
					for(int j = 0; j < 4; j++) {
						cout << tactel_array_left[c] << "      ";
						++c;
					}
					cout << endl;
				}		
				
				for(int j=0; j<array_size; j++) tactel_array_left[j]=0; 
					
				if(ros_image_left_sensor.data.size()>0) 
				{	
					ros_image_left_sensor.height=NoRows; 						// number of rows
					ros_image_left_sensor.width=NoCols; 						// number of columns
					ros_image_left_sensor.step=NoCols; 							// number of columns (3) * number of channels (1) * size of data type(1) 
					ros_image_left_sensor.encoding="8UC1";   				    // conversions (mono8: CV_8UC1, grayscale image) and in OpenCv CvMat: "8UC1" 
					ros_image_left_sensor.is_bigendian=0;       			    // false default
					ros_image_left_sensor.header.frame_id = frame;			    // frame where is attached the sensor in simulation urdf
					ros_image_left_sensor.header.stamp = ros::Time::now();      // ros time	
					msg_left_sensor = true;
				}
}	




 /**********************************************************************
 * Main
 ***********************************************************************/  
   
int main(int argc, char **argv)
{
	
  ROS_INFO("TACTILE IMAGE EXTRACTOR NODE");  
  
  /// Initialize Ros systen 
  init(argc, argv, "gazebo_tactile_image");
  
  /// Create node
  NodeHandle n;

  /// Create the suscriber to the topic collisions !this should match with gazebo topic name in the urdf model
  Subscriber subCol1 = n.subscribe("/collision_sensor", 1000, extract_imageCallback);
  
  /// Create the suscriber to the topic of gazebo links states
  Subscriber subLink = n.subscribe("/gazebo/link_states", 1000, extract_links_poseCallback);
     
  /// Create the publisher of the topic: tactile_image
  Publisher pub_image = n.advertise<sensor_msgs::Image>("ros_tactile_image", 100);

  /// Loop at 50Hz until the node is shut down
  Rate loop_rate(100);

    
 while (ok())
 {

	if(msg_right_sensor) {
		pub_image.publish(ros_image_right_sensor);
		ros_image_right_sensor.data.clear();
		ROS_INFO("Tactile image of right sensor published ... ");
		msg_right_sensor = false;
	}
	
	if(msg_center_sensor) {
		pub_image.publish(ros_image_center_sensor);
		ros_image_center_sensor.data.clear();
		ROS_INFO("Tactile image of center sensor published ... ");
		msg_center_sensor = false;
	}
	
	if(msg_left_sensor) {
		pub_image.publish(ros_image_left_sensor);
		ros_image_left_sensor.data.clear();
		ROS_INFO("Tactile image of left sensor published ... ");
		msg_left_sensor = false;
	}
	
	/// Do this
    spinOnce();
    
    /// Wait until it is time for another interaction
    loop_rate.sleep();
    			
   }
return 0;	

}
