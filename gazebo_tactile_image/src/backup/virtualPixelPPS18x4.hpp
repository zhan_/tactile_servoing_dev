/**
 * @author Galo MALDONADO, galo_xav@hotmail.com
 * @date 2014-04-16
 * @file virtualPixelPPS6x4.hpp
 * 
* Copyright Handle Project  
* Version: 1.1
* 
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*  
 * @brief : This header return the tactel number of the virtual gazebo contact state
 *          for PPS DigiTacts sensor with a 2D array configuration of 6 rows and 4 columns   
 */

#ifndef VIRTUALPIXELPPS18X4_HPP_
#define VIRTUALPIXELPPS18X4_HPP_
#include <cmath>        												// std::abs
#include "std_msgs/String.h"
using namespace std;


int virtualPixelPPS18x4 (float tactel_pose_x, float tactel_pose_y, int tactel_noRows, int tactel_noCols, float dminTactel_x, float dmaxTactel_x, float dminTactel_y, float dmaxTactel_y, float offset_sensor_x, float offset_sensor_y)
{
	int tactel;
    float dTactels_x=0, dTactels_y=0;  

    dTactels_x = (abs(dminTactel_x) + dmaxTactel_x) / tactel_noCols;	// get distance separation from tactel to tactel in x direction   20  /  4   =  5
	dTactels_y = (abs(dminTactel_y) + dmaxTactel_y) / tactel_noRows; 	// get distance separation from tactel to tactel in y direction   17  /  6   = 2.8                
    
 if(  (tactel_pose_y > dminTactel_y)  && ( tactel_pose_y <= ( dminTactel_y + dTactels_y ) )  ) {										
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel= 0; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 1; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 2; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 3; 
	}
	
	if((tactel_pose_y > (dminTactel_y +   dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 2*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 4; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 5; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 6; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 7; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 2*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 3*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 8; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 9; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 10; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 11; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 3*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 4*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 12; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 13; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 14; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 15; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 4*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 5*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 16; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 17; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 18; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 19; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 5*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 6*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 20; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 21; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 22; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 23; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 6*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 7*dTactels_y))) {				
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 24; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 25; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 26; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 27; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 7*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 8*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel[28] = 28; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 29; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 30; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 31; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 8*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 9*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 32; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 33; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 34; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 35; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 9*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 10*dTactels_y))) {		
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 36; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 37; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 38; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 39; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 10*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 11*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 40; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 41; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 42; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 43; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 11*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 12*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 44; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 45; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 46; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 47; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 12*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 13*dTactels_y))) {			
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 48; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 49; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 50; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 51; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 13*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 14*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 52; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 53; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 54; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel= 55; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 14*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 15*dTactels_y))) {		
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 56; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 57; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 58; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 59; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 15*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 16*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 60; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 61; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 62; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 63; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 16*dTactels_y))  && (tactel_pose_y <= (dminTactel_y + 17*dTactels_y))) {
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 64; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 65; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 66; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 67; 
	}
	
	if((tactel_pose_y > (dminTactel_y + 17*dTactels_y))  && (tactel_pose_y <= dmaxTactel_y)) { 
		if(  ( tactel_pose_x > dminTactel_x )  && ( tactel_pose_x <= ( dminTactel_x + dTactels_x ) )  ) tactel = 68; 
		if(  ( tactel_pose_x > ( dminTactel_x +  dTactels_x ) )  && ( tactel_pose_x <= ( dminTactel_x + 2*dTactels_x ) )  ) tactel = 69; 
		if(  ( tactel_pose_x > ( dminTactel_x + 2*dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x -  dTactels_x ) )  ) tactel = 70; 
		if(  ( tactel_pose_x > ( dmaxTactel_x -  dTactels_x ) )  && ( tactel_pose_x <= ( dmaxTactel_x ) )  ) tactel = 71; 
	}
	return tactel;
}
#endif
