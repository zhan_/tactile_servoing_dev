#include "ros/ros.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Vector3.h"
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
#include <sstream>
#include <iostream>
#include "std_msgs/Float64MultiArray.h"

/// shadow hand
/// boost
#include <boost/algorithm/string.hpp>
#include <string>
#include <math.h>
//#include <cmath>
///
//#include <boost/thread.hpp>
// boost::mutex update_mutex;
/*void callback_ff(const std_msgs::Float64ConstPtr& msg)
{
  update_mutex.lock();
  data[0] = msg->data;
  update_mutex.unlock();
}
*/

// for transformations
#include <geometry_msgs/PointStamped.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>

/// to publish in grey scale values
//#include <opencv2/core/core.hpp>
//#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS
//#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
//#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message

/// These headers will allow us to convert to ros type image
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h>

/// dynamic reconfigure
/// to change the sensitivity param online
/// if we change the stiffness of the sensor then
/// the force values will be different at each sensing sell
#include <dynamic_reconfigure/server.h>
#include <gazebo_tactile_image/gazebo_contact_paramsConfig.h>


/// class that subscribe to corresponding bumper state topic
/// ex. weiss/contacts
/// initiate with topic name to subscribe
/// read yaml file
///
/// for that I need:
/// gazebo msgs
///

/// class that publish grey scale tactile image
/// fill up ros_image format
/// initiate with topic name to publish
///
/// for publishing in the ros_image format we need open_cv


class GazeboTactilePads
{
public:
    GazeboTactilePads(std::string gazebo_bumper_topic_);
    virtual ~GazeboTactilePads();
    std::vector<float> smoothed_matrix;
    int cells_x;
    int cells_y;
    double size_x;
    double size_y;
    double sensitivity;
    double range;
    double filter_length;
    double sample_factor;
    std::string gazebo_bumper_topic;
    ros::NodeHandle nh;
    // put tactile matrix to ros image
    sensor_msgs::Image ros_image;
    bool allow_pub;
    void get_sensor_params();
    std::vector<float> create_empty_force_matrix();
    int get_index(int x_, int y_);
    tf::TransformListener tf_listener;
    geometry_msgs::PointStamped transformPoint(const tf::TransformListener& listener_,
                                                        geometry_msgs::PointStamped& point_in_);
    geometry_msgs::Vector3Stamped transformVector(const tf::TransformListener& listener_,
                                                                     geometry_msgs::Vector3Stamped& vector_in_);
    std::vector<float> update(const gazebo_msgs::ContactsState& msg_,
                                                 std::vector<float>& matrix_);
    std::vector<float> smooth(std::vector<float>& matrix_);
    std::vector<float> time_average(std::vector<float>& matrix_buffer_,
                                                       std::vector<float>& matrix_current_);
    std::vector<float> tactile_matrix();
///was private
    ros::Subscriber sub;
    void callback(const gazebo_msgs::ContactsStateConstPtr& msg_);
    void pubImage();
    ros::Publisher image_publisher;
    std::string frame_name_urdf;
    void cb_dynamic_reconf(gazebo_tactile_image::gazebo_contact_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<gazebo_tactile_image::gazebo_contact_paramsConfig> change_sensitvity_server;
    dynamic_reconfigure::Server<gazebo_tactile_image::gazebo_contact_paramsConfig> :: CallbackType f;
};

GazeboTactilePads::GazeboTactilePads(std::string gazebo_bumper_topic_)
{
    //subscriber to bumper state of Gazebo
    // ex. weiss/contacts
   // nh = *n_;
    //tactile pad
    cells_x = 14;
    cells_y = 6;
    gazebo_bumper_topic = gazebo_bumper_topic_;
    size_x = 0.05;
    size_y = 0.025;
    sensitivity = 350;
    range = 3850;
    filter_length = 5;
    sample_factor = 0;
    smoothed_matrix = create_empty_force_matrix();
    std::cout<<"topic name passed into"<<gazebo_bumper_topic_<<std::endl;
    sub = nh.subscribe(gazebo_bumper_topic_, 2, &GazeboTactilePads::callback, this);
    get_sensor_params();
    image_publisher = nh.advertise<sensor_msgs::Image>("ros_tactile_image", 1);
//    tf::TransformListener tf_listener;

// TODO read from param
    //ros_image.height=cells_x; 						// number of rows
    //ros_image.width=cells_y; 						// number of columns
    //ros_image.step=cells_y; 							// number of columns (6) * number of channels (1) * size of data type(1)
    ros_image.encoding="8UC1";   				    // conversions (mono8: CV_8UC1, grayscale image) and in OpenCv CvMat: "8UC1"
    ros_image.is_bigendian=0;       			    // false default
    //ros_image.header.frame_id = "weiss_palm" ; //frame where is located the sensor must be changed according to topic and sensor
    // now = rospy.get_rostime();   //get ros time

     allow_pub = false;

     f = boost::bind(&GazeboTactilePads::cb_dynamic_reconf, this, _1, _2);
     change_sensitvity_server.setCallback(f);

}
GazeboTactilePads::~GazeboTactilePads()
{}

/// get parameters of the sensor from
/// parameter server
/// file config/tactile_array_conf.yaml
void GazeboTactilePads::get_sensor_params(){
    std::string sensor_name;
    int32_t sensor_count = 0;
    XmlRpc::XmlRpcValue my_list;
    //get tactile sensor's parameters from param server
    // ex. /tactile_arrays_param
    nh.getParam("/tactile_arrays_param", my_list);
    ROS_ASSERT_MSG(my_list.getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array");
    if (gazebo_bumper_topic == "weiss/contacts"){
        sensor_count = 0;
    }
    /*
    if (gazebo_bumper_topic_ == "cea/ffj3"){
        sensor_count = 1;
    }*/
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("name"), "Test name");
    ROS_ASSERT_MSG(my_list[sensor_count]["name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 1st position");
    sensor_name = static_cast<std::string> (my_list[sensor_count]["name"]);
    // accessing array of dimensions
    // accrding to URDF model the number of cells in x, cells_x, must correspond
    // to fram orientation of the sensor. Check in RVIZ the direction of the axis.
    // (x toward tip,  z outward from palm, y towards little finger)
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("dimensions"), "Test dimensions");
    ROS_ASSERT_MSG(my_list[sensor_count]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
    ROS_ASSERT_MSG(my_list[sensor_count]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
    cells_x=static_cast<int> (my_list[sensor_count]["dimensions"][0]["cells_x"]);
    cells_y=static_cast<int> (my_list[sensor_count]["dimensions"][1]["cells_y"]);

    // accessing array of sizes (physical size)
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("size"), "Test size");
    ROS_ASSERT_MSG(my_list[sensor_count]["size"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 3d position");
    ROS_ASSERT_MSG(my_list[sensor_count]["size"][0]["x"].getType() == XmlRpc::XmlRpcValue::TypeDouble, "Test double at 0 position in array");
    size_x=static_cast<double> (my_list[sensor_count]["size"][0]["x"]);
    size_y=static_cast<double> (my_list[sensor_count]["size"][1]["y"]);

    // accessing sensitivity
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("sensitivity"), "Test sensitivity");
    ROS_ASSERT_MSG(my_list[sensor_count]["sensitivity"].getType() == XmlRpc::XmlRpcValue::TypeDouble, "Test double at 4rd sensitivity");
    sensitivity = static_cast<double> (my_list[sensor_count]["sensitivity"]);
    std::cout<<"in get param. sensitivity = "<<sensitivity<<std::endl;

    // accessing range
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("range"), "Test range");
    ROS_ASSERT_MSG(my_list[sensor_count]["range"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 6rd range");
    range = static_cast<int> (my_list[sensor_count]["range"]);

    // accessing filter length
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("filter_length"), "Test filter_length");
    ROS_ASSERT_MSG(my_list[sensor_count]["filter_length"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 6rd range");
    filter_length = static_cast<int> (my_list[sensor_count]["filter_length"]);
    sample_factor = 1.0 / filter_length;

    //fill up ros image
    ros_image.height = cells_x;
    ros_image.width = cells_y;
    ros_image.step = cells_y;
    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("frame_id_header"), "Test frame_id_header");
    ROS_ASSERT_MSG(my_list[sensor_count]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");
    ros_image.header.frame_id = static_cast<std::string> (my_list[sensor_count]["frame_id_header"]);


    ROS_ASSERT_MSG(my_list[sensor_count].hasMember("frame_name_urdf"), "Test frame_name_urdf");
    ROS_ASSERT_MSG(my_list[sensor_count]["frame_name_urdf"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_name_urdf");
    frame_name_urdf = static_cast<std::string> (my_list[sensor_count]["frame_name_urdf"]);

}


/// dynamic reconfigure callback function
/// change the sensitivity from rqt online
/// to adjust the output values of each contact
/// cell
void GazeboTactilePads::cb_dynamic_reconf(gazebo_tactile_image::gazebo_contact_paramsConfig &config, uint32_t level){
//    ROS_INFO("Reconfigure Request:%f", config.sensitivity_gazebo);
    sensitivity = config.sensitivity_gazebo;
    filter_length = config.filter_length_time_gazebo;
    sample_factor = 1.0 / filter_length;
//    ROS_INFO("sensitivity:%f", sensitivity);
}


/// Create a new matrix that contains the current force on each cell. Initialize
/// all cells with zeros.
/// :return: std::vector<float>
std::vector<float> GazeboTactilePads::create_empty_force_matrix(){
    std::vector<float> matrix;
    for (int i=0; i < cells_x * cells_y; ++i){
        matrix.push_back(0.0);
    }
    return matrix;
}

/// Map the two-dimensional coordinate of a tactile sensor
/// to an index in one-dimensional vector
/// : param x:
/// : param y:
/// return: int
int GazeboTactilePads::get_index(int x_, int y_){
    // put the origin in the left  corner
//    std::cout<<"In Get Index"<<std::endl;
//    std::cout<<"input x = "<<x_<<std::endl;
//    std::cout<<"input y = "<<y_<<std::endl;

    int y = cells_y - y_ - 1;
//    std::cout<<" y = "<<y<<std::endl;
    int x = x_;
    if (x_ >= cells_x) x = cells_x - 1;
    if (x_ < 0) x = 0;
//    std::cout<<"input x = "<<x<<std::endl;
    if (y_ >= cells_y) y = cells_y - 1;
    if (y_ < 0) y = 0;
//    std::cout<<"input y = "<<y<<std::endl;
    // at first put y cels
    int num = x * cells_y + y;
//    std::cout<<"cells_x = "<<cells_x<<std::endl;
//    std::cout<<"row =  "<<x*cells_y<<std::endl;
//    std::cout<<"NUM = "<<num<<std::endl;
    return num;
}

///Curent version Gazebo 1.9 contact positions published wrt world frame
/// https://github.com/ros-simulation/gazebo_ros_pkgs/blob/indigo-devel/gazebo_plugins/src/gazebo_ros_bumper.cpp#L95
/// we will use tf transformation
///  1st method transforms positions
/// :params listener_, point_in_: tf::TransformListener,  geometry_msgs::PointStamped
/// :return frame_tactile_contact: geometry_msgs::PointStamped

geometry_msgs::PointStamped GazeboTactilePads::transformPoint(const tf::TransformListener& listener_,
                                                    geometry_msgs::PointStamped& point_in_){
    geometry_msgs::PointStamped frame_tactile_contact;
    //transform to frame "XXX". the name of frame must be read from
    //yaml file. for now it is just weiss
    // in yaml file frame_name_urdf
    listener_.waitForTransform(frame_name_urdf, ros::Time(0), "world",ros::Time(0), "world", ros::Duration(10));

    try{
        listener_.transformPoint(frame_name_urdf, point_in_, frame_tactile_contact);
//        ROS_INFO("tactile_contact_world: (%.4f, %.4f. %.4f) -----> tactile_contact_weiss: (%.4f, %.4f, %.4f) at time %.4f",
//                 point_in_.point.x, point_in_.point.y, point_in_.point.z,
//                 frame_tactile_contact.point.x, frame_tactile_contact.point.y, frame_tactile_contact.point.z, frame_tactile_contact.header.stamp.toSec());
        return frame_tactile_contact;

    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return point_in_;

    }


}

///second method transforms force vectors

geometry_msgs::Vector3Stamped GazeboTactilePads::transformVector(const tf::TransformListener& listener_,
                                                                 geometry_msgs::Vector3Stamped& vector_in_){
    geometry_msgs::Vector3Stamped frame_force;
    //transform to frame "XXX". the name of frame must be read from
    //yaml file. for now it is just weiss
    //listener_.waitForTransform(frame_name_urdf, ros::Time(0), "world",ros::Time(0), "world", ros::Duration(10));

    try{
        listener_.transformVector(frame_name_urdf, vector_in_, frame_force);
//        ROS_INFO("tactile_contact_world: (%.4f, %.4f. %.4f) -----> tactile_contact_weiss: (%.4f, %.4f, %.4f) at time %.4f",
//                 vector_in_.vector.x, vector_in_.vector.y, vector_in_.vector.z,
//                 frame_force.vector.x, frame_force.vector.y, frame_force.vector.z, frame_force.header.stamp.toSec());
        return frame_force;

    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return vector_in_;

    }

}




///	Update the provided matrix with the contact information from the Gazebo
/// simulator.
/// :param msg: gazebo_msgs.msg.ContactsState
/// :param matrix: std::vector<float>
/// :return: std::vector<float>

std::vector<float> GazeboTactilePads::update(const gazebo_msgs::ContactsState& msg_,
                                             std::vector<float>& matrix_){

    std::vector<float> matrix;
    matrix=matrix_;
    ::geometry_msgs::Vector3 pos;
    pos.x=0;
    pos.y=0;
    pos.z=0;
    ::geometry_msgs::Vector3 force;
    force.x=0;
    force.y=0;
    force.z=0;

    geometry_msgs::PointStamped world_tactile_contact;
    world_tactile_contact.header.frame_id = "world";
    geometry_msgs::PointStamped atFrame_tactile_contact;

    geometry_msgs::Vector3Stamped world_force;
    world_force.header.frame_id = "world";
    geometry_msgs::Vector3Stamped atFrame_force;


    ROS_DEBUG("Touch by %s", msg_.header.frame_id.c_str());



    if(msg_.states.size()==0){
//        std::cout<<"no contacts"<<std::endl;

        return matrix;
    }
        else
    {


        // ROS_INFO("Mean force value on Z: %f",msg->states[0].total_wrench.force.z);
        unsigned int nb_wrench = msg_.states[0].wrenches.size();
        unsigned int nb_cont_poss = msg_.states[0].contact_positions.size();
        ROS_DEBUG("nb position %d", nb_cont_poss);
        ROS_DEBUG("nb wrenches %d", nb_wrench);
//        std::cout<<"number of contacts"<<nb_wrench<<std::endl;

        for (unsigned int i=0;i<nb_cont_poss;++i)
        {
            // transform contact points from world frame to sensor frame
            world_tactile_contact.header.stamp = ros::Time();
            world_tactile_contact.point.x = msg_.states[0].contact_positions[i].x;
            world_tactile_contact.point.y = msg_.states[0].contact_positions[i].y;
            world_tactile_contact.point.z = msg_.states[0].contact_positions[i].z;

            atFrame_tactile_contact = transformPoint(tf_listener,world_tactile_contact);
            ///check if the contact is on the same side as the sensor
            if (atFrame_tactile_contact.point.z <= 0.0){
                continue;
            }
            // normalize the contact position along the tactile sensor
            //we assume that the tactile senssing area occupies the complete
            //surface of the sensor, so the sensing size is equal to
            //sensor size
            double normalized_x = (atFrame_tactile_contact.point.x / size_x);
            double normalized_y = (atFrame_tactile_contact.point.y / size_y);
            /// from the normalized coordinate we can now determine the index
            ///of the tactile patch that is activated by the contact
            int x = round(normalized_x * cells_x);
            int y = round(normalized_y * cells_y);


//            std::cout<<"CONTACT Number   "<<i<<std::endl;
//            std::cout<<"size_y ="<<size_y<<std::endl;
//            std::cout<<"contact_position_y "<<atFrame_tactile_contact.point.y<<std::endl;
//            std::cout<<"world_position_y "<<msg_.states[0].contact_positions[i].y<<std::endl;
//            std::cout<<"normalized_y"<<normalized_y<<std::endl;
//            std::cout<<" cells in y dir   "<<cells_y<<std::endl;
//            std::cout<<"normalized_y*cells_y = "<<normalized_y*cells_y<<std::endl;
//            std::cout<<"INTEGER y = "<<y<<std::endl;


//            std::cout<<"size_x ="<<size_x<<std::endl;
//            std::cout<<"contact_position_x "<<atFrame_tactile_contact.point.x<<std::endl;
//            std::cout<<"world_position_x "<<msg_.states[0].contact_positions[i].x<<std::endl;
//            std::cout<<"normalized_x"<<normalized_x<<std::endl;
//            std::cout<<" cells in x dir   "<<cells_x<<std::endl;
//            std::cout<<"normalized_x*cells_x = "<<normalized_x*cells_x<<std::endl;
//            std::cout<<"INTEGER x = "<<x<<std::endl;

            //Transform forces
            world_force.header.stamp = ros::Time();
            world_force.vector.x = msg_.states[0].wrenches[i].force.x;
            world_force.vector.y = msg_.states[0].wrenches[i].force.y;
            world_force.vector.z = msg_.states[0].wrenches[i].force.z;

            atFrame_force = transformVector(tf_listener,world_force);

            force.x = atFrame_force.vector.x;
            force.y = atFrame_force.vector.y;
            force.z = atFrame_force.vector.z;

//            std::cout<<"world force x = "<<msg_.states[0].wrenches[i].force.x <<std::endl;
//            std::cout<<"frame force x = "<<atFrame_force.vector.x <<std::endl;
//            std::cout<<"world force y = "<<msg_.states[0].wrenches[i].force.y <<std::endl;
//            std::cout<<"frame force y = "<<atFrame_force.vector.y <<std::endl;
//            std::cout<<"world force z = "<<msg_.states[0].wrenches[i].force.z <<std::endl;
//            std::cout<<"frame force z = "<<atFrame_force.vector.z <<std::endl;

            /*if (force.y<0) force.y = 0;
            if (force.x<0) force.x = 0;
            if (force.z<0) force.z = 0;*/


            double friction_mu = 0.01; // in future should be loaded from param server gazebo/contact properties
            //float tt_force = (float)sqrt(pow(force.x, 2) + pow(force.y, 2) + pow(force.z, 2));

            double tt_force_z = std::abs(force.z) + std::abs(friction_mu*force.x) + std::abs(friction_mu*force.y);
//            std::cout<<"tt_force_at z = "<< tt_force_z <<std::endl;

            /// request the sensitivity value from dynamic reconfigure
            ///
//            change_sensitvity_server.setCallback(f);
            tt_force_z = tt_force_z*sensitivity*2;
//            std::cout<<"tt_force_at z = "<< tt_force_z <<std::endl;



            int index = int(get_index(x, y));
//           std::cout<<" INDEX   = "<<index<<std::endl;

            matrix[index] = +tt_force_z;
//            std::cout<<"force at contact"<<i<<"is equal ="<<matrix[index]<<std::endl;


        }

    }

    return matrix;
}

/// Run a moving average on the provided matrix.
/// :param matrix_: std::vector<float>
/// :return matrix: std::vector<float>
std::vector<float> GazeboTactilePads::smooth(std::vector<float>& matrix_){
   std::vector<float> internal_smoothed_matrix;

   internal_smoothed_matrix = create_empty_force_matrix();
//   for (int i = 0; i < internal_smoothed_matrix.size(); ++i){
//       std::cout<<"iteration number"<<i<<std::endl;

//       std::cout<<" internal_smoothed_matrix = "<<internal_smoothed_matrix[i]<<std::endl;

//   }


   for (int ind_x = 0;  ind_x < cells_x; ++ind_x){
       for (int ind_y = 0;  ind_y < cells_y; ++ind_y){
           double sum = 0;
           int count = 0;
           for (int dx = -1;  dx < 2; ++dx){
               for (int dy = -1;  dy < 2; ++dy){
                   int index_x = ind_x + dx;
                   int index_y = ind_y + dy;
//                   std::cout<<"x "<<ind_x<<std::endl;
//                   std::cout<<"y "<<ind_y<<std::endl;

//                   std::cout<<"dx "<<dx<<std::endl;
//                   std::cout<<"dy "<<dy<<std::endl;

//                   std::cout<<"index_y "<<index_y<<std::endl;
//                   std::cout<<"index_x "<<index_x<<std::endl;


                   if ((index_x < 0) || (index_x >= cells_x)) {continue;}
                   if ((index_y < 0) || (index_y >= cells_y)) {continue;}
                   int index = get_index(index_x, index_y);
                   sum = sum + matrix_[index];
                   count = count + 1;
               }
           }
           int index = get_index(ind_x,ind_y);
            internal_smoothed_matrix[index] = sum / count;
       }
   }
   return  internal_smoothed_matrix;
}

/// calculate the avarage matrix from the force buffer
/// :param matrix_buffer_: std::vector<float>
/// :param matrix_current_: std::vector<float>
/// :param filter_length_: int
/// : return matrix: std::vector<float>
std::vector<float> GazeboTactilePads::time_average(std::vector<float>& matrix_buffer_,
                                                   std::vector<float>& matrix_current_){
    std::vector<float> matrix;
    matrix = create_empty_force_matrix();

    for (int i=0; i < cells_x * cells_y; ++i){
        float tt_force = (1.0 - sample_factor) * matrix_buffer_[i];
        tt_force = tt_force + sample_factor * matrix_current_[i];
        matrix[i] = tt_force;

    }

    return matrix;
}

/// Update the pad with the Gazebo contact information.
/// :param msg_: gazebo_msgs.msg.ContactsState
void GazeboTactilePads::callback(const gazebo_msgs::ContactsStateConstPtr &msg_)
{
//    double begin_calc_time = ros::Time::now().toSec();
//    ROS_INFO("in cb %f\n", begin_calc_time );

    gazebo_msgs::ContactsState msg;
    msg = *msg_;

//    std::cout<<"in callback"<<std::endl;

    std::vector<float> matrix;

    matrix = create_empty_force_matrix();
//    std::cout<<"Just Created Matrix"<<std::endl;
//    std::cout<<"initialized with size"<<matrix.size()<<std::endl;

//    for (int i = 0; i < matrix.size(); ++i){
//        std::cout<<"iteration number"<<i<<std::endl;

//        std::cout<<" empty matrix"<<matrix[i]<<std::endl;

//    }

    matrix = update(msg, matrix);
//    std::cout<<"after update with size"<<matrix.size()<<std::endl;

//    for (int i = 0; i < matrix.size(); ++i){
//        std::cout<<"iteration number"<<i<<std::endl;

//        std::cout<<"filled matrix"<<matrix[i]<<std::endl;

//    }
//    double end1_calc_time = ros::Time::now().toSec();
//    ROS_INFO("in cb intermediate  %f\n", end1_calc_time );
//    ROS_INFO("in cb intermediate diff %f\n", end1_calc_time - begin_calc_time);

     matrix = smooth(matrix);
//    std::cout<<"after smooth with size"<<matrix.size()<<std::endl;
//    for (int i = 0; i < matrix.size(); ++i){
//        std::cout<<"iteration number"<<i<<std::endl;

//        std::cout<<"smoothed matrix"<<matrix[i]<<std::endl;

//    }


    matrix = time_average(smoothed_matrix, matrix);
//    std::cout<<"after average with size"<<matrix.size()<<std::endl;

    /*for (int i = 0; i < matrix.size(); ++i){
        std::cout<<"iteration number"<<i<<std::endl;

        std::cout<<"filtered"<<matrix[i]<<std::endl;

    }*/


    smoothed_matrix = matrix;
    /*std::cout<<"the smoothed matr with size"<<smoothed_matrix.size()<<std::endl;
    for (int i = 0; i < smoothed_matrix.size(); ++i){
        std::cout<<"iteration number"<<i<<std::endl;

        std::cout<<"here is the vector"<<smoothed_matrix[i]<<std::endl;

    }*/
    ros_image.header.stamp = ros::Time::now(); // time
    if(!allow_pub){
        for (int i = 0; i< smoothed_matrix.size(); ++i){
            double temp = 255 - smoothed_matrix[i];
            if (temp < 0) temp = 0;
            ros_image.data.push_back(temp);
            //        std::cout<<"in old matrix "<<smoothed_matrix[i]<<std::endl;
        }

        //    std::cout<<"size of image"<<ros_image.data.size()<<std::endl;
        allow_pub = true;
    }
//    double end_calc_time = ros::Time::now().toSec();
//    ROS_INFO("in cb %f\n", end_calc_time );
//    ROS_INFO("in cb diff %f\n", end_calc_time - begin_calc_time);
}


/// return tactile matrix
/// for use in publisher
std::vector<float> GazeboTactilePads::tactile_matrix()
{

    std::vector<float> matrix_to_pub = smoothed_matrix;
//    std::cout<<"here is the vector with size"<<matrix_to_pub.size()<<std::endl;
//    std::cout<<"here is the vector2 with size"<<smoothed_matrix.size()<<std::endl;
//    std::cout<<"smoothed matrix at 20"<<smoothed_matrix[20]<<std::endl;


    for (int i = 0; i < matrix_to_pub.size(); ++i){
//        std::cout<<"iteration number"<<i<<std::endl;

//        std::cout<<"here is the vector"<<matrix_to_pub.at(i)<<std::endl;

    }
    return smoothed_matrix;
}


void GazeboTactilePads::pubImage(){
    if (allow_pub){
        image_publisher.publish(ros_image);

        ros_image.data.clear();
        allow_pub = false;
    }

}


int main(int argc, char **argv)
{
    ROS_INFO("GAZEBO TACTILE IMAGE NODE");

    ros::init(argc, argv, "gazebo_weiss_image"); // create my new node. only one node

    ros::NodeHandle n;

    GazeboTactilePads weiss("/contacts/weiss");

    //pub_tactile_image = n.advertise<sensor_msgs::Image>("ros_tactile_image", 10);

    // std::vector<float> matrix_to_pub = weiss.tactile_matrix();


    ros::Rate loop_rate(500);
    while (ros::ok()){
        weiss.pubImage();
        ros::spinOnce();
        loop_rate.sleep();

    }


    return 0;
}

