#include <ros/ros.h>
#include <math.h>
#include <string>
#include <sstream>

#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/Image_weiss.h>

#include <visualization_msgs/Marker.h>

// for transformations
#include <geometry_msgs/PointStamped.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <iostream>



class transformFeatures
{
public:
    transformFeatures(int tactile_sensor_num_);
    virtual ~transformFeatures();
    ros::NodeHandle nh_;



    // subscribe to the feature publishe
    ros::Subscriber sub_contr_features;
    ros::Subscriber sub_des_features;
    ros::Subscriber sub_plan_features;

    //subscribe to control topic
    void cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_feature_p(const tactile_servo_msgs::featuresPlanningConstPtr& msg_);

    // for RVIZ
    ros::Publisher vis_pub_center;
    ros::Publisher vis_pub_des_orient_point;
    ros::Publisher vis_pub_fb_orient_point;
    ros::Publisher vis_pub_fb_orient_arrow;
    ros::Publisher vis_pub_des_orient_arrow;


    float orientation_in_deg_to_pub_rviz;

    // sensor parameters
    int cells_x;
    int cells_y;
    double size_x;
    double size_y;

    // string names topic to sub
    std::string topic_name_sub_c;
    std::string topic_name_sub_p;
    std::string topic_name_sub_d;


    geometry_msgs::PointStamped    transformPoint(const tf::TransformListener& listener,
                                                  geometry_msgs::PointStamped& point_in_);
    visualization_msgs::Marker    create_marker(geometry_msgs::PointStamped& point_to_viz_,
                                                std::string frame_name_);
    visualization_msgs::Marker create_marker_arrow(geometry_msgs::PointStamped& point_to_viz_,
                                                   double& orient_,
                                                   std::string frame_name_, float color_);

    void    rviz_marker();

    double feedback_orient_feature;
    double des_orient_feature;
    double fb_contact_senter_x;
    double fb_contact_senter_y;

    //we need to get feature' coordinates wrt world frame
    tf::TransformListener tf_listener;

};

transformFeatures::~transformFeatures(){}

transformFeatures::transformFeatures(int tactile_sensor_num_){
    // vizualize in RVIZ

    std::string tactile_sensor_num_str;
    std::stringstream out;
    out << tactile_sensor_num_;
    tactile_sensor_num_str = out.str();
    vis_pub_center = nh_.advertise<visualization_msgs::Marker>( "visualization_marker_center"+tactile_sensor_num_str, 0 );
    vis_pub_des_orient_point = nh_.advertise<visualization_msgs::Marker>( "visualization_marker_des_orient_point"+tactile_sensor_num_str, 0 );
    vis_pub_fb_orient_point = nh_.advertise<visualization_msgs::Marker>( "visualization_marker_fb_orient_point"+tactile_sensor_num_str, 0 );
    vis_pub_des_orient_arrow = nh_.advertise<visualization_msgs::Marker>("vis_marker_arrow_des"+tactile_sensor_num_str,0);
    vis_pub_fb_orient_arrow = nh_.advertise<visualization_msgs::Marker>("vis_marker_arrow_fb"+tactile_sensor_num_str,0);

    //read from param server
    int32_t sensor_count = tactile_sensor_num_;
    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);
    ROS_ASSERT_MSG(my_list.getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array");


    cells_x=static_cast<int> (my_list[sensor_count]["dimensions"][0]["cells_x"]);
    cells_y=static_cast<int> (my_list[sensor_count]["dimensions"][1]["cells_y"]);

    size_x=static_cast<double> (my_list[sensor_count]["size"][0]["x"]);
    size_y=static_cast<double> (my_list[sensor_count]["size"][1]["y"]);

    topic_name_sub_c = static_cast<std::string> (my_list[tactile_sensor_num_]["control_topic_name"]);
    topic_name_sub_p = static_cast<std::string> (my_list[tactile_sensor_num_]["planning_topic_name"]);
    topic_name_sub_d = static_cast<std::string> (my_list[tactile_sensor_num_]["desired_topic_name"]);


    sub_contr_features = nh_.subscribe("/"+topic_name_sub_c, 1, &transformFeatures::cb_feature_c, this);
    sub_des_features = nh_.subscribe("/"+topic_name_sub_d, 1, &transformFeatures::cb_feature_d, this);
    sub_plan_features = nh_.subscribe("/"+topic_name_sub_p, 1,  &transformFeatures::cb_feature_p,this);


}
//create marker only point not pose
visualization_msgs::Marker transformFeatures::create_marker(geometry_msgs::PointStamped& point_to_viz_,
                                                            std::string frame_name_){
    // draw in RVIZ
    u_int32_t shape = visualization_msgs::Marker::ARROW;
    shape = visualization_msgs::Marker::SPHERE;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/"+frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "orientations";
    marker.id = 0;
    marker.type = shape;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = point_to_viz_.point.x;
    marker.pose.position.y = point_to_viz_.point.y;
    marker.pose.position.z = point_to_viz_.point.z;

    marker.pose.orientation.x = 0.0;
    marker.pose.orientation.y = 0.0;
    marker.pose.orientation.z = 0.0;
    marker.pose.orientation.w = 1.0;
    marker.scale.x = 0.01;
    marker.scale.y = 0.01;
    marker.scale.z = 0.01;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = 0.0;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

    return marker;
}

//create marker pose
// set position
// use transform2 to transform orientation in
// weiss frame to world
visualization_msgs::Marker transformFeatures::create_marker_arrow(geometry_msgs::PointStamped& point_to_viz_, double& orient_,
                    std::string frame_name_, float color_){
    // draw in RVIZ
    u_int32_t shape = visualization_msgs::Marker::ARROW;
//    shape = visualization_msgs::Marker::SPHERE;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/"+frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "orientations";
    marker.id = 0;
    marker.type = shape;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = point_to_viz_.point.x;
    marker.pose.position.y = point_to_viz_.point.y;
    marker.pose.position.z = point_to_viz_.point.z;



    tf::StampedTransform transform2;
    //from world to weiss
//    listener_.waitForTransform("world", ros::Time(0), "weiss",ros::Time(0), "world", ros::Duration(10));

    tf_listener.waitForTransform("/weiss", "/world", ros::Time::now(), ros::Duration(3.0));

    try{
        tf_listener.lookupTransform("/weiss", "/world",
                                 ros::Time(0), transform2);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
    std::cout<<"transformation frame name = "<<transform2.frame_id_<<std::endl;
    std::cout<<"transformation frame name = "<<transform2.child_frame_id_<<std::endl;
    //transform.operator ()();
    //std::cout<<transform.getBasis()<<std::endl;

    std::cout<<" getX() = "<<transform2.getRotation().getX()<<std::endl;
    std::cout<<" getY() = "<<transform2.getRotation().getY()<<std::endl;
    std::cout<<" getZ() = "<<transform2.getRotation().getZ()<<std::endl;
    std::cout<<" getW() = "<<transform2.getRotation().getW()<<std::endl;

    tf::Transform transform;
    transform.setOrigin( tf::Vector3(point_to_viz_.point.x, point_to_viz_.point.y, point_to_viz_.point.z) );
    tf::Quaternion q;
    q.setRPY(transform.getRotation().getX(), transform.getRotation().getY()+(1.57-orient_), transform.getRotation().getZ());
    transform.setRotation(q);
    marker.pose.orientation.x = q.getX();
    marker.pose.orientation.y = q.getY();
    marker.pose.orientation.z = q.getZ();
    marker.pose.orientation.w = q.getW();
    marker.scale.x = 0.1;
    marker.scale.y = 0.001;
    marker.scale.z = 0.001;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = 1.0;
    marker.color.g = color_;
    marker.color.b = 0.0;

    return marker;


}
// transform point
geometry_msgs::PointStamped transformFeatures::transformPoint(const tf::TransformListener& listener_,
                                                    geometry_msgs::PointStamped& point_in_){
    geometry_msgs::PointStamped point_out;
    //transform to frame "XXX". the name of frame must be read from
    listener_.waitForTransform("world", ros::Time(0), "weiss",ros::Time(0), "world", ros::Duration(10));

    try{
        listener_.transformPoint("world", point_in_, point_out);
        ROS_INFO("at weiss: (%.4f, %.4f. %.4f) -----> at world: (%.4f, %.4f, %.4f) at time %.4f",
                 point_in_.point.x, point_in_.point.y, point_in_.point.z,
                 point_out.point.x, point_out.point.y, point_out.point.z, point_out.header.stamp.toSec());
        return point_out;
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return point_in_;
    }
}

// show in RVIZ
// 1. create point in weiss
// 2. transformPoint to world
// 3. create marker from transformed point

void transformFeatures::rviz_marker(){
    ros::spinOnce();
    // show in RVIZ contact center
    geometry_msgs::PointStamped frame_tactile_contact_center;
    frame_tactile_contact_center.header.frame_id = "weiss";
    geometry_msgs::PointStamped at_world_tactile_contact_center;
    frame_tactile_contact_center.header.stamp = ros::Time();
    frame_tactile_contact_center.point.x = fb_contact_senter_x;
    frame_tactile_contact_center.point.y = fb_contact_senter_y;
    std::cout<<"COC wrt weiss x = "<< frame_tactile_contact_center.point.x <<std::endl;
    std::cout<<"COC wrt weiss y = "<< frame_tactile_contact_center.point.y  <<std::endl;


    frame_tactile_contact_center.point.z = 0.02; //tactile sensor's thickness
    at_world_tactile_contact_center = transformPoint(tf_listener,frame_tactile_contact_center);

    // show in RVIZ desired orientation
    float hypotenus = 0.1;
    float temp_des_orient_end_point_y = hypotenus*(std::cos(des_orient_feature));
    float temp_des_orient_end_point_x = hypotenus*(std::sin(des_orient_feature));
    geometry_msgs::PointStamped frame_tactile_contact_des_orient;
    frame_tactile_contact_des_orient.header.frame_id = "weiss";
    geometry_msgs::PointStamped at_world_tactile_contact_des_orient;
    frame_tactile_contact_des_orient.header.stamp = ros::Time();
    frame_tactile_contact_des_orient.point.x = frame_tactile_contact_center.point.x + temp_des_orient_end_point_x;
    frame_tactile_contact_des_orient.point.y = frame_tactile_contact_center.point.y + temp_des_orient_end_point_y;
    frame_tactile_contact_des_orient.point.z = 0.02; //tactile sensor's thickness

    at_world_tactile_contact_des_orient = transformPoint(tf_listener,frame_tactile_contact_des_orient);

    // show in RVIZ actual orientation
    // find points regarding the orientation
    float temp_fb_orient_end_point_y =hypotenus*(std::cos(feedback_orient_feature));
    float temp_fb_orient_end_point_x =hypotenus*(std::sin(feedback_orient_feature));

    geometry_msgs::PointStamped frame_tactile_contact_fb_orient;
    frame_tactile_contact_fb_orient.header.frame_id = "weiss";
    geometry_msgs::PointStamped at_world_tactile_contact_fb_orient;
    frame_tactile_contact_fb_orient.header.stamp = ros::Time();
    frame_tactile_contact_fb_orient.point.x = frame_tactile_contact_center.point.x+temp_fb_orient_end_point_x;
    frame_tactile_contact_fb_orient.point.y = frame_tactile_contact_center.point.y+temp_fb_orient_end_point_y;
    frame_tactile_contact_fb_orient.point.z = 0.02; //tactile sensor's thickness

    at_world_tactile_contact_fb_orient = transformPoint(tf_listener,frame_tactile_contact_fb_orient);


    visualization_msgs::Marker marker_center = create_marker(at_world_tactile_contact_center, "world");
    visualization_msgs::Marker marker_des_orient_end = create_marker(at_world_tactile_contact_des_orient, "world");
    visualization_msgs::Marker marker_fb_orient_end = create_marker(at_world_tactile_contact_fb_orient, "world");
    visualization_msgs::Marker marker_des_orient_arrow = create_marker_arrow(at_world_tactile_contact_center, des_orient_feature, "world", 0.0);
    visualization_msgs::Marker marker_fb_orient_arrow = create_marker_arrow(at_world_tactile_contact_center, feedback_orient_feature, "world", 1.0);


    vis_pub_center.publish( marker_center );
    vis_pub_des_orient_point.publish (marker_des_orient_end);
    vis_pub_fb_orient_point.publish (marker_fb_orient_end);
    vis_pub_des_orient_arrow.publish (marker_des_orient_arrow);
    vis_pub_fb_orient_arrow.publish (marker_fb_orient_arrow);



}
// call back from Control features to fill fb feats
void transformFeatures::cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_){
    if (msg_->contactOrientation.size() > 0){
        std::cout<<"feedback orientation msg = "<< msg_->contactOrientation.at(0)  <<std::endl;
        // in cv_feature extractor I get angle between y-axes of the sensor and biggest eigenevector of
        // contact point distribution
        // but this
        feedback_orient_feature =  msg_->contactOrientation.at(0)* M_PI / 180;
        if (msg_->contactOrientation.at(0) == 90) feedback_orient_feature = feedback_orient_feature+0.001;
        std::cout<<"feedback orientation = "<< feedback_orient_feature * 180 / M_PI  <<std::endl;
    }
    else{
        std::cout<<" empty fb orient"<<std::endl;
    }
    if (msg_->centerContact_x.size() > 0){
        fb_contact_senter_y =size_y - msg_->centerContact_x.at(0) * size_y / cells_y;
        std::cout<<"fb_contact_senter_x = "<< fb_contact_senter_x <<std::endl;

    }
    if (msg_->centerContact_y.size() > 0){
        fb_contact_senter_x = size_x -  msg_->centerContact_y.at(0) * size_x / cells_x;

        std::cout<<"fb_contact_senter_y = "<< fb_contact_senter_y <<std::endl;
    }
    return;
}
// call back from Control features to fill des feats
void transformFeatures::cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_){
    if(msg_->contactOrientation.size() > 0){
        des_orient_feature = msg_->contactOrientation.at(0) * M_PI / 180;
        std::cout<<"desired orientation = "<< des_orient_feature * 180 / M_PI <<std::endl;
    }
    else{
        std::cout<<" empty desired"<<std::endl;
    }
    return;
}

void transformFeatures::cb_feature_p(const tactile_servo_msgs::featuresPlanningConstPtr& msg_){
    if(msg_->contactsNo > 1){
        //todo
    }
    else{
        //todo
    }
    return;
}


int main(int argc, char** argv)
{
    //init the ros node
    ros::init(argc, argv, "feature_frame_transform");
    ros::NodeHandle n;
    ROS_INFO("features in world frame pub rviz started ");

    transformFeatures transform_weiss_features(0);


    ros::Rate loop_rate(10);

    while( ros::ok() ){
        transform_weiss_features.rviz_marker();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}


//ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker_smth", 1);

//        u_int32_t shape = visualization_msgs::Marker::ARROW;
//        shape = visualization_msgs::Marker::SPHERE;
//        visualization_msgs::Marker marker;
//        marker.header.frame_id = "/world";
//        marker.header.stamp = ros::Time();
//        marker.ns = "orientations";
//        marker.id = 0;
//        marker.type = shape;
//        marker.action = visualization_msgs::Marker::ADD;
//        marker.pose.position.x = 0;
//        marker.pose.position.y = 0;
//        marker.pose.position.z = 0;

//        marker.pose.orientation.x = 0.0;
//        marker.pose.orientation.y = 0.0;
//        marker.pose.orientation.z = 0.0;
//        marker.pose.orientation.w = 1.0;
//        marker.scale.x = 1;
//        marker.scale.y = 1;
//        marker.scale.z = 1;
//        marker.color.a = 1.0; // Don't forget to set the alpha!
//        marker.color.r = 0.0;
//        marker.color.g = 1.0;
//        marker.color.b = 0.0;

//        marker_pub.publish (marker);


