#include <ros/ros.h>
#include <tf/tf.h>
// to use transfrom listener
#include <tf/transform_listener.h>

#include <sstream>
#include <string>
#include <math.h>

#include <geometry_msgs/PointStamped.h>
#include <tf/transform_listener.h>


bool got_transformed_point;

/// transform vector

void transformVector(const tf::TransformListener& listener_){
    geometry_msgs::Vector3Stamped force;
    force.header.frame_id = "world";
    force.header.stamp = ros::Time();
    force.vector.x = 0.00;
    force.vector.y = 1.00;
    force.vector.z = 0.00;
    try{
        geometry_msgs::Vector3Stamped force_weiss;
        listener_.transformVector("weiss", force,force_weiss);
        ROS_INFO("force_world: (%.2f, %.2f. %.2f) -----> force_weiss: (%.2f, %.2f, %.2f) at time %.2f",
                 force.vector.x, force.vector.y, force.vector.z,
                 force_weiss.vector.x, force_weiss.vector.y, force_weiss.vector.z, force_weiss.header.stamp.toSec());

    }
    catch(tf::TransformException& ex){
        ROS_ERROR("VECTOR Received an exception trying to transform a vector from \"/world\" to \"/weiss\": %s", ex.what());
    }




}

///transform point

void transformPoint(const tf::TransformListener& listener){
    //we'll create a point in the base_laser frame that we'd like to transform to the base_link frame
    geometry_msgs::PointStamped tactile_contact_world;
    tactile_contact_world.header.frame_id = "world";
    //we'll just use the most recent transform available for our simple example
    tactile_contact_world.header.stamp = ros::Time();
    //just an arbitrary point in space
    tactile_contact_world.point.x = 0.00;
    tactile_contact_world.point.y = 1;
    tactile_contact_world.point.z = 0.00;
    // try works: if it works it works, if smth wrong inside try it will go to catch like if else
    try{
        geometry_msgs::PointStamped tactile_contact_weiss;
        //arguments: the name of the frame we want to transform the point to ("weiss"),
        //the point we're transforming 'world', and storage for the transformed point 'weiss'.
        listener.transformPoint("weiss", tactile_contact_world, tactile_contact_weiss);
        //listener.transformVector
        //ros::Time ts = ros::Time().now();
        //listener.canTransform("weiss", "world",ts, got_transf);
        //std::cout<<"istener.canTransform()"<<got_transf<<std::endl;

        ROS_INFO("tactile_contact_world: (%.2f, %.2f. %.2f) -----> tactile_contact_weiss: (%.2f, %.2f, %.2f) at time %.2f",
                 tactile_contact_world.point.x, tactile_contact_world.point.y, tactile_contact_world.point.z,
                 tactile_contact_weiss.point.x, tactile_contact_weiss.point.y, tactile_contact_weiss.point.z, tactile_contact_weiss.header.stamp.toSec());
        if(tactile_contact_weiss.point.y){got_transformed_point = true; std::cout<<"tactile_contact_weiss.point.y bool ="<<got_transformed_point<<std::endl;}
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
    }

    std::cout<<"got_transformed_point"<<got_transformed_point<<std::endl;
}

int main(int argc, char** argv){
    ros::init(argc, argv, "my_tf_listener");
    ros::NodeHandle node;

    tf::TransformListener listener;
    tf::TransformListener testTrans;

//    //we'll create a point in the base_laser frame that we'd like to transform to the base_link frame
//    geometry_msgs::PointStamped tactile_contact_world;

//    //geometry_msgs::PointStamped tactile_contact_weiss;


//    tactile_contact_world.header.frame_id = "/world";
//    //we'll just use the most recent transform available for our simple example
//    tactile_contact_world.header.stamp = ros::Time();
//    //just an arbitrary point in space
//    tactile_contact_world.point.x = 0.0;
//    tactile_contact_world.point.y = 1;
//    tactile_contact_world.point.z = 0.0;

//    try{
//        geometry_msgs::PointStamped tactile_contact_weiss;
//        //arguments: the name of the frame we want to transform the point to ("weiss"),
//        //the point we're transforming 'world', and storage for the transformed point 'weiss'.
//        testTrans.transformPoint("weiss", tactile_contact_world, tactile_contact_weiss);

//        ROS_INFO("tactile_contact_world: (%.2f, %.2f. %.2f) -----> tactile_contact_weiss: (%.2f, %.2f, %.2f) at time %.2f",
//                 tactile_contact_world.point.x, tactile_contact_world.point.y, tactile_contact_world.point.z,
//                 tactile_contact_weiss.point.x, tactile_contact_weiss.point.y, tactile_contact_weiss.point.z, tactile_contact_weiss.header.stamp.toSec());
//    }
//    catch(tf::TransformException& ex){
//        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
//    }

    ros::Rate rate(10.0);
    while (node.ok()){
        tf::StampedTransform transform;
        //from world to weiss
        try{
            listener.lookupTransform("/world", "/weiss",
                                     ros::Time(0), transform);
        }
        catch (tf::TransformException ex){
            ROS_ERROR("%s",ex.what());
            ros::Duration(1.0).sleep();
        }
        std::cout<<"transformation frame name = "<<transform.frame_id_<<std::endl;
        std::cout<<"transformation frame name = "<<transform.child_frame_id_<<std::endl;
        //transform.operator ()();
        //std::cout<<transform.getBasis()<<std::endl;
        std::cout<<transform.getOrigin().getX()<<std::endl;
        //         tf::Stamped<tf::Point> varible; varible =  transform.getOrigin();
        std::cout<<transform.getOrigin().getY()<<std::endl;
        transform.getRotation().getAngle();


        std::cout<<"got_transformed_point"<<got_transformed_point<<std::endl;


        transformPoint(testTrans);
        transformVector(testTrans);

        //transform point

//        try{
//            geometry_msgs::PointStamped tactile_contact_weiss;
//            //arguments: the name of the frame we want to transform the point to ("weiss"),
//            //the point we're transforming 'world', and storage for the transformed point 'weiss'.
//            testTrans.transformPoint("palm", tactile_contact_world, tactile_contact_weiss);

//            ROS_INFO("tactile_contact_world: (%.2f, %.2f. %.2f) -----> tactile_contact_weiss: (%.2f, %.2f, %.2f) at time %.2f",
//                     tactile_contact_world.point.x, tactile_contact_world.point.y, tactile_contact_world.point.z,
//                     tactile_contact_weiss.point.x, tactile_contact_weiss.point.y, tactile_contact_weiss.point.z, tactile_contact_weiss.header.stamp.toSec());
//        }
//        catch(tf::TransformException& ex){
//            ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
//        }


        rate.sleep();
    }

    return 0;
}
