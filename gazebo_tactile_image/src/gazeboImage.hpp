#ifndef GAZEBOIMAGE_HPP_
#define GAZEBOIMAGE_HPP_

#include "virtualPixelPPS6x4.hpp"
#include "ros/ros.h"
#include <ros/console.h>
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"

#include <iostream>
#include <string>
#include <vector>
#include <stdio.h>

/// These headers will allow us to convert to ros type image
#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h> 

/// These are the gazebo headers
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_msgs/ContactState.h"
#include <gazebo_msgs/LinkStates.h>
#include <gazebo_msgs/LinkState.h>
#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

#include "sensor_msgs/JointState.h"

using namespace std;
using namespace gazebo_msgs;
using namespace ros;

/// Variables of position for matching tactile image 
double x; double y; double z;

/// Variables to store forces 
double fz;         				                                        //the normal forces 

/// Create globally messages to be published
sensor_msgs::Image ros_image_center_sensor;
sensor_msgs::Image ros_image_left_sensor;
sensor_msgs::Image ros_image_right_sensor;

/// Store contact posoition with respect to tactile sensor 
float tactel_pose_x, tactel_pose_y, tactel_pose_z ;

float offset_x_center_sensor, offset_y_center_sensor, offset_z_center_sensor;   
float offset_x_right_sensor, offset_y_right_sensor, offset_z_right_sensor; 
float offset_x_left_sensor, offset_y_left_sensor, offset_z_left_sensor; 
float x_center_sensor, y_center_sensor, z_center_sensor; 
float x_left_sensor, y_left_sensor, z_left_sensor; 
float x_right_sensor, y_right_sensor, z_right_sensor; 
float x_center_frame, y_center_frame, z_center_frame;

/// To fill Ros Image message  
string frame;  			  			    
string collision1;  		   
string collision2;  		    
ros::Time ts;							   
uint32_t seq;	

/// flag for allow publish just when detect contact
unsigned int counter;

#endif
