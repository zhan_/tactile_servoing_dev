/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */

#include "contact_points/contact_points_kuka.hpp"
#include <ros/ros.h>


ContactPointsKuks::ContactPointsKuks( int sensors_number):it(nh)
{
  get_sensor_params(sensors_number);
  //   pub_contacts = nh.advertise<PointCloud> (contact_points_topic_name_, 300);
  pub_contacts_color = nh.advertise<PointCloudColor> (contact_points_topic_name_, 300);
  pub_contact_positions_plot = nh.advertise<geometry_msgs::PointStamped>(contact_points_topic_name_to_record_, 10);
  
  image_sub = it.subscribe("/"+tact_img_top_name_, 10,
			   &ContactPointsKuks::image_cb, this);
  
  
}

ContactPointsKuks::~ContactPointsKuks(){}

void ContactPointsKuks::get_sensor_params(int& tactile_sensor_num){
    /*
    if (gazebo_bumper_topic_ == "cea/ffj3"){
        sensor_count = 1;
    }*/
    std::string sensor_name;
    XmlRpc::XmlRpcValue my_list;
    //get tactile sensor's parameters from param server
    nh.getParam("/tactile_arrays_param", my_list);
    
    ckeck_param_fields(my_list, tactile_sensor_num);
    
    tact_img_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_image_topic_name"]);
    tact_img_top_name_12bit_ = static_cast<std::string> (my_list[tactile_sensor_num]["tactile_pressure_topic_name"]);

    // get topic names of control features for a given sensor
    fb_feats_control_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_control_topic_name"]);
    fb_feats_plan_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["fb_feats_planning_topic_name"]);
    des_feats_top_name_ = static_cast<std::string> (my_list[tactile_sensor_num]["des_feats_topic_name"]);
   
    rows_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][0]["cells_x"]);
    cols_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
    step_ = static_cast<int> (my_list[tactile_sensor_num]["dimensions"][1]["cells_y"]);
    size_x_=static_cast<double> (my_list[tactile_sensor_num]["size"][0]["x"]);
    size_y_=static_cast<double> (my_list[tactile_sensor_num]["size"][1]["y"]);
    sensor_frame_name_urdf_ =  static_cast<std::string> (my_list[tactile_sensor_num]["frame_name_urdf"]);
    tactel_area_ = static_cast<double> (my_list[tactile_sensor_num]["tactel_area"]);
    contact_points_topic_name_ =  static_cast<std::string> (my_list[tactile_sensor_num]["contact_points_topic_name"]);
    hight_of_sensor_ = static_cast<double> (my_list[tactile_sensor_num]["hight_of_sensor"]);
    threshold_noise_ = static_cast<int> (my_list[tactile_sensor_num]["threshold_noise"]);
    compliance_ = static_cast<double> (my_list[tactile_sensor_num]["compliance"]);
    base_frame_name_urdf_ =  static_cast<std::string> (my_list[tactile_sensor_num]["base_frame_name_urdf"]);
    contact_points_topic_name_to_record_ = static_cast<std::string> (my_list[tactile_sensor_num]["contact_points_topic_name_to_record"]);

}

void ContactPointsKuks::ckeck_param_fields(XmlRpc::XmlRpcValue& xmlrpclist, int& tactile_sensor_num)
{
  ROS_ASSERT_MSG(xmlrpclist.getType() == XmlRpc::XmlRpcValue::TypeArray, "check param /tactile_arrays_param");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "control_topic_name string  ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["fb_feats_planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test planning_topic_name ");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["des_feats_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "desired_topic_name");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("dimensions"), "Test dimensions");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
  
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num].hasMember("frame_id_header"), "Test frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["tactel_area"].getType() == XmlRpc::XmlRpcValue::TypeDouble, "Test tactel_area");
  ROS_ASSERT_MSG(xmlrpclist[tactile_sensor_num]["contact_points_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test contact point topic name");

  
}

void ContactPointsKuks::image_cb(const sensor_msgs::ImageConstPtr& msg)
{
    uchar img[rows_][cols_];
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    int c = 0;
    total_pressure_ = 0;
    contact_points_n_ = 0;
    for(int i=0; i<rows_; ++i) {
        for(int j=0; j<cols_; j++) {
                img[i][j]=cv_ptr->image.data[c];
                total_pressure_ += img[i][j];
		c = c + 1;
		if (img[i][j] >= threshold_noise_)
		{
		  contact_points_n_ = contact_points_n_ + 1;
		}
        }
    }
    
    cv::Mat src_img = cv::Mat(rows_, cols_, CV_8UC1, img);
    
    img2points (src_img);
}

void ContactPointsKuks::img2points(cv::Mat& input_img)
{
  
//   std::vector<pcl::PointXYZ> contact_points;
//   PointCloud::Ptr msg (new PointCloud);
  PointCloudColor::Ptr msg_color (new PointCloudColor);
//   msg->points.resize(rows_*cols_);
//   msg->header.stamp = ros::Time::now();
  
//   pcl_conversions::toPCL(ros::Time::now(), msg->header.stamp);
  pcl_conversions::toPCL(ros::Time::now(), msg_color->header.stamp);

  // fill up the points
  

  for(int r = 0; r < input_img.rows; ++r)
  {
    
    for(int c = 0; c < input_img.cols; ++c)
    {
//       ROS_DEBUG_STREAM ("r = " << r);
//       ROS_DEBUG_STREAM ("c = " << c);
//       ROS_DEBUG_STREAM ("input_img= " << (int)input_img.at<uchar>(r,c));
     
//       if ((int) input_img.at<uchar>(r,c) > threshold_noise_ )
//       {
	float real_x = size_x_ - (float)((float)size_x_/
	((float)rows_-1)) * ((float) r);
	
	float real_y = size_y_ - (float)((float)size_y_/
	((float)cols_-1)) * ((float) c);
// 	ROS_DEBUG_STREAM ("real_x =" << real_x);
// 	ROS_DEBUG_STREAM ("real_y = " << real_y);
	
	//maxI(x,y) 250 -> 250 kPa -> 1.8 N / tactel_area pixels*0.00001156  -> z = 1.8 * compliacne 0.00055556
	// P(x,y) -> I(x,y)*250k/250 = I(x,y)*1000
	// force(x,y) -> I(x,y)*1000 * (1.8/area) / 250k = I(x,y)*(1.8/area) / 250
	// z = compliacne * force(x,y) 
	// z =  I(x,y)*0.00055556 (1.8/area) / 250
	// 1.8 N * tactel_area_ / 250 =
	
	// very raf estimation:
	// 250 kPa  = 1.8 N * area of one pixel
	// P_1 = x N * area of one pixel
	// x N = P_1 * 1.8 / 250k = I(x,y) * 1000 * 1.8 / 250k = I(x,y)*1.8/250 
	
// 	ROS_DEBUG_STREAM ("(float)1.8/250.0 = " << (float)1.8/250.0);
// 	ROS_DEBUG_STREAM ("img = " << (float)input_img.at<uchar>(r,c));

	
	float  esitmated_force =  (float)input_img.at<uchar>(r,c) * (float)1.8/250.0;
// 	ROS_DEBUG_STREAM ("esitmated_force = " << esitmated_force);
	
	float  displacement_by_compliance = (float)esitmated_force * compliance_; 
// 	ROS_DEBUG_STREAM ("displacement_by_compliance = " << displacement_by_compliance);
	
// 	ROS_DEBUG_STREAM ("hight_of_sensor_ = " << hight_of_sensor_);
	float real_z = hight_of_sensor_ - displacement_by_compliance; 
// 	ROS_DEBUG_STREAM ("real_z = " << real_z);
// 	msg->points.push_back(pcl::PointXYZ(real_x, real_y, real_z));
	
	pcl::PointXYZRGB point;
	point.x = real_x- size_x_ / 2.0;
	point.y = real_y- size_y_ / 2.0;
	point.z = real_z;
	if ((int) input_img.at<uchar>(r,c) > threshold_noise_ )
	{
	  point.r = 255;
	  point.g = 0;
	  point.b = 0;
	  geometry_msgs::PointStamped contact_on_sensor;
	  contact_on_sensor.header.frame_id = "ati_link";
	  contact_on_sensor.header.stamp = ros::Time();
          contact_on_sensor.point.x = real_x - size_x_ / 2.0;
          contact_on_sensor.point.y = real_y - size_y_ / 2.0;
          contact_on_sensor.point.z = real_z;
	  geometry_msgs::PointStamped contact_wrt_base;
	  transformPoint(tf_listener, contact_on_sensor, contact_wrt_base);
	  pub_contact_positions_plot.publish(contact_wrt_base);

	  
	}
	else
	{
	  point.r = 0;
	  point.g = 0;
	  point.b = 0; 
	}
	msg_color->points.push_back(point);
	
	
//       }

    }
  }
//   ROS_DEBUG_STREAM ("here");
//   ROS_DEBUG_STREAM("cop.x=" << msg->points.at(0).;
//   msg->header.frame_id = sensor_frame_name_urdf_;
//   msg->height = 1;
//   msg->width = rows_*cols_;
//   pub_contacts.publish(msg->makeShared());
  
  msg_color->header.frame_id = "ati_link";
  msg_color->height = 1;
  msg_color->width = rows_*cols_;
  pub_contacts_color.publish(msg_color->makeShared());
  
}

void ContactPointsKuks::transformPoint(const tf::TransformListener& listener_, 
			      geometry_msgs::PointStamped& point_in_,
			      geometry_msgs::PointStamped& pose_out ){

    listener_.waitForTransform("ati_link", ros::Time(0), "link_0",ros::Time(0), "link_0", ros::Duration(10));

    try{
        listener_.transformPoint("link_0", point_in_, pose_out);
//        ROS_INFO("tactile_contact_world: (%.4f, %.4f. %.4f) -----> tactile_contact_weiss: (%.4f, %.4f, %.4f) at time %.4f",
//                 point_in_.point.x, point_in_.point.y, point_in_.point.z,
//                 frame_tactile_contact.point.x, frame_tactile_contact.point.y, frame_tactile_contact.point.z, frame_tactile_contact.header.stamp.toSec());

    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/base_frame\" to \"/sensor_frame\": %s", ex.what());

    }


}

//   void publish_contacts();
