/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for palm pose tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include "tact_contr_clean/kuka_palm_pose.hpp"
#include "tact_contr_clean/servo_pid.hpp"

// subscribe to /palm/pose
// publish to /move_arm_6D/pose
// subscribe to fb_feats
// subscribe to des_feats

ServoPalmPoseKuka::ServoPalmPoseKuka()
{
  
  num_pixels_weight_gain_sub = nh.subscribe("/coc_zmp_smooth_trans", 2, &ServoPalmPoseKuka::num_pixels_weight_gain_cb, this);
  
  is_contact_kuka_sub = nh.subscribe("/is_contact", 2, &ServoPalmPoseKuka::is_contact_kuka_cb, this);
  
  sub_current_pose = nh.subscribe("/pose_current", 2, &ServoPalmPoseKuka::callback_current_pose, this);
  pub_pose = nh.advertise<geometry_msgs::PoseStamped>("/pose_desired", 2);
  
  is_pose_rec_ = 0;
  // implement in exe file not in library
  //double publish_freq;
  //publish_freq = 50;
  // read param with giving default value
  //nр.param("publish_frequency", publish_freq, 20.0);
  //publish_rate = ros::Rate(publish_freq);
  sub_feats_des = nh.subscribe("/des_feats", 2,  &ServoPalmPoseKuka::callback_desfeats,this);
  //fb_feats_avg topic bad it dies
  sub_feats_fb = nh.subscribe("/fb_feats", 2,  &ServoPalmPoseKuka::callback_fbfeats,this);
  sub_plan_feats_fb = nh.subscribe("/plan_feats", 2,  &ServoPalmPoseKuka::callback_planfbfeats,this);

  is_feat_des_rec_ = false;
  is_feat_fb_rec_ = false;
  
  sel_copx_   = 1;
  sel_copy_   = 1;
  sel_force_  = 1;
  sel_cocx_   = 1;
  sel_cocy_   = 1;
  sel_orient_ = 1;
  
  zmp_y_fb = 0;
  zmp_x_fb = 0;
  zmp_y_des = 0;
  zmp_x_des = 0;
  
  // smooth trans coc to zmp
  // 1 - rotate using COC for point
  // 0 - rotate using zmp for edge
  
  smooth_ = 1;
  
  ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////
//   bimanual_COM_coc_des_pub = nh.advertise<std_msgs::Float64>("/bimanual_cocx_des", 2);
  ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////
}

ServoPalmPoseKuka::~ServoPalmPoseKuka()
{}

void ServoPalmPoseKuka::num_pixels_weight_gain_cb(const tactile_servo_msgs::COCtoZMP& msg)
{
  smooth_ = msg.coc_to_zmp_smooth;
}

void ServoPalmPoseKuka::is_contact_kuka_cb(const std_msgs::Bool& msg)
{
  is_contact_kuka_ = msg.data;
  
}


void ServoPalmPoseKuka::callback_planfbfeats(const tactile_servo_msgs::PlanFeats& msg)
{
  contours_num_ = msg.numContours;
  
}

void ServoPalmPoseKuka::callback_current_pose(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    palm_pose_now_ = *msg;
    is_pose_rec_ = 1;
//     ROS_FATAL_STREAM("pose_curren_cb x="<<palm_pose_now_.pose.position.x);
      
//      ROS_FATAL_STREAM("current pose  received");
}
  else
  {
    is_pose_rec_ = 0;
    ROS_FATAL_STREAM("current pose is not received");
  }
}

geometry_msgs::PoseStamped ServoPalmPoseKuka::get_pose()
{
  
  return palm_pose_now_;
}

void ServoPalmPoseKuka::send_pose(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose.publish(msg_sendpose);
}

void ServoPalmPoseKuka::callback_desfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg)
{
//     ROS_FATAL_STREAM("des feat size = " << msg->control_features.size());

    if (msg->control_features.size() == 1)
    {
      feats_des_.clear();
      // Physical sizes not pixels. Feature extractor publishes real sizes
      copx_des = msg->control_features[0].centerpressure_x;
      copy_des = msg->control_features[0].centerpressure_y;
      force_des = msg->control_features[0].contactForce;
      cocx_des = msg->control_features[0].centerContact_x;
      cocy_des = msg->control_features[0].centerContact_y;
      orientz_des = msg->control_features[0].contactOrientation;
      zmp_x_des = msg->control_features[0].zmp_x;
      zmp_y_des = msg->control_features[0].zmp_y;
//       feats_des_ = {copx_des, copy_des, force_des, cocx_des, cocy_des, orientz_des };
      //////////////////////IMPORTANT///////////////////
      /////////////////////////////////////////////////
      //zmp_x not used to show that prev appr not work
      //////////////////////////////////////////////
      ///////////////////////////////////////////
//       feats_des_ = {copx_des, copy_des, force_des, zmp_y_des, zmp_x_des, orientz_des };
      // if smooth_ not used
//       feats_des_ = {copx_des, copy_des, force_des, zmp_y_des, cocx_des, orientz_des };
// for bimanual
      feats_des_ = {zmp_x_des , copy_des, force_des, zmp_y_des, copx_des, orientz_des };
      is_feat_des_rec_ = true;
       ROS_DEBUG_STREAM("des feat received");
//        ROS_FATAL_STREAM("feats_des in callback ="<<feats_des_.at(0)<<";"<<feats_des_.at(1) );
//        ROS_FATAL_STREAM("zmp_x_des, zmp_x_des ="<<feats_des_.at(3)<<";"<<feats_des_.at(4) );

      /* coordinates of COP/COC in world =
        / sequence number of the  COP/COC pixel in x or y
        / physical size of a sensor in x or y
        / number of cells in x(y)*/
      /* copx_des = copx_des*size_x / cells_x;
        copy_des = copy_des*size_y / cells_y;
        cocx_des = cocx_des*size_x / cells_x;
        cocy_des = cocy_des*size_y / cells_y; */
    }
    else
    {
      is_feat_des_rec_ = false;
      ROS_FATAL_STREAM("des feat is not received");
      feats_des_.clear();
    }
}

void ServoPalmPoseKuka::callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg->control_features.size() == 1)
  {
    feats_fb_.clear();
    copx_fb = msg->control_features[0].centerpressure_x;
    copy_fb = msg->control_features[0].centerpressure_y;
    force_fb = msg->control_features[0].contactForce;
    cocx_fb = msg->control_features[0].centerContact_x;
    cocy_fb = msg->control_features[0].centerContact_y;
    orientz_fb = msg->control_features[0].contactOrientation;
    zmp_x_fb = msg->control_features[0].zmp_x;
    zmp_y_fb = msg->control_features[0].zmp_y;
    
//     feats_fb_ = {copx_fb, copy_fb, force_fb, cocx_fb, cocy_fb, orientz_fb };
         //////////////////////IMPORTANT///////////////////
      /////////////////////////////////////////////////
      //zmp_x not used to show that prev appr not work
      //////////////////////////////////////////////
      ///////////////////////////////////////////
    // original vector
//     feats_fb_ = {copx_fb, copy_fb, force_fb, zmp_y_fb,  zmp_x_fb, orientz_fb };
    // for the rotation if smooth_ is not used
//     feats_fb_ = {copx_fb, copy_fb, force_fb, zmp_y_fb,  cocx_fb, orientz_fb };
    // bimanual manip zmp is mapped to deltaX
feats_fb_ = { zmp_x_fb, copy_fb, force_fb, zmp_y_fb,  copx_fb, orientz_fb };
    
    //     ROS_FATAL_STREAM("zmp_x_fb, zmp_x_fb ="<<feats_fb_.at(3)<<";"<<feats_fb_.at(4) );

    is_feat_fb_rec_ = true;
    ROS_DEBUG_STREAM("fb feat  received");
  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_DEBUG_STREAM("fb feat is not received");
    feats_fb_.clear();
  }
}

VECTOR6_1 ServoPalmPoseKuka::vec2VECTOR6_1 (std::vector<double> vec )
{
  VECTOR6_1  out;
  for ( unsigned int i =0;  i < vec.size(); i++)
  {
    out(i) = vec.at(i);
  }
  return out;
}

std::vector<double> ServoPalmPoseKuka::VECTOR6_1tovec (VECTOR6_1 matr )
{
  std::vector<double>  out;
  for ( unsigned int i =0;  i < 6; i++)
  {
    out.push_back(matr(i));
  }
  return out;
}

std::vector<double> ServoPalmPoseKuka::get_feat_err()
{
  ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////

  //   feats_des_.at(0) = feats_fb_.at(0) + (feats_des_.at(4) - feats_fb_.at(4));
//   bimanual_COM_coc_des_ = feats_des_.at(0);
//   std_msgs::Float64 pub_to_debug_coc_des;
//   pub_to_debug_coc_des.data = bimanual_COM_coc_des_;
//   bimanual_COM_coc_des_pub.publish();
  
    ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////
  
  std::vector<double> error_feats;
  if ((feats_fb_.size() > 0) && (feats_fb_.size() == feats_des_.size()))
  {
    for ( unsigned int i =0;  i < feats_des_.size(); i++)
    {
      error_feats.push_back(feats_des_.at(i) - feats_fb_.at(i));
    }
//     ROS_FATAL_STREAM(" float fb0 = " << feats_fb_.at(0) << 
//     " float des0 = " << feats_des_.at(0));
//     ROS_FATAL_STREAM(" double err = " << error_feats.at(0));
//     ROS_FATAL_STREAM("error_feats zmps ="<<error_feats.at(3)<<";"<<error_feats.at(4) );

  }
  return  error_feats;
}

// my approach zmp
VECTOR6_1 ServoPalmPoseKuka::convert2ZMP (VECTOR6_1 in_matr )
{
  /**
   * in_matr(0) = copx     
   * in_matr(1) = copy     
   * in_matr(2) = force
   * in_matr(3) = cocx     
   * in_matr(4) = cocy     
   * in_matr(5) = orient
   *In order to get ZMP feature:
   * zmpx = copx - cocx
   * zmpy = copy - cocy  
   * */
    VECTOR6_1  out;

  // it was when I forgot to make separate feature ZMP_X
  // and ZMP_Y
//   double zmpx = in_matr(0) - in_matr(3);
//   double zmpy = in_matr(1) - in_matr(4);
  out = in_matr;
//   out(3) = zmpx;
//   out(4) = zmpy;
  
  // now it should be directly with zmp features
  // it is more understandable
//   out(3) = zmp_x_des - zmp_x_fb;
//   out(4) = zmp_y_des - zmp_y_fb;
  // but finally it does not need any changes
  
  
  return out;
}

// bielefeld approach
VECTOR6_1 ServoPalmPoseKuka::convert2nonzmp (VECTOR6_1 in_matr )
{
  /**
   * in_matr(0) = copx     
   * in_matr(1) = copy     
   * in_matr(2) = force
   * in_matr(3) = cocx     
   * in_matr(4) = cocy     
   * in_matr(5) = orient
   *In order to get ZMP feature:
   * zmpx = copx - cocx
   * zmpy = copy - cocy  
   * */
  double nonzmpx = in_matr(0) ;
  double nonzmpy = in_matr(1) ;
  VECTOR6_1  out;
  out = in_matr;
  out(3) = nonzmpx;
  out(4) = nonzmpy;
  return out;
}


VECTOR6_1 ServoPalmPoseKuka::select_feats(VECTOR6_1 err_all)
{

  VECTOR6_6 sel_M;
  VECTOR6_1 sel_feat;
/** for this forst test no smoothing
 * disable publishe on image proc
 * and publish it by hand
 * 
**/
 // for the rotation around Y axis using coc there is an inveriton 
  sel_M << 
  sel_copx_,	    0,         0,      	0,	         0,     	  0,
  0,	        sel_copy_,     0,      	0,      	 0,    		  0,
  0,	            0,    sel_force_,  	0,	         0,    		  0,
  0,	smooth_*sel_cocx_,     0, (1 - smooth_) * sel_cocx_, 0,     	  0,
 smooth_*sel_cocy_*(-1.0), 	0,         0,      	0,     (1 - smooth_)*sel_cocy_,   0,
  0,            0,         0,      	0,        	 0,	   sel_orient_;
  
  ROS_DEBUG_STREAM("err_all ="
  <<err_all(0)
  <<";"<<err_all(1)
  <<";"<<err_all(2)
  <<";"<<err_all(3)
  <<";"<<err_all(4)
  <<";"<<err_all(5));
  
//   for (int i = 0; i < 6; i++)
//   {
//     for (int j = 0; j < 6; j++)
//     {
//       ROS_FATAL_STREAM("M"<<i<<j<<"="<<sel_M(i,j));
//     }
//   }
  
  sel_feat = sel_M*err_all; // S_Matr_force S_Matr_orient  S_Matr_force_copx_copy
  ROS_DEBUG_STREAM("sel_feat ="
  <<sel_feat(0)
  <<";"<<sel_feat(1)
  <<";"<<sel_feat(2)
  <<";"<<sel_feat(3)
  <<";"<<sel_feat(4)
  <<";"<<sel_feat(5));
	
   return sel_feat;
}

Eigen::Vector3d ServoPalmPoseKuka::msgpose2VECTOR3_1(geometry_msgs::PoseStamped& msg)
{
  Eigen::Vector3d x;
  x(0) = msg.pose.position.x;
  x(1) = msg.pose.position.y;
  x(2) = msg.pose.position.z;

  return x;  
}

tf::Quaternion ServoPalmPoseKuka::pose2quat(geometry_msgs::PoseStamped& msg)
{
      tf::Quaternion q2rpy;
      q2rpy.setX(msg.pose.orientation.x);
      q2rpy.setY(msg.pose.orientation.y);
      q2rpy.setZ(msg.pose.orientation.z);
      q2rpy.setW(msg.pose.orientation.w);
      return q2rpy;
}

std::vector<double> ServoPalmPoseKuka::quat2rpy( tf::Quaternion& quat)
{
  tf::Matrix3x3 rot_matr(quat);
  double roll, pitch, yaw;
  rot_matr.getRPY(roll, pitch, yaw);
  std::vector<double> out;
  out = {roll, pitch, yaw};
  return out;
}

Eigen::Matrix3d ServoPalmPoseKuka::quat2eigen (tf::Quaternion quat)
{
  Eigen::Matrix3d eig_rot_base2palm;
  tf::Matrix3x3 rot_matr_base2palm(quat);
  
  tf::Vector3  row0 =  rot_matr_base2palm.getRow(0);
  tf::Vector3  row1 =  rot_matr_base2palm.getRow(1);
  tf::Vector3  row2 =  rot_matr_base2palm.getRow(2);
  
  eig_rot_base2palm(0,0) = row0.getX();
  eig_rot_base2palm(0,1) = row0.getY();
  eig_rot_base2palm(0,2) = row0.getZ();
  eig_rot_base2palm(1,0) = row1.getX();
  eig_rot_base2palm(1,1) = row1.getY();
  eig_rot_base2palm(1,2) = row1.getZ();
  eig_rot_base2palm(2,0) = row2[0];
  eig_rot_base2palm(2,1) = row2[1];
  eig_rot_base2palm(2,2) = row2[2];

  return eig_rot_base2palm;
}



Eigen::Matrix3d ServoPalmPoseKuka::AxB (Eigen::Matrix3d a, Eigen::Matrix3d b)
{
  Eigen::Matrix3d res;
  res = a*b;
  return res;
}

Eigen::Vector3d ServoPalmPoseKuka::AxVector (Eigen::Matrix3d a, Eigen::Vector3d b)
{
  Eigen::Vector3d res;
  res = a*b;
  return res;
}


Eigen::Vector3d ServoPalmPoseKuka::sumAB (Eigen::Vector3d a, Eigen::Vector3d b)
{
  Eigen::Vector3d res;
  res = a+b;
  return res;
}




Eigen::Vector3d ServoPalmPoseKuka::feat_err_pos(VECTOR6_1 feats)
{
  Eigen::Vector3d error_affecting_position;
  
   error_affecting_position(0) = feats(0);
   error_affecting_position(1) = feats(1);
   error_affecting_position(2) = feats(2);
   
   return error_affecting_position;
}

Eigen::Vector3d ServoPalmPoseKuka::feat_err_rot(VECTOR6_1 feats)
{
  Eigen::Vector3d error_affecting_rotation;
  
   error_affecting_rotation(0) = feats(3);
   error_affecting_rotation(1) = feats(4);
   error_affecting_rotation(2) = feats(5);
   
   return error_affecting_rotation;
}

Eigen::Vector3d ServoPalmPoseKuka::rpy2xyz(Eigen::Vector3d tobe_rearranged)
{
  /** RPY in ROS is XZY that is why feature error affecting Z 
   * coordinate (edge orientation) should  change the place with 
   * ZMPy feature
  **/
  Eigen::Vector3d rearranged;
  
   rearranged(0) = tobe_rearranged(0);
   rearranged(1) = tobe_rearranged(2);
   rearranged(2) = tobe_rearranged(1);
   
   return rearranged;
}

tf::Matrix3x3 ServoPalmPoseKuka::vec2rot(Eigen::Vector3d vec)
{
  tf::Quaternion q;
  q.setRPY(vec(0), vec(1), vec(2));
  tf::Matrix3x3 rot_matr(q);
  return rot_matr;
}




void ServoPalmPoseKuka::set_posit_des_pose(geometry_msgs::PoseStamped& pose_msg, const Eigen::Vector3d &position)
{
  pose_msg.pose.position.x = position(0);
  pose_msg.pose.position.y = position(1);
  pose_msg.pose.position.z = position(2);
}

void ServoPalmPoseKuka::set_orient_des_pose(geometry_msgs::PoseStamped& pose_msg, const tf::Matrix3x3 &rot)
{
  tf::Quaternion q;
  rot.getRotation(q);
  
  pose_msg.pose.orientation.x = q.getX();
  pose_msg.pose.orientation.y = q.getY();
  pose_msg.pose.orientation.z = q.getZ();
  pose_msg.pose.orientation.w = q.getW();
}

  

/** Reconf and Services for servoing 
 **/



ServoReconfServicesKuka::ServoReconfServicesKuka(ServoPalmPoseKuka& servo_obj_ref, ServoPidManager& pid_obj_ref)
:servo_obj_ref_(servo_obj_ref), pid_obj_ref_(pid_obj_ref)
{
  service_sel_matr = nh.advertiseService("/select_servo_components", &ServoReconfServicesKuka::selection_matrix, this);
  service_begin_control = nh.advertiseService("/begin_tactile_servo_real", &ServoReconfServicesKuka::begin_control, this);
  begin_control_ = 0;
  service_sel_configuration_singularity = nh.advertiseService("/select_configuration", &ServoReconfServicesKuka::select_configuration, this);
  choose_configuration_ = 0;
  
  std::vector<int> sel_servo_params;
  bool ok =nh.getParam("/select_servo_feats_palm", sel_servo_params);
  if(ok)
  {
    servo_obj_ref_.sel_copx_ = sel_servo_params.at(0);
    servo_obj_ref_.sel_copy_ =sel_servo_params.at(1);
    servo_obj_ref_.sel_force_ =sel_servo_params.at(2);
    servo_obj_ref_.sel_cocx_ =sel_servo_params.at(3);
    servo_obj_ref_.sel_cocy_ =sel_servo_params.at(4);
    servo_obj_ref_.sel_orient_ =sel_servo_params.at(5);
    ROS_INFO_STREAM("selected feats params init to "<< 
    servo_obj_ref_.sel_copx_
  <<";"<<servo_obj_ref_.sel_copy_
  <<";"<<servo_obj_ref_.sel_force_
  <<";"<<servo_obj_ref_.sel_cocx_
  <<";"<<servo_obj_ref_.sel_cocy_
  <<";"<<servo_obj_ref_.sel_orient_);
    
  }
  else
  {
    ROS_FATAL_STREAM("Could not get parameter "<< 
    "/select_servo_feats_palm"<<
      "setting all to 1");
    servo_obj_ref_.sel_copx_ = 1;
    servo_obj_ref_.sel_copy_ =1;
    servo_obj_ref_.sel_force_ =1;
    servo_obj_ref_.sel_cocx_ =1;
    servo_obj_ref_.sel_cocy_ =1;
    servo_obj_ref_.sel_orient_ =1;
  }
  
  // set selected features
  f = boost::bind(&ServoReconfServicesKuka::cb_dynamic_reconf, this, _1, _2);
  set_pid_feats_server.setCallback(f);
  
  
}

ServoReconfServicesKuka::~ServoReconfServicesKuka()
{}

// set pid gains
void ServoReconfServicesKuka::cb_dynamic_reconf(tactile_servo_config::servo_feats_selectConfig &config, uint32_t level)
{
    servo_obj_ref_.sel_copx_ = config.servo_copx;
    servo_obj_ref_.sel_copy_ = config.servo_copy;
    servo_obj_ref_.sel_force_ = config.servo_force;
    servo_obj_ref_.sel_cocx_ = config.servo_cocx;
    servo_obj_ref_.sel_cocy_ = config.servo_cocy;
    servo_obj_ref_.sel_orient_ = config.servo_orient;
    ROS_INFO_STREAM("selected feats set by dynamic reconf to "<< 
    servo_obj_ref_.sel_copx_
    <<";"<<servo_obj_ref_.sel_copy_
    <<";"<<servo_obj_ref_.sel_force_
    <<";"<<servo_obj_ref_.sel_cocx_
    <<";"<<servo_obj_ref_.sel_cocy_
    <<";"<<servo_obj_ref_.sel_orient_);
        ROS_FATAL_STREAM("selected feats set by dynamic reconf to "<< 
    servo_obj_ref_.sel_copx_
    <<";"<<servo_obj_ref_.sel_copy_
    <<";"<<servo_obj_ref_.sel_force_
    <<";"<<servo_obj_ref_.sel_cocx_
    <<";"<<servo_obj_ref_.sel_cocy_
    <<";"<<servo_obj_ref_.sel_orient_);
    
    // integral part accumulates error
    //must be resetted
    pid_obj_ref_.readparams();
}

bool ServoReconfServicesKuka::selection_matrix(tactile_servo_srvs::select_matrix::Request &req,
                             tactile_servo_srvs::select_matrix::Response &res)
{
  sel_copx_ = req.copx;
  sel_copy_ = req.copy;
  sel_force_ = req.force;
  sel_cocx_ = req.cocx;
  sel_cocy_ = req.cocy;
  sel_orient_ = req.orient;
  update_mutex.lock();
  servo_obj_ref_.sel_copx_ = req.copx;
  servo_obj_ref_.sel_copy_ = req.copy;
  servo_obj_ref_.sel_force_ = req.force;
  servo_obj_ref_.sel_cocx_ = req.cocx;
  servo_obj_ref_.sel_cocy_ = req.cocy;
  servo_obj_ref_.sel_orient_ = req.orient;
  update_mutex.unlock();
  
  res.success = 1;
  
  ROS_INFO("%ld, %ld, %ld, %ld, %ld, %ld ", 
	   (long int)sel_copx_, (long int)sel_copy_,
	  (long int)sel_force_, (long int)sel_cocx_,
	  (long int)sel_cocy_, (long int)sel_orient_);
  ROS_DEBUG("sending back response: [%ld]", (long int)res.success);
  
  ROS_DEBUG_STREAM("sel_copx_=" << sel_copx_);
  ROS_DEBUG_STREAM("sel_copy_=" << sel_copy_);
  ROS_DEBUG_STREAM("sel_force_=" << sel_force_);
  ROS_DEBUG_STREAM("sel_cocx_=" << sel_cocx_);
  // reset integral error 
  pid_obj_ref_.reset();
  return true;
}

bool ServoReconfServicesKuka::begin_control(tactile_servo_srvs::begin_control::Request &req,
                             tactile_servo_srvs::begin_control::Response &res)
{
  if ( req.begin_control == 0)
  {
    ROS_INFO("stop control");
    begin_control_ = req.begin_control;
    res.success = 1;
  }
  else if ( req.begin_control == 1)
  {
    ROS_INFO("start control");
    begin_control_ = req.begin_control;
    // reset integral error 
    pid_obj_ref_.reset();
    res.success = 1;

  }
  else
  {
    res.success = 0;
  }
   
  return true;
}

bool ServoReconfServicesKuka::select_configuration(tactile_servo_srvs::choose_configuration_singularity::Request &req,
                             tactile_servo_srvs::choose_configuration_singularity::Response &res)
{
  if (req.choose_configuration == 0)
  {
    ROS_INFO("chosen configuration without rearrangement due to singulatiry");
    choose_configuration_ = req.choose_configuration;
    res.success = 1;
  }
  else if (req.choose_configuration == 1)
  {
    ROS_INFO("chosen configuration WITH rearrangement due to singulatiry");
    choose_configuration_ = req.choose_configuration;
    res.success = 1;

  }
  else
  {
    res.success = 0;
  }
   
  return true;
}



Servo4VizKuka::Servo4VizKuka()
{
  pub_pose_viz_des = nh.advertise<geometry_msgs::PoseStamped>("/servo_pose_viz_des", 2);
  pub_pose_viz_des2 = nh.advertise<geometry_msgs::PoseStamped>("/servo_pose_viz_des2", 2);
  pub_pose_viz_now = nh.advertise<geometry_msgs::PoseStamped>("/servo_pose_viz_now", 2);
  pub_pose_viz_now2 = nh.advertise<geometry_msgs::PoseStamped>("/servo_pose_viz_now2", 2);
  pub_pose_viz_contact_frame = nh.advertise<geometry_msgs::PoseStamped>("/servo_pose_viz_contact_frame", 2);

   std::string sensor_name;
    XmlRpc::XmlRpcValue my_list;
    //get tactile sensor's parameters from param server
    nh.getParam("/tactile_arrays_param", my_list);
    hight_of_sensor_ = static_cast<double> (my_list[0]["hight_of_sensor"]);
    compliance_ = static_cast<double> (my_list[0]["compliance"]);
}

Servo4VizKuka::~Servo4VizKuka(){}

void Servo4VizKuka::send_pose_des(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz_des.publish(msg_sendpose);
}

void Servo4VizKuka::send_pose_des2(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz_des2.publish(msg_sendpose);
}

void Servo4VizKuka::send_pose_now(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz_now.publish(msg_sendpose);
}


void Servo4VizKuka::send_pose_now2(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz_now2.publish(msg_sendpose);
}

void Servo4VizKuka::send_pose_contact_frame(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz_contact_frame.publish(msg_sendpose);
}

geometry_msgs::PoseStamped Servo4VizKuka::set_contact_frame(float& cocxfb, float& cocyfb, float& forcefb,
							float& zmpxfb, float& zmpyfb, float& orientfb)
{
  
  geometry_msgs::PoseStamped X_contact;
  
  X_contact.header.stamp = ros::Time::now();
  X_contact.header.frame_id = "contact_frame";
  
  X_contact.pose.position.x = cocxfb;
  X_contact.pose.position.y = cocyfb;
  X_contact.pose.position.z = (float)hight_of_sensor_ - (float)forcefb*(float)compliance_;
  
  tf::Quaternion q_contact_orient;
  q_contact_orient.setRPY( zmpyfb, 
			   zmpxfb, 
			   orientfb);
  X_contact.pose.orientation.x = q_contact_orient.getX();
  X_contact.pose.orientation.y = q_contact_orient.getY();
  X_contact.pose.orientation.z = q_contact_orient.getZ();
  X_contact.pose.orientation.w = q_contact_orient.getW();
  return X_contact;
  
}

