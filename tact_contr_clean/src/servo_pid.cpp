#include "tact_contr_clean/servo_pid.hpp"


SinglePID::SinglePID(std::string link, std::string axis)
  : position_deadband_(0.015)
{
  axis_ = axis;
  link_ = link;
}

SinglePID::~SinglePID(){};

bool SinglePID::init(ros::NodeHandle &n)
{
  node_ = n;
  std::string nmespace = n.getNamespace();
//   ROS_INFO_STREAM("the name of the namespace inside single pid = "<< nmespace);

  if (!pid_servo_.init(ros::NodeHandle(node_, link_+"/"+axis_))){
    ROS_FATAL_STREAM("Can not construct PID controller for"+link_+axis_);
    return false;
  }
}

void SinglePID::read_params()
{
    if (node_.hasParam("/servo_pid/"+link_+"/"+axis_))
  {
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/p", p_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/i", i_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/d", d_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/i_clamp", i_clamp_);
  }
  else
  {
    ROS_FATAL_STREAM("No parameters for /servo_pid/"+link_+"/"+axis_);
  }
}

void SinglePID::setGains()
{
  pid_servo_.setGains(p_, i_, d_, i_clamp_, -i_clamp_);
}

void SinglePID::getGains(double &p, double &i, double &d, double &i_max, double &i_min)
{
  pid_servo_.getGains(p, i, d, i_max, i_min);
}

void SinglePID::starting()
{
  pid_servo_.reset();
  read_params();
  setGains();
}

void SinglePID::reset()
{
  pid_servo_.reset();
}

double SinglePID::compute_cmd(double err_position, ros::Duration dt)
{
   out_ = pid_servo_.computeCommand(err_position, dt);
   return out_;
}




ServoPidManager::ServoPidManager()
{
  std::vector<std::string> link_names;
  if (node_.hasParam("/link_names_to_servo"))
  {
    node_.getParam("/link_names_to_servo",link_names);
  }
  else
  {
    link_names.push_back("palm");
  }
  
    std::vector<std::string> names_axes;
    names_axes.push_back("x");
    names_axes.push_back("y");
    names_axes.push_back("z");
    names_axes.push_back("wx");
    names_axes.push_back("wy");
    names_axes.push_back("wz");
    
    for( int i=0; i< link_names.size(); ++i)
    {
      for( int j=0; j< names_axes.size(); ++j)
      {
	palm_pids.push_back(new SinglePID(link_names.at(i),names_axes.at(j)));
      }
    }
}

ServoPidManager::~ServoPidManager(){}

bool ServoPidManager::init(ros::NodeHandle &n)
{
  
  std::string nmespace = n.getNamespace();
//   ROS_INFO_STREAM("the name of the namespace is = " << nmespace);
//   std::string ndename = n.getName();
//   ROS_INFO_STREAM("the name of the namespace is = "<< ndename);
//   
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->init(n);
  }
}

void ServoPidManager::readparams()
{
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->read_params();
  }
}

void ServoPidManager::setGains()
{
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->setGains();
  }  
}

void ServoPidManager::starting()
{
    for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->starting();
  } 
}

std::vector<double> ServoPidManager::compute_cmd(std::vector<double> err_position, 
						 ros::Duration dt)
{
  std::vector<double> out_temp;
  for( int i=0; i< palm_pids.size(); ++i)
  {
    out_temp.push_back(palm_pids.at(i)->compute_cmd(err_position.at(i), dt));
  }
  out_ = out_temp;
  return out_temp;
}

void ServoPidManager::reset()
{
    for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->reset();
  } 
}