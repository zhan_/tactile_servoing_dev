/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
//#include "tact_contr_clean/tact_contr_clean_node.hpp"
// #ifndef _PALM_POSE_HPP_
// #define _PALM_POSE_HPP_

#include "tact_contr_clean/palm_pose.hpp"
#include "tact_contr_clean/servo_pid.hpp"

#include "tact_contr_clean/control_wrj12er_gazebo.hpp"


//get pose: ServoPalmPose::get_pose() returns geometry_msgs::PoseStamped;
// get features fb
// get features desired
// calculate error
// check an hysteresis deadband.
/* apply pid to error
// Controller update loop in realtime
void MyControllerClass::update()
{
  double desired_pos = init_pos_ + amplitude_ * sin(ros::Time::now().toSec());
  double current_pos = joint_state_->position_;

  ros::Duration dt = robot_->getTime() - time_of_last_cycle_;
  time_of_last_cycle_ = robot_->getTime();
  joint_state_->commanded_effort_ = pid_controller_.updatePid(current_pos-desired_pos, dt);
}
 //shadow code
//Compute position demand from position error:
double error_position = 0.0;
double commanded_effort = 0.0;
error_position = joint_state_->position_ - command_;
bool in_deadband = hysteresis_deadband.is_in_deadband(command_, error_position, position_deadband);
//don't compute the error if we're in the deadband.
if (in_deadband) error_position = 0.0; pid_controller_position_->reset();
commanded_effort = pid_controller_position_->computeCommand(-error_position, period);
*/
//transform to cartesian error
// selection matrix
// send pose ServoPalmPose::send_pose(geometry_msgs::PoseStamped)





int main(int argc, char** argv)
{
    ros::init(argc, argv, "tact_contr_clean_node_control_joints_gazebo");
    ros::NodeHandle n;
/* // read from param and set publish rate example   
    //double publish_freq;
    //publish_freq = 50;
    // read param with giving default value
    //nр.param("publish_frequency", publish_freq, 20.0);
    // ros::Rate publish_rate = ros::Rate(publish_freq);
*/    
    ros::Rate loop_rate(1);
    ROS_INFO("tact_contr_clean_node_control_joints_gazebo");
    
    // subscribers to joint_state_
    
    ControlWrj12ErGazebo joint_controls;
    ros::spinOnce();
    sleep(1); // this is required otherwise publishers are not ready for first messages to be sent
    ros::spinOnce();
    ROS_INFO_STREAM( "joint wrj 1 = " << joint_controls.wrj1_push_now_);
    ROS_INFO_STREAM( "joint wrj 2 = " << joint_controls.wrj2_bye_now_);
    ROS_INFO_STREAM( "joint er = " << joint_controls.elbow_rotate_now_);

    
    // class with subscribers pose feats and tools
    ServoPalmPose palm_servo;
    
    // class with pids
    ServoPidManager servo_pids;
    servo_pids.init(n);
    ROS_INFO("pid is set");

    // class with publisher of pose for viz
    Servo4Viz pose_viz;
    ROS_INFO("pose visualizer publishing");

    
    double a, b, c, d, e;
    servo_pids.palm_pids.at(0)->getGains(a, b, c, d, e);
//     ROS_FATAL_STREAM("pid gains ="
//     <<a <<";"<<b <<";"<<c <<";"<<d <<";"<<e);
//     servo_pids.palm_pids.at(1)->getGains(a, b, c, d, e);
//     ROS_FATAL_STREAM("pid gains ="
//     <<a <<";"<<b <<";"<<c <<";"<<d <<";"<<e);
    
    ServoReconfServices servo_reconf(palm_servo, servo_pids);
    ROS_INFO("selectin matrix is ready");

    
    // ros::Time a  = servo_pids.time_of_last_cycle_;
    ros::Duration dt;
    
    /* this is a new pose of palm
     * because of the issues realated with 
     * singularity. One has to check in RVIZ first
     * and then call the service 
     * bool ServoReconfServices::select_configuration(tactile_servo_srvs::choose_configuration_singularity::Request &req,
     *                        tactile_servo_srvs::choose_configuration_singularity::Response &res)
     * to select 1 or 2. the weiss sensor is attached with 90 90 90 degress
     *wrt palm. When the palm is selected to servo the orientation 
     *of an edge, palm rotates in other axes instead.
     *by default ServoReconfServices::choose_configuration_ 
     * is zero. no rearranging involved
     *                
    */
    geometry_msgs::PoseStamped new_pose;
    ////////////////////////
    // just simpt test//////
    ////////////////////////    
    int test_pose_intered_once; // to remove
    
    int bullet_proof, to_initial_pose, first_entry;
    bullet_proof = 0; to_initial_pose = 0; first_entry = 0;
    
    ////////////////////////
    // just simpt test//////
    ////////////////////////
    
    geometry_msgs::PoseStamped pose_test1, pose_test2;
    int counter_pose1, counter_pose2;
    int isPose_send;
    isPose_send = 0;
    counter_pose1 = 1;
    counter_pose2 = 1;
    // test1 is a pose at almost 90 degrees
    pose_test1.pose.position.x = 0.365651626893;
    pose_test1.pose.position.y = -0.142840763911;
    pose_test1.pose.position.z = 0.0986096843796;
    pose_test1.pose.orientation.x = 0.663715656633;
    pose_test1.pose.orientation.y = -0.237149409893;
    pose_test1.pose.orientation.z = 0.641488243434;
    pose_test1.pose.orientation.w = -0.302877067576;
    pose_test1.header.frame_id = "smth";
    pose_test1.header.stamp = ros::Time::now();
    
    // test1 is a pose at super at 90 degrees
    pose_test2.pose.position.x = 0.365133699536;
    pose_test2.pose.position.y = -0.14532574506;
    pose_test2.pose.position.z = 0.0982387867438;
    pose_test2.pose.orientation.x = 0.644728742803;
    pose_test2.pose.orientation.y = -0.278690671534;
    pose_test2.pose.orientation.z = 0.663084972716;
    pose_test2.pose.orientation.w = -0.258794661386;
    pose_test2.header.frame_id = "smth";
    pose_test2.header.stamp = ros::Time::now();

    
    ////////////////////////
    // just simpt test//////
    ////////////////////////
    ROS_DEBUG("before first spin");

    
    ros::spinOnce();
    ROS_DEBUG("after first spin");

    ros::Duration(0.5).sleep();
    ROS_DEBUG("after  sleep");


    ros::spinOnce();
    ROS_DEBUG("after second spin");



    while( ros::ok() ){

      if (first_entry == 0)
      {
	servo_pids.time_of_last_cycle_ = ros::Time::now();
	ros::Duration(0.005).sleep();

	ROS_DEBUG("after get time");

	first_entry = 1;
	ROS_DEBUG("once");
      }
      
      ROS_DEBUG("before dt");
      
      // dt
      dt = ros::Time::now() - servo_pids.time_of_last_cycle_;
      servo_pids.time_of_last_cycle_ = ros::Time::now();
      ROS_DEBUG("after dt");

      // feat err
      std::vector<double> err_feats;
      
      if ((palm_servo.is_feat_des_rec_ == 1) && (palm_servo.is_feat_fb_rec_ == 1))
      {
	err_feats = palm_servo.get_feat_err();
	
	ROS_FATAL_STREAM("vec_feat_err ="
	<<err_feats.at(0)
	<<";"<<err_feats.at(1)
	<<";"<<err_feats.at(2)
	<<";"<<err_feats.at(3)
	<<";"<<err_feats.at(4)
	<<";"<<err_feats.at(5));
	
// 	palm_servo.is_feat_des_rec_ = 0;
// 	palm_servo.is_feat_fb_rec_ = 0;
      }
      else
      {
	ROS_DEBUG_STREAM("is_feat_des_rec_ ="<<palm_servo.is_feat_des_rec_
	<<"is_feat_fb_rec_"<<palm_servo.is_feat_fb_rec_ );
      }

      // eigen matr of feat err
      VECTOR6_1 m_err_f = palm_servo.vec2VECTOR6_1( err_feats );
      ROS_DEBUG_STREAM("m_err_f ="
      <<m_err_f(0)
      <<";"<<m_err_f(1)
      <<";"<<m_err_f(2)
      <<";"<<m_err_f(3)
      <<";"<<m_err_f(4)
      <<";"<<m_err_f(5));
      
      /** In order to compensate the error in feature space
       *the error must be substracted**/  
      VECTOR6_1 m_err_f_neg = m_err_f * (-1.0);
      /** not for force**/
      m_err_f_neg(2) = m_err_f_neg(2)*(-1.0);
      ROS_DEBUG_STREAM("m_err_f ="
      <<m_err_f_neg(0)
      <<";"<<m_err_f_neg(1)
      <<";"<<m_err_f_neg(2)
      <<";"<<m_err_f_neg(3)
      <<";"<<m_err_f_neg(4)
      <<";"<<m_err_f_neg(5));
      
      /** choose old cop is used to rotate or new approach zmp
       * if (servo_zmp == 1)
       * VECTOR6_1 m_err_f_zmp = palm_servo.convert2ZMP ( m_err_f );
       * if (servo_zmp == 0)
       * VECTOR6_1 m_err_f_zmp = palm_servo.convert2nonzmp ( m_err_f );
       * **/
      // matrix with zmp error
      VECTOR6_1 m_err_f_zmp = palm_servo.convert2ZMP ( m_err_f_neg );
      ROS_DEBUG_STREAM("m_err_f_zmp ="
      <<m_err_f_zmp(0)
      <<";"<<m_err_f_zmp(1)
      <<";"<<m_err_f_zmp(2)
      <<";"<<m_err_f_zmp(3)
      <<";"<<m_err_f_zmp(4)
      <<";"<<m_err_f_zmp(5));
	   
      std::vector<double> err_feats_zmp = palm_servo.VECTOR6_1tovec(m_err_f_zmp);
      ROS_DEBUG_STREAM("err_feats_zmp ="
      <<err_feats_zmp.at(0)
      <<";"<<err_feats_zmp.at(1)
      <<";"<<err_feats_zmp.at(2)
      <<";"<<err_feats_zmp.at(3)
      <<";"<<err_feats_zmp.at(4)
      <<";"<<err_feats_zmp.at(5));
            ROS_FATAL_STREAM("err_feats_zmp ="
      <<err_feats_zmp.at(0)
      <<";"<<err_feats_zmp.at(1)
      <<";"<<err_feats_zmp.at(2)
      <<";"<<err_feats_zmp.at(3)
      <<";"<<err_feats_zmp.at(4)
      <<";"<<err_feats_zmp.at(5));
      
      // pid compute get pid.out
      std::vector<double> cmdedd_pos_err;
      cmdedd_pos_err = servo_pids.compute_cmd(err_feats_zmp, dt);
      ROS_DEBUG_STREAM("cmdedd_pos_err ="
      <<cmdedd_pos_err.at(0)
      <<";"<<cmdedd_pos_err.at(1)
      <<";"<<cmdedd_pos_err.at(2)
      <<";"<<cmdedd_pos_err.at(3)
      <<";"<<cmdedd_pos_err.at(4)
      <<";"<<cmdedd_pos_err.at(5));
      ROS_FATAL_STREAM("cmdedd_pos_err ="
      <<cmdedd_pos_err.at(0)
      <<";"<<cmdedd_pos_err.at(1)
      <<";"<<cmdedd_pos_err.at(2)
      <<";"<<cmdedd_pos_err.at(3)
      <<";"<<cmdedd_pos_err.at(4)
      <<";"<<cmdedd_pos_err.at(5));
      
      //pid.out matrix 
      VECTOR6_1 cmdedd_pos_err_m = palm_servo.vec2VECTOR6_1(cmdedd_pos_err);
      ROS_DEBUG_STREAM("cmdedd_pos_err_m ="
      <<cmdedd_pos_err_m(0)
      <<";"<<cmdedd_pos_err_m(1)
      <<";"<<cmdedd_pos_err_m(2)
      <<";"<<cmdedd_pos_err_m(3)
      <<";"<<cmdedd_pos_err_m(4)
      <<";"<<cmdedd_pos_err_m(5));
      ROS_FATAL_STREAM("cmdedd_pos_err_m ="
      <<cmdedd_pos_err_m(0)
      <<";"<<cmdedd_pos_err_m(1)
      <<";"<<cmdedd_pos_err_m(2)
      <<";"<<cmdedd_pos_err_m(3)
      <<";"<<cmdedd_pos_err_m(4)
      <<";"<<cmdedd_pos_err_m(5));
 
      //select what to servo matrix
      VECTOR6_1 sel_feats = palm_servo.select_feats(cmdedd_pos_err_m);
      ROS_DEBUG_STREAM("sel_feats ="
      <<sel_feats(0)
      <<";"<<sel_feats(1)
      <<";"<<sel_feats(2)
      <<";"<<sel_feats(3)
      <<";"<<sel_feats(4)
      <<";"<<sel_feats(5));
            ROS_FATAL_STREAM("sel_feats ="
      <<sel_feats(0)
      <<";"<<sel_feats(1)
      <<";"<<sel_feats(2)
      <<";"<<sel_feats(3)
      <<";"<<sel_feats(4)
      <<";"<<sel_feats(5));
      //////////////////////////////////
      //////////////////////////////////
      //////////////////////////////////
      // do the joint controll 
      // calculate the new joint angles 
      // for wrj1 wrj2 er
      
      
      
      double er_zmpy, wrj1_zmpx, wrj2_orientz;
      wrj2_orientz = sel_feats(5) + joint_controls.wrj2_bye_now_;
      ROS_FATAL_STREAM("sel_feats(5) = "<< sel_feats(5));
      ROS_FATAL_STREAM("wrj2_bye_now_ = "<< joint_controls.wrj2_bye_now_);
      ROS_FATAL_STREAM("wrj2_orientz = "<< wrj2_orientz);     
      wrj1_zmpx = sel_feats(3) + joint_controls.wrj1_push_now_;
      ROS_FATAL_STREAM("sel_feats(3) = "<< sel_feats(3));
      ROS_FATAL_STREAM("wrj1_push_now_ = "<< joint_controls.wrj1_push_now_);
      ROS_FATAL_STREAM("wrj1_zmpx = "<< wrj1_zmpx);     
      
      er_zmpy = sel_feats(4) + joint_controls.elbow_rotate_now_;
      ROS_FATAL_STREAM("sel_feats(4) = "<< sel_feats(4));
      ROS_FATAL_STREAM("elbow_rotate_now_ = "<< joint_controls.elbow_rotate_now_);
      ROS_FATAL_STREAM("er_zmpy = "<< er_zmpy);     
      
      

      
      // get current pose  geometry_msgs::PoseStamped current_pose
      geometry_msgs::PoseStamped current_pose = palm_servo.get_pose();
      
      if (palm_servo.is_pose_rec_ == 0)
      {
	ROS_DEBUG_STREAM("current_pose isn't received"<< current_pose.pose.orientation.x);
      }
      
      // to be removed to test pose movments
      if (test_pose_intered_once == 0)
      {
// 	pose_test1 = current_pose;
	test_pose_intered_once = 1;
      }
      
      //palm position to matrix 3x1 xyz 
      Eigen::Vector3d cur_posit_m = palm_servo.msgpose2VECTOR3_1( current_pose );
      ROS_DEBUG_STREAM("cur_pose_m ="
      <<cur_posit_m(0)
      <<";"<<cur_posit_m(1)
      <<";"<<cur_posit_m(2));
      //palm orientation to quaternion 
      tf::Quaternion cur_orien_q = palm_servo.pose2quat( current_pose );
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("=check rpy quaternion=");
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("current_orientation ="
      <<current_pose.pose.orientation.x
      <<";"<<current_pose.pose.orientation.y
      <<";"<<current_pose.pose.orientation.z
      <<";"<<current_pose.pose.orientation.w);
      ROS_DEBUG_STREAM("cur_orien_q ="
      <<cur_orien_q.getX()
      <<";"<<cur_orien_q.getY()
      <<";"<<cur_orien_q.getZ()
      <<";"<<cur_orien_q.getW());
      
      /**VIZ POSE NOW**/
      pose_viz.send_pose_now(current_pose);
      /**VIZ POSE NOW**/
      
      /**void setRotation(const Quaternion& q) in Matrix3x3.h**/
      tf::Matrix3x3 test_cur(cur_orien_q);
      double roll, pitch, yaw;
      test_cur.getRPY(roll, pitch,yaw);
      ROS_DEBUG_STREAM("cur_RPY ="<<roll<<";"<<pitch<<";"<<yaw);
      tf::Quaternion q;
      q.setRPY(roll, pitch, yaw);
      ROS_DEBUG_STREAM("the same quat after RPY ="
      <<q.getX()<<";"<<q.getY()<<";"<<q.getZ()<<";"<<q.getW());
      
      /**VIZ POSE NOW2**/
      geometry_msgs::PoseStamped current_pose_for_back;
      current_pose_for_back = current_pose;
      current_pose_for_back.pose.orientation.x = q.getX();
      current_pose_for_back.pose.orientation.y = q.getY();
      current_pose_for_back.pose.orientation.z = q.getZ();
      current_pose_for_back.pose.orientation.w = q.getW();
      pose_viz.send_pose_now2(current_pose_for_back);
      /**VIZ POSE NOW2**/
      
      tf::Matrix3x3 go_back_RPY(q);
      roll, pitch, yaw;
      go_back_RPY.getRPY(roll, pitch,yaw);
      ROS_DEBUG_STREAM("back_RPY ="<<roll<<";"<<pitch<<";"<<yaw);
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("=check rpy quaternion=");
      ROS_DEBUG_STREAM("======================");
      
     
      // Rot1: base to palm in Eigen M3x3 derived from quaternion
      Eigen::Matrix3d rot_base2palm_now = palm_servo.quat2eigen( cur_orien_q );
      
      // Rot2: palm to weiss in Eigen M3x3
      // TODO at instantiation of the class read from tf and safe
      
      tf::Quaternion q_q2rot_palm2weiss;
      q_q2rot_palm2weiss.setX(0.5);
      q_q2rot_palm2weiss.setY(-0.5);
      q_q2rot_palm2weiss.setZ(0.5);
      q_q2rot_palm2weiss.setW(0.5);

      Eigen::Matrix3d rot_palm2weiss = palm_servo.quat2eigen(q_q2rot_palm2weiss);
      
      /** Position (translational component) 
       * [T'] = [T] + [Rot1]*[Rot2][F]
       * [F] = {copx; copy; force}
       * [Rot2] = rotation matrix palm2weiss, Matrix [3x3]
       *  tf::Quaternion [4x1] -> tf::rot_matr[3x3] ->
       * -> Eigen::Matrix3d [3x3], i.e. current orientation 
       * [Rot1] = rot_matr_base2palm
       * [T] = translation base2palm, i.e. current position = 
       * {cur_pose_m(0); cur_pose_m(1); cur_pose_m(2)}
       * [T'] = desired pose 
      **/
      
      Eigen::Matrix3d eig_rot_base2weiss = palm_servo.AxB(rot_base2palm_now, rot_palm2weiss);
      
      Eigen::Vector3d feat_err_pos  = palm_servo.feat_err_pos(sel_feats);

      Eigen::Vector3d rot_matr_err_wrt_base = palm_servo.AxVector(eig_rot_base2weiss, feat_err_pos);

      Eigen::Vector3d des_position = palm_servo.sumAB(cur_posit_m, rot_matr_err_wrt_base);

      // Desired pose. Set desired position
      // position changed according to applied force or COPx or COPy
      geometry_msgs::PoseStamped X_des;
      X_des.header.stamp = ros::Time::now();
      X_des.header.frame_id = "desired_pose";
      palm_servo.set_posit_des_pose(X_des, des_position);
      ROS_DEBUG_STREAM("X_des"<< X_des.pose.position.x
      <<";"<< X_des.pose.position.y
      <<";"<< X_des.pose.position.z
      <<";"<< X_des.pose.orientation.x);
      
      Eigen::Vector3d err_orient = palm_servo.feat_err_rot(sel_feats);
      ROS_DEBUG_STREAM("err_orient ="<< err_orient(0)
	<<";"<<err_orient(1)<<";"<<err_orient(2));
      
      Eigen::Vector3d err_orient_rearr = palm_servo.rpy2xyz(err_orient);
      ROS_DEBUG_STREAM("err_orient_rearr ="<< err_orient_rearr(0)
	<<";"<<err_orient_rearr(1)<<";"<<err_orient_rearr(2));
      tf::Matrix3x3 rot_m_err_wrt_sensor = palm_servo.vec2rot(err_orient_rearr);
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("=check rpy quaternion=");
      ROS_DEBUG_STREAM("======================");

      rot_m_err_wrt_sensor.getRPY(roll, pitch,yaw);
      ROS_DEBUG_STREAM("from err_orient_rearr I made matrix of rotation."
      << "should be the same as error feats rearr err RPY  ="<<roll<<";"<<pitch<<";"<<yaw);
      ROS_DEBUG_STREAM("in my function I use unnecessary step."<<
      "I set quaternion and then tf matrix. now without quaternion");
      tf::Matrix3x3 rot_matr_test(q);
      rot_matr_test.setRPY(err_orient_rearr(0), err_orient_rearr(1), err_orient_rearr(2));
      rot_matr_test.getRPY(roll, pitch,yaw);
      ROS_DEBUG_STREAM("rpy as in above withou going though quaternion"
      <<roll<<";"<<pitch<<";"<<yaw);
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("=check rpy quaternion=");
      ROS_DEBUG_STREAM("======================");
      /** Orientation (rotational component)
       * [Rot'] = [rot_delta]^-1 * [Rot1]
       * [Rot1] = rot_base2palm_now
       * [rot_delta] = rot matrix [3x3] derrived from
       *   RPY derrived from feature error in orientation [F]
       *   = {zmpx, zmpy, orientation} = rot_m_err_wrt_sensor
       **/
      
      //[rot_delta]^-1
      tf::Matrix3x3 rot_err_wrtsensor_inv = rot_m_err_wrt_sensor.transpose();
      // to eigen since Rot1 is in eigen form
      Eigen::Matrix3d rot_err_wrtsensor_inv_e;
      tf::matrixTFToEigen(rot_err_wrtsensor_inv, rot_err_wrtsensor_inv_e);
      
      // [Rot1] *[rot_delta]^-1 
      Eigen::Matrix3d rot_base2palm_des_e = palm_servo.AxB(rot_base2palm_now,
							 rot_err_wrtsensor_inv_e );
      
      // to tf in order to set quaternion to pose desired
      tf::Matrix3x3 rot_base2palm_des;
      tf::matrixEigenToTF(rot_base2palm_des_e, rot_base2palm_des);
      double roll_des, pitch_des, yaw_des;
      rot_base2palm_des.getRPY(roll_des, pitch_des, yaw_des);
      ROS_DEBUG_STREAM("RPY_des"<< roll_des<<";"<<pitch_des<<";"<<yaw_des);
      
      palm_servo.set_orient_des_pose(X_des, rot_base2palm_des);
      ROS_DEBUG_STREAM("X_des"<< X_des.pose.position.x
      <<";"<< X_des.pose.position.y
      <<";"<< X_des.pose.position.z
      <<";"<< X_des.pose.orientation.x
      <<";"<< X_des.pose.orientation.y
      <<";"<< X_des.pose.orientation.z
      <<";"<< X_des.pose.orientation.w);
      
      /**VIZ POSE DES**/
      pose_viz.send_pose_des(X_des);
      /**VIZ POSE DES**/
      
      ///////////////////////////
      //Test without rearranching error feature RPY to RYP
      ///////////////////////////
      ROS_DEBUG_STREAM("======================");
      ROS_DEBUG_STREAM("=check without rearranging=");
      ROS_DEBUG_STREAM("======================");
      
       tf::Matrix3x3 rot_m_err_wrt_sensor_test = palm_servo.vec2rot(err_orient);
       
      rot_m_err_wrt_sensor_test.getRPY(roll, pitch,yaw);
      ROS_DEBUG_STREAM("from err_orient_rearr I made matrix of rotation."
      << "should be the same as error feats rearr err RPY  ="<<roll<<";"<<pitch<<";"<<yaw);


      //[rot_delta]^-1
      tf::Matrix3x3 rot_err_wrtsensor_inv_test = rot_m_err_wrt_sensor_test.transpose();
      // to eigen since Rot1 is in eigen form
      Eigen::Matrix3d rot_err_wrtsensor_inv_e_test;
      tf::matrixTFToEigen(rot_err_wrtsensor_inv_test, rot_err_wrtsensor_inv_e_test);
      
      // [Rot1] *[rot_delta]^-1 
      Eigen::Matrix3d rot_base2palm_des_e_test = palm_servo.AxB(rot_base2palm_now,
							 rot_err_wrtsensor_inv_e_test );
      
      // to tf in order to set quaternion to pose desired
      tf::Matrix3x3 rot_base2palm_des_test;
      tf::matrixEigenToTF(rot_base2palm_des_e_test, rot_base2palm_des_test);
      rot_base2palm_des_test.getRPY(roll_des, pitch_des, yaw_des);
      ROS_DEBUG_STREAM("RPY_des_test"<< roll_des<<";"<<pitch_des<<";"<<yaw_des);
      
      geometry_msgs::PoseStamped X_des_test = X_des;
      
      palm_servo.set_orient_des_pose(X_des_test, rot_base2palm_des_test);
      ROS_DEBUG_STREAM("X_des_test"<< X_des_test.pose.position.x
      <<";"<< X_des_test.pose.position.y
      <<";"<< X_des_test.pose.position.z
      <<";"<< X_des_test.pose.orientation.x
      <<";"<< X_des_test.pose.orientation.y
      <<";"<< X_des_test.pose.orientation.z
      <<";"<< X_des_test.pose.orientation.w);
      
      
      if (servo_reconf.choose_configuration_ == 0)
      {
	new_pose = X_des;
	ROS_DEBUG_STREAM("without rearranching ");
      }
      else
      {
	new_pose = X_des_test;
	ROS_DEBUG_STREAM("with rearranching ");

      }
            
      /**VIZ POSE DES**/
      pose_viz.send_pose_des2(X_des_test);
      /**VIZ POSE DES**/
      
      geometry_msgs::PoseStamped X_contact  = pose_viz.set_contact_frame(palm_servo.cocx_fb,
	palm_servo.cocy_fb,
	palm_servo.force_fb,
	palm_servo.zmp_x_fb,
	palm_servo.zmp_y_fb,
	palm_servo.orientz_fb);
    /*  
      ROS_FATAL_STREAM("compliance = "<< pose_viz.compliance_);
      ROS_FATAL_STREAM("hight = "<< pose_viz.hight_of_sensor_);

      ROS_FATAL_STREAM("force_fb = "<< palm_servo.force_fb);
      ROS_FATAL_STREAM("X_contact.z = "<< X_contact.pose.position.z);
      */

      /**VIZ POSE CONTACT**/
      pose_viz.send_pose_contact_frame(X_contact);
      /**VIZ POSE CONTACT**/
     
//       if (myFloat != myFloat)




        
      if ((servo_reconf.begin_control_ == 1)&&(palm_servo.contours_num_ > 0))
      {
	ROS_DEBUG_STREAM(" controller started ");
	ROS_FATAL_STREAM(" move ");
	ROS_FATAL_STREAM(" wrj2_orientz " << wrj2_orientz);
	ROS_FATAL_STREAM(" wrj1_zmpx " << wrj1_zmpx);
	ROS_FATAL_STREAM(" er_zmpy " << er_zmpy);

	joint_controls.publish_joint_cmds(wrj2_orientz, wrj1_zmpx, er_zmpy);

// 	palm_servo.send_pose(new_pose);
      }
      
      ros::spinOnce();
      ROS_DEBUG("after last spin");
      
      loop_rate.sleep();
      
    }
    return 0;
}