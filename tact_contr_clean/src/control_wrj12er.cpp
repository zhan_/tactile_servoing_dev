
#include "tact_contr_clean/control_wrj12er.hpp"

ControlWrj12Er::ControlWrj12Er()
{
// get the names of the controllers
    ros::service::waitForService("/sr_controller_manager/list_controllers");
    ROS_INFO("connected to sr_controller_manager");
    controller_list_client = n.serviceClient<controller_manager_msgs::ListControllers>("/sr_controller_manager/list_controllers");
    ROS_INFO("got service response");
    // initialize the joints to be controlled
    // init treated joint (to be modified to get more generic behaviour)
   
    
    joint_name.push_back("ElbowJRotate");
    joint_name.push_back("ElbowJSwing");
    joint_name.push_back("ShoulderJSwing");
    joint_name.push_back("ShoulderJRotate");
    joint_name.push_back("WRJ1");
    joint_name.push_back("WRJ2");
    
    // init jointControllerMapping
    
    controller_list_client.call(controller_list);
    for (unsigned int i=0;i<controller_list.response.controller.size() ;i++ )
    {
      if (n.getParam("/"+controller_list.response.controller[i].name + "/joint", controlled_joint_name))
      {
	ROS_INFO("controller %d:%s controls joint %s\n",i,controller_list.response.controller[i].name.c_str(),controlled_joint_name.c_str());
	jointControllerMap[controlled_joint_name]= controller_list.response.controller[i].name ;
      }
    }      
    
    // prepare the publishers
    controller_name=jointControllerMap["WRJ1"];
    if(controller_name.compare("")!=0)
    {
      pub_wrj1_push = n.advertise<std_msgs::Float64>("/"+jointControllerMap["WRJ1"]+"/command", 2);
      sub_wrj1_push = n.subscribe("/"+jointControllerMap["WRJ1"] + "/state", 1,  &ControlWrj12Er::cb_wrj1_push_state,this);
    }
    else
    {
      ROS_WARN("Could not find a controller for joint %s","WRJ1");
    }
    
    controller_name=jointControllerMap["WRJ2"];
    if(controller_name.compare("")!=0)
    {
      pub_wrj2_bye = n.advertise<std_msgs::Float64>("/"+jointControllerMap["WRJ2"]+"/command", 2);
      sub_wrj2_bye = n.subscribe("/"+jointControllerMap["WRJ2"] + "/state", 1,  &ControlWrj12Er::cb_wrj2_bye_state,this);
    }
    else
    {
      ROS_WARN("Could not find a controller for joint %s","WRJ2");
    }
    
    controller_name=jointControllerMap["ElbowJRotate"];
    if(controller_name.compare("")!=0)
    {
      pub_wrj2_bye = n.advertise<std_msgs::Float64>("/"+jointControllerMap["ElbowJRotate"]+"/command", 2);
      sub_elbow_rotate = n.subscribe("/"+jointControllerMap["ElbowJRotate"] + "/state", 1,  &ControlWrj12Er::cb_elbow_rotate_state,this);
    }
    else
    {
      ROS_WARN("Could not find a controller for joint %s","ElbowJRotate");
    }
    

    

}

ControlWrj12Er::~ControlWrj12Er()
{

}


void ControlWrj12Er::publish_joint_cmds(double& orientz, double& orientx, double& orienty)
{

  if(pub_wrj2_bye!=NULL)
  {
    std_msgs::Float64 message;
    ROS_DEBUG("we publish to %s", pub_wrj2_bye.getTopic().c_str());
    message.data= (double) orientz;
    pub_wrj2_bye.publish(message);
  }
  
   if(pub_wrj1_push!=NULL)
  {
    std_msgs::Float64 message;
    
    ROS_DEBUG("we publish to %s", pub_wrj2_bye.getTopic().c_str());
    message.data= (double) orientx ;
    pub_wrj1_push.publish(message);

  }
  
     if(pub_elbow_rotate!=NULL)
  {
    std_msgs::Float64 message;
    
    ROS_DEBUG("we publish to %s", pub_wrj2_bye.getTopic().c_str());
    message.data= (double) orienty;
    pub_elbow_rotate.publish(message);

  }

}

void ControlWrj12Er::cb_wrj1_push_state(const  sr_robot_msgs::JointControllerState& msg)
{

   wrj1_push_now_ = msg.process_value;
   ROS_DEBUG_STREAM (" wrj1_push_now_ = "<< wrj1_push_now_);
   iswrj1_push_now_ = true;
   return;

}

void ControlWrj12Er::cb_wrj2_bye_state(const  sr_robot_msgs::JointControllerState& msg)
{

   wrj2_bye_now_ = msg.process_value;
   ROS_DEBUG_STREAM (" wrj2_bye_now_ = "<< wrj2_bye_now_);
   iswrj2_bye_now_ = true;
   return;

}

void ControlWrj12Er::cb_elbow_rotate_state(const  pr2_controllers_msgs::JointControllerState& msg)
{

   elbow_rotate_now_ = msg.process_value;
   ROS_DEBUG_STREAM (" elbow_rotate_now_ = "<< elbow_rotate_now_);
   iselbow_rotate_now_ = true;
   return;

}







