// how to use control toolbox?
// There is pid gain set, computecommand and dynamci reconfigure
#include <ros/ros.h>

#include <control_toolbox/pid.h>
#include <std_srvs/Empty.h>

#include <ros/console.h>

/* Single pid commands are similar to those that are in 
pid.h and in tutorials for pr2+add+pid and in sr_mechanism_controllers
*/

class SinglePID
{
public:
  SinglePID(std::string link, std::string axis );
  ~SinglePID();
  control_toolbox::Pid pid_servo_;
  ros::Time time_of_last_cycle_;
  
  //initalize pid with adding link_name and axis init(n, "names")
  bool init(ros::NodeHandle &n);
  //read all the controller settings from the parameter server
  void read_params();
  void setGains();
  void getGains(double &p, double &i, double &d, double &i_max, double &i_min);
  void starting();
  double compute_cmd(double err_position, ros::Duration dt);
  void reset();

  double position_deadband_;

  // We're using an hysteresis deadband.
  // sr_deadband::HysteresisDeadband<double> hysteresis_deadband;

  double p_,
  i_,
  d_,
  i_clamp_;
  
  double out_;
  std::string link_;
  std::string axis_;
  
private:
  ros::NodeHandle node_;
};

SinglePID::SinglePID(std::string link, std::string axis)
  : position_deadband_(0.015)
{
  axis_ = axis;
  link_ = link;
}

SinglePID::~SinglePID(){};

bool SinglePID::init(ros::NodeHandle &n)
{
  node_ = n;
  std::string nmespace = n.getNamespace();
  ROS_INFO_STREAM("the name of the namespace inside single pid = "<< nmespace);

  if (!pid_servo_.init(ros::NodeHandle(node_, link_+"/"+axis_))){
    ROS_FATAL_STREAM("Can not construct PID controller for"+link_+axis_);
    return false;
  }
}

void SinglePID::read_params()
{
    if (node_.hasParam("/servo_pid/"+link_+"/"+axis_))
  {
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/p", p_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/i", i_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/d", d_);
    node_.getParam("/servo_pid/"+link_+"/"+axis_+"/i_clamp", i_clamp_);
  }
  else
  {
    ROS_FATAL_STREAM("No parameters for /servo_pid/"+link_+"/"+axis_);
  }
}

void SinglePID::setGains()
{
  pid_servo_.setGains(p_, i_, d_, i_clamp_, -i_clamp_);
}

void SinglePID::getGains(double &p, double &i, double &d, double &i_max, double &i_min)
{
  pid_servo_.getGains(p, i, d, i_max, i_min);
}

void SinglePID::starting()
{
  pid_servo_.reset();
  read_params();
  setGains();
}

void SinglePID::reset()
{
  pid_servo_.reset();
}

double SinglePID::compute_cmd(double err_position, ros::Duration dt)
{
   out_ = pid_servo_.computeCommand(err_position, dt);
   return out_;
}


class ServoPidManager
{
public:
  ServoPidManager();
  ~ServoPidManager();
  
  std::vector<SinglePID*> palm_pids;
  std::vector<double> out_;
  
  bool init(ros::NodeHandle &n);
  void readparams();
  void setGains();
  void starting();
  std::vector<double> compute_cmd(std::vector<double> err_position, 
				  ros::Duration dt);
  void reset();
private:
  ros::NodeHandle node_;
};

ServoPidManager::ServoPidManager()
{
  std::vector<std::string> link_names;
  if (node_.hasParam("/link_names_to_servo"))
  {
    node_.getParam("/link_names_to_servo",link_names);
  }
  else
  {
    link_names.push_back("palm");
  }
  
    std::vector<std::string> names_axes;
    names_axes.push_back("x");
    names_axes.push_back("y");
    names_axes.push_back("z");
    names_axes.push_back("wx");
    names_axes.push_back("wy");
    names_axes.push_back("wz");
    
    for( int i=0; i< link_names.size(); ++i)
    {
      for( int j=0; j< names_axes.size(); ++j)
      {
	palm_pids.push_back(new SinglePID(link_names.at(i),names_axes.at(j)));
      }
    }
}

ServoPidManager::~ServoPidManager(){}

bool ServoPidManager::init(ros::NodeHandle &n)
{
  
  std::string nmespace = n.getNamespace();
  ROS_INFO_STREAM("the name of the namespace is = " << nmespace);

//   std::string ndename = n.getName();
//   ROS_INFO_STREAM("the name of the namespace is = "<< ndename);
//   
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->init(n);
  }
}

void ServoPidManager::readparams()
{
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->read_params();
  }
}

void ServoPidManager::setGains()
{
  for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->setGains();
  }  
}

void ServoPidManager::starting()
{
    for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->starting();
  } 
}

std::vector<double> ServoPidManager::compute_cmd(std::vector<double> err_position, 
						 ros::Duration dt)
{
  std::vector<double> out_temp;
  for( int i=0; i< palm_pids.size(); ++i)
  {
    out_temp.push_back(palm_pids.at(i)->compute_cmd(err_position.at(i), dt));
  }
  out_ = out_temp;
  return out_temp;
}

void ServoPidManager::reset()
{
    for( int i=0; i< palm_pids.size(); ++i)
  {
    palm_pids.at(i)->reset();
  } 
}

// example of using servo pid class
int main(int argc, char** argv)
{
    ros::init(argc, argv, "learn_control_toolbox");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    std::string nmespace = n.getNamespace();
    ROS_INFO_STREAM("the name of the namespace after init node is = "<< nmespace);

    ROS_INFO_STREAM("big pids instantiated");
    ServoPidManager palm_pid;
    
  
    ROS_INFO_STREAM("big pid init");
    palm_pid.init(n);
    

    ROS_INFO_STREAM("pid1/p_=" << palm_pid.palm_pids.at(0)->p_);
    ROS_INFO_STREAM("pid1/d_=" << palm_pid.palm_pids.at(0)->d_);
    
//     ROS_INFO_STREAM("ptr pid1/p_=" << palm_pid.palm_pids_ptrs.at(0)->p_);
//     ROS_FATAL_STREAM(" ptr pid1/d_=" << palm_pid.palm_pids_ptrs.at(0)->d_);

    double a, b, c, d, e;
    palm_pid.palm_pids.at(0)->getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a=" << a);
    ROS_FATAL_STREAM("b=" << b);

//     palm_pid.setGains();
    
    palm_pid.palm_pids.at(0)->getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a after setting gains =" << a);
    
    ros::Time last_cycle_;
    int first_entry = 0;
    
    while( ros::ok() )
    {
      if (first_entry == 0)
      {
	last_cycle_ = ros::Time::now();
	first_entry = 1;
	ROS_INFO("once");
      }
      else
      {
        ros::Duration dt = ros::Time::now() - last_cycle_;
	std::vector<double> vec_cmdedd_pos_err;
	std::vector<double> vec_err; // this is error = target - state
	for( int i=0; i< palm_pid.palm_pids.size(); ++i)
	{
	  vec_err.push_back(2.0);
	}	
	vec_cmdedd_pos_err = palm_pid.compute_cmd(vec_err, dt);
// 	ROS_INFO_STREAM("cmdedd_pos_err at x =" << vec_cmdedd_pos_err.at(0));
// 	double a, b, c, d, e;
// 	palm_pid.palm_pids.at(0).getGains(a, b, c, d, e);
// 	ROS_FATAL_STREAM("a after setting gains =" << a);
	last_cycle_ = ros::Time::now();
// 	ROS_INFO("continue");
      }
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
/* //for single pid test
int main(int argc, char** argv)
{
    ros::init(argc, argv, "learn_control_toolbox");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    
//     ROS_INFO_STREAM("single pid instantiated");
//     SinglePID pid1("palm", "x");
    
//     ROS_INFO_STREAM("single pid init");
//     pid1.init(n);
    
//     pid1.read_params();
//     ROS_INFO_STREAM("pid1/p_=" << pid1.p_);
//     ROS_INFO_STREAM("pid1/d_=" << pid1.d_);

    double a, b, c, d, e;
//     pid1.getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a=" << a);
    
//     pid1.setGains();
    
//     pid1.getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a after setting gains =" << a);
   
    ros::Time last_cycle_;
    int first_entry = 0;
    
    while( ros::ok() )
    {
      if (first_entry == 0)
      {
	last_cycle_ = ros::Time::now();
	first_entry = 1;
	ROS_INFO("once");
      }
      else
      {
        ros::Duration dt = ros::Time::now() - last_cycle_;
	double cmdedd_pos_err;
	std::vector<double> vec_cmdedd_pos_err;
// 	cmdedd_pos_err = pid1.pid_servo_.computeCommand(2.0, dt);
// 	ROS_INFO_STREAM("cmdedd_pos_err=" << cmdedd_pos_err);
	last_cycle_ = ros::Time::now();
// 	ROS_INFO("continue");
      }
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
} */
/* // with pointers
int main(int argc, char** argv)
{
    ros::init(argc, argv, "learn_control_toolbox");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    
    ROS_INFO_STREAM("big pids instantiated");
    ServoPidManager palm_pid;
  
    ROS_INFO_STREAM("big pid init");
    palm_pid.init(n);
    

//     ROS_INFO_STREAM("ptr pid1/p_=" << palm_pid.palm_pids_ptrs.at(0)->p_);
//     ROS_FATAL_STREAM(" ptr pid1/d_=" << palm_pid.palm_pids_ptrs.at(0)->d_);

    double a, b, c, d, e;
    palm_pid.palm_pids.at(0)->getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a=" << a);
    
    palm_pid.setGains();
    
    palm_pid.palm_pids.at(0)->getGains(a, b, c, d, e);
    ROS_FATAL_STREAM("a after setting gains =" << a);
    
    ros::Time last_cycle_;
    int first_entry = 0;
    
    while( ros::ok() )
    {
      if (first_entry == 0)
      {
	last_cycle_ = ros::Time::now();
	first_entry = 1;
	ROS_INFO("once");
      }
      else
      {
        ros::Duration dt = ros::Time::now() - last_cycle_;
	std::vector<double> vec_cmdedd_pos_err;
	std::vector<double> vec_err;
	for( int i=0; i< palm_pid.palm_pids.size(); ++i)
	{
	  vec_err.push_back(2.0);
	}	
	vec_cmdedd_pos_err = palm_pid.compute_cmd(vec_err, dt);
// 	ROS_INFO_STREAM("cmdedd_pos_err at x =" << vec_cmdedd_pos_err.at(0));
// 	double a, b, c, d, e;
// 	palm_pid.palm_pids.at(0).getGains(a, b, c, d, e);
// 	ROS_FATAL_STREAM("a after setting gains =" << a);
	last_cycle_ = ros::Time::now();
// 	ROS_INFO("continue");
      }
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}*/
/* // clean main
 * int main(int argc, char** argv)
{
  ros::init(argc, argv, "learn_control_toolbox");
  ros::NodeHandle n;
  ros::Rate loop_rate(100);
  ServoPidManager palm_pid;
  while( ros::ok() )
  {
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
*/