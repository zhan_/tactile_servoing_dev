/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
#include "tact_contr_clean/palm_pose.hpp"
// #include "tact_contr_clean/servo_pid.hpp"

#include <tf/tf.h>
#include <tf/transform_listener.h>

#include "tactile_servo_srvs/set_external_motion.h"

//get pose: ServoPalmPose::get_pose() returns geometry_msgs::PoseStamped;
// get features fb
// get features desired


/* apply pid to error
// Controller update loop in realtime
void MyControllerClass::update()
{
  double desired_pos = init_pos_ + amplitude_ * sin(ros::Time::now().toSec());
  double current_pos = joint_state_->position_;

  ros::Duration dt = robot_->getTime() - time_of_last_cycle_;
  time_of_last_cycle_ = robot_->getTime();
  joint_state_->commanded_effort_ = pid_controller_.updatePid(current_pos-desired_pos, dt);
}
 //shadow code
//Compute position demand from position error:
double error_position = 0.0;
double commanded_effort = 0.0;
error_position = joint_state_->position_ - command_;
bool in_deadband = hysteresis_deadband.is_in_deadband(command_, error_position, position_deadband);
//don't compute the error if we're in the deadband.
if (in_deadband) error_position = 0.0; pid_controller_position_->reset();
commanded_effort = pid_controller_position_->computeCommand(-error_position, period);
*/
//transform to cartesian error
// selection matrix
// send pose ServoPalmPose::send_pose(geometry_msgs::PoseStamped)

class LineFollow4Viz
{
public:
  LineFollow4Viz();
  ~LineFollow4Viz();
  void send_pose_line_des(geometry_msgs::PoseStamped msg_sendpose);

private:
    ros::NodeHandle nh;
    ros::Publisher pub_pose_viz__line_follow_des;
};

LineFollow4Viz::LineFollow4Viz()
{
  pub_pose_viz__line_follow_des = 
  nh.advertise<geometry_msgs::PoseStamped>("/line_follow_pose_viz_des", 2);
}

LineFollow4Viz::~LineFollow4Viz(){}

void LineFollow4Viz::send_pose_line_des(geometry_msgs::PoseStamped msg_sendpose)
{
  pub_pose_viz__line_follow_des.publish(msg_sendpose);
}


class LineFollowService
{
public:
  LineFollowService();
  ~LineFollowService();
  
  void send_pose_line_des(geometry_msgs::PoseStamped msg_sendpose);
  
  int isbegin_line_follow_;
  
  bool begin_line_follow(tactile_servo_srvs::begin_control::Request &req,
			 tactile_servo_srvs::begin_control::Response &res);
  
  void transformPoint(geometry_msgs::PointStamped& point_in_, 
		      geometry_msgs::PointStamped& pose_out );
  
  tf::TransformListener tf_listener;
  double delta_z_ ;
  double delta_y_ ;
  double delta_x_ ;
  double delta_wz_ ;
  double delta_wy_ ;
  double delta_wx_ ;
  
  bool set_ext_motion(tactile_servo_srvs::set::Request &req,
                             tactile_servo_srvs::select_matrix::Response &res);
  
private:
  ros::NodeHandle n;
  ros::ServiceServer service_begin_follow;
  ros::ServiceServer service_set_ext_motion;
  

};

LineFollowService::LineFollowService()
{
  delta_z_ = 0.01;
  delta_y_ = 0;
  delta_x_ = 0;
  delta_wz_ = 0.0;
  delta_wy_ = 0;
  delta_wx_ = 0;
  
  service_begin_follow = n.advertiseService("/begin_line_follow", &LineFollowService::begin_line_follow, this);
  service_set_ext_motion = n.advertiseService("/set_ext_motion", &LineFollowService::set_ext_motion, this);
  
  
}

LineFollowService::~LineFollowService(){}


bool LineFollowService::begin_line_follow(tactile_servo_srvs::begin_control::Request &req,
		       tactile_servo_srvs::begin_control::Response &res)
{
  if ( req.begin_control == 0)
  {
    ROS_INFO("stop control");
    isbegin_line_follow_ = req.begin_control;
    res.success = 1;
  }
  else if ( req.begin_control == 1)
  {
    ROS_INFO("start control");
    isbegin_line_follow_ = req.begin_control;
    res.success = 1;

  }
  else
  {
    res.success = 0;
  }
   
  return true;
  
}

void LineFollowService::transformPoint( geometry_msgs::PointStamped& point_in_,
					geometry_msgs::PointStamped& pose_out )
{

    tf_listener.waitForTransform("palm", ros::Time(0), "shadowarm_base",ros::Time(0), "shadowarm_base", ros::Duration(10));

    try
    {
      // transform point in palm frame to a point in base frame
      tf_listener.transformPoint("shadowarm_base", point_in_, pose_out);
      //        ROS_INFO("tactile_contact_world: (%.4f, %.4f. %.4f) -----> tactile_contact_weiss: (%.4f, %.4f, %.4f) at time %.4f",
      //                 point_in_.point.x, point_in_.point.y, point_in_.point.z,
      //                 frame_tactile_contact.point.x, frame_tactile_contact.point.y, frame_tactile_contact.point.z, frame_tactile_contact.header.stamp.toSec());
      
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/base_frame\" to \"/sensor_frame\": %s", ex.what());

    }


}

// get current pose  geometry_msgs::PoseStamped current_pose
geometry_msgs::PoseStamped current_pose = palm_servo.get_pose();


// Rot1: base to palm in Eigen M3x3 derived from quaternion
Eigen::Matrix3d rot_base2palm_now = palm_servo.quat2eigen( cur_orien_q );


int main(int argc, char** argv)
{
    ros::init(argc, argv, "test_transoforms_and_rots");
    ros::NodeHandle n;
    
    
    
    ros::Rate loop_rate(5);
    
    //subsribers to pose and feats and publisher to pose_new
    ServoPalmPose line_follow;
    //visualize pose
    LineFollow4Viz line_follow_vis;
    //service to start
    LineFollowService line_service;
    geometry_msgs::PoseStamped current_pose = line_follow.get_pose();
    geometry_msgs::PoseStamped new_pose_line;
    geometry_msgs::PointStamped line_following_next_point_wrt_palm;
    geometry_msgs::PointStamped line_following_next_point_wrt_base;

    /**in order to move palm among the line, 
     * set the desired orientation to zero
     * new pose of the palm will be
     * current pose + z (x of the weiss sensor)
     * time = 1/hz = 1/5 = 0.2 sec 
     * if I want to move 5 mm per sec 
     * then I should move 1 mm at 0.2 sec 
     * delta position.y =+ 0.001
    
    **/
    
  


    ROS_INFO("line_follow");
      
    ros::Duration(1).sleep();


    

    while( ros::ok() )
    {
      // get current pose  geometry_msgs::PoseStamped current_pose
      current_pose = line_follow.get_pose();
      new_pose_line = current_pose;
      
      line_following_next_point_wrt_palm.header.frame_id = "palm";
      line_following_next_point_wrt_palm.header.stamp = ros::Time();
      line_following_next_point_wrt_palm.point.x = line_service.delta_x;
      line_following_next_point_wrt_palm.point.y = line_service.delta_y;
      line_following_next_point_wrt_palm.point.z = line_service.delta_z;
      
      line_service.transformPoint(line_following_next_point_wrt_palm, line_following_next_point_wrt_base);
      
      new_pose_line.pose.position.z = line_following_next_point_wrt_base.point.z;
      new_pose_line.pose.position.y = line_following_next_point_wrt_base.point.y;
      new_pose_line.pose.position.x = line_following_next_point_wrt_base.point.x;

      
      /**what will be the coordinates of point (x = 0, y = 0, z = 0.001)
       * of palm frame in shadowarm_base
       *  
       **/
      
      
      line_follow_vis.send_pose_line_des(new_pose_line);
      if ((line_follow.contours_num_ > 0)&&(line_service.isbegin_line_follow_ == 1))
      {
	if(std::abs(line_follow.orientz_fb - line_follow.orientz_des) < 0.06)
	{
	  line_follow.send_pose(new_pose_line);
	}
      }
            
      ros::spinOnce();
      loop_rate.sleep();
      
    }
    return 0;
}