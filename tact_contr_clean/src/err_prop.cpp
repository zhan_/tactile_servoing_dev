/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for palm pose tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include "tact_contr_clean/err_prop.hpp"
#include <ros/ros.h>
ErrProp::ErrProp(int tactile_sensor_num_){
  
    des_feats_sub = n.subscribe("/des_feats", 1,  &ErrProp::cb_des_feats_sub,this);
    fb_feats_sub = n.subscribe("/fb_feats", 1,  &ErrProp::cb_fb_feats_sub,this);

    err_prop_pub = n.advertise<tactile_servo_msgs::ErrProp>("/err_propagation", 2 );
  
    copx_des = 0;
    copy_des = 0;
    force_des =0;
    cocx_des=0;
    cocy_des =0;
    orientz_des=0;
    copx_fb=0;
    copy_fb=0;
    force_fb=0;
    cocx_fb=0;
    cocy_fb=0;
    orientz_fb=0;
    zmpx_des = zmpy_des = zmpx_fb = zmpy_fb = 0;
}
ErrProp::~ErrProp(){}
void ErrProp::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_des = msg_->control_features[0].centerpressure_x;
        copy_des = msg_->control_features[0].centerpressure_y;
        force_des = msg_->control_features[0].contactForce;
        cocx_des = msg_->control_features[0].centerContact_x;
        cocy_des = msg_->control_features[0].centerContact_y;
        orientz_des = msg_->control_features[0].contactOrientation;
	zmpx_des = msg_->control_features[0].zmp_x;
	zmpy_des = msg_->control_features[0].zmp_y;
   }
}
void ErrProp::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_fb = msg_->control_features[0].centerpressure_x;
        copy_fb = msg_->control_features[0].centerpressure_y;
        force_fb = msg_->control_features[0].contactForce;
        cocx_fb = msg_->control_features[0].centerContact_x;
        cocy_fb = msg_->control_features[0].centerContact_y;
        orientz_fb = msg_->control_features[0].contactOrientation;
       	zmpx_fb = msg_->control_features[0].zmp_x;
	zmpy_fb = msg_->control_features[0].zmp_y;
    }
}


void ErrProp::send(){
  // cop marker
  
  err.header.frame_id = "weiss";
  err.header.stamp = ros::Time::now();
  err.centerContact_x = cocx_des - cocx_fb;
  err.centerContact_y = cocy_des - cocy_fb;
  err.centerpressure_x = copx_des - copx_fb;
  err.centerpressure_x = copy_des - copy_fb;
  err.contactOrientation = orientz_des - orientz_fb;
  err.contactForce = force_des - force_fb;
  err.zmp_x = zmpx_des - zmpx_fb;
  err.zmp_y = zmpy_des - zmpy_fb;
  
  err_prop_pub.publish(err);
  return;
}
