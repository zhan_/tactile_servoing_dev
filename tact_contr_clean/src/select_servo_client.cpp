#include "ros/ros.h"
#include <cstdlib>
#include "tactile_servo_srvs/select_matrix.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "sle_matr_client");
  if (argc != 7)
  {
    ROS_INFO("usage:  X Y Z wx wy wz ");
    return 1;
  }

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<tactile_servo_srvs::select_matrix>("select_servo_components");
  tactile_servo_srvs::select_matrix srv;
  srv.request.copx = atoll(argv[1]);
  srv.request.copy = atoll(argv[2]);
  srv.request.force = atoll(argv[3]);
  srv.request.cocx = atoll(argv[4]);
  srv.request.cocy = atoll(argv[5]);
  srv.request.orient = atoll(argv[6]);

  if (client.call(srv))
  {
    ROS_INFO("success: %ld", (long int)srv.response.success);
  }
  else
  {
    ROS_ERROR("Failed to call service sel matr");
    return 1;
  }

  return 0;
}