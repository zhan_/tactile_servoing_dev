/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
//#include "tact_contr_clean/tact_contr_clean_node.hpp"
// #ifndef _PALM_POSE_HPP_
// #define _PALM_POSE_HPP_

#include "tact_contr_clean/kuka_palm_pose.hpp"
#include "tact_contr_clean/servo_pid.hpp"

#include "tactile_servo_msgs/tunePid.h"




//get pose: ServoPalmPose::get_pose() returns geometry_msgs::PoseStamped;
// get features fb
// get features desired
// calculate error
// check an hysteresis deadband.
/* apply pid to error
// Controller update loop in realtime
void MyControllerClass::update()
{
  double desired_pos = init_pos_ + amplitude_ * sin(ros::Time::now().toSec());
  double current_pos = joint_state_->position_;

  ros::Duration dt = robot_->getTime() - time_of_last_cycle_;
  time_of_last_cycle_ = robot_->getTime();
  joint_state_->commanded_effort_ = pid_controller_.updatePid(current_pos-desired_pos, dt);
}
 //shadow code
//Compute position demand from position error:
double error_position = 0.0;
double commanded_effort = 0.0;
error_position = joint_state_->position_ - command_;
bool in_deadband = hysteresis_deadband.is_in_deadband(command_, error_position, position_deadband);
//don't compute the error if we're in the deadband.
if (in_deadband) error_position = 0.0; pid_controller_position_->reset();
commanded_effort = pid_controller_position_->computeCommand(-error_position, period);
*/
//transform to cartesian error
// selection matrix
// send pose ServoPalmPose::send_pose(geometry_msgs::PoseStamped)

    ros::Publisher pub_pid_vals;
tactile_servo_msgs::tunePid pid_vals;

    ros::Publisher pub_pid_vals_copx;
tactile_servo_msgs::tunePid pid_vals_copx;
    ros::Publisher pub_pid_vals_copy;
tactile_servo_msgs::tunePid pid_vals_copy;

int read_pose_once = 0;
int main(int argc, char** argv)
{
    ros::init(argc, argv, "tact_contr_clean_node");
    ros::NodeHandle n;
    pub_pid_vals = n.advertise<tactile_servo_msgs::tunePid>("/tune_pid", 2);
    pid_vals.err_in = 0;
    pid_vals.err_out = 0;
    pid_vals.header.frame_id = "pid_force";
        pub_pid_vals_copx = n.advertise<tactile_servo_msgs::tunePid>("/tune_pid_copx", 2);
    pid_vals_copx.err_in = 0;
    pid_vals_copx.err_out = 0;
    pid_vals_copx.header.frame_id = "pid_copx";
        pub_pid_vals_copy = n.advertise<tactile_servo_msgs::tunePid>("/tune_pid_copy", 2);
    pid_vals_copy.err_in = 0;
    pid_vals_copy.err_out = 0;
    pid_vals_copy.header.frame_id = "pid_copy";
/* // read from param and set publish rate example   
    //double publish_freq;
    //publish_freq = 50;
    // read param with giving default value
    //nр.param("publish_frequency", publish_freq, 20.0);
    // ros::Rate publish_rate = ros::Rate(publish_freq);
*/    
    ros::Rate loop_rate(100);
    ROS_INFO("tact_contr_clean_node");
    
    // class with subscribers pose feats and tools
    ServoPalmPoseKuka palm_servo;
    
    // class with pids
    ServoPidManager servo_pids;
    servo_pids.init(n);
    ROS_INFO("pid is set");

//     // class with publisher of pose for viz
    Servo4VizKuka pose_viz;
    ROS_INFO("pose visualizer publishing");

    
    double a, b, c, d, e;
    servo_pids.palm_pids.at(0)->getGains(a, b, c, d, e);
//     ROS_FATAL_STREAM("pid gains ="
//     <<a <<";"<<b <<";"<<c <<";"<<d <<";"<<e);
//     servo_pids.palm_pids.at(1)->getGains(a, b, c, d, e);
//     ROS_FATAL_STREAM("pid gains ="
//     <<a <<";"<<b <<";"<<c <<";"<<d <<";"<<e);
    
    ServoReconfServicesKuka servo_reconf(palm_servo, servo_pids);
    ROS_INFO("selectin matrix is ready");

    
    // ros::Time a  = servo_pids.time_of_last_cycle_;
    ros::Duration dt;
    
    /* this is a new pose of palm
     * because of the issues realated with 
     * singularity. One has to check in RVIZ first
     * and then call the service 
     * bool ServoReconfServices::select_configuration(tactile_servo_srvs::choose_configuration_singularity::Request &req,
     *                        tactile_servo_srvs::choose_configuration_singularity::Response &res)
     * to select 1 or 2. the weiss sensor is attached with 90 90 90 degress
     *wrt palm. When the palm is selected to servo the orientation 
     *of an edge, palm rotates in other axes instead.
     *by default ServoReconfServices::choose_configuration_ 
     * is zero. no rearranging involved
     *                
    */
    geometry_msgs::PoseStamped new_pose;
    geometry_msgs::PoseStamped current_pose; 
    ros::spinOnce();
    ROS_DEBUG("after first spin");

    ros::Duration(0.5).sleep();
    ROS_DEBUG("after  sleep");


    ros::spinOnce();
    ROS_DEBUG("after second spin");


    int first_entry = 0;
    while( ros::ok() ){

      if (first_entry == 0)
      {
	servo_pids.time_of_last_cycle_ = ros::Time::now();
	ros::Duration(0.005).sleep();
	ROS_DEBUG("after get time");
	first_entry = 1;
	ROS_DEBUG("once");
      }
      
      ROS_DEBUG("before dt");
      
      // dt
      dt = ros::Time::now() - servo_pids.time_of_last_cycle_;
      servo_pids.time_of_last_cycle_ = ros::Time::now();
      ROS_DEBUG("after dt");
      // feat err
      std::vector<double> err_feats;
      
      if ((palm_servo.is_feat_des_rec_ == 1) && (palm_servo.is_feat_fb_rec_ == 1))
      {
	err_feats = palm_servo.get_feat_err();
	ROS_DEBUG_STREAM("vec_feat_err ="
	<<err_feats.at(0)
	<<";"<<err_feats.at(1)
	<<";"<<err_feats.at(2)
	<<";"<<err_feats.at(3)
	<<";"<<err_feats.at(4)
	<<";"<<err_feats.at(5));
      }
      else
      {
	ROS_DEBUG_STREAM("is_feat_des_rec_ ="<<palm_servo.is_feat_des_rec_
	<<"is_feat_fb_rec_"<<palm_servo.is_feat_fb_rec_ );
      }
      // eigen matr of feat err
      VECTOR6_1 m_err_f = palm_servo.vec2VECTOR6_1( err_feats );
      ROS_DEBUG_STREAM("m_err_f ="
      <<m_err_f(0)
      <<";"<<m_err_f(1)
      <<";"<<m_err_f(2)
      <<";"<<m_err_f(3)
      <<";"<<m_err_f(4)
      <<";"<<m_err_f(5));
      
      /** In order to compensate the error in feature space
       *the error must be substracted**/  
      VECTOR6_1 m_err_f_neg = m_err_f * (-1.0);
      /** not for force in shadow arm**/
//       m_err_f_neg(2) = m_err_f_neg(2)*(-1.0);
      /** not for orientation in kuka**/
      m_err_f_neg(5) = m_err_f_neg(5)*(-1.0);

      ROS_DEBUG_STREAM("m_err_f =" <<m_err_f_neg(0) <<";"<<m_err_f_neg(1) <<";"<<m_err_f_neg(2) <<";"<<m_err_f_neg(3) <<";"<<m_err_f_neg(4) <<";"<<m_err_f_neg(5));
      
      /** choose old cop is used to rotate or new approach zmp
       * if (servo_zmp == 1)
       * VECTOR6_1 m_err_f_zmp = palm_servo.convert2ZMP ( m_err_f );
       * if (servo_zmp == 0)
       * VECTOR6_1 m_err_f_zmp = palm_servo.convert2nonzmp ( m_err_f );
       * **/
      
      // matrix with zmp error
      VECTOR6_1 m_err_f_zmp = palm_servo.convert2ZMP ( m_err_f_neg );
      ROS_DEBUG_STREAM("m_err_f_zmp =" <<m_err_f_zmp(0) <<";"<<m_err_f_zmp(1) <<";"<<m_err_f_zmp(2) <<";"<<m_err_f_zmp(3) <<";"<<m_err_f_zmp(4) <<";"<<m_err_f_zmp(5));
	   
      std::vector<double> err_feats_zmp = palm_servo.VECTOR6_1tovec(m_err_f_zmp);
      ROS_DEBUG_STREAM("err_feats_zmp ="
      <<err_feats_zmp.at(0) <<";"<<err_feats_zmp.at(1) <<";"<<err_feats_zmp.at(2) <<";"<<err_feats_zmp.at(3) <<";"<<err_feats_zmp.at(4) <<";"<<err_feats_zmp.at(5));
      
      // for pid tuning
      pid_vals.err_in = err_feats_zmp.at(2);
      pid_vals_copx.err_in = err_feats_zmp.at(0);
      pid_vals_copy.err_in = err_feats_zmp.at(1);

      // pid compute get pid.out
      std::vector<double> cmdedd_pos_err;
      cmdedd_pos_err = servo_pids.compute_cmd(err_feats_zmp, dt);
      ROS_DEBUG_STREAM("cmdedd_pos_err =" <<cmdedd_pos_err.at(0) <<";"<<cmdedd_pos_err.at(1) <<";"<<cmdedd_pos_err.at(2) <<";"<<cmdedd_pos_err.at(3) <<";"<<cmdedd_pos_err.at(4) <<";"<<cmdedd_pos_err.at(5));
      
      pid_vals.err_out = cmdedd_pos_err.at(2);
      pid_vals_copx.err_out = cmdedd_pos_err.at(0);
      pid_vals_copy.err_out = cmdedd_pos_err.at(1);
      
      pid_vals.dt = dt.toSec();
      pid_vals_copx.dt = dt.toSec();
      pid_vals_copy.dt = dt.toSec();
      pid_vals.header.stamp = ros::Time::now();
      pid_vals_copx.header.stamp = ros::Time::now();
      pid_vals_copy.header.stamp = ros::Time::now();

      //pid.out matrix 
      VECTOR6_1 cmdedd_pos_err_m = palm_servo.vec2VECTOR6_1(cmdedd_pos_err);
      ROS_DEBUG_STREAM("cmdedd_pos_err_m =" <<cmdedd_pos_err_m(0) <<";"<<cmdedd_pos_err_m(1) <<";"<<cmdedd_pos_err_m(2) <<";"<<cmdedd_pos_err_m(3) <<";"<<cmdedd_pos_err_m(4) <<";"<<cmdedd_pos_err_m(5));
 
      //select what to servo matrix
      VECTOR6_1 sel_feats = palm_servo.select_feats(cmdedd_pos_err_m);
      ROS_DEBUG_STREAM("sel_feats ="
      <<sel_feats(0) <<";"<<sel_feats(1) <<";"<<sel_feats(2) <<";"<<sel_feats(3) <<";"<<sel_feats(4) <<";"<<sel_feats(5));
      
      if (read_pose_once == 0)
      {
    
      // get current pose  geometry_msgs::PoseStamped current_pose
       current_pose = palm_servo.get_pose();
       read_pose_once = 1;
      }
      if (palm_servo.is_pose_rec_ == 0)
      {
	ROS_DEBUG_STREAM("current_pose isn't received"<< current_pose.pose.orientation.x);
      }
      
      //palm position to matrix 3x1 xyz 
      Eigen::Vector3d cur_posit_m = palm_servo.msgpose2VECTOR3_1( current_pose );
      ROS_DEBUG_STREAM("cur_pose_m =" <<cur_posit_m(0) <<";"<<cur_posit_m(1) <<";"<<cur_posit_m(2));
      //palm orientation to quaternion 
      tf::Quaternion cur_orien_q = palm_servo.pose2quat( current_pose );
           

           
      // Rot1: base to palm in Eigen M3x3 derived from quaternion
      Eigen::Matrix3d rot_base2palm_now = palm_servo.quat2eigen( cur_orien_q );
      
      // Rot2: palm to weiss in Eigen M3x3
      // TODO at instantiation of the class read from tf and safe
      
      tf::Quaternion q_q2rot_palm2weiss;
      q_q2rot_palm2weiss.setX(1);
      q_q2rot_palm2weiss.setY(0);
      q_q2rot_palm2weiss.setZ(0);
      q_q2rot_palm2weiss.setW(0);

      Eigen::Matrix3d rot_palm2weiss = palm_servo.quat2eigen(q_q2rot_palm2weiss);
      
      /** Position (translational component) 
       * [T'] = [T] + [Rot1]*[Rot2][F]
       * [F] = {copx; copy; force}
       * [Rot2] = rotation matrix palm2weiss, Matrix [3x3]
       *  tf::Quaternion [4x1] -> tf::rot_matr[3x3] ->
       * -> Eigen::Matrix3d [3x3], i.e. current orientation 
       * [Rot1] = rot_matr_base2palm
       * [T] = translation base2palm, i.e. current position = 
       * {cur_pose_m(0); cur_pose_m(1); cur_pose_m(2)}
       * [T'] = desired pose 
      **/
      
      Eigen::Matrix3d eig_rot_base2weiss = palm_servo.AxB(rot_base2palm_now, rot_palm2weiss);
      
      Eigen::Vector3d feat_err_pos  = palm_servo.feat_err_pos(sel_feats);

      Eigen::Vector3d rot_matr_err_wrt_base = palm_servo.AxVector(eig_rot_base2weiss, feat_err_pos);

      Eigen::Vector3d des_position = palm_servo.sumAB(cur_posit_m, rot_matr_err_wrt_base);

      // Desired pose. Set desired position
      // position changed according to applied force or COPx or COPy
      geometry_msgs::PoseStamped X_des;
      X_des.header.stamp = ros::Time::now();
      X_des.header.frame_id = "link_0";
//       X_des = current_pose;
      palm_servo.set_posit_des_pose(X_des, des_position);
      ROS_DEBUG_STREAM("X_des"<< X_des.pose.position.x <<";"<< X_des.pose.position.y <<";"<< X_des.pose.position.z <<";"<< X_des.pose.orientation.x);
      
      Eigen::Vector3d err_orient = palm_servo.feat_err_rot(sel_feats);
      ROS_DEBUG_STREAM("err_orient ="<< err_orient(0) <<";"<<err_orient(1)<<";"<<err_orient(2));
      
      
      ///////////////////////////
      //without rearranching error feature
      ///////////////////////////
      
      tf::Matrix3x3 rot_m_err_wrt_sensor_test = palm_servo.vec2rot(err_orient);
       
//       rot_m_err_wrt_sensor_test.getRPY(roll, pitch,yaw);
//       ROS_DEBUG_STREAM("from err_orient_rearr I made matrix of rotation."
//       << "should be the same as error feats rearr err RPY  ="<<roll<<";"<<pitch<<";"<<yaw);


      //[rot_delta]^-1
      tf::Matrix3x3 rot_err_wrtsensor_inv_test = rot_m_err_wrt_sensor_test.transpose();
      // to eigen since Rot1 is in eigen form
      Eigen::Matrix3d rot_err_wrtsensor_inv_e_test;
      tf::matrixTFToEigen(rot_err_wrtsensor_inv_test, rot_err_wrtsensor_inv_e_test);
      
      // [Rot1] *[rot_delta]^-1 
      Eigen::Matrix3d rot_base2palm_des_e_test = palm_servo.AxB(rot_base2palm_now,
							 rot_err_wrtsensor_inv_e_test );
      
      // to tf in order to set quaternion to pose desired
      tf::Matrix3x3 rot_base2palm_des_test;
      tf::matrixEigenToTF(rot_base2palm_des_e_test, rot_base2palm_des_test);
      
      double roll_des, pitch_des, yaw_des;
      rot_base2palm_des_test.getRPY(roll_des, pitch_des, yaw_des);
      ROS_DEBUG_STREAM("RPY_des_test"<< roll_des<<";"<<pitch_des<<";"<<yaw_des);
      
      geometry_msgs::PoseStamped X_des_test = X_des;
      
      palm_servo.set_orient_des_pose(X_des_test, rot_base2palm_des_test);
      ROS_DEBUG_STREAM("X_des_test"<< X_des_test.pose.position.x <<";"<< X_des_test.pose.position.y <<";"<< X_des_test.pose.position.z <<";"<< X_des_test.pose.orientation.x <<";"<< X_des_test.pose.orientation.y <<";"<< X_des_test.pose.orientation.z <<";"<< X_des_test.pose.orientation.w);
      
      //for the kuka no any rearranching
      new_pose = X_des_test;
      ROS_DEBUG_STREAM("without rearranching ");

      /**VIZ POSE DES**/
      pose_viz.send_pose_des2(X_des_test);
      /**VIZ POSE DES**/
      
      geometry_msgs::PoseStamped X_contact  = pose_viz.set_contact_frame(palm_servo.cocx_fb, palm_servo.cocy_fb, palm_servo.force_fb, palm_servo.zmp_x_fb, palm_servo.zmp_y_fb, palm_servo.orientz_fb);
    /*  
      ROS_FATAL_STREAM("compliance = "<< pose_viz.compliance_);
      ROS_FATAL_STREAM("hight = "<< pose_viz.hight_of_sensor_);

      ROS_FATAL_STREAM("force_fb = "<< palm_servo.force_fb);
      ROS_FATAL_STREAM("X_contact.z = "<< X_contact.pose.position.z);
      */

      /**VIZ POSE CONTACT**/
      pose_viz.send_pose_contact_frame(X_contact);
      /**VIZ POSE CONTACT**/
     
      if (palm_servo.copx_fb != palm_servo.copx_fb)
      {
	palm_servo.is_feat_fb_rec_ == false;
      }
      
      //pid tuning
      pub_pid_vals.publish(pid_vals);
      pub_pid_vals_copx.publish(pid_vals_copx);
      pub_pid_vals_copy.publish(pid_vals_copy);
      /**VIZ POSE NOW**/
      pose_viz.send_pose_now(current_pose);
      /**VIZ POSE NOW**/
      
      if ((servo_reconf.begin_control_ == 1)&&(palm_servo.is_feat_fb_rec_ == true)&&(palm_servo.contours_num_ > 0))
      {
	ROS_DEBUG_STREAM(" controller started ");
	//ROS_FATAL_STREAM(" move ");
	//ROS_FATAL_STREAM(" is_feature = " << palm_servo.is_feat_fb_rec_); 

	palm_servo.send_pose(new_pose);
      }
      
      ros::spinOnce();
      ROS_DEBUG("after last spin");
      
      loop_rate.sleep();
      
    }
    return 0;
}