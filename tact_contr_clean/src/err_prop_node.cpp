#include "tact_contr_clean/err_prop.hpp"

int main(int argc, char** argv)
{
  //init the ros node
  ros::init(argc, argv, "err_prop");
  ros::NodeHandle n;
  ROS_INFO("err_prop ");
  
  ErrProp err(0);
  
  ros::Rate loop_rate(100);
  
  while( ros::ok() ){
    
    err.send();
    
    
    
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
