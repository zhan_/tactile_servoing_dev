#include <ros/ros.h>

#include "tactile_servo_srvs/begin_control.h"

ros::ServiceClient init_pose_client;

tactile_servo_srvs::begin_control servo_pose_start_client_srv;
tactile_servo_srvs::begin_control::Request  servo_pose_start_client_req;
tactile_servo_srvs::begin_control::Response servo_pose_start_client_res;


int move();

int boolet_proof = 0;
int move(){
  servo_pose_start_client_srv.request.begin_control = 0;
  if(init_pose_client.call(servo_pose_start_client_srv))
  {
    ROS_INFO_STREAM ( " res =" << servo_pose_start_client_srv.response.success);
    boolet_proof = 1;
    return 0;
    
  }
  else
  {
    ROS_INFO_STREAM ("Failed to begin");
    boolet_proof = 1;
    return 1;
    
  }
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "begin_line_follow");
  ros::NodeHandle n;
  ros::service::waitForService("begin_line_follow",ros::Duration(1));
  ROS_INFO("connected service provider");

  init_pose_client = n.serviceClient<tactile_servo_srvs::begin_control>("begin_tactile_servo_real");
  if (boolet_proof == 0)
  {
    move();
  }
  ros::spinOnce();
  
}