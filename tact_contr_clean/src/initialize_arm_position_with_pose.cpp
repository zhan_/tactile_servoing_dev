/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a executable to move palm using palm_pose libraray 
 * consisting of tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>
#include "tact_contr_clean/palm_pose.hpp"


int main(int argc, char** argv)
{
    ros::init(argc, argv, "init_arm_pose_one_node");
    ros::NodeHandle n;
    
    ros::Rate loop_rate(1);
    
    //subsribers to pose and feats and publisher to pose_new
    ServoPalmPose init_arm;
   
    geometry_msgs::PoseStamped current_pose = init_arm.get_pose();
    geometry_msgs::PoseStamped init_pose;
   


    ROS_INFO("init_arm_pose_one_node");
      
    ros::Duration(1).sleep();
    

   //Loading the parameters that have the list of objects to be loaded

    std::string PARAM_NAME = "init_arm_pose_one" ;
    //PARAM_NAME = "object_to_be_spawn" ;
    std::vector<float> coordinates;
    bool ok =ros::param::get(PARAM_NAME, coordinates);
    if(!ok)
    {
        ROS_FATAL_STREAM( "Could not get parameter "<< PARAM_NAME << "set to default") ;
        coordinates = {0.4532, -0.12025, 0.029525, 0.687808, 0.073303, 0.680994, -0.240400};
    }
    
    

//     while( ros::ok() )
//     {
      // get current pose  geometry_msgs::PoseStamped current_pose
      current_pose = init_arm.get_pose();
      init_pose = current_pose;
      init_pose.pose.position.x = coordinates.at(0);
      init_pose.pose.position.y = coordinates.at(1);
      init_pose.pose.position.z = coordinates.at(2);
      init_pose.pose.orientation.x = coordinates.at(3);
      init_pose.pose.orientation.y = coordinates.at(4);
      init_pose.pose.orientation.z = coordinates.at(5);
      init_pose.pose.orientation.w = coordinates.at(6);
      
      init_arm.send_pose(init_pose);
      
      ros::spinOnce();
//       loop_rate.sleep();
//       
//     }
    return 0;
}