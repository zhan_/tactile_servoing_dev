/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for palm pose tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#ifndef _PALM_POSE_HPP_
#define _PALM_POSE_HPP_

#include <ros/ros.h>

#include <geometry_msgs/PoseStamped.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/PlanFeats.h"
#include <Eigen/Core>
#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>
#include "tactile_servo_srvs/select_matrix.h"
#include "tactile_servo_srvs/begin_control.h"
#include "tactile_servo_srvs/choose_configuration_singularity.h"

#include <mutex>
#include <dynamic_reconfigure/server.h>
#include <tactile_servo_config/servo_feats_selectConfig.h>

#include "tact_contr_clean/servo_pid.hpp"

//for kuka: kuka follows traj until it reaches a contact
#include <std_msgs/Bool.h>

// for smooth transitions
#include "tactile_servo_msgs/COCtoZMP.h"

// for bimanual COM compenstaion
#include <std_msgs/Float64.h>

// matrix operations
typedef Eigen::Matrix<float, 6, 1>  VECTOR6_1;
typedef Eigen::Matrix<float, 6, 6>  VECTOR6_6;

float WEISS_STIFFNESS_KUKA = 0.00055556;
float WEISS_HIGHT_KUKA = 0.015;


// subscribe to /palm/pose
// publish to /move_arm_6D/pose 

class ServoPalmPoseKuka

{
public:
  ServoPalmPoseKuka();
  ~ServoPalmPoseKuka();
  //get pose from /palm/pose
  geometry_msgs::PoseStamped get_pose();
  //send pose to /move_arm_6D
  void send_pose(geometry_msgs::PoseStamped msg_sendpose);
  bool is_pose_rec_;
  
  //arrange feature vals to eigen matrix defined above
  VECTOR6_1 get_featsdes();
  VECTOR6_1 get_featsfb();
  bool is_feat_des_rec_;
  bool is_feat_fb_rec_;
  
  float copx_des, copy_des, force_des,
  cocx_des, cocy_des, orientz_des, zmp_x_des, zmp_y_des;
  float copx_fb, copy_fb, force_fb,
  cocx_fb, cocy_fb, orientz_fb, zmp_x_fb, zmp_y_fb;
  
  std::vector<float> feats_des_;
  std::vector<float> feats_fb_;
  
  VECTOR6_1 vec2VECTOR6_1 (std::vector<double> vec );
  std::vector<double> VECTOR6_1tovec (VECTOR6_1 matr );
  std::vector<double> get_feat_err();
  VECTOR6_1 convert2ZMP (VECTOR6_1 in_matr );
  VECTOR6_1 convert2nonzmp (VECTOR6_1 in_matr );

  Eigen::Vector3d msgpose2VECTOR3_1(geometry_msgs::PoseStamped& msg);
  tf::Quaternion pose2quat( geometry_msgs::PoseStamped& msg);
  std::vector<double> quat2rpy( tf::Quaternion& quat);
  VECTOR6_1 msgpose2VECTOR6_1(geometry_msgs::PoseStamped& msg);
  
  VECTOR6_1 select_feats(VECTOR6_1 err_all);
  
  Eigen::Matrix3d quat2eigen (tf::Quaternion quat);

  Eigen::Matrix3d AxB (Eigen::Matrix3d a, Eigen::Matrix3d b);
  Eigen::Vector3d AxVector(Eigen::Matrix3d a, Eigen::Vector3d b);
  Eigen::Vector3d sumAB (Eigen::Vector3d a, Eigen::Vector3d b);

  Eigen::Vector3d feat_err_pos(VECTOR6_1 feats);
  Eigen::Vector3d feat_err_rot(VECTOR6_1 feats);
  Eigen::Vector3d rpy2xyz(Eigen::Vector3d tobe_rearranged);

  void set_posit_des_pose(geometry_msgs::PoseStamped& pose_msg,
			  const Eigen::Vector3d &position);

  tf::Matrix3x3 vec2rot(Eigen::Vector3d vec);

  void set_orient_des_pose(geometry_msgs::PoseStamped& pose_msg,
			   const tf::Matrix3x3 &rot);


  int sel_copx_;
  int sel_copy_;
  int sel_force_;
  int sel_cocx_;
  int sel_cocy_;
  int sel_orient_;
  
  int contours_num_;
  
  //for kuka
  bool is_contact_kuka_;
  
  // for smooth coc2zmp
  ros::Subscriber num_pixels_weight_gain_sub;
  double smooth_;
  void num_pixels_weight_gain_cb (const tactile_servo_msgs::COCtoZMP& msg);
  
  ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////  float bimanual_COM_coc_des_;
//   ros::Publisher bimanual_COM_coc_des_pub;
  ///////////////////////////////////////////////
  // for the bimanual center mass compensation///
  ///////////////////////////////////////////////  
private:
  
  ros::NodeHandle nh;
  
  ros::Subscriber is_contact_kuka_sub;
  void is_contact_kuka_cb (const std_msgs::Bool& msg);

  
  //std::string topic_sub_pose;
  //std::string topic_pub_pose;
  geometry_msgs::PoseStamped palm_pose_now_;
  geometry_msgs::PoseStamped palm_pose_send_;
  
  /** 
   * Subscribes to the palm pose published by 
   * @tools/kinematics_tools/scripts/palm_pose_publisher.py
   */
  ros::Subscriber sub_current_pose;
 
  /**
   * Callback function called when a msg is received. 
   * Update the palm_pose_now
   * @param msg the message containing the palm pose
   */
  void callback_current_pose(const geometry_msgs::PoseStampedPtr& msg);
    
   /** 
   * Publishes to the iklistener of palm pose published by 
   * @tools/kinematics_tools/srs/arm_IK_listener.cpp
   */
   ros::Publisher pub_pose;
   
   ///////add this to header/////////////

   ros::Subscriber sub_feats_fb;
   ros::Subscriber sub_feats_des;
   ros::Subscriber sub_plan_feats_fb;

   void callback_desfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg);
   void callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg);
   void callback_planfbfeats(const tactile_servo_msgs::PlanFeats& msg);

};

class ServoReconfServicesKuka

{
public:
  ServoReconfServicesKuka(ServoPalmPoseKuka& servo_obj_ref, ServoPidManager& pid_obj_ref);
  ~ServoReconfServicesKuka();
  
  ros::ServiceServer service_sel_matr;
  bool selection_matrix(tactile_servo_srvs::select_matrix::Request &req,
                             tactile_servo_srvs::select_matrix::Response &res);
  
  ros::ServiceServer service_begin_control;
  bool begin_control(tactile_servo_srvs::begin_control::Request &req,
                             tactile_servo_srvs::begin_control::Response &res);
  int begin_control_;
  ros::ServiceServer service_sel_configuration_singularity;
  bool select_configuration(tactile_servo_srvs::choose_configuration_singularity::Request &req,
                             tactile_servo_srvs::choose_configuration_singularity::Response &res);
  int choose_configuration_;
  /// tune pid param by dynamic reconfigure
  void cb_dynamic_reconf(tactile_servo_config::servo_feats_selectConfig &config, uint32_t level);
  dynamic_reconfigure::Server<tactile_servo_config::servo_feats_selectConfig> set_pid_feats_server;
  dynamic_reconfigure::Server<tactile_servo_config::servo_feats_selectConfig> :: CallbackType f;
  
  int sel_copx_;
  int sel_copy_;
  int sel_force_;
  int sel_cocx_;
  int sel_cocy_;
  int sel_orient_;
  
private:
  ros::NodeHandle nh;
  ServoPalmPoseKuka& servo_obj_ref_;
  ServoPidManager& pid_obj_ref_;
  std::mutex update_mutex;;

  
};


class Servo4VizKuka
{
public:
  Servo4VizKuka();
  ~Servo4VizKuka();
  void send_pose_des(geometry_msgs::PoseStamped msg_sendpose);
  void send_pose_des2(geometry_msgs::PoseStamped msg_sendpose);
  void send_pose_now(geometry_msgs::PoseStamped msg_sendpose);
  void send_pose_now2(geometry_msgs::PoseStamped msg_sendpose);
  
  void send_pose_contact_frame(geometry_msgs::PoseStamped msg_sendpose);
  double hight_of_sensor_;
  double compliance_;
  geometry_msgs::PoseStamped set_contact_frame(float& cocxfb, float& cocyfb, float& forcefb,
  float& zmpxfb, float& zmpyfb, float& orientfb);
private:
    ros::NodeHandle nh;
    ros::Publisher pub_pose_viz_des;
    ros::Publisher pub_pose_viz_des2;
    ros::Publisher pub_pose_viz_now;
    ros::Publisher pub_pose_viz_now2;
    ros::Publisher pub_pose_viz_contact_frame;

};

#endif