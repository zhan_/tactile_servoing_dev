/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @brief  This is a library for a set of PIDs for servoing. 
*/
#ifndef _SERVO_ERR_PROP_HPP_
#define _SERVO_ERR_PROP_HPP_

#include <ros/ros.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/ErrProp.h"


class ErrProp
{
public:
    ErrProp (int tactile_sensor_num_);
    ~ErrProp();
    ros::NodeHandle n;

    ros::Subscriber des_feats_sub;
    ros::Subscriber fb_feats_sub;
    void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void send();
  
    ros::Publisher err_prop_pub;
    tactile_servo_msgs::ErrProp err;
    //coordinates
    float copx_fb,
    copy_fb,
    force_fb,
    cocx_fb,
    cocy_fb,
    orientz_fb, zmpx_fb, zmpy_fb;
    
    float copx_des,
    copy_des,
    force_des,
    cocx_des,
    cocy_des,
    orientz_des, zmpx_des, zmpy_des;

};

#endif