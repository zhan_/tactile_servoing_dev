/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @brief  This is a library for a set of PIDs for servoing. 
*/
#ifndef _SERVO_PIDs_HPP_
#define _SERVO_PIDs_HPP_
#include <ros/ros.h>
#include <control_toolbox/pid.h>

class SinglePID
{
public:
  SinglePID(std::string link, std::string axis );
  ~SinglePID();
  control_toolbox::Pid pid_servo_;
  
  //initalize pid with adding link_name and axis init(n, "names")
  bool init(ros::NodeHandle &n);
  //read all the controller settings from the parameter server
  void read_params();
  void setGains();
  void getGains(double &p, double &i, double &d, double &i_max, double &i_min);
  void starting();
  double compute_cmd(double err_position, ros::Duration dt);
  void reset();

  double position_deadband_;

  double p_,
  i_,
  d_,
  i_clamp_;
  
  double out_;
  std::string link_;
  std::string axis_;
  
private:
  ros::NodeHandle node_;
};


class ServoPidManager
{
public:
  ServoPidManager();
  ~ServoPidManager();
  ros::Time time_of_last_cycle_;
  std::vector<SinglePID*> palm_pids;
  std::vector<double> out_;
  
  bool init(ros::NodeHandle &n);
  void readparams();
  void setGains();
  void starting();
  std::vector<double> compute_cmd(std::vector<double> err_position, 
				  ros::Duration dt);
  void reset();
private:
  ros::NodeHandle node_;
};
#endif