/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   Thu Mar 10 11:07:10 2011
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for palm pose tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#ifndef _control_wrj12er_HPP_
#define _control_wrj12er_HPP_

#include <ros/ros.h>
#include <controller_manager_msgs/ListControllers.h>
#include <pr2_controllers_msgs/JointControllerState.h>
#include <control_msgs/JointControllerState.h>
#include "sr_robot_msgs/JointControllerState.h"


#include <map>
#include <string>
#include <std_msgs/Float64.h>

// subscribe to /joint controllers

class ControlWrj12ErGazebo

{
public:
  ControlWrj12ErGazebo();
  ~ControlWrj12ErGazebo();
  std::map<std::string,std::string> jointControllerMap;
std::map<std::string,unsigned int> jointPubIdxMap;
ros::Publisher pub_wrj1_push;
ros::Publisher pub_wrj2_bye;
ros::Publisher pub_elbow_rotate;
ros::NodeHandle n;
ros::ServiceClient controller_list_client;

std::string controlled_joint_name;
std::vector<std::string> joint_name;

    controller_manager_msgs::ListControllers controller_list;
std::string controller_name;

void publish_joint_cmds(double& orientz, double& orientx, double& orienty);


ros::Subscriber sub_wrj1_push;
ros::Subscriber sub_wrj2_bye;
ros::Subscriber sub_elbow_rotate;

void cb_wrj1_push_state(const control_msgs::JointControllerState& msg);
void cb_wrj2_bye_state(const control_msgs::JointControllerState& msg);
void cb_elbow_rotate_state(const control_msgs::JointControllerState& msg);

double wrj1_push_now_;
double wrj2_bye_now_;
double elbow_rotate_now_;

bool iswrj1_push_now_, iswrj2_bye_now_, iselbow_rotate_now_;


};


#endif