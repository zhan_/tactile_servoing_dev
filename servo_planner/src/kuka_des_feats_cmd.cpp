#include <ros/ros.h>


// dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <servo_planner/servo_planner_des_feats_paramsConfig.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

#include "tactile_servo_srvs/set_des_feats_srv.h"

class plannerDesFeats
{
public:
    plannerDesFeats();
    virtual ~plannerDesFeats();

    ros::NodeHandle nh;

    ros::Publisher pub_feats;

    void pub();
    void loop();

    float copx, copy, force, cocx, cocy, orientz, zmp_x, zmp_y;
    tactile_servo_msgs::ContsFeats des_feat_set;
    tactile_servo_msgs::OneContFeats one_cont_feats;
    
    ros::ServiceServer service_set_feats;
    bool set_des_feats_srv_cb(tactile_servo_srvs::set_des_feats_srv::Request &req,
                             tactile_servo_srvs::set_des_feats_srv::Response &res);


};
plannerDesFeats::plannerDesFeats(){
    pub_feats = nh.advertise<tactile_servo_msgs::ContsFeats>("des_feats",1);
    des_feat_set.header.frame_id = "smth";
    copx = 0.025;
    copy = 0.0125;
    force = 1;
    cocx = 0.025;
    cocy = 0.0125;
    orientz = 0.0;
    zmp_x = 0.0;
    zmp_y = 0.0;

    loop();
    
    service_set_feats = nh.advertiseService("/set_des_feats", &plannerDesFeats::set_des_feats_srv_cb, this);

}
plannerDesFeats::~plannerDesFeats(){}



bool plannerDesFeats::set_des_feats_srv_cb(tactile_servo_srvs::set_des_feats_srv::Request &req,
                             tactile_servo_srvs::set_des_feats_srv::Response &res)
{
  copx = req.copx;
  copy = req.copy;
  force = req.force;
  cocx = req.cocx;
  cocy = req.cocy;
  orientz = req.orient;
  zmp_x = req.zmp_x;
  zmp_y = req.zmp_y;
  res.success = 1;
  
  ROS_DEBUG("sending back response: [%ld]", (long int)res.success);
  ROS_INFO("copx and etc srvs = %f, %f, %f, %f, %f, %f, %f, %f ", copx, copy, force, cocx, cocy, orientz, zmp_x, zmp_y );
  
  return true;
}

void plannerDesFeats::pub(){
    des_feat_set.header.stamp = ros::Time::now();
    one_cont_feats.centerpressure_x = copx;
    one_cont_feats.centerpressure_y = copy;
    one_cont_feats.contactForce = force;
    one_cont_feats.centerContact_x = cocx;
    one_cont_feats.centerContact_y = cocy;
    one_cont_feats.contactOrientation = orientz;
    one_cont_feats.zmp_x = zmp_x;
    one_cont_feats.zmp_y = zmp_y;
    des_feat_set.control_features.push_back(one_cont_feats);
    pub_feats.publish(des_feat_set);
    des_feat_set.control_features.clear();
}
void plannerDesFeats::loop(){
//    ros::Rate loop_rate(75);
//    while( ros::ok() ){
//        pub();
//        ros::spinOnce();
//        loop_rate.sleep();
//    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "set_des_feats");
    ros::NodeHandle n;

    ros::Rate loop_rate(200);
    plannerDesFeats practise1;

    while( ros::ok() ){
//        std::cout<<"copx to pub = "<< practise1.copx<<std::endl;
//        std::cout<<"in loop "<< std::endl;
        practise1.pub();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}






