#include <ros/ros.h>


// dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <servo_planner/servo_planner_plan_feats_paramsConfig.h>

#include "tactile_servo_msgs/PlanFeats.h"
class plannerDesFeats
{
public:
    plannerDesFeats();
    virtual ~plannerDesFeats();

    ros::NodeHandle nh;

    ros::Publisher pub_feats;

    void pub();
    void loop();

    int contours_num;
    std::string type_contact;
    double contactArea;
    bool isCorner;

    tactile_servo_msgs::PlanFeats des_feat_set;

};
plannerDesFeats::plannerDesFeats(){
    pub_feats = nh.advertise<tactile_servo_msgs::PlanFeats>("plan_feats",2);
    des_feat_set.header.frame_id = "smth";
contours_num = 1;
contactArea = 0.0;

    loop();

}
plannerDesFeats::~plannerDesFeats(){}

void plannerDesFeats::pub(){
    des_feat_set.header.stamp = ros::Time::now();
    des_feat_set.numContours = contours_num;
  
    pub_feats.publish(des_feat_set);
}
void plannerDesFeats::loop(){
//    ros::Rate loop_rate(75);
//    while( ros::ok() ){
//        pub();
//        ros::spinOnce();
//        loop_rate.sleep();
//    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "set_plan_feats");
    ros::NodeHandle n;

    ros::Rate loop_rate(200);
    plannerDesFeats practise1;

    while( ros::ok() ){
//        std::cout<<"copx to pub = "<< practise1.copx<<std::endl;
//        std::cout<<"in loop "<< std::endl;
        practise1.pub();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}






