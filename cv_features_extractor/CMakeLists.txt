cmake_minimum_required(VERSION 2.8.3)
project(cv_features_extractor)

find_package(OpenCV REQUIRED)
find_package(catkin REQUIRED COMPONENTS 
			     roslib 
			     roscpp
			     sensor_msgs
			     std_msgs
			     cv_bridge
			     image_transport
			     dynamic_reconfigure
			     cmake_modules
			     tactile_servo_msgs)
find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})
include_directories( ${catkin_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})
include_directories(${dynamic_reconfigure_PACKAGE_PATH}/cmake/cfgbuild.cmake)
#include_directories(include ${catkin_INCLUDE_DIRS} ${Eigen_INCLUDE_DIRS})

generate_dynamic_reconfigure_options(
# image processing parameters (threshold velue, etc)
cfg/img_proc_param.cfg
# fake features for debug
cfg/fake_fb_feats.cfg
)

catkin_package(
  DEPENDS
  CATKIN_DEPENDS
	 roslib
	 message_runtime
	 std_msgs
	 sensor_msgs
	 geometry_msgs
	 roscpp
	 tactile_servo_msgs
  INCLUDE_DIRS include
  LIBRARIES
)

#add_definitions(${EIGEN_DEFINITIONS})

# add_executable(weiss_display_image  src/weiss_display_image.cpp)
# target_link_libraries(weiss_display_image ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
# add_dependencies(weiss_display_image ${${PROJECT_NAME}_EXPORTED_TARGETS} ${PROJECT_NAME}_generate_messages_cpp ${PROJECT_NAME}_gencfg)

add_executable(publish_fake_feats_to_test_jacob  src/publish_fake_feats_to_test_jacob.cpp)
target_link_libraries(publish_fake_feats_to_test_jacob ${catkin_LIBRARIES} ${OpenCV_LIBRARIES} )
add_dependencies(publish_fake_feats_to_test_jacob ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${PROJECT_NAME}_generate_messages_cpp ${PROJECT_NAME}_gencfg)

#add_executable(corners  src/corner_test.cpp)
#add_dependencies(corners ${catkin_EXPORTED_TARGETS})
#target_link_libraries(corners ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
# 
# 
# add_executable(extract_features  src/extract_features.cpp)
# target_link_libraries(extract_features ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
# add_dependencies(extract_features ${catkin_EXPORTED_TARGETS} ${PROJECT_NAME}_generate_messages_cpp ${PROJECT_NAME}_gencfg)
# 
# add_executable(extract_features_edissons_sensor  src/extract_features_edissons_sensor.cpp)
# target_link_libraries(extract_features_edissons_sensor ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
# add_dependencies(extract_features_edissons_sensor ${${PROJECT_NAME}_EXPORTED_TARGETS} ${PROJECT_NAME}_generate_messages_cpp ${PROJECT_NAME}_gencfg)
# 
# add_executable(weiss_display_image_two_point  src/weiss_display_image_two_point.cpp)
# target_link_libraries(weiss_display_image_two_point ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})
# add_dependencies(weiss_display_image_two_point ${${PROJECT_NAME}_EXPORTED_TARGETS} ${PROJECT_NAME}_generate_messages_cpp ${PROJECT_NAME}_gencfg)



#add_executable(extractor_pps_once  src/pca_once.cpp)
#target_link_libraries(extractor_pps_once ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

#add_executable(extractor_pps_once_weiss  src/pca_once_weiss.cpp)
#target_link_libraries(extractor_pps_once_weiss ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})



#add_executable(rviz_display_features src/rviz_display_features.cpp)
#target_link_libraries(rviz_display_features ${catkin_LIBRARIES} )

#add_executable(extractor_pps_once_simple  src/simple.cpp)
#target_link_libraries(extractor_pps_once_simple ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})



#add_executable(corners_zhanat  src/corner_test_zhanat.cpp)
#target_link_libraries(corners_zhanat ${catkin_LIBRARIES} ${OpenCV_LIBRARIES})

#src/features_extractor.cpp
#src/pca.cpp
#pca_once
#pca_cea_sensor.cpp

# Catkin dependencies

#############################
#          Install          #
#############################
# install(DIRECTORY include/${PROJECT_NAME}/
#   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
# )
# install(TARGETS ${PROJECT_NAME}
#   	ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#         LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
#         RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION})
