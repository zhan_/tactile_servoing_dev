/**
  * Node: features_extractor.cpp
  * Version: 1.1
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS
#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message
#include <opencv2/imgproc/imgproc.hpp>       							// Include headers for OpenCV's image processing
#include <opencv2/highgui/highgui.hpp>		 							// Include GUI modules
#include <math.h>   													// sqrt
//#include <Eigen/src/plugins/BlockMethods.h>
#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/Image_weiss.h>

#include <dynamic_reconfigure/server.h>
#include <cv_features_extractor/improc_paramsConfig.h>

#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

using namespace cv;
using namespace std;

/// class feature extraction
/// convert ros_image to open_cv image
/// subscribe to /tactile_image topic
/// extract features
/// publish features
///
/// later: call service to corner servoing,
/// edge, etc.


class featureExtract
{
public:
    ros::NodeHandle nh_;
    //create transporter
    image_transport::ImageTransport it_;
    featureExtract(int tactile_sensor_num_);
    ~featureExtract();

    // for converting from ros image to opencv
    image_transport::Subscriber image_sub_;

    /// function members
    void img_proc (cv::Mat& t_image_orogin_);
    void imageCb(const sensor_msgs::ImageConstPtr& msg_);
    //void callback_pressures(const tactile_servo_msgs::Image_weissConstPtr& msg_weiss);
    void publish_features();

    /// extracted features
    float offset_x, offset_y, total_pressure, offset_x_coc, offset_y_coc ;
    /// OpenCv Headers
    cv::Mat src, cop_img, no_noise, bin_img, rsz;

    /// sensor center to compute offset with object frame of reference
    unsigned int sensor_center_x;
    unsigned int sensor_center_y;

    string SensorName;

    int rows;
    int cols;
    int step;

    // TODO add coc_threshold to dynamic reconf
    int noise_threshold, coc_threshold;
    int PcaAreaThreshold;// = 14;
    int scaling_factor; // 30

    // publish not published
    bool is_ready_to_pub;
    bool is_published;

    // change im proc params on the fly
    void cb_dynamic_reconf(cv_features_extractor::improc_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<cv_features_extractor::improc_paramsConfig> change_improc_param_server;
    dynamic_reconfigure::Server<cv_features_extractor::improc_paramsConfig> :: CallbackType f;

    tactile_servo_msgs::ContsFeats fb_feat_set;
    tactile_servo_msgs::OneContFeats fb_one_cont_feats;

    ros::Publisher fb_feats_pub;

    float copx, copy, force, cocx, cocy, orientz;

    void show_windows();
    cv::Point2d cop, coc;
    float contact_orientation;

    double size_x;
    double size_y;
    double force_scale;
    
    cv::Point pos;
    //cv::Point COP_CALC;
    //float to_find_cop;

};


featureExtract::featureExtract(int tactile_sensor_num_):it_(nh_)
{

    // read from config file the name of a topic
    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);
    ROS_ASSERT_MSG(my_list.getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["tactile_image_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 8th pos");
    std::string topic_name = static_cast<std::string> (my_list[tactile_sensor_num_]["tactile_image_topic_name"]);
    std::string topic_name_pressure = static_cast<std::string> (my_list[tactile_sensor_num_]["tactile_pressure_topic_name"]);

    // subscribe to the tactile image topic
    image_sub_ = it_.subscribe("/"+topic_name, 1, &featureExtract::imageCb, this);
    //pressure_val_sub_  = nh_.subscribe("/"+topic_name_pressure,1, &featureExtract::callback_pressures,this);

    // get topic names of control features for a given sensor
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 9th pose");
    std::string topic_name_pub_c = static_cast<std::string> (my_list[tactile_sensor_num_]["control_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    std::string topic_name_pub_p = static_cast<std::string> (my_list[tactile_sensor_num_]["planning_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["desired_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    std::string topic_name_pub_d = static_cast<std::string> (my_list[tactile_sensor_num_]["desired_topic_name"]);

    // get the name of a sensor frame from ros_image message
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_].hasMember("frame_id_header"), "Test frame_id_header");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");

    ROS_ASSERT_MSG(my_list[tactile_sensor_num_].hasMember("dimensions"), "Test dimensions");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
    rows = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][0]["cells_x"]);
    cols = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][1]["cells_y"]);
    step = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][1]["cells_y"]);
    size_x=static_cast<double> (my_list[tactile_sensor_num_]["size"][0]["x"]);
    size_y=static_cast<double> (my_list[tactile_sensor_num_]["size"][1]["y"]);


    //booleans for publishing and cleaning
    is_ready_to_pub = false;
    is_published = false;

    sensor_center_x = rows/2;
    sensor_center_y = cols/2;

    coc_threshold = 30;
    noise_threshold = 15;
    PcaAreaThreshold = 14;
    scaling_factor = 30;

    cv::namedWindow("TI&F", WINDOW_NORMAL);
    cv::namedWindow("COC_binary", WINDOW_NORMAL);
    cv::namedWindow("not_filtered", WINDOW_NORMAL);
    cv::startWindowThread();
    // dynamic reconfigure
    f = boost::bind(&featureExtract::cb_dynamic_reconf, this, _1, _2);
    change_improc_param_server.setCallback(f);

    fb_feats_pub = nh_.advertise<tactile_servo_msgs::ContsFeats>("fb_feats", 1);
    fb_feat_set.header.frame_id = "fb_feats_weiss_palm";

}

featureExtract::~featureExtract(){
    cv::destroyAllWindows();
}
/// dynamic reconfigure callback function change the sensitivity
///  from rqt online to adjust the output values of each contact cell
void featureExtract::cb_dynamic_reconf(cv_features_extractor::improc_paramsConfig &config, uint32_t level){
//    ROS_INFO("Reconfigure Request:%f", config.sensitivity_gazebo);
    noise_threshold = config.noise_threshold;
    coc_threshold = config.coc_threshold;
    PcaAreaThreshold = config.improc_area_thresh;
    scaling_factor = config.improc_scaling_factor;
    force_scale = config.force_scale;
    ROS_INFO("noise_threshold:%d", (int)noise_threshold);
    ROS_INFO("coc_threshold:%d", (int)coc_threshold);
    ROS_INFO("PcaAreaThreshold:%d", (int)PcaAreaThreshold);
}


/********************************************
 * Processing Image
 *********************************************/

void featureExtract::show_windows(){
    cv::Mat rsz_cop;
    cv::resize(no_noise, rsz_cop, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
    cv::Mat rsz_color_cop;
    cv::cvtColor(rsz_cop, rsz_color_cop, CV_GRAY2BGR);
    //cop
    cv::circle(rsz_color_cop, scaling_factor*cop, 3, CV_RGB(255, 0, 0), 3);
    float y_temp_val_to_show_orient = std::tan(contact_orientation);
     cv::line(rsz_color_cop, scaling_factor*cop, (scaling_factor*cop + 2 * cv::Point2d(scaling_factor, scaling_factor*y_temp_val_to_show_orient)) , CV_RGB(255, 0, 0), 3,8);
     cv::line(rsz_color_cop, scaling_factor*cop, (scaling_factor*cop - 2 * cv::Point2d(scaling_factor, scaling_factor*y_temp_val_to_show_orient)) , CV_RGB(255, 0, 0), 3,8);
   //coc
     cv::circle(rsz_color_cop, scaling_factor*coc, 3, CV_RGB(255, 0, 0), 3);
   //coc rect
    cv::Rect rect1;
rect1.x = scaling_factor*(coc.x-0.5);
rect1.y = scaling_factor*(coc.y-0.5);
rect1.width = scaling_factor*0.5;
rect1.height = scaling_factor*0.5;
      cv::Rect rect2;
rect2.x = scaling_factor*(coc.x);
rect2.y = scaling_factor*coc.y;
rect2.width = scaling_factor*0.5;
rect2.height = scaling_factor*0.5;
    cv::Rect rect3;
rect3.x = scaling_factor*(coc.x-0.5);
rect3.y = scaling_factor*(coc.y);
rect3.width = scaling_factor*0.5;
rect3.height = scaling_factor*0.5;
    cv::Rect rect4;
rect4.x = scaling_factor*(coc.x);
rect4.y = scaling_factor*(coc.y-0.5);
rect4.width = scaling_factor*0.5;
rect4.height = scaling_factor*0.5;
//     cv::rectangle(rsz_color_cop, rect1,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect2,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect3,  CV_RGB(255, 0, 0),3, 3);
//     cv::rectangle(rsz_color_cop, rect4,  CV_RGB(255, 0, 0),3, 3);

// cv::circle(rsz_color_cop, scaling_factor*pos, 4, CV_RGB(0, 200, 200), 3);
// cv::circle(rsz_color_cop, scaling_factor*COP_CALC, 4, CV_RGB(0, 200, 200), 3);


    ///display image cop
//    cv::resizeWindow("TI&F", cols*scaling_factor, rows*scaling_factor);
    cv::imshow( "TI&F", rsz_color_cop );

    ///display image coc
    cv::Mat rsz_coc;
    cv::resize(bin_img, rsz_coc, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
    cv::imshow("COC_binary", rsz_coc);

    ///display image src img
    cv::Mat rsz_src;
    cv::resize(src, rsz_src, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
    cv::imshow("not_filtered", rsz_src);
}

void featureExtract::imageCb(const sensor_msgs::ImageConstPtr& msg_)
{
    uchar img[rows][cols];
    int imgshow[rows][cols];
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg_, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    /// fill data extracted from array in a tactile image matrix
    int c;
    c = 0;
    total_pressure = 0;
    for(int i=rows-1; i>=0; --i) {
        for(int j=0; j<cols; j++) {
                // to display in screan
//                 imgshow[i][j]=(c+1)*cv_ptr->image.data[c];
                // to process
                img[i][j]=cv_ptr->image.data[c];
                total_pressure += img[i][j];
// 		to_find_cop += imgshow[i][j];
               
        ++c;
        }
    }
    src = cv::Mat(rows, cols, CV_8UC1, img);
    //analyse image Principle components
    img_proc (src);
}

void featureExtract::img_proc (cv::Mat& input_img){
    // http://stackoverflow.com/questions/8449378/finding-contours-in-opencv
    //        cv::cvtColor(image, image, CV_BGR2GRAY);
    //        cv::threshold(image, image, 128, 255, CV_THRESH_BINARY);
    //  canny esge detector can also work to prepare image
    // http://docs.opencv.org/doc/tutorials/imgproc/shapedescriptors/find_contours/find_contours.html
    // http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html
    // filter small noises
    //cv::Mat rsz;
    //cv::resize(input_img, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);

    cv::threshold(input_img, no_noise, noise_threshold, 255, CV_THRESH_TOZERO);//CV_THRESH_TOZERO
    /// Get the moments
    cv::Moments m = moments(no_noise,false);
    /// COP x and y
    /// Centroid computation. Get the mass centers:
    cop.x= (m.m10/m.m00) + (double)0.5;
    cop.y= (m.m01/m.m00) + (double)0.5;
    total_pressure = total_pressure;
    if (cop.y > 84) cop.y = 84;    
    if (cop.y < 0) cop.y = 0;    
    if (cop.x > 84) cop.x = 84;    
    if (cop.x < 0) cop.x = 0;    
//     cout<<"copx"<<cop.x<<endl;

    cv::threshold(input_img, bin_img, coc_threshold, 255, CV_THRESH_BINARY);
    cv::Moments m2 = moments(bin_img,true);
    /// COC x and y
    /// Centroid computation. Get the mass centers:
    coc.x= m2.m10/m2.m00 + (double)0.5;
    coc.y= m2.m01/m2.m00 + (double)0.5;

    ///Orientation
   // contact_orientation  = 1.57-0.5*std::atan2(m2.mu11,(m2.mu20-m2.mu02));
    contact_orientation  = 0.5*std::atan2(m2.mu11,(m2.mu20-m2.mu02));
    /// calculate offset of COP from center
    offset_x = cop.x -sensor_center_x;
    offset_y = cop.y - sensor_center_y;

    /// calculate offset of COC from center
    offset_x_coc = coc.x -sensor_center_x;
    offset_y_coc = coc.y - sensor_center_y;
    
    copx = size_x - (float)((float)size_x/(float)rows)* ((float) (cop.y+1));
    // for the fake tactile image copx is not inversed
//     copx = (float)((float)size_x/(float)rows)* ((float) (cop.y+1));
    copy = size_y - (float)((float)size_y/cols)* ((float) (cop.x+1));
    force = total_pressure/force_scale;
    cocx = (size_x/rows)* ((float) coc.y);
    cocy = size_y - (size_y/cols)* ((float) coc.x);
    orientz = (float) contact_orientation;

    fb_feat_set.header.stamp = ros::Time::now();
    fb_one_cont_feats.centerpressure_x =copx;
    fb_one_cont_feats.centerpressure_y =copy;
    fb_one_cont_feats.contactForce = force;
    fb_one_cont_feats.centerContact_x = cocx;
    fb_one_cont_feats.centerContact_y = cocy;
    fb_one_cont_feats.contactOrientation = orientz;
    fb_feat_set.control_features.push_back(fb_one_cont_feats);
    show_windows();
    is_ready_to_pub = true;
 /*  
    ////PcaAreaThreshold/// Construct a buffer used by the pca analysis
   /// Find all objects of interest
    vector<vector<cv::Point> > contours;
    vector<Vec4i> hierarchy;
    findContours(no_noise, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
    if (contours.size() > 0){
    vector<Point> pts = contours[0]; 
    cv::Mat img = no_noise.clone();
    cv::Mat data_pts = cv::Mat(pts.size(), 2, CV_64FC1);
    
    
    for (int ind1 = 0; ind1 < data_pts.rows; ++ind1) {
      
      data_pts.at<double>(ind1, 0) = pts[ind1].x;
      data_pts.at<double>(ind1, 1) = pts[ind1].y;
      
    }
    
    
    /// Perform PCA analysis of the contact image
    cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
    
    
    
    /// Store the position of the object
    pos = cv::Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
    
        cout<<"PCApos.x="<<pos.x<<endl;

    
    /// To store the eigenvalues and eigenvectors
    vector<cv::Point2d> eigen_vecs(2);
    vector<double> eigen_val(2);
    
    
    for (int ind2 = 0; ind2 < 2; ++ind2) {
      
      eigen_vecs[ind2] = cv::Point2d(pca_analysis.eigenvectors.at<double>(ind2, 0), pca_analysis.eigenvectors.at<double>(ind2, 1));
      eigen_val[ind2] = pca_analysis.eigenvalues.at<double>(0, ind2);
      
    }
    }*/
     /* nothing importan
    /// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
//     circle(colorPca, pos, 3, CV_RGB(255, 0, 0), 3); // center of pressure
//     line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8); // first component of pca
//     line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8); // second component of pca ok. 
//     
//     /// Center of pressure    
//     object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor; 	//what is scaling factor? scaling factor is use to scale the small image and be able to see.
//     // so when you apply pca, you apply it in the big image, but then you need to know the center in the original image so you divide by the scaling factor.
//     object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
//     //cout << "object_center_x" << object_center_x << endl;
//     }
    //total_pressure;
    //COP_CALC = 
//     float center_pressure = to_find_cop/(total_pressure*255);
// //     ROS_INFO("center_pressure: %d", (float)center_pressure);
//     std::cout<<center_pressure<<std::endl;
//     std::cout<<to_find_cop<<std::endl;
//     std::cout<<total_pressure<<std::endl;
// 
//     int x_pressure = (int)floor(center_pressure/cols);
// //     ROS_INFO("x_pressure %d", x_pressure);
//     std::cout<<x_pressure<<std::endl;
// 
//     int y_pressure = center_pressure - x_pressure*cols;
//     COP_CALC = cv::Point(y_pressure, x_pressure);
*/
}


 void featureExtract::publish_features(){
     // if can_pub
     if(is_ready_to_pub){

         if (fb_one_cont_feats.contactForce > 0){
             fb_feats_pub.publish(fb_feat_set);
             fb_feat_set.control_features.clear();
             is_published = true;
             is_ready_to_pub = false;
         }
     }
     else{
         is_published = false;
     }
     ros::spinOnce();
}

int main(int argc, char** argv)
{
    ROS_INFO("EXTRACT FEATURES NODE");
    ros::init(argc, argv, "feature_extractor");

    featureExtract weiss_extract(0);



    ros::Rate loop_rate(200);												// Loop at 100Hz until the node is shut down
    while (ros::ok())
    {
        weiss_extract.publish_features();
//        weiss_extract.show_windows();
        ros::spinOnce();
        loop_rate.sleep();			 										// wait until it is time for another interaction
    }

    return 0;

}

