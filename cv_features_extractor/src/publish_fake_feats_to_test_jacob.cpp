/*
 * fake feedback features to test jacobian
 * */
#include <ros/ros.h>

#include <Eigen/Core>
#include <Eigen/Dense>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

#include <dynamic_reconfigure/server.h>
#include <cv_features_extractor/fb_feats_paramsConfig.h>

tactile_servo_msgs::ContsFeats feat_set;
tactile_servo_msgs::OneContFeats one_cont_feats;

//world_tactile_contact.point.x = msg_.states[0].contact_positions[i].x;


ros::Publisher feats_pub;

Eigen::Matrix2f A;
Eigen::Matrix3f Aa;
Eigen::Vector3f bb;

float copx, copy, force, cocx, cocy, orientz;
float     zmp_x,  zmp_y;

void cb_dynamic_reconf(cv_features_extractor::fb_feats_paramsConfig &config, uint32_t level){
    copx = config.centerpressure_x;
    copy = config.centerpressure_y;
    force = config.contactForce;
    cocx = config.centerContact_x;
    cocy = config.centerContact_y;
    orientz = config.contactOrientation;
    zmp_x = config.zmp_x;
    zmp_y = config.zmp_y;
    ROS_INFO("copx reconf = %f", copx );
}



int main(int argc, char** argv)
{
    ROS_INFO("Publish fake fetures node");
    ros::init(argc, argv, "pub_fake_features");
    ros::NodeHandle n;
    copx = 0.0;
    copy = 0.0;
    force = 0.0;
    cocx = 0.0;
    cocy = 0.0;
    orientz = 0.0;

    feats_pub = n.advertise<tactile_servo_msgs::ContsFeats>("fb_feats", 1);

    dynamic_reconfigure::Server<cv_features_extractor::fb_feats_paramsConfig> server;
    dynamic_reconfigure::Server<cv_features_extractor::fb_feats_paramsConfig>::CallbackType f;

    f = boost::bind(&cb_dynamic_reconf, _1, _2);
    server.setCallback(f);
    ros::spinOnce();

/***EIGEN
    A << 1, 2, 2, 3;
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix2f> eigensolver(A);
    if (eigensolver.info() != Eigen::Success) abort();
    std::cout << "The eigenvalues of A are:\n" << eigensolver.eigenvalues() << std::endl;
    std::cout << "Here's a matrix whose columns are eigenvectors of A \n"
         << "corresponding to these eigenvalues:\n"
         << eigensolver.eigenvectors() << std::endl;

    Aa << 1,2,3, 4,5,6, 7,8,10;
    bb << 3, 3, 4;
    std::cout << "Here is the matrix A:\n" << Aa << std::endl;
    std::cout << "Here is the vector b:\n" << bb << std::endl;
    Eigen::Vector3f x = Aa.colPivHouseholderQr().solve(bb);
    std::cout << "The solution is:\n" << x << std::endl;
*/





    ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
    while (ros::ok())
    {
//       ROS_INFO("working");
       feat_set.header.stamp = ros::Time::now();
       feat_set.header.frame_id = "smth";
       feat_set.header.stamp = ros::Time::now();
       one_cont_feats.centerContact_x = cocx;
       one_cont_feats.centerContact_y = cocy;
       one_cont_feats.contactForce = force;
       one_cont_feats.centerpressure_x = copx;
       one_cont_feats.centerpressure_y = copy;
       one_cont_feats.contactOrientation = orientz;
       one_cont_feats.zmp_x = zmp_x;
       one_cont_feats.zmp_y = zmp_y;
       feat_set.control_features.push_back(one_cont_feats);
//       ROS_INFO(feat_set.header.frame_id.c_str());
//       ROS_INFO(feat_set.control_features[0].centerContact_x);
//       std::cout<<" number of contacts "<<feat_set.control_features.size()<<std::endl;
//       std::cout<<"frame nanme = "<<feat_set.header.frame_id<<std::endl;
//       std::cout<<"feat_set.control_features[0].centerContact_x = "<<feat_set.control_features[0].centerContact_x<<std::endl;
//       std::cout<<"feat_set.control_features[0].contactOrientation = "<<feat_set.control_features[0].contactOrientation<<std::endl;
       feats_pub.publish(feat_set);
       feat_set.control_features.clear();
        ros::spinOnce();
        // do this
        loop_rate.sleep();			 										// wait until it is time for another interaction
    }

    return 0;

}
