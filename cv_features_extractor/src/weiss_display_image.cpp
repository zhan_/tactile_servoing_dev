/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS
#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message
#include <opencv2/imgproc/imgproc.hpp>       							// Include headers for OpenCV's image processing
#include <opencv2/highgui/highgui.hpp>		 							// Include GUI modules
#include <math.h>   													// sqrt
#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/Image_weiss.h>

#include <dynamic_reconfigure/server.h>
#include <cv_features_extractor/improc_paramsConfig.h>

using namespace cv;
using namespace std;

/// class feature extraction
/// convert ros_image to open_cv image
/// subscribe to /tactile_image topic
/// extract features
/// publish features
///
/// later: call service to corner servoing,
/// edge, etc.


class featureExtract
{
public:
    ros::NodeHandle nh_;
    //create transporter
    image_transport::ImageTransport it_;
    featureExtract(int tactile_sensor_num_);
    ~featureExtract();



    // for converting from ros image to opencv
    image_transport::Subscriber image_sub_;
    // create subscriber to subscribe to pressure values matrix
    ros::Subscriber pressure_val_sub_;
    // publish control features
    ros::Publisher pub_featuresc;
    ros::Publisher pub_featuresp;
    // publish desired features
    ros::Publisher pub_featuresd;

    // openCV windows name
    string tact_im_PCA;
    string Tactile_Image;
    string window_weiss;

    /// function members
    double getOrientation(vector<cv::Point> &pts, cv::Mat &img);
    void pcaAnalysis (cv::Mat& t_image_orogin_);
    void imageCb(const sensor_msgs::ImageConstPtr& msg_);
    //void callback_pressures(const tactile_servo_msgs::Image_weissConstPtr& msg_weiss);
    void publish_features();
    /// messages to be published
    tactile_servo_msgs::featuresControl features_for_control;
    tactile_servo_msgs::featuresPlanning features_for_planning;
    tactile_servo_msgs::featuresControl features_desired;

    /// extracted features
    float offset_x, offset_y, total_pressure;
    float orient_des;
    float object_center_x, object_center_y, pos1_x, pos2_x, pos1_y, pos2_y;
    // to get line equation
    float object_center_x_2, object_center_y_2;

    /// OpenCv Headers
    cv::Mat src, bw, rsz, pcaImg, tactImg, colorImg, colorPca;

    // weiss pressure 12 bit
    cv::Mat weiss_pressure;

    /// sensor center to compute offset with object frame of reference
    unsigned int sensor_center_offset_x;
    unsigned int sensor_center_offset_y;

    string SensorName;

    bool publish;

    int rows;
    int cols;
    int step;


    // TODO must be reconfigurable dynamic reconfigure
    int PcaThreshold;
    int PcaAreaThreshold;// = 14;

    //to scale the size of tactile image
    int scaling_factor; // 30


    float orientation_in_deg_old;
    float filter_length_features;
    float sample_factor_features;

    // publish not published
    bool is_ready_to_pub;
    bool is_published;
    bool is_spinned;

    //check how long it takes in general
    double global_end_calc_time;
    double global_begin_calc_time;
    double global_first_time_ready_to_pub;
    double global_second_time_ready_to_pub;
    bool first_entry;

    // change im proc params on the fly
    void cb_dynamic_reconf(cv_features_extractor::improc_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<cv_features_extractor::improc_paramsConfig> change_improc_param_server;
    dynamic_reconfigure::Server<cv_features_extractor::improc_paramsConfig> :: CallbackType f;



};


featureExtract::featureExtract(int tactile_sensor_num_):it_(nh_)
{
    //nh_ = n_h;
    //it_ = nh_;

    // read from config file the name of a topic
    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);
    ROS_ASSERT_MSG(my_list.getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["tactile_image_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 8th pos");
    std::string topic_name = static_cast<std::string> (my_list[tactile_sensor_num_]["tactile_image_topic_name"]);
    std::string topic_name_pressure = static_cast<std::string> (my_list[tactile_sensor_num_]["tactile_pressure_topic_name"]);

    // subscribe to the tactile image topic
    image_sub_ = it_.subscribe("/"+topic_name, 1, &featureExtract::imageCb, this);
    //pressure_val_sub_  = nh_.subscribe("/"+topic_name_pressure,1, &featureExtract::callback_pressures,this);



    // get topic names of control features for a given sensor
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 9th pose");
    std::string topic_name_pub_c = static_cast<std::string> (my_list[tactile_sensor_num_]["control_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    std::string topic_name_pub_p = static_cast<std::string> (my_list[tactile_sensor_num_]["planning_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["desired_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    std::string topic_name_pub_d = static_cast<std::string> (my_list[tactile_sensor_num_]["desired_topic_name"]);



    ///pubish features to the topic according to the parameters
    pub_featuresc = nh_.advertise<tactile_servo_msgs::featuresControl>(topic_name_pub_c, 1);
    pub_featuresp = nh_.advertise<tactile_servo_msgs::featuresPlanning>(topic_name_pub_p, 1);
    pub_featuresd = nh_.advertise<tactile_servo_msgs::featuresControl>(topic_name_pub_d,1);

    // get the name of a sensor frame from ros_image message
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_].hasMember("frame_id_header"), "Test frame_id_header");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["frame_id_header"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string frame_id_header");
    SensorName = static_cast<std::string> (my_list[tactile_sensor_num_]["frame_id_header"]);
    // fill up the header of the message to control tactile_servo_msgs::featuresControl
    features_for_control.header.frame_id = SensorName;
    features_desired.header.frame_id = SensorName;

    ROS_ASSERT_MSG(my_list[tactile_sensor_num_].hasMember("dimensions"), "Test dimensions");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["dimensions"].getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array at 2nd position");
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["dimensions"][0]["cells_x"].getType() == XmlRpc::XmlRpcValue::TypeInt, "Test double at 2nd position in array");
    rows = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][0]["cells_x"]);
    cols = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][1]["cells_y"]);
    step = static_cast<int> (my_list[tactile_sensor_num_]["dimensions"][1]["cells_y"]);

    //filtering the orientation of an edge
    orientation_in_deg_old = 0;

    //booleans for publishing and cleaning
    is_ready_to_pub = false;
    is_published = false;

    float area_tactel = 8;



    sensor_center_offset_x = rows/2;
    sensor_center_offset_y = cols/2;


    PcaThreshold = 30;
    PcaAreaThreshold = 14;
    scaling_factor = 30;


    string Tactile_Image = "Tactile_Image";
    string window_weiss = "Weiss_12bit";
    string tact_im_PCA = "tactile image and PCA";

    cv::namedWindow("TI&F", WINDOW_NORMAL);
//    cv::resizeWindow("TI&F", cols*scaling_factor, rows*scaling_factor);
    cv::namedWindow("binarized", WINDOW_NORMAL);
    cv::startWindowThread();



//    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown.
//    cv::fwind(tact_im_PCA);
//    cv::namedWindow(Tactile_Image);
//    cv::namedWindow( "TempWindow", WINDOW_AUTOSIZE );
//    cv::namedWindow( "convertedFromPressure", WINDOW_AUTOSIZE );
//    cv::namedWindow( window_weiss, WINDOW_AUTOSIZE );




    /// proper way for subscriber and pub
    /// http://docs.ros.org/groovy/api/turtlesim/html/turtle_8cpp_source.html#l00054
    /// velocity_sub_ = nh_.subscribe("command_velocity", 1, &Turtle::velocityCallback, this);
    /// pose_pub_ = nh_.advertise<Pose>("pose", 1);
    /// set_pen_srv_ = nh_.advertiseService("set_pen", &Turtle::setPenCallback, this);
    /// teleport_relative_srv_ = nh_.advertiseService("teleport_relative", &Turtle::teleportRelativeCallback, this);

    // calculation time check
    first_entry = true;

    // features publish filter in time domain
    filter_length_features = 2;
    sample_factor_features = (float)1.0/filter_length_features;

    // dynamic reconfigure
    f = boost::bind(&featureExtract::cb_dynamic_reconf, this, _1, _2);
    change_improc_param_server.setCallback(f);



}

featureExtract::~featureExtract(){
    cv::destroyAllWindows();
}
/// dynamic reconfigure callback function
/// change the sensitivity from rqt online
/// to adjust the output values of each contact
/// cell
void featureExtract::cb_dynamic_reconf(cv_features_extractor::improc_paramsConfig &config, uint32_t level){
//    ROS_INFO("Reconfigure Request:%f", config.sensitivity_gazebo);
    PcaThreshold = config.improc_bin_thresh;
    filter_length_features = config.improc_filter_length_features;
    PcaAreaThreshold = config.improc_area_thresh;
    scaling_factor = config.improc_scaling_factor;
    sample_factor_features = (float) 1.0 / filter_length_features;
    ROS_INFO("PcaThreshold:%d", (int)PcaThreshold);
    ROS_INFO("filter_length_features:%d", (int)filter_length_features);
    ROS_INFO("PcaAreaThreshold:%d", (int)PcaAreaThreshold);
}




/********************************************
 * Processing Image
 *********************************************/

///http://docs.opencv.org/master/d1/dee/tutorial_introduction_to_pca.html
/// https://robospace.wordpress.com/2013/10/09/object-orientation-principal-component-analysis-opencv/
double featureExtract::getOrientation(vector<cv::Point> &pts, cv::Mat &img)
{
    double begin_calc_time = ros::Time::now().toSec();
//    ROS_INFO("in get orientation begin %f\n", begin_calc_time );
    /// Construct a buffer used by the pca analysis
    Mat data_pts = Mat(pts.size(), 2, CV_64FC1);
//    std::cout<<"******size of vector of points for PCA "<<pts.size()<<std::endl;
//    std::cout<<"****** x of point 0 for PCA "<<pts[0].x  <<std::endl;
//    std::cout<<"****** y of point 0 for PCA "<<pts[0].y  <<std::endl;
//    std::cout<<"****** y of last point for PCA "<<pts[pts.size()-1].y  <<std::endl;
    double orientation_deg, orientation_rad;

    for (int i = 0; i < data_pts.rows; ++i){
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }
    /// Perform PCA analysis
    cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
    /// Store the position of the object
    cv::Point pos = cv::Point(pca_analysis.mean.at<double>(0, 0),
                      pca_analysis.mean.at<double>(0, 1));
    /// Store the eigenvalues and eigenvectors
    vector<cv::Point2d> eigen_vecs(2);
    vector<double> eigen_val(2);
    for (int i = 0; i < 2; ++i){
        eigen_vecs[i] = cv::Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                pca_analysis.eigenvectors.at<double>(i, 1));

        eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
    }

    /// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)

    cv::circle(colorPca, pos, 3, CV_RGB(255, 255, 255), 3);
//    cv::line(colorPca, pos, pos + 0.02 * cv::Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
    cv::line(colorPca, pos, pos + 0.02 * cv::Point(eigen_vecs[0].x * eigen_val[0], eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);

    //cv::line(colorPca, pos, pos + 0.02 * cv::Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);
    //draw desired orientation

    orient_des = 89.9;
    float temp_y = std::tan(orient_des*M_PI/180);

    // draw desired features
    cv::line(colorPca, pos, pos + 2 * cv::Point(1, temp_y), CV_RGB(0, 255, 255), 1,8);
    cv::line(colorPca, pos, pos - 2 * cv::Point(1, temp_y), CV_RGB(0, 255, 255), 1,8);
    cv::circle(colorPca, cv::Point(0,10), 3, CV_RGB(0, 255, 255), 3);
    cv::circle(colorPca, cv::Point(0,50), 3, CV_RGB(0, 255, 255), 3);
    cv::circle(colorPca, cv::Point(0,0.0), 3, CV_RGB(0, 120, 120), 3);
//    cv::circle(colorPca, cv::Point(10,0), 3, CV_RGB(0, 120, 120), 3);
//    cv::circle(colorPca, cv::Point(50,0), 3, CV_RGB(0, 120, 120), 3);

    ///display image
    cv::resizeWindow("TI&F", cols*scaling_factor, rows*scaling_factor);
    cv::imshow( "TI&F", colorPca );
    // cv::imshow("binarized", bw);
   // cv::waitKey(3);

    /// axes
    /// drawAxis(img, cntr, p1, Scalar(0, 255, 0), 1);
    /// drawAxis(img, cntr, p2, Scalar(255, 255, 0), 5);

    /// The orientation of the main component of PCA
//    orientation_rad = std::atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);
    orientation_rad = std::atan2(eigen_vecs[0].y, eigen_vecs[0].x);

    orientation_deg = orientation_rad * (180 / M_PI);



    /// TODO
    /// sensor is 6 by 14. the first vector is the largest eigen value.
    /// there can be cases when the orientation of the largest eigen
    /// vecto we were tracking will switch to the orientation of the
    /// second eigenv vector which suddenly bacame larger than the first
    /// eigen vector.

    /// Center of contact
    object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor;
    object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;

    /// To check if it is a line, a push or if is not known
    pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor;
    pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
    pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor;
    pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;

    /// return orientation_deg;
    /// length
    /// coc
    /// cop
    ///
    double end_calc_time = ros::Time::now().toSec();
//    ROS_INFO("in get orientation end %f\n", end_calc_time );
//    ROS_INFO("in get orientation diff%f\n", end_calc_time - begin_calc_time);
    return orientation_deg;
}

void featureExtract::pcaAnalysis (cv::Mat& t_image_orogin_){

    //	cout<<"*********************  PCA  ********************"<<endl;
    //  Prepare the image for findContours
    // http://stackoverflow.com/questions/8449378/finding-contours-in-opencv
    //        cv::cvtColor(image, image, CV_BGR2GRAY);
    //        cv::threshold(image, image, 128, 255, CV_THRESH_BINARY);
    //  canny esge detector can also work to prepare image
    // http://docs.opencv.org/doc/tutorials/imgproc/shapedescriptors/find_contours/find_contours.html
    // http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_canny/py_canny.html
    pcaImg = rsz.clone();


    /// Threshold image for binarize the data and store in bw (binary image)


    cv::threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
cv::imshow("binarized", bw);
    /// Find all objects of interest
    vector<vector<cv::Point> > contours;
    vector<cv::Vec4i> hierarchy;

    /// Find Contours
    cv::findContours(bw, contours, hierarchy,  CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0));

    /// Extract the contours so that
    vector<vector<cv::Point> > approxShape;

    /// Resize to use with approximating function
    approxShape.resize(contours.size());

    /// For each object get features
    if (contours.size() > 0){
//         std::cout<<"Features extracted from "<<contours.size()<<" contacts detected"<< std::endl;

        /// Iterate for each contact detected
        ///             for (size_t i = 0; i < contours.size(); ++i){

            for (size_t i = 0; i < 1; ++i){
                /// Aproximate the contour to detect the edges from an approximate shape and not from raw data
                double epsilon = cv::arcLength( contours[i], false ) / scaling_factor;
//                ROS_INFO("arc_length of contour = %f", (double) epsilon);

                cv::approxPolyDP(contours[i], approxShape[i], cv::arcLength( contours[i], false ) / scaling_factor, true);

                int edgesNumber = approxShape[i].size() / approxShape.size();
//                std::cout<<"Number of edges: "<< approxShape[i].size() / approxShape.size() <<std::endl;

                /// Get perimeter and area
                //double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
                double areaContour = cv::contourArea( approxShape[i] ,false) / scaling_factor;
                //cout << "Area: "<<areaContour<<"  Perimeter: "<<perimeterContour<<endl;

                /// Ignore if too small or too large
                if ( areaContour < PcaAreaThreshold ) continue;

                /// Draw the contour
                cv::drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);

                /// Get the object orientation and the PCA components at once
                float orientation_in_deg_new = getOrientation(contours[i], bw);

                /// calculate offset with the sensor frame
                offset_x = object_center_x-sensor_center_offset_x;
                offset_y = sensor_center_offset_y-object_center_y;

                /// Calculate the pressure/force
                ///

                /// todo find total pressure of the contact area
                /// todo COP of each contact


                /// at the end of processing each contact
                /// push the extracted features
                /// when they published clear them

                /// filter length = 2; sample factor = 1/filter_length;
                float orientation_in_deg_to_pub = (1.0 - sample_factor_features) * orientation_in_deg_old;
//                orientation_in_deg_to_pub_rviz = orientation_in_deg_to_pub;
                orientation_in_deg_to_pub = orientation_in_deg_to_pub + sample_factor_features * orientation_in_deg_new;
                orientation_in_deg_old = orientation_in_deg_to_pub;
                float to_test = 90; // orientation_in_deg_to_pub

                features_for_control.contactOrientation.push_back(orientation_in_deg_to_pub);
                features_for_control.contactOffset_x.push_back(offset_x);
                features_for_control.contactOffset_y.push_back(offset_y);
                features_for_control.contactPressure.push_back(total_pressure);
                features_for_control.centerContact_x.push_back(object_center_x);
                features_for_control.centerContact_y.push_back(object_center_y);

                features_for_planning.contactsNo = contours.size();
                is_ready_to_pub = true;

//                std::cout<< " size of an array  in PCA = "<< features_for_control.contactOrientation.size()  <<std::endl;

                /*if (features_for_control.contactOrientation.size()>0){ is_ready_to_pub = true;
                    std::cout<< " size of an array in PCA must be 1 = "<< features_for_control.contactOrientation.size() <<std::endl;}
                else {
                    std::cout<< " size of an array in PCA must be 0 = "<< features_for_control.contactOrientation.size() <<std::endl;
                    //is_ready_to_pub = true;
                }*/



            }
            // rof contacts

    }
}

void featureExtract::imageCb(const sensor_msgs::ImageConstPtr& msg_)
{
    double begin_calc_time = ros::Time::now().toSec();
//    ROS_INFO("%f\n", begin_calc_time );


    /// todo functions in callack as in gazebo_image


    uchar img[rows][cols];
    int imgshow[rows][cols];

//    std::cout<<"********************number of x cells= "  << rows<< std::endl;

//    std::cout<<"********************number of y cells= "  << cols<< std::endl;

//    img[rows][cols];
//    imgshow[rows][cols] = {0};

    /// windows and trackbars name
    /// to delete

    //convert the ROS image message to a CvImage suitable for working with OpenCV
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
        cv_ptr = cv_bridge::toCvCopy(msg_, sensor_msgs::image_encodings::TYPE_8UC1);
    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    // pressureToImage(cv_ptr->image.data);

    /// fill data extracted from array in a tactile image matrix
    int c;
    c = 0;
    for(int i=rows-1; i>=0; --i) {
        for(int j=0; j<cols; j++) {
                // to display in screan
                imgshow[i][j]=cv_ptr->image.data[c];
                // to debug
                img[i][j]=cv_ptr->image.data[c];
                if (imgshow[i][j] > 1) { //13
                    total_pressure += img[i][j];
                }
        ++c;
        }
    }
    src = cv::Mat(rows, cols, CV_8U, img);
    /// 255 is min pressure
    /// TODO send not inversed values
    /// use mono16: CV_16UC1, 16-bit grayscale image
    /// do not display image to control faster
    /// do not resize image
//    cv::Mat invSrc =  cv::Scalar::all(255) - src;
//    src = invSrc.clone();


    cv::resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
    // convert to color image
    cv::cvtColor(rsz, colorImg, CV_GRAY2BGR);
    colorPca = colorImg.clone();


    // todo pass to pcaAnalysis cv::Mat with greyscale original image
    cv::Mat smthing = Mat(rows,cols, CV_8U, double(0));

    //analyse image Principle components
    pcaAnalysis (rsz);

    ///todo smooth the feature values with PID as in Bielefeld
    /// or filter in time domain.
    ///
    double end_calc_time = ros::Time::now().toSec();
//    ROS_INFO("%f\n", end_calc_time );
//    ROS_INFO("%f\n", end_calc_time - begin_calc_time);
}

/// prcessing Image 12 bit
/// todo
/// image moments as in what can be inffered
/// delete temporaly for optimization
/* void featureExtract::callback_pressures(const tactile_servo_msgs::Image_weissConstPtr& msg_weiss)
{
    int col_weiss = msg_weiss->width;
    int rows_weiss = msg_weiss->height;
    int img_weiss[14][6] = {};
    cout<<"Number of raws weiss: "<<  rows_weiss <<endl;
    ROS_INFO("I heard: [%d]", msg_weiss->data[1]);
    int counter_wess =0;
    for(int i=rows_weiss-1; i>=0; --i) {
        for(int j=0; j<col_weiss; j++) {
            img_weiss[i][j]=msg_weiss->data[counter_wess];
            ++counter_wess;
        }
    }
    cout<<"weiss_pressure"<<  img_weiss[5] <<endl;
    weiss_pressure = cv::Mat(14, 6, CV_16U, img_weiss);

}*/
 void featureExtract::publish_features(){
     // if can_pub
     features_for_control.header.stamp = ros::Time::now();
     features_for_planning.header.stamp = ros::Time::now();
     features_desired.header.stamp = ros::Time::now();


     /* for testing data published with empty features_for_control.contactOrientation
     features_for_control.contactOrientation.clear();
     float to_test = 90; // orientation_in_deg_to_pub
     features_for_control.contactOrientation.push_back(to_test);*/
//     std::cout<< " is ready to pub in publish function = "<< is_ready_to_pub <<std::endl;

     if(is_ready_to_pub){
         if(first_entry){
             global_first_time_ready_to_pub = ros::Time::now().toSec();
//             ROS_INFO("publish first entry %f\n", global_first_time_ready_to_pub );
//             ROS_INFO("publish difference btw entrees beg %f\n", global_first_time_ready_to_pub - global_second_time_ready_to_pub );
//             ROS_INFO("size to pub %ld \n", (long int)features_for_control.contactOrientation.size() );
             first_entry = false;
         }
         else{
             global_second_time_ready_to_pub = ros::Time::now().toSec();
//             ROS_INFO("publish second entry %f\n", global_second_time_ready_to_pub );
//             ROS_INFO("publish difference btw entrees %f\n", global_second_time_ready_to_pub - global_first_time_ready_to_pub);
//             ROS_INFO("size to pub %ld \n", (long int)features_for_control.contactOrientation.size() );
             first_entry = true;
         }

         // pub_featuresc.publish(features_for_control);
         features_desired.contactOrientation.push_back( orient_des );
         pub_featuresd.publish(features_desired);

//         std::cout<< " des orient"<< features_desired.contactOrientation.at(0) <<std::endl;
//         std::cout<< " size of an array = "<< features_for_control.contactOrientation.size()  <<std::endl;

         if (features_for_control.contactOrientation.size()>0){
//             std::cout<< " feedback orient"<< features_for_control.contactOrientation.at(0) <<std::endl;
             pub_featuresc.publish(features_for_control);
             features_for_control.contactOffset_x.clear();
             features_for_control.contactOffset_y.clear();
             features_for_control.contactPressure.clear();
             features_for_control.contactOrientation.clear();
             features_desired.contactOrientation.clear();
             features_for_control.centerContact_x.clear();
             features_for_control.centerContact_y.clear();

             is_published = true;
             is_ready_to_pub = false;

         }



     }
     else{
//         std::cout<< " is ready to pub in publish function should be false = "<< is_ready_to_pub <<std::endl;
         is_published = false;

     }

     /// boolean to see the triggering

//     is_spinned = false;
//     std::cout<< " is_spinned = "<< is_spinned <<std::endl;

     ros::spinOnce();
//     std::cout<< " is ready to pub in publish function after spin = "<< is_ready_to_pub <<std::endl;

//     is_spinned = true;
//     std::cout<< " is_spinned = "<< is_spinned <<std::endl;


     //pub_featuresp.publish(features_for_planning);
     //clear features vector
     /// clear arrays
     ///
     /// we can clean it if it is published
     ///
     /*if(is_published){
         features_for_control.contactOffset_x.clear();
         features_for_control.contactOffset_y.clear();
         features_for_control.contactPressure.clear();
         features_for_control.contactOrientation.clear();
         features_desired.contactOrientation.clear();
         is_published = false;
     }*/

}



/**
 * Main functions
 */
int main(int argc, char** argv)
{
    ROS_INFO("EXTRACT FEATURES NODE");
    ros::init(argc, argv, "weiss_display_image");

    featureExtract weiss_extract(0);
    //ros::spinOnce();


    // Set up a dynamic reconfigure server.
    // This should be done before reading parameter server values.
    //   dynamic_reconfigure::Server<cv_features_extractors::cv_features_extractor_paramsConfig> dr_srv;
    //   dynamic_reconfigure::Server<cv_features_extractors::cv_features_extractor_paramsConfig>::CallbackType cb;
    //   cb = boost::bind(&featureExtract::imageCb, ic, _1, _2);
    //   dr_srv.setCallback(cb);

    //   // Declare variables that can be modified by launch file or command line.
    //   int a;
    //   int b;


    ros::Rate loop_rate(75);												// Loop at 100Hz until the node is shut down
    while (ros::ok())
    {
        weiss_extract.publish_features();

        //ros::spinOnce();													// do this
        loop_rate.sleep();			 										// wait until it is time for another interaction
    }

    return 0;

}
