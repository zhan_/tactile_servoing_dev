#ifndef PCA_HPP_
#define PCA_HPP_
# include "extractor.hpp"


/// Tactile Sensor data
const int sensorsRows = 18;
const int sensorsCols = 4;
const int noTactels = 3;
unsigned int scaling_factor = 30;										//to scale the size of tactile image
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
const string pcaAreaTrackbarName = "Area";
int rangePcaArea = 100; 
int PcaAreaThreshold = 14;

const string windowBW = "Binarize";
const string bwTrackbarName = "Th";
int binarizeThreshold= 190;

class PCAnalysis 
{
  private:

  
  public:
    PCAnalysis ()
    {
      cv::namedWindow(windowBW, WINDOW_AUTOSIZE );
    }

    ~PCAnalysis()
    { 
	  //cv::destroyWindow(windowBW);
    }
	   
    bool getFeatures_ (Mat TactImg, Mat colorImg , int no_tactels, float sensor_center_offsetX , float sensor_center_offsetY)
    {
	  pcaImg = TactImg.clone(); Mat pcaColImg = colorImg.clone();
	  				
	  createTrackbar( bwTrackbarName, windowBW, &binarizeThreshold, 255);
	  threshold(pcaImg, bw, binarizeThreshold, 255, CV_THRESH_BINARY);      // Threshold image for binarize the data and store in bw (binary image)
	  bitwise_not ( bw, bw );  
	  imshow(windowBW, bw); 
	  waitKey(3);	 

      /// Find all objects of interest
      vector<vector<Point> > contours;  vector<Vec4i> hierarchy;
      findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0)); // find contacts
      
	  vector<vector<Point> > approxShape;  approxShape.resize(contours.size());  // improve data
	  contacts = contours.size(); features_for_planning.contactsNo = contacts;   // store contacts number in message

      if (contours.size() > 0)
      {	// if there are contacts
        const int areaSensor = 720; float normalized_areas[100]={};     // Normalize the area with the tactile image area 
		for (size_t i = 0; i < contacts; ++i)
		{// Iterate for each contact detected		
		  approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true); // Aproximate the contour to detect the edges from an approximate shape and not from raw data
		  int edgesNumber = approxShape[0].size() / approxShape.size(); // Get the number of edges
		  features_for_planning.contactEdges.push_back(edgesNumber);	// store the number of edges in the message 
		  double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor; // Get perimeter 
		  features_for_planning.contactPerimeter.push_back(perimeterContour);	
		  double areaContour = contourArea( approxShape[i] ,false) / scaling_factor; // Get area
		  normalized_areas[i] = areaContour / areaSensor;               // Normalize the are for small point and big point contacts
		  features_for_planning.contactArea.push_back(areaContour);	
		  int pcaContact = PcaAreaThreshold; if ( areaContour < pcaContact+90 ) continue; // Ignore contact if too small or too large, the threshold can be adjusted dinamically
		  drawContours(pcaColImg, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0); // Draw the contour of the contact in the color image	
		  /// Construct a buffer used by the pca analysis
		  vector<Point> pts = contours[i];  
		  Mat img = bw.clone();
		  Mat data_pts = Mat(pts.size(), 2, CV_64FC1);			
		  for (int i = 0; i < data_pts.rows; ++i) 
		  {				
			data_pts.at<double>(i, 0) = pts[i].x;
			data_pts.at<double>(i, 1) = pts[i].y;	
   		  }
          PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW); // Perform PCA analysis of the contact image		  
		  Point pos = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1)); // Store the position of the object
		  vector<Point2d> eigen_vecs(2);  vector<double> eigen_val(2);  // To store the eigenvalues and eigenvectors	
		  for (int i = 0; i < 2; ++i) 
		  { // get the first 2 principal components
		    eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0), pca_analysis.eigenvectors.at<double>(i, 1));
			eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
		  }
		  /// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
		  circle(pcaColImg, pos, 3, CV_RGB(255, 0, 0), 3);
		  line(pcaColImg, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
		  line(pcaColImg, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);		  		  		     
		  object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor; object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor; // Center of pressure
          /// Adjust the size of the eigen vectors to fit in the image
		  pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
		  pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;	
		  orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);	orientation_deg = -(orientation_rad * (180 / 3.1416)) - 90; // The orientation of the main component of PCA
		  features_for_control.contactOrientation.push_back(orientation_deg);
		  /// Now calculate offset with the sensor frame
		  offset_x = object_center_x - sensor_center_offsetX;	
	      features_for_control.contactOffset_x.push_back(offset_x);	    // store the offeset in control message
	      features_for_planning.contactPoses.push_back(offset_x);		// store center of pressure in planing message	 
		  offset_y = sensor_center_offsetY - object_center_y;	
		  features_for_control.contactOffset_y.push_back(offset_y);
		  features_for_planning.contactPoses.push_back(offset_y);
		  /// Calculate the pressure
		  float area_tactel = 8;
		  total_pressure /= 100;
		  features_for_control.contactPressure.push_back(total_pressure);
		  total_force = total_pressure * (no_tactels * area_tactel);
		  features_for_control.contactForce.push_back(total_force);
		  total_pressure = 0;
		  /// Detect Slipage		
		  //	slipDetection (offset_x, offset_y,15);
		  features_for_planning.slip.push_back(false); 			
		  /// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
		  distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) ); distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );									
			/// Get the contact type and shape
			//get_contactTypeShape (normalized_areas,contacts, distance1, distance2, edgesNumber, perimeterContour);
			//features_for_planning.contactTypes.push_back(contactType);	
			//features_for_planning.contactShapes.push_back(contactShape);
			
			publish = true;				
    }

			/// Display it
	  namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	  //createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	  createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	  imshow(windowPCA, pcaColImg); 
	  waitKey(3);

		/// Reset variables 
		total_force = 0; orientation_deg = 0; 
		offset_x = 0; offset_y = 0;	no_tactels = 0;
	
	
	return 1;
		

	}

	}
	

};
# endif
