#ifndef IMAGEFILTERS_HPP_
#define IMAGEFILTERS_HPP_

const string MedianImage = "Median F";
const string GaussianImage= "Gaussian F";
int gaussian_val = 2;
int gaussian_max_trackbar = 7;

using namespace cv;

class ImageFilters 
{
  private:

  
  public:
    ImageFilters()
    {
      //cv::namedWindow(windowBW, WINDOW_AUTOSIZE );
    }

    ~ImageFilters()
    { 
	  //cv::destroyWindow(windowBW);
    }
	   
	Mat filterMedian_ (Mat image, int ksize, bool show)
    {
	  Mat medImg;
      cv::medianBlur(image, medImg, ksize);                            // median filter
      if (show)
      {
        namedWindow( MedianImage, WINDOW_AUTOSIZE );					// Create a window for display.
	    imshow( MedianImage, medImg );                  				// Show image inside it.
	    waitKey(3);
      }
	  return medImg;
	}
		
	Mat filterGaussian_ (Mat image, int sigmaX, int sigmaY, int borderType, cv::Size ksize, bool show)
    {
	  Mat gaussImg;
	  cv::GaussianBlur(image, gaussImg, ksize, sigmaX, sigmaY, borderType );   // gaussian filter 
      if (show)
      {
        namedWindow( GaussianImage, WINDOW_AUTOSIZE );					// Create a window for display.
       // createTrackbar( "Gaussian", GaussianImage, &gaussian_val , gaussian_max_trackbar );
	    imshow( GaussianImage, gaussImg );                  			// Show image inside it.
	    waitKey(3);
      }
	  return gaussImg;
	}
};
#endif
