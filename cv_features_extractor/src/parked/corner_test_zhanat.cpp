/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
# include "extractor.hpp"
# include "cornerGoodFeaturesToTrack.hpp"

float area_tactel = 8;

unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string windowArray = "Tactile Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;


/// PCA
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 30;
int maxPCAThreshold = 255; 
const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 14;
int rangePcaArea = 100; 
int inc = 0;




/// Harris
int thresh = 200;





/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber

 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowPCA);
    
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
	  
	  
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	
    /// Extract from topic header the name of the sensor in contact
	SensorName = cv_ptr->header.frame_id;
	features_for_control.header.frame_id = SensorName;
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 	   									// step --> cols
    if (rows != SENSOR_ROWS) cout << "cv_bri" <<endl;
    
    sensor_center_offset_x = SENSOR_COLS/2;
    sensor_center_offset_y = SENSOR_ROWS/2;
    
	uchar img[18][4] = {}; 		
    int c = 0;															
    total_pressure = 0;    no_tactels = 0; 
    /// fill data extracted from array in a tactile image matrix    	
	for(int i=SENSOR_ROWS-1; i>=0; --i) 
	{	
	  for(int j=0; j<SENSOR_COLS; j++) 
	  {
		img[i][j]=cv_ptr->image.data[c];						        
		if (img[i][j] > 30) 
		{ 
		  total_pressure += img[i][j]; 						
		  ++no_tactels; 
		}		
	    ++c;
	  }
	}

	//if(total_pressure > 0) 
	//{
	  src = Mat(18, 4, CV_8U, img);									    // gray scale image, convert it to Mat type matrix img
      //cout << "Image " << endl<<src<<endl;
      
      /// src is a tactile image
       
      Mat normIm;
      normalize( src, normIm, 0, 255, NORM_MINMAX, CV_8U, Mat() );      // Normalize data so it can be processed as an gray scale image and also to increase the range for detection 
      //cout << "Normalized Image " << endl<<normIm<<endl;
	  threshold(src, bw, PcaThreshold, 255, CV_THRESH_BINARY);       // black and white image
	  if(total_pressure > 20) 
	  {
	    cvtColor(normIm, colorImg, CV_GRAY2BGR);   						// color  image
	  }
	  else
	  {
		cvtColor(src, colorImg, CV_GRAY2BGR);  
	  }
	  bitwise_not ( colorImg, colorImg );                               // mirror image colors

	  /// Resize the tactile image and display it
      Mat rsz_colorImg;
      resize( colorImg , rsz_colorImg, Size(), SCALING_FACTOR, SCALING_FACTOR ,  INTER_NEAREST);  
      namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
      imshow( windowArray, rsz_colorImg );              				// Show image inside it.
	  waitKey(3);


/// GoodFeaturesToTrack



	  /// Find corners
	  
	  
	  Corner_GoodToTrack co;
	  co.getCorners_(0,0,src,colorImg);	
		
	  /// Display the processed image 
	 // namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	  //createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	  //createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	  //imshow(windowPCA, colorPca); 
	  //waitKey(3);
    //}	
}

/**
static void corners( int, void*, cv::Mat normIm)
{
  h_gray_img  = normIm.clone();  h_col_img = colorImg.clone();      // Copy original images to then operate just in copies
  resize( h_col_img, colorPca, Size(), SCALING_FACTOR , SCALING_FACTOR ,  INTER_NEAREST);  // resize to plot corners
  Mat dst, dst_norm;                                                    // This is to store the detector results
  dst = Mat::zeros( h_col_img.size(), CV_32FC1 );                       // chanel must be CV_32FC1 type and with the size of source image
  /// Detecting corners with harris
  int blockSize = 8, kernel_mask = 9; double k = 0.04;                  // Detector parameters --> tune it
  cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );
  normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );     // Normalizing results from 0 to 255
  /// Drawing a circle around corners and getting the number of corners
  int pos_x, pos_y, TEMP1[200], TEMP2[200];                           // to store the corner positions
  int cornersNumber = 0;	
  for( int j = 0; j < dst_norm.rows ; j++ )
  { 
    for( int i = 0; i < dst_norm.cols; i++ )
    {
      if( (int) dst_norm.at<float>(j,i) > PcaThreshold + 200)
	  {
	    cornersNumber ++; 
		circle(colorPca, Point( i*SCALING_FACTOR , j*SCALING_FACTOR  ), 5, CV_RGB(0, 25, 255) , 2, 8, 0 ); //draw circle arround corner
		pos_x = (i/SCALING_FACTOR ) - sensor_center_offset_x;			// get values with respect to the sensor frame
		pos_y = sensor_center_offset_y - (j/SCALING_FACTOR);				
		TEMP1[cornersNumber] = i; TEMP2[cornersNumber] = j;             // temporal debugging data
 		features_for_planning.cornersPoses.push_back(pos_x);            // store corners positions in ros message
		features_for_planning.cornersPoses.push_back(pos_y);	
	  }			
	}
  }
  if ( cornersNumber < 3 ) for (int i = 1 ; i <= cornersNumber ; i++)  cout << "i " << TEMP1[i]<< "   ,j " << TEMP2[i] << endl; // debugging purposes
  features_for_planning.cornersNo = cornersNumber;                      // store the number of corners
}
 */







};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "extractor_pps_once");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<cv_features_extractor::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<cv_features_extractor::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this
  

  int count = 0;
  while (ros::ok())
  {
	if (publish) { 
		
		publish = false;
		
		if ( contacts>0 ) {
			
			
			features_for_control.header.stamp = ros::Time::now(); 
			pub_featuresc.publish(features_for_control);								
			
			/// clear arrays	
			features_for_control.contactOffset_x.clear();											
			features_for_control.contactOffset_y.clear();
			features_for_control.contactForce.clear();
			features_for_control.contactPressure.clear();
			features_for_control.contactOrientation.clear();
			
		
			features_for_planning.header.stamp = ros::Time::now();
			pub_featuresp.publish(features_for_planning);
			
			/// clear arrays
			features_for_planning.contactTypes.clear();
			features_for_planning.contactPoses.clear();
			features_for_planning.contactShapes.clear();
			features_for_planning.cornersPoses.clear();
			features_for_planning.contactEdges.clear();
			features_for_planning.edgesDimensions.clear();
			features_for_planning.contactArea.clear();
			features_for_planning.contactPerimeter.clear();
			features_for_planning.slip.clear();
 
			
		}  
		
		
	}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
