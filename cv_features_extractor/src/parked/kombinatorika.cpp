#include <ros/ros.h>
#include <math.h>   													// sqrt
#include <tactile_servo_msgs/featuresControl.h>

#include <dynamic_reconfigure/server.h>
#include <cv_features_extractor/kombinatorika_paramsConfig.h>

#include <opencv2/core/core.hpp>


class featureExtract
{
public:
    ros::NodeHandle nh_;
    featureExtract();
    ~featureExtract();
    ros::Publisher pub_featuresc;

    tactile_servo_msgs::featuresControl features_for_control;


    // change im proc params on the fly
    void cb_dynamic_reconf(cv_features_extractor::kombinatorika_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<cv_features_extractor::kombinatorika_paramsConfig> change_improc_param_server;
    dynamic_reconfigure::Server<cv_features_extractor::kombinatorika_paramsConfig> :: CallbackType f;

    int rows;
    int cols;
    int step;
    double cell_size_x;
    double cell_size_y;
    double angle;
    int active_cells;
    double delta_angle;

    void processing();

};

featureExtract::featureExtract()
{
    pub_featuresc = nh_.advertise<tactile_servo_msgs::featuresControl>("control_features", 1);

    features_for_control.header.frame_id = "weiss";

    // dynamic reconfigure
    f = boost::bind(&featureExtract::cb_dynamic_reconf, this, _1, _2);
    change_improc_param_server.setCallback(f);

    rows = 2;
    cols = 2;
    active_cells = 2;
}
featureExtract::~featureExtract(){

}

void featureExtract::cb_dynamic_reconf(cv_features_extractor::kombinatorika_paramsConfig &config, uint32_t level){
    rows = config.rows;
    cols = config.cols;
    cell_size_x = config.cell_size_x;
    cell_size_y = config.cell_size_y;
    angle = config.angle;
    active_cells = config.active_cells;
    delta_angle = config.delta_angle;
}

void featureExtract::processing(){
    int c;
    int img[rows][cols];
    img[0][0] = 255;
    img[0][1] = 255;
    img[1][0] = 0;
    img[1][1] = 0;

    src = cv::Mat(rows, cols, CV_8U, img);





}
