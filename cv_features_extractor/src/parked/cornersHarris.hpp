#ifndef CORNERHARRIS_HPP_
#define CORNERHARRIS_HPP_
# include "extractor.hpp"

int harris_thresh = 240;
int harris_max_thresh = 255;

const string corners_window = "Corners";


class Corner_Harris {
  private:


  public:
 
bool getCorners_( int, void*, cv::Mat normIm, cv::Mat colorImg, float sensor_center_offsetX , float sensor_center_offsetY , bool show)
{
  namedWindow( corners_window, CV_WINDOW_AUTOSIZE );
  createTrackbar( "th", corners_window, &harris_thresh, harris_max_thresh );
  h_gray_img  = normIm.clone();  h_col_img = colorImg.clone();      // Copy original images to then operate just in copies
  Mat dst, dst_norm;                                                    // This is to store the detector results
  dst = Mat::zeros( h_col_img.size(), CV_32FC1 );                       // chanel must be CV_32FC1 type and with the size of source image
  /// Detecting corners with harris
  int blockSize = 8, kernel_mask = 9; double k = 0.04;                  // Detector parameters --> tune it
  cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );
  if (dst.rows > 0) 
  { // if there are corners
    cout<<"corners:"<<endl;	
    normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );     // Normalizing results from 0 to 255
    int size_cornersArray = 10;
    int pos_x[size_cornersArray], pos_y[size_cornersArray], i_prev = 0, j_prev = 0, similarity_th = 10;                           // to store the corner positions
    int cornersNumber = 0;	
    for( int j = 0; j < dst_norm.rows ; j++ )
    { 	  
      for( int i = 0; i < dst_norm.cols; i++ )
      {
        if( (int) dst_norm.at<float>(j,i) >  harris_thresh)
	    { //corner according to thrshold	  
	    if ( i < abs(i_prev + similarity_th) && j < abs(j_prev + similarity_th) )  { i_prev = i; j_prev = j; }   // if corner is very close to previous one, then update previous corner value with the actual
	      if ( i != i_prev && j != j_prev )
	      {	// if corner is different enough from the previous one then	lets extract the corner position  
			cornersNumber ++;  
			if (cornersNumber > size_cornersArray )
			{ 
			  cout << "corners detected greater than storing array" << endl;
			  cout << "adjust the threshold or change the detector parameters" << endl;	
			  return 0;
			}
			else
			{	// now extract the info	and draw the corners	  	
     	      pos_x[cornersNumber] = (i/SCALING_FACTOR ) - sensor_center_offsetX;			// get values with respect to the sensor frame
		      pos_y[cornersNumber] = sensor_center_offsetY - (j/SCALING_FACTOR);
		      circle(h_col_img, Point(i, j), 10, CV_RGB(0, 25, 255) , 2, 8, 0 ); //draw circle arround corner
		      i_prev = i; j_prev = j;  
		      cout << (i/SCALING_FACTOR ) - sensor_center_offsetX << " , " << sensor_center_offsetY - (j/SCALING_FACTOR) << endl;	  
		     }		  	
	      }			
	    }
      }
    }
    features_for_planning.cornersNo = cornersNumber;                      // store the number of corners
    for (int i = 0; i < cornersNumber; i++)
    {
	  features_for_planning.cornersPoses.push_back(pos_x[cornersNumber]);           // store corners positions in ros message
	  features_for_planning.cornersPoses.push_back(pos_y[cornersNumber]);
	}

    if (show)  imshow( corners_window, h_col_img );						// Showing the result
    return 1;
  }
  else 
  {
    cout<<"No corners found..."<<endl;
    return 0;
  }
}

};
#endif
