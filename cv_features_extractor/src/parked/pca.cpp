/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include "extractor.hpp"
float pi = 3.14159265359;

/// Tactile Sensor data
const int sensorsRows = 6;
const int sensorsCols = 4;
const int noTactels = 3;
unsigned int scaling_factor = 30;										//to scale the size of tactile image


unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string windowArray = "Tactile Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;

/// PCA
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 30;
int maxPCAThreshold = 255; 
const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 30;
int rangePcaArea = 100; 
int inc = 0;
bool publish = false;
int temp = 0;
int sensors = 0;

/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


uchar img1[sensorsRows][sensorsCols] = {}; 							// image matrix for debuggin 
int imgshow1[sensorsRows][sensorsCols] = {}; 						// image matrix for showing
uchar img2[sensorsRows][sensorsCols] = {}; 							// image matrix for debuggin 
int imgshow2[sensorsRows][sensorsCols] = {}; 						// image matrix for showing
uchar img3[sensorsRows][sensorsCols] = {}; 							// image matrix for debuggin 
int imgshow3[sensorsRows][sensorsCols] = {}; 						// image matrix for showing
bool sensor1_off = true, sensor2_off=true, sensor3_off=true;
/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	SensorName = cv_ptr->header.frame_id;
	cout<< SensorName<<endl;
	if (SensorName == prevSensorName) {
		ROS_ERROR("same name");
		return;
	}
	else {
		//prevSensorName = SensorName;
	}
	//	ROS_INFO("Getting Image...");
	
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 										// step --> cols

    int c = 0;															// to store vector data in matrix
    
    sensor_center_offset_x = sensorsCols/2;
    sensor_center_offset_y = sensorsRows/2;
    
	uchar img[18][4] = {}; 		
	int imgshow[18][4] = {};
	

    if(SensorName == "tactel_right") { 									/// fill right tactel image
		sensor1_off=false;
		ROS_INFO("Filling tactel right");
    /// fill data extracted from array in a tactile image matrix 1    
		for(int i=0; i<sensorsRows; i++) {		
			for(int j=0; j<sensorsCols; j++) {
					imgshow[i][j]=cv_ptr->image.data[c];					// to display in screan
					img[i][j]=cv_ptr->image.data[c];						// to debug
					if (imgshow[i][j] > 0) {
						total_force += imgshow[i][j]; 						// accumulate forces
						++no_tactels;
						}
	//				cout << "   " <<imgshow[i][j];						// show in screen				
			++c;
			}
	//		cout<<"\n";
		}
		++sensors;
		c = 0;
	}
    
	if(SensorName == "tactel_center"){									/// fill center tactel image
		sensor2_off=false;
		ROS_INFO("Filling tactel center");
	  /// fill data extracted from array in a tactile image matrix 2    
		for(int i=6; i<12; i++) {		
			for(int j=0; j<sensorsCols; j++) {
					imgshow[i][j]=cv_ptr->image.data[c];					// to display in screan
					img[i][j]=cv_ptr->image.data[c];						// to debug
					if (imgshow[i][j] > 0) {
						total_force += imgshow[i][j]; 						// accumulate forces
						++no_tactels;
						}
			//		cout << "   " <<imgshow[i][j];						// show in screen				
			++c;
			}
			//cout<<"\n";
		}
		++sensors;
		c = 0;
	}
	
	if(SensorName == "tactel_left"){                                    /// fill left tactel image
		sensor3_off=false;
		ROS_INFO("Filling tactel left");
	  /// fill data extracted from array in a tactile image matrix 3    
		for(int i=12; i<18; i++) {		
			for(int j=0; j<sensorsCols; j++) {
					imgshow[i][j]=cv_ptr->image.data[c];					// to display in screan
					img[i][j]=cv_ptr->image.data[c];						// to debug
					if (imgshow[i][j] > 0) {
						total_force += imgshow[i][j]; 						// accumulate forces
						++no_tactels;
						}
		//			cout << "   " <<imgshow[i][j];						// show in screen				
			++c;
			}
		//	cout<<"\n";
		} 
		++sensors;
		c = 0;
	}
	

	
	/// form complete image from all sensors 	
	
	if(sensors > 3) {
	sensors = 0;
	publish = false;
	//std::cout<<"\n";
	/// Convert to Mat type matrix img
	src = Mat(18, 4, CV_8U, img);
	
	///Add Noise to images
	//saltpepperNoise();
	//gaussianNoise();
	
	/// Resize image for visualize it using interpolation and store in rsz
    resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
	Mat tactImg = rsz.clone();
	
	cvtColor(tactImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	colorPca = colorImg.clone();
	
	namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
    imshow( windowArray, colorImg );              						// Show our image inside it.
	waitKey(3);
	
	CannyThreshold(0, 0);
	cvtColor(edgesImg, colorImg, CV_GRAY2BGR);   						// convert to color image

	pcaAnalysis (); 	/// Apply PCA for edge detection
	
    }
}
 
/***********************************************************************
 * Canny Detector
 * ********************************************************************/
static void CannyThreshold(int, void*)
{		
	if (isshownCannyThreshold){	
		namedWindow( windowCanny, WINDOW_AUTOSIZE );						// Create a window for display.
		createTrackbar( CannyTrackbarName, windowCanny, &lowThreshold, max_lowThreshold, CannyThreshold );
		isshownCannyThreshold = 0;
	}
    /// Reduce noise with a kernel 3x3
    blur( rsz, edgesImg, Size(3,3) );
    /// Canny detector
    lowThreshold = max(lowThreshold, 1);
    Canny( rsz, edgesImg, lowThreshold, lowThreshold*ratio, kernel_size ); // detect the edges of the image 50 200 3									
    /// Canny (source, destiny, low treshold, high treshold, kernel size)
	/// low treshold: tune this parameter
	/// high treshold: Set in the program as three times the lower threshold (following Canny’s recommendation)
	/// kernel size: We defined it to be 3 (the size of the Sobel kernel mask to be used internally)
    /// Using Canny's output as a mask, we display our result
    
	imshow(windowCanny, edgesImg);										// Show our image inside it.
	waitKey(3);
}


/*********************************************************************** 
 * Perform PCA
************************************************************************/

void pcaAnalysis ()
{   
//	cout<<"*********************  PCA  ********************"<<endl;
	pcaImg = rsz.clone();
	
	/// Threshold image for binarize the data and store in bw (binary image)
	threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
	
    /// Find all objects of interest
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    
    /// Find Contours  
    findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
 
	/// Extract the contours so that
    vector<vector<Point> > approxShape;
    
    /// Resize to use with approximating function
	approxShape.resize(contours.size());

	/// For each object get features 
    if (contours.size() > 0){		
		//cout<<"Features extracted from "<<contours.size()<<" contacts detected"<<endl;	
    
    /// Iterate for each contact detected
		for (size_t i = 0; i < contours.size(); ++i)
		{
		
			/// Aproximate the contour to detect the edges from an approximate shape and not from raw data
			approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true);
		
			int edgesNumber = approxShape[0].size() / approxShape.size();
			//cout<<"Number of edges: "<< approxShape[0].size() / approxShape.size() <<endl;
		
        
			/// Get perimeter and area
			double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
			double areaContour = contourArea( approxShape[i] ,false) / scaling_factor;
			//cout << "Area: "<<areaContour<<"  Perimeter: "<<perimeterContour<<endl;
        
			/// Ignore if too small or too large
			int pcaContact = PcaAreaThreshold ;     	
			if ( areaContour < pcaContact ) continue; 
        
        
			/// Draw the contour
			drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
        
        
			/// Get the object orientation and the PCA components at once
			getOrientation(contours[i], bw);	
			
			/// Now calculate offset with the sensor frame
			offset_x = object_center_x-sensor_center_offset_x;					 
			offset_y = sensor_center_offset_y-object_center_y;	
			
			/// Detect Slipage		
		//	slipDetection (offset_x, offset_y,15);
			
			/// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
			distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) );
			distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );
			
			/// Calculate the pressure
			pressure = total_force / (no_tactels);	
									
			/// Get the offset and center of preassure
			get_contactType (distance1,distance2,contours.size(),edgesNumber, areaContour, perimeterContour,pressure);
			
			/// Fill in the publishing message the rest data
			fillFeaturesMsg ();
							
    }

		/// Reset variables 
		gotPCA=1;	total_force = 0; orientation_deg = 0;
		offset_x = 0; offset_y = 0;	no_tactels = 0;
	
		/// Display it
		namedWindow( windowPCA, WINDOW_AUTOSIZE );						
		createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
		createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
		imshow(windowPCA, colorPca); 
		waitKey(3);

	}
}

		

/***********************************************************************
 * Function to get orientation and offset
 ***********************************************************************/
 
/// Get the orientation of the contact edge with respect to the sensor frame
double getOrientation(vector<Point> &pts, Mat &img)
{
    
    /// Construct a buffer used by the pca analysis
    Mat data_pts = Mat(pts.size(), 2, CV_64FC1);
    for (int i = 0; i < data_pts.rows; ++i)
    {
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }
    
    /// Perform PCA analysis
    PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);

    /// Store the position of the object
    Point pos = Point(pca_analysis.mean.at<double>(0, 0),
                      pca_analysis.mean.at<double>(0, 1));
    				
    
    /// Store the eigenvalues and eigenvectors
    vector<Point2d> eigen_vecs(2);
    vector<double> eigen_val(2);
    for (int i = 0; i < 2; ++i)
    {
        eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                pca_analysis.eigenvectors.at<double>(i, 1));

        eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
    }
	
	
    /// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
    circle(pcaImg, pos, 3, CV_RGB(255, 255, 255), 3);
    line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
    line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);

    /// The orientation of the main component of PCA
    orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
	orientation_deg = -orientation_rad * (180 / 3.1416); 

    
    /// Center of pressure      
	object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor;
    object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
    
    /// To check if it is a line, a push or if is not known
    pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
    pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;
	
    /// The orientation of the main component of PCA
    orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
	orientation_deg = -orientation_rad * (180 / 3.1416); 
}




double get_contactType (float dx, float dy, int contacts, int edges, float a, float p, float pressure)  {
	
	int threshold = 230;
	//cout<<"Check if "<<dx<<" > "<<2*dy<<" ? and if "<<dy<<" > "<<2*dx<<" ? "<<endl;
	if (edges == 4)  { 	
	//	if (dx-dy <= 1 &&  p > 4) cout<<"GEOMETRY: "<<"Square"<<endl;
	//	if (dx-dy <= 1 &&  p < 4) cout<<"CONTACT TYPE: "<<"Point contact detected"<< endl;			
		
	}	
	
    if (dx-dy <= 1) {
		if (p >= 4) {
	//	cout<<"GEOMETRY: "<<"Poligon with "<<edges<<" edges"<<endl;
	//	cout<<"CONTACT TYPE: "<<"Push contact detected"<< endl;
		features_for_planning.contactType="push";
		features_for_planning.contactList.push_back("push");
		}
	}	
		
	if ( dx > 2*dy || dy > 2*dx) {
	//		cout<<"GEOMETRY: "<<"Rectangle"<<endl;
	//		cout<<"CONTACT TYPE: "<<"Line contact dected"<< endl;
			features_for_planning.contactType="edge";
			features_for_planning.contactList.push_back("edge");	
	}	
		
	if (edges > 50) {
	//cout<<"GEOMETRY: "<<"Poligon with "<<edges<<" edges"<<endl;
	//cout<<"CONTACT TYPE: "<<"RAW contact detected"<< endl;	
	}
	
    if (contacts > 5) cout<<"Calibrate sensor, I see noise"<< endl;
	
	//if (pressure > threshold) cout << "PLANNER: "<< "Servo with Force" << endl;
	//if (pressure < threshold) cout << "PLANNER: "<< "Servo with Orientation" << endl;
	/// slip detector
	
}





/***********************************************************************
 * Fill messages
 **********************************************************************/

void fillFeaturesMsg () {
	
	
	int plan = 2; 
	
	// publish for force
	if (plan == 1) {
	if (total_force > 100 ) {
	
	total_force += total_force;
	pressure += pressure;
	temp += orientation_deg ;
	cout<<"Orientation (deg): "<<temp<<endl;	
	offset_x += offset_x;
	offset_y += offset_y;
	++inc;
	int avg = 100;
	if (inc >= avg ) {
	//	cout<<"counter: "<<inc<<endl;
		total_force /= avg;
		pressure /= avg;
		temp /= avg;
		offset_x /= avg;
		offset_y /= avg;
		features_for_control.data.push_back(total_force);		
	//	cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
		features_for_control.data.push_back(pressure);	
	//	cout<<"Pressure: "<<pressure<<endl;								// show the pressure applied by current object						
		features_for_control.data.push_back(orientation_deg);	
		cout<<"**Orientation (deg): "<<temp<<endl;				        // show the orientation of current object	
		features_for_control.data.push_back(offset_x);
	//	cout<<"Offset x: "<<offset_x<<endl;	    						// show "x" offset of the contact edge with respect to the "x" axis of the sensor       
		features_for_control.data.push_back(offset_y);	
	//	cout<<"Offset y: "<<offset_y<<endl;								// show "y" offset of the contact edge with respect to the "y" axis of the sensor 
		inc = 0;
		temp = 0;
		publish = true;
		}	
	}
	}
	
	// publish for orientation
	if (plan == 2) {
		features_for_control.data.push_back(total_force);		
		//cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
		features_for_control.data.push_back(pressure);						
		features_for_control.data.push_back(orientation_deg);	
		features_for_control.data.push_back(offset_x);   
		features_for_control.data.push_back(offset_y);	
		publish = true;
	}
	}

   
};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<cv_features_extractor::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<cv_features_extractor::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this

	for (int i=0 ; i<5; i++ ) features_for_control.data.push_back(0);
	features_for_planning.contactList.push_back("no contact");
	features_for_planning.contactType="no contact";
	

  int count = 0;
  while (ros::ok())
  {
	if (publish) { 
		if (features_for_control.data.size()>0){
			pub_featuresc.publish(features_for_control);									// Publish features
		
			features_for_control.data.clear();											// clear array	
			features_for_planning.contactList.clear();
			features_for_planning.contactType="no contact";
		}  
	}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
