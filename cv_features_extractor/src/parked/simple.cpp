/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include "extractor.hpp"
float pi = 3.14159265359;

/// Tactile Sensor data
const int sensorsRows = 18;
const int sensorsCols = 4;
const int noTactels = 3;
unsigned int scaling_factor = 30;										//to scale the size of tactile image
float area_tactel = 8;

unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string windowArray = "Tactile Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;


/// PCA
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 30;
int maxPCAThreshold = 255; 
const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 14;
int rangePcaArea = 100; 
int inc = 0;




/// Harris
int thresh = 200;





/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowPCA);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
	  
	  
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	
	
	
	
	SensorName = cv_ptr->header.frame_id;
	features_for_control.header.frame_id = SensorName;
	features_for_planning.header.frame_id = SensorName;
	
	//	ROS_INFO("Getting Image...");
	
	
	
	
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 										// step --> cols

    int c = 0;															// to store vector data in matrix
    
    sensor_center_offset_x = sensorsCols/2;
    sensor_center_offset_y = sensorsRows/2;
    
	uchar img[18][4] = {}; 		
	int imgshow[18][4] = {};
	



     /// fill data extracted from array in a tactile image matrix    
    //for(int i=sensorsRows-1; i>=0; --i) {		
	for(int i=sensorsRows-1; i>=0; --i) {	
		for(int j=0; j<sensorsCols; j++) {
				imgshow[i][j]=cv_ptr->image.data[c]*2;					// to display in screan
				img[i][j]=cv_ptr->image.data[c]*2;						// to debug
				if (imgshow[i][j] > 13) { //13
					total_pressure += imgshow[i][j]; 						// accumulate forces
					++no_tactels;
					}
				//std::cout << "   " <<imgshow[i][j];						// show in screen				
		++c;
		}
	//	std::cout<<"\n";
	}
	
//	cout<<"\n";
	
	//std::cout<<"\n";
	
	
	
	
	
	/// Convert to Mat type matrix img
	src = Mat(18, 4, CV_8U, img);
	
	
	
	
	///Add Noise to images
	//saltpepperNoise();
	//gaussianNoise();
	
	
	
	
	/// Resize image for visualize it using interpolation and store in rsz
    resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
	Mat tactImg = rsz.clone();
	
	cvtColor(tactImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	bitwise_not ( colorImg, colorImg );
	colorPca = colorImg.clone();
	
	
	namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
    imshow( windowArray, colorImg );              						// Show our image inside it.
	waitKey(3);
	
	
	/// Extract features
	extractFeatures(); 	/// Apply PCA for edge detection
	

	/// Display it
	namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	imshow(windowPCA, colorPca); 
	waitKey(3);
	
}



/*********************************************************************** 
 * Perform PCA
************************************************************************/

void extractFeatures ()
{   

	/// PRE-PROCESSING
	pcaImg = rsz.clone();
	
	
	/// Threshold image for binarize the data and store in bw (binary image)
	threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
	
	
	
    /// Find all objects of interest
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;



    /// Find Contours  
    findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
 
 
 
	/// Extract the contours so that
    vector<vector<Point> > approxShape;
    
    
    
    /// Resize to use with approximating function
	approxShape.resize(contours.size());
	contacts = contours.size();
	features_for_planning.contactsNo = contacts;
	
	
	
	
	/// GET FEATURES WITH PCA
	
	
	/// For each object get features 
    if (contours.size() > 0){		
    
    
    
    
    /// Iterate for each contact detected
		for (size_t i = 0; i < contours.size(); ++i)
		{
		
			/// Aproximate the contour to detect the edges from an approximate shape and not from raw data
			approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true);
		
		
			/// Get the number of edges
			int edgesNumber = approxShape[0].size() / approxShape.size();
			features_for_planning.contactEdges.push_back(edgesNumber);	 
        
        
			/// Get perimeter and area
			double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
			features_for_planning.contactPerimeter.push_back(perimeterContour);	
			
			
			double areaContour = contourArea( approxShape[i] ,false) / scaling_factor;
			features_for_planning.contactArea.push_back(areaContour);	
        
        
			/// Ignore contact if too small or too large, the threshold can be adjusted
			int pcaContact = PcaAreaThreshold ;     	
			if ( areaContour < pcaContact+90 ) continue; 
			//cout << "Contact  :" << areaContour<<endl;
			//cout << "Trashold:"  << pcaContact+110<<endl;
        
			/// Draw the contour of the contact
			drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
			
			
			
			/// Construct a buffer used by the pca analysis
			vector<Point> pts = contours[i]; 
			Mat img = bw.clone();
			Mat data_pts = Mat(pts.size(), 2, CV_64FC1);

			
			for (int i = 0; i < data_pts.rows; ++i) {
				
				data_pts.at<double>(i, 0) = pts[i].x;
				data_pts.at<double>(i, 1) = pts[i].y;
			
			}


			/// Perform PCA analysis of the contact image
			PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);



			/// Store the position of the object
			Point pos = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
 
 
 
			/// To store the eigenvalues and eigenvectors
			vector<Point2d> eigen_vecs(2);
			vector<double> eigen_val(2);

			
			for (int i = 0; i < 2; ++i) {
			
				eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0), pca_analysis.eigenvectors.at<double>(i, 1));
				eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
			
			}
	
	
	
			/// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
			circle(colorPca, pos, 3, CV_RGB(255, 0, 0), 3);
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);




			/// The orientation of the main component of PCA
			orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
			orientation_deg = -orientation_rad * (180 / 3.1416); 

    



			/// Center of pressure      
			object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor;
			features_for_planning.contactPoses.push_back(object_center_x);	
			object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
			features_for_planning.contactPoses.push_back(object_center_y);	
    
    
    
    
			/// Adjust the size of the eigen vectors to fit in the image
			pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
			pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;
	
	
	
			/// The orientation of the main component of PCA
			orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
			orientation_deg = -(orientation_rad * (180 / 3.1416)) - 90; 
			//if (orientation_deg > 180) orientation_deg=;
			//if (orientation_deg < 180) orientation_deg=;
			features_for_control.contactOrientation.push_back(orientation_deg);



			/// Now calculate offset with the sensor frame
			offset_x = object_center_x-sensor_center_offset_x;	
			features_for_control.contactOffset_x.push_back(offset_x);				 
			offset_y = sensor_center_offset_y-object_center_y;	
			features_for_control.contactOffset_y.push_back(offset_y);	
			
			
			
			/// Calculate the pressure
			total_pressure /= 100;
			features_for_control.contactPressure.push_back(total_pressure);
			total_force = total_pressure * (no_tactels * area_tactel);
			features_for_control.contactForce.push_back(total_force);
			total_pressure = 0;
			
			
			/// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
			distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) );
			distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );

			publish = true;				
    }



		/// Reset variables 
		total_force = 0; orientation_deg = 0; 
		offset_x = 0; offset_y = 0;	no_tactels = 0;

	}
}



   
};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "extract_pps_once_simple");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<cv_features_extractor::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<cv_features_extractor::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this
  

  int count = 0;
  while (ros::ok())
  {
	//if (publish) { 
		
	//	publish = false;
		
	//	if ( contacts>0 ) {
			
			
			features_for_control.header.stamp = ros::Time::now(); 
			pub_featuresc.publish(features_for_control);								
			
			/// clear arrays	
			features_for_control.contactOffset_x.clear();											
			features_for_control.contactOffset_y.clear();
			features_for_control.contactForce.clear();
			features_for_control.contactPressure.clear();
			features_for_control.contactOrientation.clear();

			
	//	}  
		
		
	//}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
