#ifndef GETANGLESSOBEL_HPP_
#define GETANGLESSOBEL_HPP_

# include "extractor.hpp"
# include "matrix_utils.hpp"
const string TwoImages = "Two Images";


class Sobel_angles {
  private:


  public:
    bool getAngles_ ( cv::Mat grayImg, int angles_number )
    {	  
			  //Mat Img = grayImg.clone();
	    ///// Get the image gradients in x and y directions to detect pixels variation. Use Scharr method is prefered over Sobel
	    //Mat gradx, grady;
	    //int scale = 1;  int delta = 0; int ddepth= CV_16S; // Function parameters
	    //Scharr(Img, gradx, ddepth, 1, 0, scale, delta, BORDER_DEFAULT);  //Sobel(subImg, gradx, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	    //Scharr(Img, grady, ddepth, 0, 1, scale, delta, BORDER_DEFAULT);  //Sobel(subImg, grady, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);	
	    ///// First lest find an appropiate threshold
	    //Matrix_utils utils;	    
      //int mean_magnitude = utils.getMagnitudeMean_( gradx , grady );  // Use mean of image magnitude as threshold    
	    ///// Get orientations in the image according to pixel intensity variation with calculated threshold
	    //vector <int> orientations_database;
	    //bool angles_in_degrees = true;
	    //int magnitude_threshold = mean_magnitude/5;
      //orientations_database =  utils.getImageOrientations_( gradx , grady, magnitude_threshold , angles_in_degrees ); // lets call by value
	    //if ( orientations_database.size() > 0)
	  Mat subImg = grayImg.clone();
	  Mat gradx, grady;
	  int scale = 1;  int delta = 0; int ddepth= CV_16S; // Sobel parameters
	  Scharr(subImg, gradx, ddepth, 1, 0, scale, delta, BORDER_DEFAULT);
	  Scharr(subImg, grady, ddepth, 0, 1, scale, delta, BORDER_DEFAULT);
	  //Sobel(subImg, gradx, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
	  //Sobel(subImg, grady, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);		  
	  /// Calculate orientation and magnitude of each pixel
	  /// First lest find an appropiate threshold
	  vector <int> temporal_magnitude_array;
	  for (int i = 0 ; i < subImg.rows ; i++)
	  {
	    for (int j = 0 ; j < subImg.cols ; j++)
	    {
		    int Magnitude = sqrt( pow( gradx.at<int>(i,j) , 2 ) + pow ( grady.at<int>(i,j) , 2 ) ); // cout << Magnitude << endl;
		    temporal_magnitude_array.push_back( Magnitude ); 		   
	    }
	  }
	  int mean_mag;
	  cv::Scalar meanMagnitude = cv::mean( temporal_magnitude_array ); //cout << "the mean is: " << medianMagnitude << endl;	  
	  mean_mag = meanMagnitude[0]; //cout << "mean: " << mean_mag << endl;
	  double max_mag, min_mag;
	  cv::minMaxIdx( temporal_magnitude_array , &min_mag, &max_mag, NULL, NULL); //cout << "max " << max_mag << ",  min: " << min_mag << endl;
	  /// get orientations in image
	  vector <int> orientations_database;
	  orientations_database.clear();
	  for (int i = 0 ; i < subImg.rows ; i++)
	  {
	    for (int j = 0 ; j < subImg.cols ; j++)
	    {
		    int Magnitude = sqrt( pow( gradx.at<int>(i,j) , 2 ) + pow ( grady.at<int>(i,j) , 2 ) ); // cout << Magnitude << endl; <hanat are you there?
		    //int edge_magnitude_threshold = (max_mag + mean_mag) / 3;	
		    int edge_magnitude_threshold = mean_mag/5 ;	
		    //int edge_magnitude_threshold = 1;	
		    if (Magnitude > edge_magnitude_threshold) 
		    {
		      int orientation = atan2(grady.at<int>(i,j),gradx.at<int>(i,j)) * (180/M_PI);
		      if ( abs(orientation) >= 0 && abs(orientation <= 180) )  
		      {
					  orientations_database.push_back(abs(orientation));		      
					}
		    }
	    }
	  }
	  //Matrix_utils array_operation;
	  if ( orientations_database.size() > 0)
	  {
			//cout << "size " << orientations_database.size() << endl;
	    /// delete repeated
	    int database_size = orientations_database.size(); 	    
	    vector <int>  no_repeated_data = orientations_database; 
	    int no_repeated_size = no_repeated_data.size();
	    for (int i = 0 ; i < no_repeated_size ; i++)
	    {
		    for (int j = i+1 ; j < no_repeated_size ; )
		    {
	        if ( no_repeated_data[j] == no_repeated_data[i] )
		      {
		        for( int k=j ; k < no_repeated_size ; k++ )
		        {
		          no_repeated_data[k] = no_repeated_data[k+1];
		        }
		        no_repeated_size --;
	        }
	        else 
	        j++;
	      }
	    }
	    //cout << "Not repeated" << endl;
	    //for (int i = 0 ; i < no_repeated_size ; i ++ ) cout << no_repeated_data[i] << " " ;
	    //cout << endl << endl ;
	    /// score
	    vector <int>  scores = no_repeated_data; 
	    for (int i = 0 ; i < database_size ; i++)
	    {
	      for ( int k = 0 ; k < scores.size() ; k++ )
	      {					
			    if ( orientations_database[k] == orientations_database[i] ) 
			    {
						scores[k] +=1;
						
					}
		    } 		  
	    }
	    //cout << "Scores" << endl;
	    //for (int i = 0 ; i < no_repeated_size ; i ++ ) cout << scores[i] << " " ;
	    //cout << endl << endl << endl ;
	    /// Ordering in descending order depending on scoring
	    vector <int>  descending_ordered = no_repeated_data; 
	    int temp = descending_ordered[0];
	    for (int i = 0 ; i < no_repeated_size ; i++)
	    {
		    for (int j = i; j < no_repeated_size ; j++ )
		    {     
	         if ( scores[i] < scores[j] )
		       {
		         temp = descending_ordered[i];
		         descending_ordered[i] = descending_ordered[j];
		         descending_ordered[j] = temp;
		       }
		     }	     	    
	     }
	     //cout << "Ordered" << endl;
	     features_for_control.contactOrientation_corner.clear();
	     if ( angles_number <= no_repeated_size ) // check bounds
	     {  
	       for (int i = 0 ; i < angles_number ; i ++ ) 
	       {
	         cout << descending_ordered[i] << " " ;
	         cout << endl ;
	         features_for_control.contactOrientation_corner.push_back(descending_ordered[i]);
	       }
	     }
	     else
	     {
				 cout << "WARNING" << endl;
				 cout << "Solicited angles for corner servo are bigger than the number of orientations detected in the image" << endl;
				 cout << "Change the angles number or adjust the settings. For example the magnitud threshold." << endl;
		   }
		   scores.clear();
		   no_repeated_data.clear();
		   descending_ordered.clear();
		   orientations_database.clear();
		   return 1;
		   
	  }	  
	  else
	  {
			return 0;
	  }
	  //namedWindow(TwoImages, CV_WINDOW_AUTOSIZE);	
	  //Mat abs_gradx, abs_grady;	
	  //convertScaleAbs(gradx, abs_gradx);
	  //convertScaleAbs(grady, abs_grady);
	  //Mat grad;
	  //addWeighted(abs_gradx, 0.5, abs_grady, 0.5, 0, grad);
	  //imshow(TwoImages, grad);
	  //waitKey(1);	  
    }  
};
#endif
