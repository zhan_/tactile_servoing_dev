  /**
  *   Ros Image Publisher  
  *   Creates an image using ros message type: sensor_msgs/Image Message
  *   to be able import in OpenCv after 
  */
 
// TRANSFORM IMAGE TO ROS TYPE FROM MESSAGE  imgDataArray.msg
// PUBLISH MSG AS ROS IMAGE TYPE
// RECEIVE IMAGE IN NODE features_extractor AND PUBLISH FEATURES
 
#include <ros/ros.h>

#include <sensor_msgs/Image.h>
#include <std_msgs/Header.h> 
// These headers will allow us to convert to ros type image. 

#include <cv_features_extractor/imgDataArray.h>
// These header has the form of the topic to wich we will subscribe. 

#include "std_msgs/Header.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "std_msgs/String.h"
#include <vector>
// These headers are for display messages in prompt screen

/**
 * Callback function that store image, convert to Ros type and publish using ros message type: sensor_msgs/Image Message
 */
 
void extract_imageCallback(const cv_features_extractor::imgDataArray::ConstPtr& msg)
{	
	if(msg != NULL && msg->c_force>0) { 
		ROS_INFO("callback function ... "); 
		/**
		* Get the arrays data from tactile_image topic
		* For simulating sensor the message is encoded in imgDataArray 
		* Store in a single vector data of ros image type message
		*/
		ros::NodeHandle n;
 
  // Create the publisher of the topic: ros_tactile_image
  ros::Publisher pub_image = n.advertise<sensor_msgs::Image>("ros_tactile_image", 100);

  
		//cv_features_extractor::imgDataArray array;
		//array.arraydata1.push_back(tactel_array[j]);
		//std::cout << "Vector data : ["<<a<<"]\n";
		// Create messages to be published
		
		sensor_msgs::Image ros_image;	
		cv_features_extractor::imgDataArray array;
		//int tactel_array[]=msg->msg.arraydata1[];
		
			int i = 0;
			unsigned int Arr[72];
			
			for(std::vector<int>::const_iterator it = msg->arraydata1.begin(); it != msg->arraydata1.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			for(std::vector<int>::const_iterator it = msg->arraydata2.begin(); it != msg->arraydata2.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			for(std::vector<int>::const_iterator it = msg->arraydata3.begin(); it != msg->arraydata3.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			for(std::vector<int>::const_iterator it = msg->arraydata4.begin(); it != msg->arraydata4.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			for(std::vector<int>::const_iterator it = msg->arraydata5.begin(); it != msg->arraydata5.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			for(std::vector<int>::const_iterator it = msg->arraydata6.begin(); it != msg->arraydata6.end(); ++it) {	
				Arr[i] = *it;
				std::cout << "   " << Arr[i];
				ros_image.data.push_back(Arr[i]);
				i++;		
			}
			std::cout<<"\n";
			
			
		//for(int i=1;i<73; i++){						
			//ROS_INFO("for ... ");
			//if((i >=1 )  && (i < 13)) ros_image.data.push_back(1);
			//ros_image.data.push_back(msg.arraydata1[i]);
			//if((i >=1 )  && (i < 13)) ros_image.data.push_back(msg.arraydata1[i]);
			//if((i >=13 )  && (i < 25)) ros_image.data.push_back(msg.arraydata2[i]);
			//if((i >=25 )  && (i < 37)) ros_image.data.push_back(msg.arraydata3[i]);
			//if((i >=37 )  && (i < 49)) ros_image.data.push_back(msg.arraydata4[i]);
			//if((i >=49 )  && (i < 61)) ros_image.data.push_back(msg.arraydata5[i]);
			//if((i >=61 )  && (i < 73)) ros_image.data.push_back(msg.arraydata6[i]);						
		//}
		ROS_INFO("data got ... "); 	
		pub_image.publish(ros_image);
		ROS_INFO("published ... "); 	
		/**
		* Get the number if rows and the number of columns
		* 
		*/
		
	}
}

/**
 * MAIN 
 * Subscribe to tactile_image topic and goes to callback function
 * 
 */
 
int main(int argc, char **argv)
{
  ros::init(argc, argv, "image_publisher");
  ros::NodeHandle n;
  
  // Create the msg
  cv_features_extractor::imgDataArray array;
  sensor_msgs::Image ros_image;
  
  // Create the suscriber to the topic tactile_image
  ros::Subscriber sub = n.subscribe("/tactile_image", 1000, extract_imageCallback);
  ROS_INFO("subscriber created ... "); 
  ros::spinOnce();
  
  // Create the publisher of the topic: ros_tactile_image
  ros::Publisher pub_image = n.advertise<sensor_msgs::Image>("ros_tactile_image", 100);
  ros::spinOnce();
  
  ros::Rate loop_rate(10);
  
  while (n.ok()) {
    pub_image.publish(ros_image);
    ros::spinOnce();
    loop_rate.sleep();
  }
}
