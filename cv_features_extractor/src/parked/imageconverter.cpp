/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber

 /*********************************************
 * Create publisher and subscriber
 *********************************************/
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);

    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown.
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
    cv::destroyWindow(cannyThresholdTrackbarName);
    cv::destroyWindow(CannyTrackbarName);
  }
