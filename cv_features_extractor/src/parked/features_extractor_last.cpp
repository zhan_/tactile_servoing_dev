/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS 
#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message
#include <opencv2/imgproc/imgproc.hpp>       							// Include headers for OpenCV's image processing 
#include <opencv2/highgui/highgui.hpp>		 							// Include GUI modules
#include <cv_features_extractor/featureArray.h>			 				// Include msg type header for publish features
#include <math.h>   													// sqrt


using namespace cv;
using namespace std;

/// Create globally messages to be published
cv_features_extractor::featureArray features;

/// Global variables
float offset_x, offset_y, total_force, pressure, orientation_deg, orientation_rad;
float object_center_x, object_center_y, pos1_x, pos2_x, pos1_y, pos2_y, distance1, distance2;
unsigned int gotPCA, no_tactels=0;
float pi = 3.14159265359;
const int sensorsRows = 18;
const int sensorsCols = 4;


Mat src, bw, rsz, pcaImg, tactImg, edgesImg, colorImg, colorPca;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;
unsigned int scaling_factor = 30;										//to scale the size of tactile image
unsigned int sensor_center_offset_x;									//to compute offset with object frame of reference
unsigned int sensor_center_offset_y;

/// windows and trackbars name
const string windowArray = "Tactile Image";
const string windowCanny = "Canny";
const string windowHoughL = "Hough Line Transform";
const string windowHoughLP = "Hough Probabilistic Line Transform";
const string windowHoughC = "Hough Circle Transform";
const string cannyThresholdTrackbarName = "Canny";
const string accumulatorThresholdTrackbarName = "Accumulator";
const string minRadiusTrackbarName = "Min Radius";


/// Hough Circle 
/// initial and max values of the parameters of interests.
const int cannyThresholdInitialValue = 200;
const int accumulatorThresholdInitialValue = 10;
const int minRadiusInitialValue = 10;    
const int maxAccumulatorThreshold = 200;
const int maxCannyThreshold = 255;    
const int maxMinRadius = 500;

/// for tunning, declare and initialize both parameters that are subjects to change
int cannyThreshold = cannyThresholdInitialValue;
int accumulatorThreshold = accumulatorThresholdInitialValue;
int minRadius = minRadiusInitialValue;


/// Canny Detector in circle detection data
const string CannyTrackbarName = "treshold";					// Internal for hough circle detector

int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;

/// PCA
const string windowPCA = "PCA";

const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 30;
int maxPCAThreshold = 255; 

const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 30;
int rangePcaArea = 100; 

int inc = 0;
bool publish = false;
int temp = 0;
/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);

    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
    cv::namedWindow(windowHoughL);
    cv::namedWindow(windowHoughLP);
    cv::namedWindow(windowHoughC);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(windowHoughL);
	cv::destroyWindow(windowHoughLP);
	cv::destroyWindow(windowHoughC);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(accumulatorThresholdTrackbarName);
	cv::destroyWindow(minRadiusTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	///	ROS_INFO("Getting Image...");
	
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 										// step --> cols
    
    uchar img[sensorsRows][sensorsCols] = {}; 							// image matrix for debuggin 
    int imgshow[sensorsRows][sensorsCols] = {}; 						// image matrix for showing
    int c = 0;															// to store vector data in matrix
    sensor_center_offset_x = sensorsCols/2;
    sensor_center_offset_y = sensorsRows/2;
    
    
    /// fill data extracted from array in a tactile image matrix    
    for(int i=0; i<sensorsRows; i++) {		
		for(int j=0; j<sensorsCols; j++) {
				imgshow[i][j]=cv_ptr->image.data[c];					// to display in screan
				img[i][j]=cv_ptr->image.data[c];						// to debug
				if (imgshow[i][j] > 0) {
					total_force += imgshow[i][j]; 						// accumulate forces
					++no_tactels;
					}
			//	std::cout << "   " <<imgshow[i][j];						// show in screen				
		++c;
		}
	//	std::cout<<"\n";
	}
	
	
	publish = false;
	//std::cout<<"\n";
	/// Convert to Mat type matrix img
	src = Mat(sensorsRows, sensorsCols, CV_8U, img);
	
	///Add Noise to images
	//saltpepperNoise();
	//gaussianNoise();
	
	/// Resize image for visualize it using interpolation and store in rsz
    resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
	Mat tactImg = rsz.clone();
	
	cvtColor(tactImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	colorPca = colorImg.clone();
	
	namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
    imshow( windowArray, colorImg );              						// Show our image inside it.
	waitKey(3);
	
	CannyThreshold(0, 0);
	cvtColor(edgesImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	
	//houghLine ();		/// Apply standar hough line transform
	//houghLinep ();		/// Apply probabilistic hough line transform
	//houghCircle ();		/// Apply hough circle detector
	pcaAnalysis (); 	/// Apply PCA for edge detection
    }

/**********************************************************************
 * Functions to add noise to the original image
 **********************************************************************/	  
void saltpepperNoise ()
{
	Mat saltpepper_noise = Mat::zeros(src.rows, src.cols,CV_8U);
	randu(saltpepper_noise,0,255);

	Mat black = saltpepper_noise < 30;
	Mat white = saltpepper_noise > 225;

	Mat saltpepper_img = src.clone();	
	saltpepper_img.setTo(255,white);
	saltpepper_img.setTo(0,black);
	//namedWindow( "SALTPEPPER NOISE", WINDOW_AUTOSIZE );					// Create a window for display.
	//imshow("SALTPEPPER NOISE", saltpepper_img);							// Show our image inside it.
}	

void gaussianNoise ()
{
	Mat gaussian_noise = src.clone();
	randn(gaussian_noise,128,30);
	//namedWindow( "GAUSSIAN NOISE", WINDOW_AUTOSIZE );					// Create a window for display.
	//imshow("GAUSSIAN NOISE", gaussian_noise);							// Show our image inside it.
}	



 
/***********************************************************************
 * Canny Detector
 * ********************************************************************/
static void CannyThreshold(int, void*)
{		
	if (isshownCannyThreshold){	
		namedWindow( windowCanny, WINDOW_AUTOSIZE );						// Create a window for display.
		createTrackbar( CannyTrackbarName, windowCanny, &lowThreshold, max_lowThreshold, CannyThreshold );
		isshownCannyThreshold = 0;
	}
    /// Reduce noise with a kernel 3x3
    blur( rsz, edgesImg, Size(3,3) );
    /// Canny detector
    lowThreshold = max(lowThreshold, 1);
    Canny( rsz, edgesImg, lowThreshold, lowThreshold*ratio, kernel_size ); // detect the edges of the image 50 200 3									
    /// Canny (source, destiny, low treshold, high treshold, kernel size)
	/// low treshold: tune this parameter
	/// high treshold: Set in the program as three times the lower threshold (following Canny’s recommendation)
	/// kernel size: We defined it to be 3 (the size of the Sobel kernel mask to be used internally)
    /// Using Canny's output as a mask, we display our result
    
	imshow(windowCanny, edgesImg);										// Show our image inside it.
	waitKey(3);
}




 /**********************************************************************
 * Hough Line Transform: lines in cdst
 **********************************************************************/	
 void houghLine ()
 {
	///cout<<"************** HOUGH LINE TRANSFORM ************"<<endl;
	Mat hlImg;
	hlImg = colorImg.clone();
	vector<Vec2f> lines;
	HoughLines(edgesImg, lines, 1, CV_PI/180, 80, 0, 0 );
	///HoughLines(InputArray image dst, OutputArray lines, double rho, double theta, int threshold, double srn=0, double stn=0 )¶
    ///dst: Output of the edge detector. It should be a grayscale image (although in fact it is a binary one)
    ///lines: A vector that will store the parameters (r,\theta) of the detected lines
    ///rho : The resolution of the parameter r in pixels. We use 1 pixel.
    ///theta: The resolution of the parameter \theta in radians. We use 1 degree (CV_PI/180)
    ///threshold: The minimum number of intersections to “detect” a line
    ///srn and stn: Default parameters to zero. Check OpenCV reference for more info.
    	
	/// Perform hough transform
	for( size_t i = 0; i < lines.size(); i++ )
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000*(-b));
		pt1.y = cvRound(y0 + 1000*(a));
		pt2.x = cvRound(x0 - 1000*(-b));
		pt2.y = cvRound(y0 - 1000*(a));
		line(hlImg, pt1, pt2, Scalar(0,0,255), 3, CV_AA);
		float h_angle = theta * 180/pi - 90;
		//cout<<"Hough Orientation "<<h_angle<<endl;
	}
	if (lines.size()) {
		//cout<<"Lines size "<<lines.size()<<endl;
		namedWindow( windowHoughL, WINDOW_AUTOSIZE );// Create a window for display.
		imshow(windowHoughL, hlImg);
		waitKey(3);
	}					
  }
  
  
  
  
  
 /**********************************************************************
 * Hough Line Probabilistic Transform: lines in cdst
 **********************************************************************/	
void houghLinep () {
	//cout<<"****** HOUGH PROBABILISTIC LINE TRANSFORM ******"<<endl;
	Mat hlpImg; Mat hl;
	hl = edgesImg.clone();
	hlpImg = colorImg.clone();
	vector<Vec4i> lines;
	/// Apply hough probabilistic algorithm
    HoughLinesP( hl, lines, 1, CV_PI/180, 80, 30, 10 ); 
    /// dst: Output of the edge detector. It should be a grayscale image (although in fact it is a binary one)
    /// lines: A vector that will store the parameters (x_{start}, y_{start}, x_{end}, y_{end}) of the detected lines
    /// rho : The resolution of the parameter r in pixels. We use 1 pixel.
    /// theta: The resolution of the parameter \theta in radians. We use 1 degree (CV_PI/180)
    /// threshold: The minimum number of intersections to “detect” a line
    /// minLinLength: The minimum number of points that can form a line. Lines with less than this number of points are disregarded.
    /// maxLineGap: The maximum gap between two points to be considered in the same line.

    for( size_t i = 0; i < lines.size(); i++ )
    {
        line( hlpImg, Point(lines[i][0], lines[i][1]),
            Point(lines[i][2], lines[i][3]), Scalar(0,0,255), 3, 8 );
    }
    if (lines.size() > 0){
		namedWindow( windowHoughLP, WINDOW_AUTOSIZE );// Create a window for display.
		imshow(windowHoughLP, hlpImg);
		waitKey(3);
	}
}




/*********************************************************************** 
 * Perform PCA
************************************************************************/
void pcaAnalysis ()
{   
//	cout<<"*********************  PCA  ********************"<<endl;
	pcaImg = rsz.clone();
	
	/// Threshold image for binarize the data and store in bw (binary image)
	threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
	
    /// Find all objects of interest
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    
    /// Find Contours  
    findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
 
	/// Extract the contours so that
    vector<vector<Point> > approxShape;
    
    /// Resize to use with approximating function
	approxShape.resize(contours.size());

	/// For each object get features 
    if (contours.size() > 0){		
		//cout<<"Features extracted from "<<contours.size()<<" contacts detected"<<endl;	
    
    /// Iterate for each contact detected
		for (size_t i = 0; i < contours.size(); ++i)
		{
		
			/// Aproximate the contour to detect the edges from an approximate shape and not from raw data
			approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true);
		
			int edgesNumber = approxShape[0].size() / approxShape.size();
			//cout<<"Number of edges: "<< approxShape[0].size() / approxShape.size() <<endl;
		
        
			/// Get perimeter and area
			double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
			double areaContour = contourArea( approxShape[i] ,false) / scaling_factor;
			//cout << "Area: "<<areaContour<<"  Perimeter: "<<perimeterContour<<endl;
        
			/// Ignore if too small or too large
			int pcaContact = PcaAreaThreshold ;     	
			if ( areaContour < pcaContact ) continue; 
        
        
			/// Draw the contour
			drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
        
        
			/// Get the object orientation and the PCA components at once
			getOrientation(contours[i], bw);	
			
			/// Now calculate offset with the sensor frame
			offset_x = object_center_x-sensor_center_offset_x;					 
			offset_y = sensor_center_offset_y-object_center_y;			
			
			/// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
			distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) );
			distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );
			
			/// Calculate the pressure
			pressure = total_force / (no_tactels);	
									
			/// Get the offset and center of preassure
			get_contactType (distance1,distance2,contours.size(),edgesNumber, areaContour, perimeterContour,pressure);
			
			/// Fill in the publishing message the rest data
			fillFeaturesMsg ();
							
    }

		/// Reset variables 
		gotPCA=1;	total_force = 0; orientation_deg = 0;
		offset_x = 0; offset_y = 0;	no_tactels = 0;
	
		/// Display it
		namedWindow( windowPCA, WINDOW_AUTOSIZE );						
		createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
		createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
		imshow(windowPCA, colorPca); 
		waitKey(3);

	}
}

		

/***********************************************************************
 * Function to get orientation and offset
 ***********************************************************************/
 
/// Get the orientation of the contact edge with respect to the sensor frame
double getOrientation(vector<Point> &pts, Mat &img)
{
    
    /// Construct a buffer used by the pca analysis
    Mat data_pts = Mat(pts.size(), 2, CV_64FC1);
    for (int i = 0; i < data_pts.rows; ++i)
    {
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }
    
    /// Perform PCA analysis
    PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);

    /// Store the position of the object
    Point pos = Point(pca_analysis.mean.at<double>(0, 0),
                      pca_analysis.mean.at<double>(0, 1));
    				
    
    /// Store the eigenvalues and eigenvectors
    vector<Point2d> eigen_vecs(2);
    vector<double> eigen_val(2);
    for (int i = 0; i < 2; ++i)
    {
        eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
                                pca_analysis.eigenvectors.at<double>(i, 1));

        eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
    }
	
	
    /// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
    circle(pcaImg, pos, 3, CV_RGB(255, 255, 255), 3);
    line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
    line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);

    /// The orientation of the main component of PCA
    orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
	orientation_deg = -orientation_rad * (180 / 3.1416); 

    
    /// Center of pressure      
	object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor;
    object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
    
    /// To check if it is a line, a push or if is not known
    pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
    pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;
	
    /// The orientation of the main component of PCA
    orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
	orientation_deg = -orientation_rad * (180 / 3.1416); 
}




double get_contactType (float dx, float dy, int contacts, int edges, float a, float p, float pressure)  {
	
	int threshold = 230;
	//cout<<"Check if "<<dx<<" > "<<2*dy<<" ? and if "<<dy<<" > "<<2*dx<<" ? "<<endl;
	if (edges == 4)  { 	
	//	if (dx-dy <= 1 &&  p > 4) cout<<"GEOMETRY: "<<"Square"<<endl;
	//	if (dx-dy <= 1 &&  p < 4) cout<<"CONTACT TYPE: "<<"Point contact detected"<< endl;			
		
	}	
	
    if (dx-dy <= 1) {
		if (p >= 4) {
	//	cout<<"GEOMETRY: "<<"Poligon with "<<edges<<" edges"<<endl;
	//	cout<<"CONTACT TYPE: "<<"Push contact detected"<< endl;
		features.contactType="push";
		features.contactList.push_back("push");
		}
	}	
		
	if ( dx > 2*dy || dy > 2*dx) {
	//		cout<<"GEOMETRY: "<<"Rectangle"<<endl;
	//		cout<<"CONTACT TYPE: "<<"Line contact dected"<< endl;
			features.contactType="edge";
			features.contactList.push_back("edge");	
	}	
		
	if (edges > 50) {
	//cout<<"GEOMETRY: "<<"Poligon with "<<edges<<" edges"<<endl;
	//cout<<"CONTACT TYPE: "<<"RAW contact detected"<< endl;	
	}
	
    if (contacts > 5) cout<<"Calibrate sensor, I see noise"<< endl;
	
	//if (pressure > threshold) cout << "PLANNER: "<< "Servo with Force" << endl;
	//if (pressure < threshold) cout << "PLANNER: "<< "Servo with Orientation" << endl;
	/// slip detector
	
}





/***********************************************************************
 * Fill messages
 **********************************************************************/

void fillFeaturesMsg () {
	
	int plan = 2; 
	
	if (plan == 1) {
	if (total_force > 100 ) {
	
	total_force += total_force;
	pressure += pressure;
	temp += orientation_deg ;
	cout<<"Orientation (deg): "<<temp<<endl;	
	offset_x += offset_x;
	offset_y += offset_y;
	++inc;
	int avg = 100;
	if (inc >= avg ) {
	//	cout<<"counter: "<<inc<<endl;
		total_force /= avg;
		pressure /= avg;
		temp /= avg;
		offset_x /= avg;
		offset_y /= avg;
		features.data.push_back(total_force);		
	//	cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
		features.data.push_back(pressure);	
	//	cout<<"Pressure: "<<pressure<<endl;								// show the pressure applied by current object						
		features.data.push_back(orientation_deg);	
		cout<<"**Orientation (deg): "<<temp<<endl;				        // show the orientation of current object	
		features.data.push_back(offset_x);
	//	cout<<"Offset x: "<<offset_x<<endl;	    						// show "x" offset of the contact edge with respect to the "x" axis of the sensor       
		features.data.push_back(offset_y);	
	//	cout<<"Offset y: "<<offset_y<<endl;								// show "y" offset of the contact edge with respect to the "y" axis of the sensor 
		inc = 0;
		temp = 0;
		publish = true;
		}	
	}
	}
	
	
	if (plan == 2) {
		features.data.push_back(total_force);		
		cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
		features.data.push_back(pressure);						
		features.data.push_back(orientation_deg);	
		features.data.push_back(offset_x);   
		features.data.push_back(offset_y);	
		publish = true;
	}
	}
/***********************************************************************
 * Hough Circle transform
 **********************************************************************/
void houghCircle ()
{
	
	//cout<<"**************HOUGH CIRCLE TRANSFORM************"<<std::endl;
	Mat colorhc = colorImg.clone();
	Mat  hcImg;
    hcImg = rsz.clone();
	
	/// Reduce the noise so we avoid false circle detection
	//cout<<"Filtering "<<std::endl;
	//GaussianBlur( hcImg, hcImg, Size(9, 9), 2, 2 );
	//namedWindow( "BLUR FILTER", WINDOW_AUTOSIZE );// Create a window for display.
	//imshow("BLUR FILTER", hcImg);
	
	/// Apply the Hough Transform to find the circles
	//cout<<"Finding circles "<<endl;
    
	/// create the main window, and attach the trackbars
		if (isshownHC){
			namedWindow( windowHoughC, WINDOW_AUTOSIZE );
			createTrackbar(cannyThresholdTrackbarName, windowHoughC, &cannyThreshold,maxCannyThreshold);
			createTrackbar(accumulatorThresholdTrackbarName, windowHoughC, &accumulatorThreshold, maxAccumulatorThreshold);
			createTrackbar(minRadiusTrackbarName, windowHoughC, &minRadius, maxMinRadius);
			isshownHC = 0;
		}
		cannyThreshold = max(cannyThreshold, 1);
		accumulatorThreshold = max(accumulatorThreshold, 1);
		minRadius = max(minRadius, 1);
		///runs the detection, and update the display
		HoughCircle(rsz, colorImg, cannyThreshold, accumulatorThreshold, minRadius);
		waitKey(3);
 }
   
   void HoughCircle(const Mat& src_gray, const Mat& src_display, int cannyThreshold, int accumulatorThreshold, int minRadius)
    {
    // will hold the results of the detection
    vector<Vec3f> circles;
    // runs the actual detection
    HoughCircles( src_gray, circles, CV_HOUGH_GRADIENT, 1, src_gray.rows/8, cannyThreshold, accumulatorThreshold, minRadius, 0 );
	/*******************************************************************
    *hcImg: Input image (grayscale)
    *circles: A vector that stores sets of 3 values: x_{c}, y_{c}, r for each detected circle.
    *CV_HOUGH_GRADIENT: Define the detection method. Currently this is the only one available in OpenCV
    *dp = 1: The inverse ratio of resolution
    *min_dist = src_gray.rows/8: Minimum distance between detected centers
    *param_1 = 100: Upper threshold for the internal Canny edge detector
    *param_2 = 100*: Threshold for center detection.
    *min_radius = 0: Minimum radio to be detected. If unknown, put zero as default.
    *max_radius = 0: Maximum radius to be detected. If unknown, put zero as default
	*******************************************************************/
	//cout<<"Circles no: "<<circles.size()<<endl;
    // clone the colour, input image for displaying purposes
    Mat display = src_display.clone();
    /// Draw if circles detected
	//if (circles.size() > 0){        
	for( size_t i = 0; i < circles.size(); i++ )
	{
		Point center((circles[i][0]), (circles[i][1]));
		double radius = (circles[i][2]);
		// circle center
		circle( display, center, 3, Scalar(255,0,0), -1, 8, 0 );
		// circle outline
		circle( display, center, radius, Scalar(0,0,255), 3, 8, 0 );
		
		float c_x = ((circles[i][0])/scaling_factor)-sensor_center_offset_x;  // scale 30 due to interpolation 
		float c_y = sensor_center_offset_y-((circles[i][1])/scaling_factor);  // scale 30 due to interpolation 
		float r = (circles[i][2])/scaling_factor;
		//cout<<"Center: "<<"( "<<c_x<<" ; "<<c_y<<" )"<<std::endl;
		//cout<<"Radius: "<<r<<std::endl;
	}
        // shows the results
        //cout<<"Drawing circles "<<endl;
		namedWindow( windowHoughC, WINDOW_AUTOSIZE );					// Create a window for display.
		imshow(windowHoughC, display);
    }
   // }
   
};

/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_features = n.advertise<cv_features_extractor::featureArray>("tilt_pan/features", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this

	for (int i=0 ; i<5; i++ ) features.data.push_back(0);
	features.contactList.push_back("no contact");
	features.contactType="no contact";
	

  int count = 0;
  while (ros::ok())
  {
	//if(gotPCA) {
		if (publish) { 
			if (features.data.size()>0){
		pub_features.publish(features);									// Publish features
		
		features.data.clear();											// clear array
		//for (int i=0 ; i<5; i++ ) features.data.push_back(0);
		
		features.contactList.clear();
		features.contactType="no contact";
		}  
		}
		//gotPCA=0;
	//	ROS_INFO("Features published ... \n");
		//}
		
	//features.data.clear();											// clear array
	//features.contactList.clear();
	//pub_features.publish(features);	
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
