#ifndef IMAGECONVERTER_HPP_
#define IMAGECONVERTER_HPP_
#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS 
#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message
#include <opencv2/imgproc/imgproc.hpp>       							// Include headers for OpenCV's image processing 
#include <opencv2/highgui/highgui.hpp>		 							// Include GUI modules
#include <cv_features_extractor/featureArray.h>			 				// Include msg type header for publish features
#include <math.h>  
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);

    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
    cv::namedWindow(windowHoughL);
    cv::namedWindow(windowHoughLP);
    cv::namedWindow(windowHoughC);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(windowHoughL);
	cv::destroyWindow(windowHoughLP);
	cv::destroyWindow(windowHoughC);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(accumulatorThresholdTrackbarName);
	cv::destroyWindow(minRadiusTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
