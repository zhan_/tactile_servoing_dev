/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
# include "extractor.hpp"
# include "Corner_detectors.hpp"
# include "pca.hpp"
# include "ImageFilters.hpp"

unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string RawImage = "Sensor Image";
const string NormalizedImage = "Normalized Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;


/// PCA


int inc = 0;



/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber

 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(RawImage);
    cv::namedWindow(NormalizedImage);
    cv::namedWindow(windowPCA);
    
  }

  ~ImageConverter()
  {
    cv::destroyWindow(RawImage);
    cv::destroyWindow(NormalizedImage);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
	  
	  
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }	
	/// 1. EXTRACT DATA FROM ROS IMAGE MESSAGE   
	SensorName = cv_ptr->header.frame_id; 								// Extract from topic header
	features_for_control.header.frame_id = SensorName;					//  the name of the sensor in contact
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 	   									// step --> cols
    if (rows != SENSOR_ROWS && cols != SENSOR_COLS) 
    {
      ROS_ERROR("The matrix size in the ros message and in the extractor header file must be the same");
      return;
    }  
    /// 2. BUILD THE 2D TACTILE IMAGE  	
	uchar img[SENSOR_ROWS][SENSOR_COLS] = {};  int c = 0;															
    total_pressure = 0;  no_tactels = 0;
	for(int i=SENSOR_ROWS-1; i>=0; --i) 
	{	
	  for(int j=0; j<SENSOR_COLS; j++) 
	  {
		img[i][j]=cv_ptr->image.data[c];						        
		if (img[i][j] > 30) 
		{ 
		  total_pressure += img[i][j]; 						
		  ++no_tactels; 
		}		
	    ++c;
	  }
	}
    /// 3. IMAGE ENHANCEMET 
    src = Mat(18, 4, CV_8U, img);									    // convert the gray scale image into Mat type matrix img
    if (!src.rows > 0)
    {
	  cout << "Image was not correctly rebuild from the message" << endl;
	  return;
	}
    bitwise_not ( src, src );                                           // mirror the colors of the image
    // Display the raw image without processing
    bool showRawImage = true;
    if (showRawImage)
    {
	  Mat rawImg, raw_colorImg;   	
	  resize( src, rawImg, Size(), SCALING_FACTOR, SCALING_FACTOR ,  INTER_NEAREST);
      namedWindow( RawImage, WINDOW_AUTOSIZE );						// Create a window for display.
	  imshow( RawImage, rawImg );              				    // Show image inside it.
	  waitKey(3);
	}    
    Mat normImg, rszImg;    
    if(total_pressure > 10) 
	{ // normalize and resize the image
	  normalize( src, normImg, 0, 255, NORM_MINMAX, CV_8U, Mat() );      // Normalize data so it can be processed as an gray scale image and also to increase the range for detection 
      resize( normImg, rszImg, Size(), SCALING_FACTOR, SCALING_FACTOR ,  INTER_NEAREST);
	}
	else
	{ // dont normalize noise and just resize
	  resize( src, rszImg, Size(), SCALING_FACTOR, SCALING_FACTOR ,  INTER_NEAREST);	
	}
	if (!rszImg.rows > 0)
    {
	  cout << "Image is not correctly resized..." << endl;
	  return;
	}
    namedWindow( NormalizedImage, WINDOW_AUTOSIZE );					// Create a window for display.
	imshow( NormalizedImage, rszImg );              				    // Show image inside it.
	waitKey(3);
    
    /// 4. IMAGE FILTERING
    Mat processImg;
    std::string filter_selection = "notfilter";                           // modify here for test filters
    if ( filter_selection == "median" )
    {
	  ImageFilters filters;
	  int ksize = 35; bool display_medianImage = true;
      Mat medImg = filters.filterMedian_ (rszImg, ksize = 35, display_medianImage);
      processImg = medImg;
      cvtColor(processImg, colorImg, CV_GRAY2BGR);   						    // convert to color image 	
	}
    else if ( filter_selection == "gaussian" )
    {
	  ImageFilters filters;
      double sigmaX = 2, sigmaY=2; int borderType = BORDER_DEFAULT; cv::Size kernel_g(3,3); bool display_gaussImage = true;
      Mat gaussImg = filters.filterGaussian_ (rszImg, sigmaX, sigmaY, borderType, kernel_g, display_gaussImage );
      processImg = gaussImg;
      cvtColor(processImg, colorImg, CV_GRAY2BGR);   						    // convert to color image 	
    }
    else if ( filter_selection == "notfilter" )
    {
	  processImg = rszImg;
	  cvtColor(processImg, colorImg, CV_GRAY2BGR);   						    // convert to color image 	
	}
	else 
	{
	  cout << "Choose a valid option..." << endl;
	  return;
	}	

    if (!processImg.rows > 0)
    {
	  cout << "Image has not been processed correctly..." << endl;
	  return;
	}
    
    /// 5. EXTRACT FEATURES WITH PCA
	float sensor_center_offsetX = SENSOR_COLS/2;
    float sensor_center_offsetY = SENSOR_ROWS/2;
	PCAnalysis pca;
	if ( !pca.getFeatures_ (processImg, colorImg, no_tactels, sensor_center_offsetX, sensor_center_offsetY) ) 
	{
	  cout << "Couldnt extract features..." << endl;
      return;
	}
	
	/// 6. EXTRACT THE CORNERS
	std::string corner_selection = "tomasi";
    if ( corner_selection == "harris" )
    {
	  Corner_detectors corners;
	  bool display_cornersHarris = true;
      if ( !corners.getHarris_(0,0,processImg,colorImg, sensor_center_offsetX, sensor_center_offsetY, display_cornersHarris ) ) 
	  {
	    cout << "Couldnt extract corners using harris method..." << endl;
        return;
	  }
	} 
	else if ( corner_selection == "tomasi" )  	
	{
	  Corner_detectors corners;
	  bool display_cornersTomasi = true;
      if ( !corners.getTomasi_(0,0,processImg,colorImg, sensor_center_offsetX, sensor_center_offsetY, display_cornersTomasi ) ) 
	  {
	    cout << "Couldnt extract corners using shi and tomasi method..." << endl;
        return;
	  }
    }
    else 
	{
	  cout << "Choose a valid option..." << endl;
	  return;
	}


}

};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "extractor_pps_once");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<tactile_servo_msgs::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<tactile_servo_msgs::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this
  

  int count = 0;
  while (ros::ok())
  {
	if (publish) { 
		
		publish = false;
		
		if ( contacts>0 ) {
			
			
			features_for_control.header.stamp = ros::Time::now(); 
			pub_featuresc.publish(features_for_control);								
			
			/// clear arrays	
			features_for_control.contactOffset_x.clear();											
			features_for_control.contactOffset_y.clear();
			features_for_control.contactForce.clear();
			features_for_control.contactPressure.clear();
			features_for_control.contactOrientation.clear();
			
		
			features_for_planning.header.stamp = ros::Time::now();
			pub_featuresp.publish(features_for_planning);
			
			/// clear arrays
			features_for_planning.contactTypes.clear();
			features_for_planning.contactPoses.clear();
			features_for_planning.contactShapes.clear();
			//features_for_planning.cornersPoses.clear();
			features_for_planning.contactEdges.clear();
			features_for_planning.edgesDimensions.clear();
			features_for_planning.contactArea.clear();
			features_for_planning.contactPerimeter.clear();
			features_for_planning.slip.clear();
 
			
		}  
		
		
	}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
