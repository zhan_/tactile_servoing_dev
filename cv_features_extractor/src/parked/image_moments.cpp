/**
  * Node: features_extractor.cpp
  * Version: 1.1
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
