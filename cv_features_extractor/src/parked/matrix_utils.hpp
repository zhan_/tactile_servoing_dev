#ifndef MATRIX_UTILS_HPP_
#define MATRIX_UTILS_HPP_

class Matrix_utils {
  private:


  public:
		
		int getMagnitudeMean_ (Mat &gradx, Mat &grady)
		{
			vector <int> temporal_magnitude_array;
			for (int i = 0 ; i < gradx.rows ; i++)
	    {
	      for (int j = 0 ; j < gradx.cols ; j++)
	      {
		      int Magnitude = sqrt( pow( gradx.at<int>(i,j) , 2 ) + pow ( grady.at<int>(i,j) , 2 ) ); // cout << Magnitude << endl;
		      temporal_magnitude_array.push_back( Magnitude ); 		   
	      }
	    }    
	    cv::Scalar meanMagnitude = cv::mean( temporal_magnitude_array ); //cout << "the mean is: " << medianMagnitude << endl;	  
	    int mean_mag = meanMagnitude[0]; 
			return mean_mag;
		}
		
		vector <int> getImageOrientations_ ( Mat &gradx, Mat &grady, int magnitude_threshold,  bool degrees )
		{
			vector <int> orientations_database;
	    for (int i = 0 ; i < gradx.rows ; i++)
	    {
	      for (int j = 0 ; j < gradx.rows ; j++)
	      {
		      int Magnitude = sqrt( pow( gradx.at<int>(i,j) , 2 ) + pow ( grady.at<int>(i,j) , 2 ) ); // cout << Magnitude << endl; <hanat are you there?
		      if (Magnitude > magnitude_threshold) 
		      {
						int orientation;
						( degrees ? orientation = atan2(grady.at<int>(i,j),gradx.at<int>(i,j)) * (180/M_PI) :  orientation = atan2(grady.at<int>(i,j),gradx.at<int>(i,j)) );
		        
		        if ( abs(orientation) >= 0 && abs(orientation <= 180) )  
		        {
					    orientations_database.push_back(abs(orientation));		      
					  }
		      }
	      }
	    }
	    return  orientations_database;
	  }
		/** This member functino gives the score of a given value according
     *  to the times the value is repeated inside a given array of data
		 *  this function works with opencv array data type 
		*/		
    int giveScoreOfRepetition_ ( Mat data_array , int value )
    {
	    int score = 0;
	    int size = data_array.rows;
	    for ( int i = 0 ; i < size ; i++ )
	    {
			  if ( data_array.at<int>(i,0) == value ) score ++;
		  }		  
	    return score;
	  }
	  
	  /** This member functino deletes the repeated data of a given matrix */ 
	  vector <double> deleteDuplicates_ ( vector <double> data_array  )
    {
			int size = data_array.size();
			//cout << " Size 1 " << size << endl;
      for (int i = 0 ; i < size ; i++)
	    {
		    for (int j = i+1 ; j < size ; )
		    {
	        if ( data_array[j] == data_array[i] )
		      {
		        for( int k=j ; k < size ; k++ )
		        {
		          data_array[k] = data_array[k+1];
		        }
		        size --;
	        }
	        else 
	        j++;
	      }
	    }
	    //for (int i = 0 ; i < data_array.size() ; i ++ ) cout << data_array[i] << " " ;
	    //cout << " Size 2 " << data_array.size() << endl; 
	    return data_array;
	  }

 };
#endif
