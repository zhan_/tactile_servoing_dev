#ifndef CORNERHARRIS_HPP_
#define CORNERHARRIS_HPP_
# include "extractor.hpp"

int harris_thresh = 240;
int harris_max_thresh = 255;

const string corners_window = "Corners";


class Corner_Harris {
  private:

  
  public:
    bool init();

void getCorners_( int, void*, cv::Mat normIm, cv::Mat colorImg)
{
  namedWindow( corners_window, CV_WINDOW_AUTOSIZE );
  createTrackbar( "th", corners_window, &harris_thresh, harris_max_thresh );
  h_gray_img  = normIm.clone();  h_col_img = colorImg.clone();      // Copy original images to then operate just in copies
  resize( h_col_img, colorPca, Size(), SCALING_FACTOR , SCALING_FACTOR ,  INTER_NEAREST);  // resize to plot corners
  Mat dst, dst_norm;                                                    // This is to store the detector results
  dst = Mat::zeros( h_col_img.size(), CV_32FC1 );                       // chanel must be CV_32FC1 type and with the size of source image
  /// Detecting corners with harris
  int blockSize = 8, kernel_mask = 9; double k = 0.04;                  // Detector parameters --> tune it
  cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );
  normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );     // Normalizing results from 0 to 255
  /// Drawing a circle around corners and getting the number of corners
  int pos_x, pos_y, TEMP1[200], TEMP2[200];                           // to store the corner positions
  int cornersNumber = 0;	
  for( int j = 0; j < dst_norm.rows ; j++ )
  { 
    for( int i = 0; i < dst_norm.cols; i++ )
    {
      if( (int) dst_norm.at<float>(j,i) >  harris_thresh)
	  {
	    cornersNumber ++; 
		circle(colorPca, Point( i*SCALING_FACTOR , j*SCALING_FACTOR  ), 5, CV_RGB(0, 25, 255) , 2, 8, 0 ); //draw circle arround corner
		pos_x = (i/SCALING_FACTOR ) - sensor_center_offset_x;			// get values with respect to the sensor frame
		pos_y = sensor_center_offset_y - (j/SCALING_FACTOR);				
		TEMP1[cornersNumber] = i; TEMP2[cornersNumber] = j;             // temporal debugging data
 		features_for_planning.cornersPoses.push_back(pos_x);            // store corners positions in ros message
		features_for_planning.cornersPoses.push_back(pos_y);	
	  }			
	}
  }
  if ( cornersNumber < 3 ) for (int i = 1 ; i <= cornersNumber ; i++)  cout << "i " << TEMP1[i]<< "   ,j " << TEMP2[i] << endl; // debugging purposes
  features_for_planning.cornersNo = cornersNumber;                      // store the number of corners
  /// Showing the result
  imshow( corners_window, colorPca );
  
  // namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	  //createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	  //createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	  //imshow(windowPCA, colorPca); 
	  //waitKey(3);
}

};
#endif
