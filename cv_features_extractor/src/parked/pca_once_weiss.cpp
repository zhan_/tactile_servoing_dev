/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
 */
#include "extractor.hpp"
float pi = 3.14159265359;

/// Tactile Sensor data
const int sensorsRows = 14;
const int sensorsCols = 6;
const int noTactels = 1;
unsigned int scaling_factor = 30;										//to scale the size of tactile image
float area_tactel = 8;

unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string windowArray = "Tactile Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;


/// PCA
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 30;
int maxPCAThreshold = 255; 
const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 14;
int rangePcaArea = 100; 
int inc = 0;




/// Harris
int thresh = 200;





/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
	  
	  
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	
	
	
	
	SensorName = cv_ptr->header.frame_id;
	features_for_control.header.frame_id = SensorName;
	features_for_planning.header.frame_id = SensorName;
	
	//	ROS_INFO("Getting Image...");
	
	
	
	
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 										// step --> cols

    int c = 0;															// to store vector data in matrix
    
    sensor_center_offset_x = sensorsCols/2;
    sensor_center_offset_y = sensorsRows/2;
    
	uchar img[14][6] = {}; 		
	int imgshow[14][6] = {};
	



     /// fill data extracted from array in a tactile image matrix    
    //for(int i=sensorsRows-1; i>=0; --i) {		
    for(int i=sensorsRows-1; i>=0; --i) {
        for(int j=0; j<sensorsCols; j++) {
                imgshow[i][j]=cv_ptr->image.data[c]*2;					// to display in screan
                img[i][j]=cv_ptr->image.data[c]*2;						// to debug
                if (imgshow[i][j] > 1) { //13
                    total_pressure += imgshow[i][j]; 						// accumulate forces
                    ++no_tactels;
                }
        ++c;
        }
    }

	/// Convert to Mat type matrix img
    src = Mat(14, 6, CV_8U, img);
	//namedWindow( "convertedFromPressure", WINDOW_AUTOSIZE );						// Create a window for display.
    //imshow( "convertedFromPressure", src );              						// Show our image inside it.

	///Add Noise to images
	//saltpepperNoise();
	//gaussianNoise();


	/// Resize image for visualize it using interpolation and store in rsz
    resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
    Mat tactImg = rsz.clone();
    //cout << "tactile image at coord 2,2 =" << tactImg.at<int>(29,220)<< endl << endl;
	
	cvtColor(tactImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	bitwise_not ( colorImg, colorImg );
	colorPca = colorImg.clone();
	colorPca_line = colorImg.clone();
	
	namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
    imshow( windowArray, colorImg );              						// Show our image inside it.
	waitKey(3);
	
	//namedWindow( "greyscale_image", WINDOW_AUTOSIZE );						// Create a window for display.
    //imshow( "greyscale_image", tactImg );              						// Show our image inside it.
	
	
	/// Canny
	CannyThreshold(0, 0);
	cvtColor(edgesImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	
	
	/// Extract features
	extractFeatures(); 	/// Apply PCA for edge detection
	

	/// Display it
	namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	imshow(windowPCA, colorPca); 
	imshow("line_window", colorPca_line);  // show line btw two contact points
	waitKey(3);
	
}


static void corners( int, void*)
{
	
	h_gray_img = bw.clone();
	h_col_img = colorPca.clone();
	
	Mat dst, dst_norm, dst_norm_scaled;
	dst = Mat::zeros( h_col_img.size(), CV_32FC1 );



	/// Detector parameters 
	int blockSize = 2; 
	int kernel_mask = 3;
	double k = 0.04;



	/// Detecting corners
	cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );



	/// Normalizing
	normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
	convertScaleAbs( dst_norm, dst_norm_scaled );


	int cornersNumber = 0;	
	
	
	/// Drawing a circle around corners
	for( int j = 0; j < dst_norm.rows ; j++ )
		{ for( int i = 0; i < dst_norm.cols; i++ )
			{

				if( (int) dst_norm.at<float>(j,i) > PcaThreshold+200 )
				{
					cornersNumber ++; 
					circle( colorPca, Point( i, j ), 5,  CV_RGB(0, 255, 255), 2, 8, 0 );
					int temp_1 = (i/scaling_factor) - sensor_center_offset_x;			 
					int temp_2 = sensor_center_offset_y - (j/scaling_factor);	
					
					features_for_planning.cornersPoses.push_back(temp_1);	
					features_for_planning.cornersPoses.push_back(temp_2);	
				}
				
			}
		}
	
    features_for_planning.cornersNo = cornersNumber;


}
 
/***********************************************************************
 * Canny Detector
 * ********************************************************************/
static void CannyThreshold(int, void*)
{		
	
	
	if (isshownCannyThreshold){	
		
		
		namedWindow( windowCanny, WINDOW_AUTOSIZE );				
		createTrackbar( CannyTrackbarName, windowCanny, &lowThreshold, max_lowThreshold, CannyThreshold );
		isshownCannyThreshold = 0;
		
		
	}
	
	
    /// Reduce noise with a kernel 3x3
    blur( rsz, edgesImg, Size(3,3) );
    
    
    /// Canny detector
    lowThreshold = max(lowThreshold, 1);
    Canny( rsz, edgesImg, lowThreshold, lowThreshold*ratio, kernel_size ); // detect the edges of the image 50 200 3									
    /// Canny (source, destiny, low treshold, high treshold, kernel size)
	/// low treshold: tune this parameter
	/// high treshold: Set in the program as three times the lower threshold (following Canny’s recommendation)
	/// kernel size: We defined it to be 3 (the size of the Sobel kernel mask to be used internally)
    /// Using Canny's output as a mask, we display our result
    
    
    
    imshow(windowCanny, edgesImg);										// Show our image inside it.
	waitKey(3);
}


/*********************************************************************** 
 * Perform PCA
************************************************************************/

void extractFeatures ()
{   

	/// PRE-PROCESSING
	pcaImg = rsz.clone();
	
	
	/// Threshold image for binarize the data and store in bw (binary image)
	threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
	
	
	
    /// Find all objects of interest
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;



    /// Find Contours  
    findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
 
 
 
	/// Extract the contours so that
    vector<vector<Point> > approxShape;
    
    
    
    /// Resize to use with approximating function
	approxShape.resize(contours.size());
	contacts = contours.size();
	features_for_planning.contactsNo = contacts;
	

	
	/// GET FEATURES WITH PCA
	
	vector<float> cont_avgs(contours.size(), 0.f); // This contains the averages of each contour. needed for pressure
	Mat labels = Mat::zeros(pcaImg.size(), CV_8UC1); // for calculating average pressure in contact
	imshow("pressure_contour", labels);
	
	/// For each object get features 
    if (contours.size() > 0){		
    
    
    /// Normalize the area with the tactile image area 
    const int areaSensor = 720;
	float normalized_areas[100]={};
   
    Point pos_1;
    
    /// Iterate for each contact detected
		for (size_t i = 0; i < contacts; ++i)
		{
		
			/// Aproximate the contour to detect the edges from an approximate shape and not from raw data
			approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true);
		
		
			/// Get the number of edges
			int edgesNumber = approxShape[0].size() / approxShape.size();
			features_for_planning.contactEdges.push_back(edgesNumber);	 
        
        
			/// Get perimeter and area
			double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
			features_for_planning.contactPerimeter.push_back(perimeterContour);	
			
			double areaContour = contourArea( approxShape[i] ,false) / scaling_factor;
			normalized_areas[i] = areaContour / areaSensor; 
			features_for_planning.contactArea.push_back(areaContour);	
      
			/// Ignore contact if too small or too large, the threshold can be adjusted
			int pcaContact = PcaAreaThreshold ;     	
			if ( areaContour < pcaContact+90 ) continue; 
			//cout << "Contact  :" << areaContour<<endl;
			//cout << "Trashold:"  << pcaContact+110<<endl;
        
			/// Draw the contour of the contact
			drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
			
			/// !!!
			/// overal intencity(pressure) value for each contour
			/// must be changed later the overal mean of pcaImg is smaller than mean of each contour
			Scalar intensity_mean_val = mean(pcaImg); // for all image. mean function returns Scalar type [0 0 0 0]
			//Scalar intensity_mean_val_contour = mean(contours[i]);
			//cout << "pca image mean =" << intensity_mean_val << endl;
			//cout << "pca image mean =" << intensity_mean_val(0) << endl;
			//cout << "contours mean at 0 =" << intensity_mean_val_contour(0) << endl;
			
			drawContours(labels, contours, i, Scalar(i),  CV_FILLED); // labels is zero matrix http://stackoverflow.com/questions/17936510/how-to-find-average-intensity-of-opencv-contour-in-realtime
			//cout << "pca image start" << labels << endl;
			Rect roi = boundingRect(contours[i]);  // returns coordinates of box
			cout << "ROI_begin" << roi << endl;
			cout << "ROI_end" << endl;
			Scalar pressure_mean = mean(pcaImg(roi),labels(roi) == i); // pressure_mean within the contour
			//cont_avgs[i] = pressure_mean[0];
			//cout << "number of pixels in contour real" << contours[i].size() << endl; 
			cout << "iter =" << i << endl;
			//cout << "contour mean =" << cont_avgs[i] << endl;
 
			
			/// get corners with Harris detector
			corners(0,0);
			
			
			
			/// Construct a buffer used by the pca analysis
			vector<Point> pts = contours[i]; 
			Mat img = bw.clone();
			Mat data_pts = Mat(pts.size(), 2, CV_64FC1);

			
			for (int ind1 = 0; ind1 < data_pts.rows; ++ind1) {
				
				data_pts.at<double>(ind1, 0) = pts[ind1].x;
				data_pts.at<double>(ind1, 1) = pts[ind1].y;
			
			}


			/// Perform PCA analysis of the contact image
			PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);



			/// Store the position of the object
			Point pos = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
 
 
 
			/// To store the eigenvalues and eigenvectors
			vector<Point2d> eigen_vecs(2);
			vector<double> eigen_val(2);

			
			for (int ind2 = 0; ind2 < 2; ++ind2) {
			
				eigen_vecs[ind2] = Point2d(pca_analysis.eigenvectors.at<double>(ind2, 0), pca_analysis.eigenvectors.at<double>(ind2, 1));
				eigen_val[ind2] = pca_analysis.eigenvalues.at<double>(0, ind2);
			
			}
	
			/// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
			circle(colorPca, pos, 3, CV_RGB(255, 0, 0), 3); // center of pressure
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8); // first component of pca
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8); // second component of pca ok. 

			/// Center of pressure    
			object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor; 	//what is scaling factor? scaling factor is use to scale the small image and be able to see.
			// so when you apply pca, you apply it in the big image, but then you need to know the center in the original image so you divide by the scaling factor.
			object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
			//cout << "object_center_x" << object_center_x << endl;

			
			// temporal
			// Point temp_pos = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));	  
			// check in opencv array of points, something like
			// wait 
			// vector <Point> arrayPoins;
			// then you store in each iteration the points in the array
			

			/// Adjust the size of the eigen vectors to fit in the image
			pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
			pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;

			/// Now calculate offset with the sensor frame // here i take the sensor center and find the CoP with respect to it
			offset_x = object_center_x-sensor_center_offset_x;	// where I should create offset_x2 and offset_y2 for the second contact and save it?
			features_for_control.contactOffset_x.push_back(offset_x);		// this is a loop so in each iteration you calculate it and pushback each value to
			// this array ok then I should access to features_for_control.contactOffset_x yes[ but yo0u do it in tactile servo node. ok. thank you	 
			offset_y = sensor_center_offset_y-object_center_y;	
			features_for_control.contactOffset_y.push_back(offset_y);
	
			/// Center of pressure  with respect to the sensor frame
			features_for_planning.contactPoses.push_back(offset_x); // here I see that the CoP is sent with respect to the center of sensor.yes
			features_for_planning.contactPoses.push_back(offset_y);	// please show me the part of code where you visualize the points or lines inside the tactile image	

			/// If there is only one contact point get the PCA components. If there are two contact points. Get the line equation.
			
			Point pos_obj1_temp;
			Point pos_obj2_temp;
			
			if((contacts == 2)&&(i == 0)){
				pos_1 = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
				//cout << "Pos1  " << pos_1 << endl;
			}
			
			if ((contacts == 2)&&(i == 1)){
				Point pos_2 = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
				//cout << "Pos1  " << pos_1 << endl;
				//cout << "Pos2  " << pos_2 << endl;
				
				//orientation_rad = atan2(features_for_control.contactOffset_y[1] - features_for_control.contactOffset_y[0], features_for_control.contactOffset_x[1] - features_for_control.contactOffset_x[0]);
				orientation_rad = atan2(pos_2.y - pos_1.y, pos_2.x - pos_1.x);
				
				orientation_deg = (orientation_rad * (180 / 3.1416))+90;
				//cout << "CoP1_x:" << features_for_control.contactOffset_x[0] << endl;
				//cout << "CoP2_x:" << features_for_control.contactOffset_x[1] << endl;
				
				cout << "two point contact orientation: " << orientation_deg << endl;
				//pos_obj1_temp = Point(features_for_control.contactOffset_x.at(0), features_for_control.contactOffset_y.at(0));
				//pos_obj2_temp = Point(features_for_control.contactOffset_x[1], features_for_control.contactOffset_y[1]);
				 
				line(colorPca_line, pos_1, pos_2, CV_RGB(0, 0, 255), 3,8); // line btw two points
				circle(colorPca_line, pos_2, 3, CV_RGB(255, 0, 0), 3); // center of pressure
				circle(colorPca_line, pos_1, 3, CV_RGB(255, 0, 0), 3); // center of pressure
				//cout << "Pos of COP" << pos << endl;
                features_for_control.contactOrientation.clear();
				features_for_control.contactOrientation.push_back(orientation_deg);

			}
			else{
				/// The orientation of the main component of PCA
				orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
				orientation_deg = -(orientation_rad * (180 / 3.1416)) - 90;
				features_for_control.contactOrientation.clear();
				features_for_control.contactOrientation.push_back(orientation_deg);
			}
/*
line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8); // first component of pca
C++: void circle(Mat& img, Point center, int radius, const Scalar& color, int thickness=1, int lineType=8, int shift=0)
C: void cvCircle(CvArr* img, CvPoint center, int radius, CvScalar color, int thickness=1, int line_type=8, int shift=0 )
    Parameters:
        img – Image where the circle is drawn.
        center – Center of the circle.
        radius – Radius of the circle.
        color – Circle color.
        thickness – Thickness of the circle outline, if positive. Negative thickness means that a filled circle is to be drawn.
        lineType – Type of the circle boundary. See the line() description.
        shift – Number of fractional bits in the coordinates of the center and in the radius value.
 */			
	
			/*
in "pca_once.cpp" the CoP of each contact is saved in iterations.
then in order to get the line between two contacts I need to safe 
somehow the coordinates of two CoPs
in "tactile_servo.cpp" I can access to CoP coordinates
*/


	//~ 
			//~ if ((contacts == 2)&&(i == 2)){
				//~ 
				//~ orientation_deg = 
				//~ features_for_control.contactOrientation.push_back(orientation_deg);
				// then here you use 
				// circle(NewImageName, access the point 0 of the vector, 3, CV_RGB(255, 0, 0), 3); 
				// circle(NewImageName, access the point 1 of the vector, 3, CV_RGB(255, 0, 0), 3); thats it. will it show on the same tactile image?
				// it will show in NewImageName, you can also show in the same but then why not use pca image?
				// yes. I will see it then on PCA image, but I did not see that... when i run the nodes. Ok i understand
				 
			//~ } 
			// in fact you have to do that after the loop because we want to see all contacts
			// it is showing the last one. in that case do what I just said and put 
				// circle(colorPca, access the point 0 of the vector, 3, CV_RGB(255, 0, 0), 3);
	//~ 
			
			
			
			/// Calculate the pressure
			total_pressure /= 100;
			//features_for_control.contactPressure.push_back(total_pressure); --> for experiments
			features_for_control.contactPressure.push_back(pressure_mean[0]);
			total_force = total_pressure * (no_tactels * area_tactel); //this is wrong now it is for all tactile array
			total_force = pressure_mean[0] * (areaContour);
			features_for_control.contactForce.push_back(total_force);
			
			
			
			
			total_pressure = 0;
			
			
			/// Detect Slipage		
			//	slipDetection (offset_x, offset_y,15);
			features_for_planning.slip.push_back(false); 
			
			
			/// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
			distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) );
			distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );
			

									
			/// Get the contact type and shape
			get_contactTypeShape (normalized_areas,contacts, distance1, distance2, edgesNumber, perimeterContour);
			features_for_planning.contactTypes.push_back(contactType);	
			features_for_planning.contactShapes.push_back(contactShape);
			
			publish = true;				
    }

        
       // here
       // check the contacts size
       // and plot them as mentioned with the array of points? 
		/// Reset variables 
		total_force = 0; orientation_deg = 0; 
		offset_x = 0; offset_y = 0;	no_tactels = 0;
	
	
	
		

	}
}



double get_contactTypeShape (float Areas[], int contacts, float dx, float dy, int edges, float size)  {
	
	for(unsigned int i = 0; i < contacts; i++ )
	{
	  if(Areas[i] < 0.2) contactType = "small_point";
	  if(Areas[i] > 0.2) contactType = "large_point";
	}
	
	if (size > 4) {
	
		if ( dx-dy <= 1 &&  edges == 4 ) contactShape = "square";
		if ( edges == 3 ) contactShape = "triangle";
		if ( dx > 4*dy || dy > 4*dx) {
			contactShape = "rectangle";
			contactType = "edge";
		}	
	
		if( edges>4 )  {
			contactShape = "polygon";
		//	contactType = "push";
		}
	}
	



	else {	
		
		if (dx-dy <= 1 &&  size < 4) contactType = "point";
		
	}


}

};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "extractor_pps_once_weiss");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<tactile_servo_msgs::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<tactile_servo_msgs::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this
  

  int count = 0;
  while (ros::ok())
  {
	if (publish) { 
		
		publish = false;
		
		if ( contacts>0 ) {
			
			
			features_for_control.header.stamp = ros::Time::now(); 
			pub_featuresc.publish(features_for_control);								
			
			/// clear arrays	
			features_for_control.contactOffset_x.clear();											
			features_for_control.contactOffset_y.clear();
			features_for_control.contactForce.clear();
			features_for_control.contactPressure.clear();
			features_for_control.contactOrientation.clear();
			features_for_control.contactOrientationTwopoints.clear();
			
		
			features_for_planning.header.stamp = ros::Time::now();
			pub_featuresp.publish(features_for_planning);
			
			/// clear arrays
			features_for_planning.contactTypes.clear();
			features_for_planning.contactPoses.clear();
			features_for_planning.contactShapes.clear();
			features_for_planning.cornersPoses.clear();
			features_for_planning.contactEdges.clear();
			features_for_planning.edgesDimensions.clear();
			features_for_planning.contactArea.clear();
			features_for_planning.contactPerimeter.clear();
			features_for_planning.slip.clear();
 
			
		}  
		
		
	}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
