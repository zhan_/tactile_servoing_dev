#ifndef CORNER_DETECTORS_HPP_
#define CORNER_DETECTORS_HPP_
# include "extractor.hpp"
# include <geometry_msgs/Point.h>   //add this type for points
# include "getAnglesSobel.hpp"
int harris_thresh = 80;
int harris_max_thresh = 100;
const string harris_window = "Harris";

int tomasi_max_corners = 1;
int tomasi_max_trackbar = 10;

int tomasi_max_window = 90;
int tomasi_max_window_trackbar = 150 ;

const string tomasi_window = "Tomasi";


bool second=false;
int xcorner=0, ycorner=0;

class Corner_detectors {
  private:


  public:
    bool getHarris_( int, void*, cv::Mat normIm, cv::Mat colorImg, float sensor_center_offsetX , float sensor_center_offsetY , bool show)
    {
      namedWindow( harris_window, CV_WINDOW_AUTOSIZE );
	  createTrackbar( "th", harris_window, &harris_thresh, harris_max_thresh );
	  h_gray_img  = normIm.clone();  h_col_img = colorImg.clone();      // Copy original images to then operate just in copies
	  Mat dst, dst_norm;                                                    // This is to store the detector results
	  dst = Mat::zeros( h_col_img.size(), CV_32FC1 );                       // chanel must be CV_32FC1 type and with the size of source image
	  /// Detecting corners with harris
	  int blockSize = 8, kernel_mask = 9; double k = 0.04;                  // Detector parameters --> tune it
	  cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );
	  if (dst.rows > 0) 
	  { // if there are corners
		normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );     // Normalizing results from 0 to 255
		int size_cornersArray = 10;
		int pos_x[size_cornersArray], pos_y[size_cornersArray], i_prev = 0, j_prev = 0, similarity_th = 10;                           // to store the corner positions
		int cornersNumber = 0;	
		for( int j = 0; j < dst_norm.rows ; j++ )
		{ 	  
		  for( int i = 0; i < dst_norm.cols; i++ )
		  {
			if( (int) dst_norm.at<float>(j,i) >  harris_thresh * 3)
			{ //corner according to thrshold	  
			  if ( i < abs(i_prev + similarity_th) && j < abs(j_prev + similarity_th) )  { i_prev = i; j_prev = j; }   // if corner is very close to previous one, then update previous corner value with the actual
			  if ( i != i_prev && j != j_prev )
			  {	// if corner is different enough from the previous one then	lets extract the corner position  
			    cornersNumber ++;  
				if (cornersNumber > size_cornersArray )
			    { 
				  cout << "corners detected greater than storing array" << endl;
				  cout << "adjust the threshold or change the detector parameters" << endl;	
				  return 0;
				}
				else
				{	// now extract the info	and draw the corners	  	
				  pos_x[cornersNumber] = (i/SCALING_FACTOR ) - sensor_center_offsetX;			// get values with respect to the sensor frame
				  pos_y[cornersNumber] = sensor_center_offsetY - (j/SCALING_FACTOR);
				  circle(h_col_img, Point(i, j), 10, CV_RGB(0, 25, 255) , 2, 8, 0 ); //draw circle arround corner
				  i_prev = i; j_prev = j;  
				  //cout << (i/SCALING_FACTOR ) - sensor_center_offsetX << " , " << sensor_center_offsetY - (j/SCALING_FACTOR) << endl;	  
				}		  	
			  }			
			}
		  }  
		}
	    features_for_planning.cornersNo = cornersNumber;                      // store the number of corners
	    features_for_planning.cornersPoses.clear();
        for (int i = 0; i < cornersNumber; i++)
        {
	      features_for_planning.cornersPoses.push_back(pos_x[cornersNumber]);           // store corners positions in ros message
	      features_for_planning.cornersPoses.push_back(pos_y[cornersNumber]);
	    }
        if (show)  imshow( harris_window, h_col_img );						// Showing the result
        return 1;
     }
     else 
     {
		features_for_planning.cornersPoses.clear();
       features_for_planning.cornersNo = 0;   
       cout<<"No corners found..."<<endl;
       return 0;
     }
  }  
  /// Shi-Tomasi method paper Good Features to track
  bool getTomasi_( int, void*, cv::Mat grayImg, cv::Mat colorImg, float sensor_center_offsetX , float sensor_center_offsetY , bool show)
  {
    namedWindow( tomasi_window, CV_WINDOW_AUTOSIZE );
	createTrackbar( "No", tomasi_window, &tomasi_max_corners, tomasi_max_trackbar );
	createTrackbar( "track", tomasi_window, &tomasi_max_window, tomasi_max_window_trackbar );
	if (tomasi_max_window == 0) tomasi_max_window =1;
	Mat gray_img  = grayImg.clone();  Mat col_img = colorImg.clone();      // Copy original images to then operate just in copies
	if( tomasi_max_corners < 1 ) { tomasi_max_corners = 1; }
    /// Parameters for Shi-Tomasi algorithm
    vector<Point2f> corners;
    double qualityLevel = 0.01;
    double minDistance = 10;
    int blockSize = 3;
    bool useHarrisDetector = false;
    double k = 0.04;
    
    Mat mask = Mat::ones(gray_img.rows,gray_img.cols,CV_8UC1);
    /// Mask to try to track corner 
    if (second && tomasi_max_corners==1) 
    {
	  int x_min = xcorner - tomasi_max_window;	  int x_max	= xcorner + tomasi_max_window; //check limits of image
	  int y_min = ycorner - tomasi_max_window;	  int y_max	= ycorner + tomasi_max_window; //check limits of image
      Mat base = Mat::zeros(gray_img.rows,gray_img.cols,CV_8UC1);      
      Point pt1 (x_min,y_min);   Point pt2 (x_max,y_max);
      rectangle(base, pt1,pt2, Scalar(255,255,255), CV_FILLED, 8, 0);      
      /*for (int j = 0; j < base.cols; j++)
      {
	    if (j > x_min && j < x_max) 
	    {		
          for (int i = 0; i < base.rows; i++)
          {		  
	        if (i > y_min && i < y_max)  base.at<int>(i,j) = 1;	         				  				
	      }
	    }   
      }   */ 
      namedWindow( "mask", CV_WINDOW_AUTOSIZE );
      imshow( "mask", base );	
      mask = base.clone();      
    }
    /// Apply corner detection Mat()
    goodFeaturesToTrack( gray_img, corners, tomasi_max_corners, qualityLevel, minDistance, mask, blockSize, useHarrisDetector, k );
    /** void goodFeaturesToTrack(InputArray image, OutputArray corners, int maxCorners, double qualityLevel, double minDistance, InputArray mask=noArray(), int blockSize=3, bool useHarrisDetector=false, double k=0.04 )
     *  image – Input 8-bit or floating-point 32-bit, single-channel image.
     *  corners – Output vector of detected corners. 
     *  maxCorners – Maximum number of corners to return. If there are more corners than are found, the strongest of them is returned.
     *  qualityLevel – Parameter characterizing the minimal accepted quality of image corners. 
     * 				  The parameter value is multiplied by the best corner quality measure, which is the minimal eigenvalue (see cornerMinEigenVal() ) or the Harris function response (see cornerHarris() ). 
     * 				  The corners with the quality measure less than the product are rejected. For example, if the best corner has the quality measure = 1500, and the qualityLevel=0.01 , 
     * 				  then all the corners with the quality measure less than 15 are rejected.
     *  minDistance – Minimum possible Euclidean distance between the returned corners.
     *  mask – Optional region of interest. If the image is not empty (it needs to have the type CV_8UC1 and the same size as image ), it specifies the region in which the corners are detected.
     *  blockSize – Size of an average block for computing a derivative covariation matrix over each pixel neighborhood. See cornerEigenValsAndVecs() .
     *  useHarrisDetector – Parameter indicating whether to use a Harris detector (see cornerHarris()) or cornerMinEigenVal().
     *  k – Free parameter of the Harris detector. 
    */
    /// Draw corners detected
    if ( corners.size() > 0 )
    {
	  features_for_planning.cornersNo = corners.size();                      // store the number of corners
	  int radius = 10;
      RNG rng(12345);
      for( int i = 0; i < corners.size(); i++ )
      { 
	    circle( col_img, corners[i], radius, Scalar(rng.uniform(0,255), rng.uniform(0,255), rng.uniform(0,255)), -1, 8, 0 ); 
      }    
      features_for_planning.cornersPoses.clear();
     // geomtry_msgs.Point corn
      for (int i = 0; i < corners.size(); i++)
      {
        float x = corners.at(i).x; float y = corners.at(i).y;           // get individual corner values from vectorof points
        xcorner=x; ycorner=y;
        float pos_x = (x/SCALING_FACTOR ) - sensor_center_offsetX;		// get values with respect to the sensor frame
		float pos_y = sensor_center_offsetY - (y/SCALING_FACTOR);
        features_for_planning.cornersPoses.push_back(pos_x);
        features_for_planning.cornersPoses.push_back(pos_y);
	  }
      if (show)  imshow( tomasi_window, col_img );						// Showing the result by displaying the image
      second=true;
      int number_of_angles = 2;
      Sobel_angles anglesToServo;
      if ( !anglesToServo.getAngles_ (gray_img, number_of_angles) )           // this function calculates the more repeated angles in the image and then store in the controlFeatures message 
      {
		  cout<<"Couldnt extract angles using gradient method"<< endl; 
	  }
      return 1;
    }
    else 
    {
	  features_for_planning.cornersPoses.clear();
	  features_for_planning.cornersNo = 0;                    
      cout<<"No corners found with Shi and Tomasi method..."<<endl;
      return 0;
    }
    
  }  
};
#endif
