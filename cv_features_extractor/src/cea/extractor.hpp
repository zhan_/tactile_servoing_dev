/// PCA Header
#ifndef EXTRACTOR_HPP_
#define EXTRACTOR_HPP_

#include <ros/ros.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 							// Include image_transport for publishing and subscribing to images in ROS 
#include <cv_bridge/cv_bridge.h> 			 							// Include the header for CvBridge as well as some useful constants and functions related to image encodings
#include <sensor_msgs/image_encodings.h> 	 							// Include header for image encoding sensor message
#include <opencv2/imgproc/imgproc.hpp>       							// Include headers for OpenCV's image processing 
#include <opencv2/highgui/highgui.hpp>		 							// Include GUI modules
#include <cv_features_extractor/featuresPlanning.h>			 				// Include msg type header for publish features
#include <cv_features_extractor/featuresControl.h>			 				// Include msg type header for publish features
#include <math.h>   													// sqrt

using namespace cv;
using namespace std;

/// Create globally messages to be published
cv_features_extractor::featuresControl features_for_control;
cv_features_extractor::featuresPlanning features_for_planning;
float offset_x, offset_y, total_force, total_pressure, orientation_deg, orientation_rad;
float object_center_x, object_center_y, pos1_x, pos2_x, pos1_y, pos2_y, distance1, distance2;

/// OpenCv Headers
Mat src, bw, rsz, pcaImg, tactImg, edgesImg, colorImg, colorPca;
/// Harris
Mat h_gray_img, h_col_img;

unsigned int sensor_center_offset_x;									//to compute offset with object frame of reference
unsigned int sensor_center_offset_y;

string SensorName;
string prevSensorName;
string contactType;
string contactShape;
unsigned int contacts;
bool publish;

#endif
