#ifndef CEA_SENSORS_HEADER_HPP_
#define CEA_SENSORS_HEADER_HPP_

static const float pi = 3.14159265359;

/// Tactile Sensor data
static const int Index_sensorsRows = 3;
static const int Index_sensorsCols = 3;

static const double Index_scaling_factor = 100;										//to scale the size of tactile image
static const double Index_area_tactel = 8;
static const unsigned int scaling_factor = 100;	
static const float area_tactel = 8;

const int noTactels = 9;
unsigned int scaling_factor = 100;										//to scale the size of tactile image
float area_tactel = 8;



#endif

