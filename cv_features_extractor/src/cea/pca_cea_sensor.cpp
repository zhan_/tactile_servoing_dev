/**
  * Node: features_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	- Subscribe to ros type tactile image from simulation or real data
  * 	- Perform PCA for calculating the orientation of image
  * 	- Calculate the force, pression and offset
  * 	- Show tactel image and features	
  * 	- Publish features extracted from image
  * Dependencies: OpenCv, this_package
  * Topic: tactile_features
  * Topic Msg Type: created
  * 
 */
#include "extractor.hpp"
float pi = 3.14159265359;

/// Tactile Sensor data
const int sensorsRows = 3;
const int sensorsCols = 3;
const int noTactels = 9;
unsigned int scaling_factor = 100;										//to scale the size of tactile image
float area_tactel = 8;

unsigned int gotPCA, no_tactels=0;

///to reduce innecesary debugging 
int isshownCannyThreshold = 1, isshownHC = 1, isshownPca = 1;

/// windows and trackbars name
const string windowArray = "Tactile Image";

/// Canny
const string windowCanny = "Canny";
const string cannyThresholdTrackbarName = "Canny";
const string CannyTrackbarName = "treshold";					
const int cannyThresholdInitialValue = 200;
const int maxCannyThreshold = 255;    
int cannyThreshold = cannyThresholdInitialValue;
int edgeThresh = 1;
int lowThreshold=50;
int max_lowThreshold = 255;
int ratio = 3;
int kernel_size = 3;


/// PCA
const string windowPCA = "PCA";
const string pcaThresholdTrackbarName = "Th";
int PcaThreshold = 5;
int maxPCAThreshold = 255; 
const string pcaAreaTrackbarName = "Area";
int PcaAreaThreshold = 14;
int rangePcaArea = 100; 
int inc = 0;




/// Harris
int thresh = 200;





/// For Slip Detection
ros::Time actTime;
ros::Time prevTime;  
double t = 0; //time
int firstSlip = 1;
double prevx = 0;
double prevy = 0;
int slip = 0;


/*********************************************
 * Convert Image node and transporters
 *********************************************/
class ImageConverter
{
  ros::NodeHandle nh_; 													
  image_transport::ImageTransport it_;									// create transporter
  image_transport::Subscriber image_sub_;								// create subscriber
  
 /*********************************************
 * Create publisher and subscriber
 *********************************************/ 
public:
  ImageConverter()
    : it_(nh_)
  {
    /// Subscribe to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/ros_tactile_image", 1, &ImageConverter::imageCb, this);
	   
    ///OpenCV HighGUI calls to create/destroy a display window on start-up/shutdown. 
    cv::namedWindow(windowArray);
    cv::namedWindow(windowCanny);
    cv::namedWindow(windowPCA);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(windowArray);
    cv::destroyWindow(windowCanny);
    cv::destroyWindow(windowPCA);
    cv::destroyWindow(pcaAreaTrackbarName);
	cv::destroyWindow(cannyThresholdTrackbarName);
	cv::destroyWindow(CannyTrackbarName);
  }
  
/********************************************
 * Processing Image
 *********************************************/
  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
	  
	  
    cv_bridge::CvImagePtr cv_ptr; 										//we first convert the ROS image message to a CvImage suitable for working with OpenCV
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1); //Since we will overwrithe the image for display we need a mutable copy of it
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
	
	
	
	
	
	SensorName = cv_ptr->header.frame_id;
	features_for_control.header.frame_id = SensorName;
	features_for_planning.header.frame_id = SensorName;
	
	//	ROS_INFO("Getting Image...");
	
	
	
	
	/// Store data from subscription topic
    int rows = cv_ptr->image.rows; 										// rows
    int cols = cv_ptr->image.cols; 										// cols
    int step = cv_ptr->image.step; 										// step --> cols

    int c = 0;															// to store vector data in matrix
    
    sensor_center_offset_x = sensorsCols/2;
    sensor_center_offset_y = sensorsRows/2;
    
	uchar img[sensorsRows][sensorsCols] = {}; 		
	int imgshow[sensorsRows][sensorsCols] = {};
	


     /// fill data extracted from array in a tactile image matrix    
    //for(int i=sensorsRows-1; i>=0; --i) {		
	for(int i=sensorsRows-1; i>=0; --i) {	
		for(int j=0; j<sensorsCols; j++) {
				imgshow[i][j]=cv_ptr->image.data[c];					// to display in screan
				img[i][j]=cv_ptr->image.data[c];						// to debug
				if (imgshow[i][j] > 13) {
					total_pressure += imgshow[i][j]; 						// accumulate forces
					++no_tactels;
					}
				std::cout << "   " <<imgshow[i][j];						// show in screen				
		++c;
		}
		std::cout<<"\n";
	}
	
	cout<<"\n";
	
	//std::cout<<"\n";
	
	
	
	
	
	/// Convert to Mat type matrix img
	src = Mat(sensorsRows, sensorsCols, CV_8U, img);
	
	
	
	
	///Add Noise to images
	//saltpepperNoise();
	//gaussianNoise();
	
	
	
	
	/// Resize image for visualize it using interpolation and store in rsz
    resize(src, rsz, Size(), scaling_factor, scaling_factor,  INTER_NEAREST);
	Mat tactImg = rsz.clone();
	
	cvtColor(tactImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	colorPca = colorImg.clone();
	
	
	namedWindow( windowArray, WINDOW_AUTOSIZE );						// Create a window for display.
    imshow( windowArray, colorImg );              						// Show our image inside it.
	waitKey(3);
	
	
	/// Canny
	CannyThreshold(0, 0);
	cvtColor(edgesImg, colorImg, CV_GRAY2BGR);   						// convert to color image
	
	
	/// Extract features
	extractFeatures(); 	/// Apply PCA for edge detection
	

	/// Display it
	namedWindow( windowPCA, WINDOW_AUTOSIZE );						
	createTrackbar( pcaThresholdTrackbarName, windowPCA, &PcaThreshold, maxPCAThreshold);
	createTrackbar( pcaAreaTrackbarName, windowPCA, &PcaAreaThreshold, rangePcaArea);
	imshow(windowPCA, colorPca); 
	waitKey(3);
	
}


static void corners( int, void*)
{
	
	h_gray_img = bw.clone();
	h_col_img = colorPca.clone();
	
	Mat dst, dst_norm, dst_norm_scaled;
	dst = Mat::zeros( h_col_img.size(), CV_32FC1 );



	/// Detector parameters 
	int blockSize = 2; 
	int kernel_mask = 3;
	double k = 0.04;



	/// Detecting corners
	cornerHarris( h_gray_img, dst, blockSize, kernel_mask, k, BORDER_DEFAULT );



	/// Normalizing
	normalize( dst, dst_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat() );
	convertScaleAbs( dst_norm, dst_norm_scaled );
	


	int cornersNumber = 0;	
	
	
	/// Drawing a circle around corners
	for( int j = 0; j < dst_norm.rows ; j++ )
		{ for( int i = 0; i < dst_norm.cols; i++ )
			{

				if( (int) dst_norm.at<float>(j,i) > PcaThreshold+180 )
				{
					cornersNumber ++; 
					circle( colorPca, Point( i, j ), 5,  CV_RGB(0, 255, 255), 2, 8, 0 );
					features_for_planning.cornersPoses.push_back(i/scaling_factor);	
					features_for_planning.cornersPoses.push_back(j/scaling_factor);	
				}
				
			}
		}
	
    features_for_planning.cornersNo = cornersNumber;


}
 
/***********************************************************************
 * Canny Detector
 * ********************************************************************/
static void CannyThreshold(int, void*)
{		
	
	
	if (isshownCannyThreshold){	
		
		
		namedWindow( windowCanny, WINDOW_AUTOSIZE );				
		createTrackbar( CannyTrackbarName, windowCanny, &lowThreshold, max_lowThreshold, CannyThreshold );
		isshownCannyThreshold = 0;
		
		
	}
	
	
    /// Reduce noise with a kernel 3x3
    blur( rsz, edgesImg, Size(3,3) );
    
    
    /// Canny detector
    lowThreshold = max(lowThreshold, 1);
    Canny( rsz, edgesImg, lowThreshold, lowThreshold*ratio, kernel_size ); // detect the edges of the image 50 200 3									
    /// Canny (source, destiny, low treshold, high treshold, kernel size)
	/// low treshold: tune this parameter
	/// high treshold: Set in the program as three times the lower threshold (following Canny’s recommendation)
	/// kernel size: We defined it to be 3 (the size of the Sobel kernel mask to be used internally)
    /// Using Canny's output as a mask, we display our result
    
    
    
    imshow(windowCanny, edgesImg);										// Show our image inside it.
	waitKey(3);
}


/*********************************************************************** 
 * Perform PCA
************************************************************************/

void extractFeatures ()
{   

	/// PRE-PROCESSING
	pcaImg = rsz.clone();
	
	
	/// Threshold image for binarize the data and store in bw (binary image)
	threshold(pcaImg, bw, PcaThreshold, 255, CV_THRESH_BINARY);
	
	
	
    /// Find all objects of interest
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;



    /// Find Contours  
    findContours(bw, contours, hierarchy,  CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0,0));
 
 
 
	/// Extract the contours so that
    vector<vector<Point> > approxShape;
    
    
    
    /// Resize to use with approximating function
	approxShape.resize(contours.size());
	contacts = contours.size();
	features_for_planning.contactsNo = contacts;
	
	
	
	
	/// GET FEATURES WITH PCA
	
	
	/// For each object get features 
    if (contours.size() > 0){		
    
    
    
    
    /// Iterate for each contact detected
		for (size_t i = 0; i < contours.size(); ++i)
		{
		
			/// Aproximate the contour to detect the edges from an approximate shape and not from raw data
			approxPolyDP(contours[i], approxShape[i], arcLength( contours[i], false ) / scaling_factor, true);
		
		
			/// Get the number of edges
			int edgesNumber = approxShape[0].size() / approxShape.size();
			features_for_planning.contactEdges.push_back(edgesNumber);	 
        
        
			/// Get perimeter and area
			double perimeterContour = arcLength( approxShape[i], true ) / scaling_factor;
			features_for_planning.contactPerimeter.push_back(perimeterContour);	
			
			
			double areaContour = contourArea( approxShape[i] ,false) / scaling_factor;
			features_for_planning.contactArea.push_back(areaContour);	
        
        
			/// Ignore contact if too small or too large, the threshold can be adjusted
			int pcaContact = PcaAreaThreshold ;     	
			if ( areaContour < pcaContact ) continue; 

        
			/// Draw the contour of the contact
			drawContours(colorPca, contours, i, CV_RGB(255, 0, 0), 2, 8, hierarchy, 0);
 
			
			/// get corners with Harris detector
			corners(0,0);
			
			
			
			/// Construct a buffer used by the pca analysis
			vector<Point> pts = contours[i]; 
			Mat img = bw.clone();
			Mat data_pts = Mat(pts.size(), 2, CV_64FC1);

			
			for (int i = 0; i < data_pts.rows; ++i) {
				
				data_pts.at<double>(i, 0) = pts[i].x;
				data_pts.at<double>(i, 1) = pts[i].y;
			
			}


			/// Perform PCA analysis of the contact image
			PCA pca_analysis(data_pts, Mat(), CV_PCA_DATA_AS_ROW);



			/// Store the position of the object
			Point pos = Point(pca_analysis.mean.at<double>(0, 0), pca_analysis.mean.at<double>(0, 1));
 
 
 
			/// To store the eigenvalues and eigenvectors
			vector<Point2d> eigen_vecs(2);
			vector<double> eigen_val(2);

			
			for (int i = 0; i < 2; ++i) {
			
				eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0), pca_analysis.eigenvectors.at<double>(i, 1));
				eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
			
			}
	
	
	
			/// Draw the principal components (http://docs.opencv.org/modules/core/doc/drawing_functions.html)
			circle(colorPca, pos, 3, CV_RGB(0, 255, 0), 3);
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[0].x * eigen_val[0], -eigen_vecs[0].y * eigen_val[0]) , CV_RGB(0, 0, 255), 3,8);
			line(colorPca, pos, pos + 0.02 * Point(-eigen_vecs[1].x * eigen_val[1], -eigen_vecs[1].y * eigen_val[1]) , CV_RGB(0, 0, 255), 3,8);




			/// The orientation of the main component of PCA
			orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
			orientation_deg = -orientation_rad * (180 / 3.1416); 

    



			/// Center of pressure      
			object_center_x = pca_analysis.mean.at<double>(0, 0) / scaling_factor;
			features_for_planning.contactPoses.push_back(object_center_x);	
			object_center_y = pca_analysis.mean.at<double>(0, 1) / scaling_factor;
			features_for_planning.contactPoses.push_back(object_center_y);	
    
    
    
    
			/// Adjust the size of the eigen vectors to fit in the image
			pos1_x = 0.02 * (-eigen_vecs[0].x * eigen_val[0]) / scaling_factor; pos1_y = 0.02 * (-eigen_vecs[0].y * eigen_val[0]) / scaling_factor;
			pos2_x = 0.02 * (-eigen_vecs[1].x * eigen_val[1]) / scaling_factor; pos2_y = 0.02 * (-eigen_vecs[1].y * eigen_val[1]) / scaling_factor;
	
	
	
			/// The orientation of the main component of PCA
			orientation_rad = atan2(-eigen_vecs[0].y, -eigen_vecs[0].x);		
			orientation_deg = -orientation_rad * (180 / 3.1416); 
			features_for_control.contactOrientation.push_back(orientation_deg);



			/// Now calculate offset with the sensor frame
			offset_x = object_center_x-sensor_center_offset_x;	
			features_for_control.contactOffset_x.push_back(offset_x);				 
			offset_y = sensor_center_offset_y-object_center_y;	
			features_for_control.contactOffset_y.push_back(offset_y);	
			
			
			
			/// Calculate the pressure
			total_pressure /= 100;
			features_for_control.contactPressure.push_back(total_pressure);
			total_force = total_pressure * (no_tactels * area_tactel);
			features_for_control.contactForce.push_back(total_force);
			total_pressure = 0;
			
			
			/// Detect Slipage		
			//	slipDetection (offset_x, offset_y,15);
			features_for_planning.slip.push_back(false); 
			
			
			/// Calculate a raw ecuclidean distance of PCA vectors to the use in contact detection
			distance1 = sqrt ( pow(pos1_x,2) + pow(pos1_y,2) );
			distance2 = sqrt ( pow(pos2_x,2) + pow(pos2_y,2) );
			

									
			/// Get the contact type and shape
			get_contactTypeShape (distance1, distance2, edgesNumber, perimeterContour);
			features_for_planning.contactTypes.push_back(contactType);	
			features_for_planning.contactShapes.push_back(contactShape);
			
			
			/// Fill in the publishing message the rest data
			//fillFeaturesMsg ();
			publish = true;				
    }



		/// Reset variables 
		total_force = 0; orientation_deg = 0; 
		offset_x = 0; offset_y = 0;	no_tactels = 0;
	
	
	
		

	}
}



double get_contactTypeShape (float dx, float dy, int edges, float size)  {
	
	
	if (size > 4) {
	
		if ( dx-dy <= 1 &&  edges == 4 ) contactShape = "square";
		if ( edges == 3 ) contactShape = "triangle";
		if ( dx > 2*dy || dy > 2*dx) {
			contactShape = "rectangle";
			contactType = "edge";
		}	
	
		if( edges>4 )  {
			contactShape = "polygon";
			contactType = "push";
		}
	}
	



	else {	
		
		if (dx-dy <= 1 &&  size < 4) contactType = "point";
		
	}




}





/***********************************************************************
 * Fill messages
 **********************************************************************/

void fillFeaturesMsg () {
	
	
	int plan = 2; 
	
	// publish for force
	if (plan == 1) {
	if (total_pressure > 100 ) {
	
	total_force += total_force;
	total_pressure += total_pressure;
	//temp += orientation_deg ;
	//cout<<"Orientation (deg): "<<temp<<endl;	
	offset_x += offset_x;
	offset_y += offset_y;
	++inc;
	int avg = 100;
	if (inc >= avg ) {
	//	cout<<"counter: "<<inc<<endl;
		total_force /= avg;
		total_pressure /= avg;
	//	temp /= avg;
		offset_x /= avg;
		offset_y /= avg;
	//	features_for_control.controlFeatures.push_back(total_force);		
	//	cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
//		features_for_control.controlFeatures.push_back(total_pressure);	
	//	cout<<"Pressure: "<<pressure<<endl;								// show the pressure applied by current object						
	//	features_for_control.controlFeatures.push_back(orientation_deg);	
	//	cout<<"**Orientation (deg): "<<temp<<endl;				        // show the orientation of current object	
	//	features_for_control.controlFeatures.push_back(offset_x);
	//	cout<<"Offset x: "<<offset_x<<endl;	    						// show "x" offset of the contact edge with respect to the "x" axis of the sensor       
	//	features_for_control.controlFeatures.push_back(offset_y);	
	//	cout<<"Offset y: "<<offset_y<<endl;								// show "y" offset of the contact edge with respect to the "y" axis of the sensor 
		inc = 0;
	//	temp = 0;
		publish = true;
		}	
	}
	}
	
	// publish for orientation
	if (plan == 2) {
	//	features_for_control.controlFeatures.push_back(total_force);		
		//cout<<"Force: "<<total_force<<endl;								// show the force applied by current object											
	//	features_for_control.controlFeatures.push_back(total_pressure);						
//		features_for_control.controlFeatures.push_back(orientation_deg);	
	//	features_for_control.controlFeatures.push_back(offset_x);   
	//	features_for_control.controlFeatures.push_back(offset_y);	
		publish = true;
	}
	}

   
};



/**
 * Main functions
 */
int main(int argc, char** argv)
{
  ROS_INFO("EXTRACT FEATURES NODE");
  ros::init(argc, argv, "image_converter");
  ros::NodeHandle n; 													// Create node
  /// Create the publisher of the topic: tilt_pan/features
  ros::Publisher pub_featuresc = n.advertise<cv_features_extractor::featuresControl>("tilt_pan/Features/controlFeatures", 100); 
  ros::Publisher pub_featuresp = n.advertise<cv_features_extractor::featuresPlanning>("tilt_pan/Features/planningFeatures", 100); 
  /// Create the publisher for redundant data such are the contact type, and the sensor name
  
  ros::Rate loop_rate(100);												// Loop at 100Hz until the node is shut down
  ImageConverter ic;													// do ic
  ros::spinOnce(); 														// do this
  

  int count = 0;
  while (ros::ok())
  {
	if (publish) { 
		
		publish = false;
		
		if ( contacts>0 ) {
			
			
			features_for_control.header.stamp = ros::Time::now(); 
			pub_featuresc.publish(features_for_control);								
			
			/// clear arrays	
			features_for_control.contactOffset_x.clear();											
			features_for_control.contactOffset_y.clear();
			features_for_control.contactForce.clear();
			features_for_control.contactPressure.clear();
			features_for_control.contactOrientation.clear();
			
		
			features_for_planning.header.stamp = ros::Time::now();
			pub_featuresp.publish(features_for_planning);
			
			/// clear arrays
			features_for_planning.contactTypes.clear();
			features_for_planning.contactPoses.clear();
			features_for_planning.contactShapes.clear();
			features_for_planning.cornersPoses.clear();
			features_for_planning.contactEdges.clear();
			features_for_planning.edgesDimensions.clear();
			features_for_planning.contactArea.clear();
			features_for_planning.contactPerimeter.clear();
			features_for_planning.slip.clear();
 
			
		}  
		
		
	}
	ros::spinOnce();													// do this
    loop_rate.sleep();			 										// wait until it is time for another interaction     
    ++count; 
    			
  }
 
  return 0;

}
