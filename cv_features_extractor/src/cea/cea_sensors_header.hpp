#ifndef CEA_SENSORS_HEADER_HPP_
#define CEA_SENSORS_HEADER_HPP_
#include <cv_features_extractor/fingers.h>

static const float pi = 3.14159265359;

/// Tactile Sensor data
static const int Index_sensorsRows = 3;
static const int Index_sensorsCols = 3;
static const int Middle_sensorsRows = 3;
static const int Middle_sensorsCols = 2;
static const int PalmIndex_sensorsRows = 2;
static const int PalmIndex_sensorsCols = 3;
static const int Thumb_sensorsRows = 3;
static const int Thumb_sensorsCols = 2;
static const int Palm_sensorsRows = 3;
static const int Palm_sensorsCols = 3;

static const double scaling_factor = 100;										//to scale the size of tactile image
static const double area_tactel = 8;





#endif

