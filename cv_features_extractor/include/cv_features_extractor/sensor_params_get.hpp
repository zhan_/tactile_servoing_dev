#include <ros/ros.h>


class SensorParams
{
public:
  SensorParams(std::string sensor_param_name);
  ~SensorParams();
  std::string name_;
  int nrows_;
  int ncols_;
  int step_;
  double size_x_;
  double size_y_;
  double force_scale_;
private:
  std::string sensor_param_name_;
}