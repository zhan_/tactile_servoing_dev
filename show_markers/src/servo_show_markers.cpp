/**
 * @author Zhhanat KAPPASSOV <kappassov@isir.upmc.com>, Contact <zhanat kappassov >
 * @date   19 jan 2016
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 2 of the License, or (at your option)
* any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*
 * @brief  This is a library class for palm pose tools get, send and transfrom 
 * from quartieniton to Euler.
 */
#include "show_markers/servo_show_markers.hpp"

markersShow::markersShow(int tactile_sensor_num_){
    std::string tactile_sensor_num_str;
    std::stringstream out;
    out << tactile_sensor_num_;
    tactile_sensor_num_str = out.str();

    des_feats_sub = n.subscribe("/des_feats", 1,  &markersShow::cb_des_feats_sub,this);
    fb_feats_sub = n.subscribe("/fb_feats", 1,  &markersShow::cb_fb_feats_sub,this);
    fb_planfeats_sub = n.subscribe("/plan_feats", 1,  &markersShow::cb_fb_planfeats_sub,this);

    cop_des_pub = n.advertise<visualization_msgs::Marker>("mark_cop_des_center"+tactile_sensor_num_str, 0 );
    cop_fb_pub = n.advertise<visualization_msgs::Marker>("mark_cop_fb_center"+tactile_sensor_num_str, 0 );
    forcez_des_pub = n.advertise<visualization_msgs::Marker>("mark_forcez_des_pub"+tactile_sensor_num_str, 0 );
    forcez_fb_pub = n.advertise<visualization_msgs::Marker>("mark_forcez_fb_pub"+tactile_sensor_num_str, 0 );
    forcez_diff_fb_des_pub = n.advertise<visualization_msgs::Marker>("mark_forcez_diff_pub"+tactile_sensor_num_str, 0 );

    coc_des_pub = n.advertise<visualization_msgs::Marker>("mark_coc_des_center"+tactile_sensor_num_str, 0 );
    coc_fb_pub = n.advertise<visualization_msgs::Marker>("mark_coc_fb_center"+tactile_sensor_num_str, 0 );
    orientz_des_pub = n.advertise<visualization_msgs::Marker>("mark_orientz_des_pub"+tactile_sensor_num_str, 0 );
    orientz_fb_pub = n.advertise<visualization_msgs::Marker>("mark_orientz_fb_pub"+tactile_sensor_num_str, 0 );

    //read from param server
    int32_t sensor_count = tactile_sensor_num_;
    XmlRpc::XmlRpcValue my_list;
    n.getParam("/tactile_arrays_param", my_list);
    ROS_ASSERT_MSG(my_list.getType() == XmlRpc::XmlRpcValue::TypeArray, "Test array");

    cells_x=static_cast<int> (my_list[sensor_count]["dimensions"][0]["cells_x"]);
    cells_y=static_cast<int> (my_list[sensor_count]["dimensions"][1]["cells_y"]);

    size_x=static_cast<double> (my_list[sensor_count]["size"][0]["x"]);
    size_y=static_cast<double> (my_list[sensor_count]["size"][1]["y"]);

    thickness_z = 0.015;
    forcr_vector_length = 0.01;
    static const float red[] = {255.0,0.0,0.0};
    static const float green[] = {0.0,255.0,0.0};
    static const float blue[] = {0.0,0.0,255.0};
    static const float yellow[] = {255.0,255.0,0.0};
    color_cop_des.assign(green, green + sizeof(green) / sizeof(green[0]) );
    color_cop_fb.assign(red, red + sizeof(red) / sizeof(red[0]) );

    color_forcez_des.assign(green, green + sizeof(green) / sizeof(green[0]) );
    color_forcez_fb.assign(red, red + sizeof(red) / sizeof(red[0]) );

    color_coc_des.assign( green, green + sizeof(green) / sizeof(green[0]) );
    color_coc_fb.assign( red, red + sizeof(red) / sizeof(red[0]) );

    color_orientz_des.assign(green, green + sizeof(green) / sizeof(green[0]) );
    color_orientz_fb.assign( red, red + sizeof(red) / sizeof(red[0]) );

    color_forcez_diff.assign( blue, blue + sizeof(blue) / sizeof(blue[0]) );
    static const float def_scales[] = {0.01,0.01,0.01};
    static const float arrow_scales[] = {0.05,0.01,0.01};
    static const float cylinder_scales[] = {0.05,0.005,0.005};
    scale_default.assign(def_scales, def_scales + sizeof(def_scales) / sizeof(def_scales[0]));
    scale_arrow.assign(arrow_scales, arrow_scales + sizeof(arrow_scales) / sizeof(arrow_scales[0]));
    scale_cylinder.assign(cylinder_scales, cylinder_scales + sizeof(cylinder_scales) / sizeof(cylinder_scales[0]));


//    color_coc_des = 2;
//    color_coc_fb = 3;
//    color_forcez_des = 4;
//    color_forcez_fb = 5;
//    color_forcez_des = 4;
//    color_orientz_des = 5;
//    color_orientz_fb = 6;
    copx_des = 0;
    copy_des = 0;
    force_des =0;
    cocx_des=0;
    cocy_des =0;
    orientz_des=0;
    copx_fb=0;
    copy_fb=0;
    force_fb=0;
    cocx_fb=0;
    cocy_fb=0;
    orientz_fb=0;

}
markersShow::~markersShow(){}
void markersShow::cb_fb_planfeats_sub(const tactile_servo_msgs::PlanFeats& msg_)
{
  contours_num = msg_.numContours;
}

void markersShow::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_des = msg_->control_features[0].centerpressure_x;
        copy_des = msg_->control_features[0].centerpressure_y;
        force_des = msg_->control_features[0].contactForce;
        cocx_des = msg_->control_features[0].centerContact_x;
        cocy_des = msg_->control_features[0].centerContact_y;
        orientz_des = msg_->control_features[0].contactOrientation;
        /* coordinates of COP/COC in world =
        / sequence number of the  COP/COC pixel in x or y
        / *
        / physical size of a sensor in x or y
        / /
        / number of cells in x(y)*/
//        copx_des = copx_des*size_x / cells_x;
//        copy_des = copy_des*size_y / cells_y;
//        cocx_des = cocx_des*size_x / cells_x;
//        cocy_des = cocy_des*size_y / cells_y;
    }
}
void markersShow::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_fb = msg_->control_features[0].centerpressure_x;
        copy_fb = msg_->control_features[0].centerpressure_y;
        force_fb = msg_->control_features[0].contactForce;
        cocx_fb = msg_->control_features[0].centerContact_x;
        cocy_fb = msg_->control_features[0].centerContact_y;
        orientz_fb = msg_->control_features[0].contactOrientation;
        /* coordinates of COP/COC in world =
        / sequence number of the  COP/COC pixel in x or y
        / *
        / physical size of a sensor in x or y
        / /
        / number of cells in x(y)*/
//        copx_fb = copx_fb*size_x / cells_x;
//        copy_fb = copy_fb*size_y / cells_y;
//        cocx_fb = cocx_fb*size_x / cells_x;
//        cocy_fb = cocy_fb*size_y / cells_y;
    }
}
visualization_msgs::Marker markersShow::create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                                      std::string frame_name_,
                                                      std::vector<float> color_,
                                                      u_int32_t shape_,
                                                      std::vector<float> scale_){
    u_int32_t shape = visualization_msgs::Marker::ARROW;
    shape = shape_;
//    shape = visualization_msgs::Marker::SPHERE;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/"+frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "orientations";
    marker.id = 0;
    marker.type = shape;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = point_to_viz_.pose.position.x;
    marker.pose.position.y = point_to_viz_.pose.position.y;
    marker.pose.position.z = point_to_viz_.pose.position.z;

    marker.pose.orientation.x = point_to_viz_.pose.orientation.x;
    marker.pose.orientation.y = point_to_viz_.pose.orientation.y;
    marker.pose.orientation.z = point_to_viz_.pose.orientation.z;
    marker.pose.orientation.w = point_to_viz_.pose.orientation.w;

    marker.scale.x = scale_.at(0);
    marker.scale.y = scale_.at(1);
    marker.scale.z = scale_.at(2);
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = color_.at(0);
    marker.color.g = color_.at(1);
    marker.color.b = color_.at(2);

    return marker;
}
geometry_msgs::PoseStamped markersShow::transform_pose(geometry_msgs::PoseStamped& pose_in_){
    geometry_msgs::PoseStamped pose_out;
    tf_listener.waitForTransform("world", ros::Time(0), "weiss",ros::Time(0), "world", ros::Duration(10));


    try{
        tf_listener.transformPose("world", pose_in_, pose_out);
//        ROS_INFO("at world: (%.4f, %.4f. %.4f) -----> at shadowarm_base: (%.4f, %.4f, %.4f) at time %.4f",
//                 pose_in_.pose.position.x, pose_in_.pose.position.y, pose_in_.pose.position.z,
//                 pose_out.pose.position.x, pose_out.pose.position.y, pose_out.pose.position.z, pose_out.header.stamp.toSec());
        return pose_out;
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return pose_in_;
    }
}

geometry_msgs::PoseStamped markersShow::fillup_pose(float x_, float y_, float z_, float qx_,
                                                    float qy_,float qz_,float qw_){

    geometry_msgs::PoseStamped any_feat_wrt_sensor_pose;
    any_feat_wrt_sensor_pose.header.frame_id = "weiss";
    any_feat_wrt_sensor_pose.header.stamp = ros::Time();

    any_feat_wrt_sensor_pose.pose.position.x = x_;
    any_feat_wrt_sensor_pose.pose.position.y = y_;
    any_feat_wrt_sensor_pose.pose.position.z = z_;
    any_feat_wrt_sensor_pose.pose.orientation.x = qx_;
    any_feat_wrt_sensor_pose.pose.orientation.y = qy_;
    any_feat_wrt_sensor_pose.pose.orientation.z = qz_;
    any_feat_wrt_sensor_pose.pose.orientation.w = qw_;
    return any_feat_wrt_sensor_pose;
}

void markersShow::rviz(){
    // cop marker
    geometry_msgs::PoseStamped cop_des_pose = fillup_pose(copx_des, copy_des, thickness_z, 0,0,0,0);
    visualization_msgs::Marker cop_des_marker = create_marker(cop_des_pose,
                                                              cop_des_pose.header.frame_id.c_str(),
                                                              color_cop_des,
                                                              visualization_msgs::Marker::SPHERE,
                                                              scale_default);
    cop_des_pub.publish(cop_des_marker);
    geometry_msgs::PoseStamped cop_fb_pose = fillup_pose(copx_fb, copy_fb, thickness_z, 0,0,0,0);
    visualization_msgs::Marker cop_fb_marker = create_marker(cop_fb_pose,
                                                              cop_fb_pose.header.frame_id.c_str(),
                                                              color_cop_fb,
                                                              visualization_msgs::Marker::SPHERE,
                                                              scale_default);
    cop_fb_pub.publish(cop_fb_marker);
    // coc marker
    geometry_msgs::PoseStamped coc_fb_pose = fillup_pose(cocx_fb, cocy_fb, thickness_z, 0,0,0,0);
    visualization_msgs::Marker coc_fb_marker = create_marker(coc_fb_pose,
                                                              coc_fb_pose.header.frame_id.c_str(),
                                                              color_coc_fb,
                                                              visualization_msgs::Marker::CUBE,
                                                              scale_default);
    coc_fb_pub.publish(coc_fb_marker);
    geometry_msgs::PoseStamped coc_des_pose = fillup_pose(cocx_des, cocy_des, thickness_z, 0,0,0,0);
    visualization_msgs::Marker coc_des_marker = create_marker(coc_des_pose,
                                                              coc_des_pose.header.frame_id.c_str(),
                                                              color_coc_des,
                                                              visualization_msgs::Marker::CUBE,
                                                              scale_default);
    coc_des_pub.publish(coc_des_marker);
    // forces marker
    forcr_vector_length = force_des/100;// 0.05;
    scale_arrow.at(0) = forcr_vector_length;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(0, 0, 0) );
    tf::Quaternion q;
    q.setRPY(0, -1.57, 0);
    transform.setRotation(q);
    float qx = q.getX();
    float qy = q.getY();
    float qz = q.getZ();
    float qw = q.getW();
//    std::cout<<"qw =  \n"<<qw<< "qz =  \n" <<qz << " qy =  \n" << qy<< "qx =  \n"<<qx<<std::endl;
    geometry_msgs::PoseStamped forcez_des_pub_pose = fillup_pose(copx_des, copy_des, thickness_z, qx,qy,qz,qw);
    visualization_msgs::Marker forcez_des_pub_marker = create_marker(forcez_des_pub_pose,
                                                                     forcez_des_pub_pose.header.frame_id.c_str(),
                                                                     color_forcez_des,
                                                                     visualization_msgs::Marker::ARROW,
                                                                     scale_arrow);
    forcez_des_pub.publish(forcez_des_pub_marker);
    //
    forcr_vector_length = force_fb;//0.05;
    scale_arrow.at(0) = forcr_vector_length;
    transform.setOrigin( tf::Vector3(0, 0, 0) );
    q.setRPY(0, -1.57, 0);
    transform.setRotation(q);
    qx = q.getX();
    qy = q.getY();
    qz = q.getZ();
    qw = q.getW();
    geometry_msgs::PoseStamped forcez_fb_pub_pose = fillup_pose(copx_fb, copy_fb, thickness_z, qx,qy,qz,qw);
    visualization_msgs::Marker forcez_fb_pub_marker = create_marker(forcez_fb_pub_pose,
                                                                    forcez_fb_pub_pose.header.frame_id.c_str(),
                                                                    color_forcez_fb,
                                                                    visualization_msgs::Marker::ARROW,
                                                                    scale_arrow);
    forcez_fb_pub.publish(forcez_fb_pub_marker);
    //
    forcr_vector_length = force_des - force_fb; // force_fb/100;//0.05;
    scale_arrow.at(0) = std::abs(forcr_vector_length)/50;
    transform.setOrigin( tf::Vector3(0, 0, 0) );
    if (forcr_vector_length > 0) q.setRPY(0, -1.57, 0);
    if (forcr_vector_length <= 0) q.setRPY(0, 1.57, 0);
    transform.setRotation(q);
    qx = q.getX();
    qy = q.getY();
    qz = q.getZ();
    qw = q.getW();
    geometry_msgs::PoseStamped forcez_diff_pose = fillup_pose(copx_fb, copy_fb, thickness_z, qx,qy,qz,qw);
    visualization_msgs::Marker forcez_diff_marker = create_marker(forcez_diff_pose,
                                                                  forcez_diff_pose.header.frame_id.c_str(),
                                                                  color_forcez_diff,
                                                                  visualization_msgs::Marker::ARROW,
                                                                  scale_arrow);
    forcez_diff_fb_des_pub.publish(forcez_diff_marker);
    //orientation
    forcr_vector_length = 0.05;
    scale_cylinder.at(0) = forcr_vector_length;
    transform.setOrigin( tf::Vector3(0, 0, 0) );
    q.setRPY(0, 0, 0 + orientz_des);
    transform.setRotation(q);
    qx = q.getX();
    qy = q.getY();
    qz = q.getZ();
    qw = q.getW();
    geometry_msgs::PoseStamped orient_des_pose = fillup_pose(copx_des, copy_des, thickness_z, qx,qy,qz,qw);
    visualization_msgs::Marker orient_des_marker = create_marker(orient_des_pose,
                                                              orient_des_pose.header.frame_id.c_str(),
                                                              color_orientz_des,
                                                              visualization_msgs::Marker::CYLINDER,
                                                             scale_cylinder);
    orientz_des_pub.publish(orient_des_marker);
    //
    forcr_vector_length = 0.05;
    scale_cylinder.at(0) = forcr_vector_length;
    transform.setOrigin( tf::Vector3(0, 0, 0) );
    q.setRPY(0, 0, 0 + orientz_fb);
    transform.setRotation(q);
    qx = q.getX();
    qy = q.getY();
    qz = q.getZ();
    qw = q.getW();
    geometry_msgs::PoseStamped orient_fb_pose = fillup_pose(copx_fb, copy_fb, thickness_z, qx,qy,qz,qw);
    visualization_msgs::Marker orient_fb_marker = create_marker(orient_fb_pose,
                                                              orient_fb_pose.header.frame_id.c_str(),
                                                              color_orientz_fb,
                                                              visualization_msgs::Marker::CYLINDER,
                                                             scale_cylinder);
    orientz_fb_pub.publish(orient_fb_marker);

    return;
}

PoseShow::PoseShow()
{
  /** TODO vecotrs of subs
  std::vector<std::string> top_name_viz_pose;
  bool ok =nh.getParam("/top_name_viz_pose", top_name_viz_pose);
  if(ok)
  {
    ROS_INFO_STREAM("top_names_viz_pose ="<<top_name_viz_pose);
  }
  else
  {
    top_name_viz_pose.push_back("/servo_pose_viz_des");
    top_name_viz_pose.push_back("/servo_pose_viz_des2");
    top_name_viz_pose.push_back("/servo_pose_viz_now");
    top_name_viz_pose.push_back("/servo_pose_viz_now2");

    ROS_FATAL_STREAM("Could not get parameter "<< 
    "/top_name_viz_pose"<<  top_name_viz_pose);
  }
  
  for (unsigned int i = 0; i < top_name_viz_pose.size(); i++)
  {
     pose_viz.push_back(nh.subscribe(top_name_viz_pose.at(i), 
			2, boost::bind( &PoseShow::cb_pose_viz_des_sub, 
					this, _1, i) ) );
  }
 **/
 
 
  pose_viz_des_sub = nh.subscribe("/servo_pose_viz_des", 
				  2, &PoseShow::cb_pose_viz_des_sub, this);
  pose_viz_des2_sub = nh.subscribe("/servo_pose_viz_des2", 
				   2, &PoseShow::cb_pose_viz_des2_sub, this);
  pose_viz_now_sub = nh.subscribe("/servo_pose_viz_now", 
				  2, &PoseShow::cb_pose_viz_now_sub, this);
  pose_viz_now2_sub = nh.subscribe("/servo_pose_viz_now2", 
				   2, &PoseShow::cb_pose_viz_now2_sub, this);
  pose_viz_contact_frame_sub = nh.subscribe("/servo_pose_viz_contact_frame", 
				   2, &PoseShow::cb_pose_viz_contact_frame_sub, this);

  pose_viz_line_follow_next_sub = nh.subscribe("/line_follow_pose_viz_des", 
				   2, &PoseShow::cb_pose_viz_line_follow_next_sub, this);
}
PoseShow::~PoseShow(){}

void PoseShow::cb_pose_viz_now2_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    transform_now2_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_now2_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_FATAL_STREAM("viz now2 pose no");
  }
}
void PoseShow::cb_pose_viz_now_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    transform_now_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_now_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_FATAL_STREAM("viz now pose no");
  }
}
void PoseShow::cb_pose_viz_des2_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    transform_des2_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_des2_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_FATAL_STREAM("viz des2 pose no");
  }
}
void PoseShow::cb_pose_viz_des_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    geometry_msgs::PoseStamped pose_des;
    pose_des = *msg;
    transform_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_FATAL_STREAM("viz des pose no");
  }
}

void PoseShow::cb_pose_viz_contact_frame_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    transform_contact_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_contact_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_FATAL_STREAM("viz contact frame pose no");
  }
}


void PoseShow::cb_pose_viz_line_follow_next_sub(const geometry_msgs::PoseStampedPtr& msg)
{
  if (msg)
  {
    transform_line_follow_.setOrigin( tf::Vector3(msg->pose.position.x, msg->pose.position.y,
			 msg->pose.position.z) );
    
    transform_line_follow_.setRotation( tf::Quaternion (msg->pose.orientation.x, msg->pose.orientation.y,
		  msg->pose.orientation.z, msg->pose.orientation.w) );
}
  else
  {
    ROS_DEBUG_STREAM("viz line follow next pose no");
  }
}

void PoseShow::br()
{
  br_frame_.sendTransform(tf::StampedTransform(transform_, ros::Time::now(), "shadowarm_base", "palm desired"));
}
void PoseShow::br_des2()
{
  br_frame_des2_.sendTransform(tf::StampedTransform(transform_des2_, ros::Time::now(), "shadowarm_base", "palm desired2"));
}
void PoseShow::br_now()
{
  br_frame_now_.sendTransform(tf::StampedTransform(transform_now_, ros::Time::now(), "shadowarm_base", "palm now"));
}
void PoseShow::br_now2()
{
  br_frame_now2_.sendTransform(tf::StampedTransform(transform_now2_, ros::Time::now(), "shadowarm_base", "palm now2"));
}
void PoseShow::br_contact()
{
  br_frame_contact_.sendTransform(tf::StampedTransform(transform_contact_, ros::Time::now(), "weiss", "contact frame"));
}
void PoseShow::br_line_follow()
{
  br_line_follow_next_.sendTransform(tf::StampedTransform(transform_line_follow_, ros::Time::now(), "shadowarm_base", "line_follow_palm_next"));
}


