#include "show_markers/kuka_servo_show_markers.hpp"

int main(int argc, char** argv)
{
    //init the ros node
    ros::init(argc, argv, "show_markers");
    ros::NodeHandle n;
    ROS_INFO("show_markers features in world frame pub rviz started ");

    markersShow markersShow_weiss_features(0);
    PoseShow frameShow;


    ros::Rate loop_rate(10);

    while( ros::ok() ){
//         if (markersShow_weiss_features.contours_num > 0)
// 	{
	  markersShow_weiss_features.rviz();
// 	  frameShow.br();
// 	  
// 	  frameShow.br_des2();
// 	  
// 	  frameShow.br_now();
// 	  
// 	  frameShow.br_contact();
// 	  
// 	  frameShow.br_line_follow();
// 	}
	
        
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}


/*uint8 ARROW=0
uint8 CUBE=1
uint8 SPHERE=2
uint8 CYLINDER=3
uint8 LINE_STRIP=4
uint8 LINE_LIST=5
uint8 CUBE_LIST=6
uint8 SPHERE_LIST=7
uint8 POINTS=8
uint8 TEXT_VIEW_FACING=9
uint8 MESH_RESOURCE=10
uint8 TRIANGLE_LIST=11*/
//            ros::Publisher cop_des_pub;
//            ros::Publisher cop_fb_pub;
//            ros::Publisher forcez_des_pub;
//            ros::Publisher forcez_fb_pub;
//            ros::Publisher coc_des_pub;
//            ros::Publisher coc_fb_pub;
//            ros::Publisher orientz_des_pub;
