#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

#include "tactile_servo_msgs/CalibWeissNano.h"

#include <geometry_msgs/WrenchStamped.h>

#include <geometry_msgs/PoseStamped.h>

#include "tactile_servo_msgs/PlanFeats.h"

#include "tactile_servo_msgs/PlotMatlab.h"

#include <geometry_msgs/PointStamped.h>


#include "tactile_servo_msgs/PlotMatlabfarea.h"

#include <std_msgs/Float64.h>

// for smooth transitions
#include "tactile_servo_msgs/COCtoZMP.h"

class PlotMatlabfpa

{
public:
  PlotMatlabfpa();
  ~PlotMatlabfpa();
  
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  float fx, fy, fz, mx, my, mz;
  
  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg);
  float fx_weiss, mx_weiss, my_weiss, copx_weiss, copy_weiss, orient_weiss, cocx_weiss, cocy_weiss;
  
  ros::Subscriber sub_feats_des;
  void callback_desfeats(const tactile_servo_msgs::ContsFeats& msg);
  float fx_weissd, mx_weissd, my_weissd, copx_weissd, copy_weissd, orient_weissd, cocx_weissd, cocy_weissd;
  
  ros::Subscriber sub_feats_plan;
  void callback_planfeats(const tactile_servo_msgs::PlanFeats& msg);
  int num_contours;
  
  ros::Subscriber pose_now;
  void callback_posenow(const geometry_msgs::PoseStamped& msg);
  float x, y, z, wx, wy, wz, ww;

  ros::Subscriber pose_des;
  void callback_posedes(const geometry_msgs::PoseStamped& msg);
  float xd, yd, zd, wxd, wyd, wzd, wwd;

  ros::Subscriber contacts_hist;
  void callback_contact_hist(const geometry_msgs::PointStamped& msg);
  float xc, yc, zc;
  
  long double t_now_sec, t_begin_sec;
  long double t_now_nsec;
  ros::Duration dt, test_time;
  
  ros::Time t_old_sec_total, test_time_begin;
  long double n_cycle;
  long double test_time2;
  
  bool init_time;

  ros::NodeHandle nh;
  ros::Publisher pub_plot_matlab;
  
  bool is_feat_fb_rec_, is_feat_des_rec_,
  is_feat_plan_rec_, is_ati_rec_, is_pose_now_rec_,
  is_pose_des_rec_, is_cont_hist_rec_, is_farea_rec_;
  
  void send_plot();

  ros::Time the_time;

  ros::Subscriber forcearea_sub;
  void callback_forceparea(const tactile_servo_msgs::COCtoZMP & msg);
  float force_area;
  
  ros::Publisher zend_one_to_avoid_nan;
  bool is_switched;

  
};

PlotMatlabfpa::PlotMatlabfpa()
{
  force = nh.subscribe("/ft_sensor/wrench", 10, &PlotMatlabfpa::callback_force, this);
  
  sub_feats_fb = nh.subscribe("/fb_feats", 10,  &PlotMatlabfpa::callback_fbfeats,this);
  
  sub_feats_des = nh.subscribe("/des_feats", 10,  &PlotMatlabfpa::callback_desfeats,this);
  
  sub_feats_plan = nh.subscribe("/plan_feats", 10,  &PlotMatlabfpa::callback_planfeats,this);
  
  pose_now = nh.subscribe("/pose_current", 10,  &PlotMatlabfpa::callback_posenow,this);
  
  pose_des = nh.subscribe("/pose_desired", 10,  &PlotMatlabfpa::callback_posedes,this);
  
  contacts_hist = nh.subscribe("/contact_points_weiss_plot", 10,  &PlotMatlabfpa::callback_contact_hist,this);
  
  pub_plot_matlab = nh.advertise<tactile_servo_msgs::PlotMatlabfarea>("/plot_matlab", 10);
  
  forcearea_sub = nh.subscribe("/coc_zmp_smooth_trans", 10, &PlotMatlabfpa::callback_forceparea, this );

  zend_one_to_avoid_nan = nh.advertise<tactile_servo_msgs::COCtoZMP>("/coc_zmp_smooth_trans",1);
  
  is_switched = false;
  
  is_feat_fb_rec_ = false;
  is_feat_des_rec_ = false;
  is_feat_plan_rec_ = false;
  is_ati_rec_ = false;
  is_pose_now_rec_ = false;
  is_pose_des_rec_ = false;
  is_cont_hist_rec_ =false;
  is_farea_rec_ =false;
  init_time = false;
  
  n_cycle = 0;
  
  force_area  = 1.0;
}
PlotMatlabfpa::~PlotMatlabfpa()
{}

void PlotMatlabfpa::callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg.control_features.size() == 1)
  {
    fx_weiss = msg.control_features[0].contactForce;
    mx_weiss = msg.control_features[0].zmp_x;
    my_weiss = msg.control_features[0].zmp_y;
    copx_weiss =msg.control_features[0].centerpressure_x;
    copy_weiss=msg.control_features[0].centerpressure_y;
    orient_weiss=msg.control_features[0].contactOrientation;
    cocx_weiss=msg.control_features[0].centerContact_x;
    cocy_weiss=msg.control_features[0].centerContact_y;
    is_feat_fb_rec_ = true;
    
    if (force_area != 0.0)
    {
      tactile_servo_msgs::COCtoZMP tmp;
      tmp.coc_to_zmp_smooth = 1.0;
      tmp.header.frame_id = "weiss";
      tmp.header.stamp = ros::Time::now();
      zend_one_to_avoid_nan.publish(tmp);
    }
    if (is_switched)
    {
      tactile_servo_msgs::COCtoZMP tmp;
      tmp.coc_to_zmp_smooth = 0.0;
      tmp.header.frame_id = "weiss";
      tmp.header.stamp = ros::Time::now();
      zend_one_to_avoid_nan.publish(tmp);
    }
    if (force_area == 0.0)
    {
      is_switched = true;
    }
    
  
  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_DEBUG_STREAM("fb feat is not received");
  }
}

void PlotMatlabfpa::callback_forceparea (const tactile_servo_msgs::COCtoZMP& msg)
{
  force_area = msg.coc_to_zmp_smooth;

 
  is_farea_rec_ = true;
}

void PlotMatlabfpa::callback_desfeats(const tactile_servo_msgs::ContsFeats& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg.control_features.size() == 1)
  {
    fx_weissd = msg.control_features[0].contactForce; 
    mx_weissd = msg.control_features[0].zmp_x;
    my_weissd = msg.control_features[0].zmp_y;
    copx_weissd =msg.control_features[0].centerpressure_x;
    copy_weissd =msg.control_features[0].centerpressure_y;
    orient_weissd =msg.control_features[0].contactOrientation;
    cocx_weissd =msg.control_features[0].centerContact_x;
    cocy_weissd=msg.control_features[0].centerContact_y;
    is_feat_des_rec_ = true;
  }
  else
  {
    ROS_DEBUG_STREAM("fb feat is not received");
    is_feat_des_rec_ = false;
  }
}

void PlotMatlabfpa::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz = msg.wrench.force.z;
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  is_ati_rec_ = true;
}

void PlotMatlabfpa::callback_planfeats(const tactile_servo_msgs::PlanFeats& msg)
{
  num_contours = msg.numContours;
  is_feat_plan_rec_ = true;
}

void PlotMatlabfpa::callback_posenow(const geometry_msgs::PoseStamped& msg)
{
  x = msg.pose.position.x;
  y = msg.pose.position.y;
  z = msg.pose.position.z;
  wx = msg.pose.orientation.x;
  wy = msg.pose.orientation.y;
  wz = msg.pose.orientation.z;
  ww = msg.pose.orientation.w;
  is_pose_now_rec_ = true;
}

void PlotMatlabfpa::callback_posedes(const geometry_msgs::PoseStamped& msg)
{
  
  
    
  xd = msg.pose.position.x;
  yd = msg.pose.position.y;
  zd = msg.pose.position.z;
  wxd = msg.pose.orientation.x;
  wyd = msg.pose.orientation.y;
  wzd = msg.pose.orientation.z;
  wwd = msg.pose.orientation.w;
  is_pose_des_rec_ = true;
}

void PlotMatlabfpa::callback_contact_hist(const geometry_msgs::PointStamped& msg)
{
  xc = msg.point.x;
  yc = msg.point.y;
  zc = msg.point.z;
  is_cont_hist_rec_ = true;
}


void PlotMatlabfpa::send_plot()
{
   if ( is_feat_fb_rec_ && is_feat_des_rec_ )
  {
    tactile_servo_msgs::PlotMatlabfarea plot_matl;
    plot_matl.header.frame_id = "weiss_plot_matl_fpresarea";
    plot_matl.header.stamp = ros::Time::now();
    
    plot_matl.fx = fx;
    plot_matl.fy = fy;
    plot_matl.fz = fz;
    plot_matl.fwx = mx;
    plot_matl.fwy = my;
    plot_matl.fwz = mz;
    
    plot_matl.coc_x = cocx_weiss;
    plot_matl.coc_y = cocy_weiss;
    plot_matl.f = fx_weiss;
    plot_matl.zmp_x = mx_weiss;
    plot_matl.zmp_y = my_weiss;
    plot_matl.orient_z = orient_weiss;
    plot_matl.cop_x = copx_weiss;
    plot_matl.cop_y = copy_weiss;
    
    plot_matl.coc_xd = cocx_weissd;
    plot_matl.coc_yd = cocy_weissd;
    plot_matl.fd = fx_weissd;
    plot_matl.zmp_xd = mx_weissd;
    plot_matl.zmp_yd = my_weissd;
    plot_matl.orient_zd = orient_weissd;
    plot_matl.cop_xd = copx_weissd;
    plot_matl.cop_yd = copy_weissd;
    
    plot_matl.num_contours = num_contours;
    
    plot_matl.x_now = x;
    plot_matl.y_now = y;
    plot_matl.z_now = z;
    plot_matl.wx_now = wx;
    plot_matl.wy_now = wy;
    plot_matl.wz_now = wz;
    plot_matl.ww_now = ww;
    
    plot_matl.x_des = xd;
    plot_matl.y_des = yd;
    plot_matl.z_des = zd;
    plot_matl.wx_des = wxd;
    plot_matl.wy_des = wyd;
    plot_matl.wz_des = wzd;
    plot_matl.ww_des = wwd;
    
    plot_matl.x_c = xc;
    plot_matl.y_c = yc;
    plot_matl.z_c = zc;
    
    plot_matl.farea = force_area;
    if (!init_time)
    {
      test_time_begin = ros::Time::now();
    }
    // correct time
    test_time = ros::Time::now() - test_time_begin;
    
    if(init_time)
    {
      dt = ros::Time::now() - t_old_sec_total;
    }
    
    t_old_sec_total = ros::Time::now();
    
    if(init_time)
    {
      n_cycle = n_cycle + 1;
      plot_matl.dt = dt.toSec();
      test_time2 = dt.toSec() * n_cycle;
      // not correct time as the dt is not stable
      plot_matl.test_time2 = test_time2;
    }
    
    init_time = true;
    
    plot_matl.time = test_time.toSec();
    
    ROS_DEBUG_STREAM("all_recieved");
    pub_plot_matlab.publish(plot_matl);
    is_feat_des_rec_ = false;
    is_feat_fb_rec_ = false;
    is_pose_now_rec_ = false;
    is_pose_des_rec_ = false;
    is_farea_rec_ = false;
  }
  
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "plot_matlab_fpresarea");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    ROS_INFO("plot_matlab");
    PlotMatlabfpa plot_matlab;
    while( ros::ok() )
    {
      plot_matlab.send_plot();
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


