#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

#include "tactile_servo_msgs/CalibWeissNano.h"

#include <geometry_msgs/WrenchStamped.h>

#include <geometry_msgs/PoseStamped.h>

#include "tactile_servo_msgs/PlanFeats.h"

#include "tactile_servo_msgs/PlotMatlabImg.h"

#include <geometry_msgs/PointStamped.h>

#include <sensor_msgs/Image.h>
#include <opencv2/core/core.hpp>
#include <image_transport/image_transport.h> 
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
class PlotMatlab

{
public:
  PlotMatlab();
  ~PlotMatlab();
  
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  float fx, fy, fz, mx, my, mz;
  
  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg);
  float fx_weiss, mx_weiss, my_weiss, copx_weiss, copy_weiss, orient_weiss, cocx_weiss, cocy_weiss;
  
  ros::Subscriber sub_feats_des;
  void callback_desfeats(const tactile_servo_msgs::ContsFeats& msg);
  float fx_weissd, mx_weissd, my_weissd, copx_weissd, copy_weissd, orient_weissd, cocx_weissd, cocy_weissd;
  
  ros::Subscriber sub_feats_plan;
  void callback_planfeats(const tactile_servo_msgs::PlanFeats& msg);
  int num_contours;
  
  ros::Subscriber pose_now;
  void callback_posenow(const geometry_msgs::PoseStamped& msg);
  float x, y, z, wx, wy, wz, ww;

  ros::Subscriber pose_des;
  void callback_posedes(const geometry_msgs::PoseStamped& msg);
  float xd, yd, zd, wxd, wyd, wzd, wwd;

  ros::Subscriber contacts_hist;
  void callback_contact_hist(const geometry_msgs::PointStamped& msg);
  float xc, yc, zc;
  
  long double t_now_sec, t_begin_sec;
  long double t_now_nsec;
  ros::Duration dt, test_time;
  
  ros::Time t_old_sec_total, test_time_begin;
  long double n_cycle;
  long double test_time2;
  
  bool init_time;

  ros::NodeHandle nh;
  ros::Publisher pub_plot_matlab;
  
  bool is_feat_fb_rec_, is_feat_des_rec_,
  is_feat_plan_rec_, is_ati_rec_, is_pose_now_rec_,
  is_pose_des_rec_, is_cont_hist_rec_;
  
  void send_plot();

  ros::Time the_time;
  
  image_transport::ImageTransport it_viz;
  image_transport::Subscriber  img_now;
  void callback_img_now(const sensor_msgs::ImageConstPtr& msg);
  std::vector<float> img;
bool is_img_rec;
bool is_img_sent;
};

PlotMatlab::PlotMatlab():it_viz(nh)
{
  force = nh.subscribe("/ft_sensor/wrench", 10, &PlotMatlab::callback_force, this);
  
  sub_feats_fb = nh.subscribe("/fb_feats", 10,  &PlotMatlab::callback_fbfeats,this);
  
  sub_feats_des = nh.subscribe("/des_feats", 10,  &PlotMatlab::callback_desfeats,this);
  
  sub_feats_plan = nh.subscribe("/plan_feats", 10,  &PlotMatlab::callback_planfeats,this);
  
  pose_now = nh.subscribe("/pose_current", 10,  &PlotMatlab::callback_posenow,this);
  
  pose_des = nh.subscribe("/pose_desired", 10,  &PlotMatlab::callback_posedes,this);
  
  contacts_hist = nh.subscribe("/contact_points_weiss_plot", 10,  &PlotMatlab::callback_contact_hist,this);
  
  pub_plot_matlab = nh.advertise<tactile_servo_msgs::PlotMatlabImg>("/plot_matlab", 10);
  
  is_feat_fb_rec_ = false;
  is_feat_des_rec_ = false;
  is_feat_plan_rec_ = false;
  is_ati_rec_ = false;
  is_pose_now_rec_ = false;
  is_pose_des_rec_ = false;
  is_cont_hist_rec_ =false;
  init_time = false;
  
  n_cycle = 0;
  
  img_now = it_viz.subscribe("/ros_tactile_image", 1, &PlotMatlab::callback_img_now, this);
  is_img_rec = 0;
  is_img_sent = 0;
}
PlotMatlab::~PlotMatlab()
{}

void PlotMatlab::callback_img_now(const sensor_msgs::ImageConstPtr& msg)
{
  int rows_ = 14;
  int cols_ = 6;
  cv_bridge::CvImagePtr cv_ptr;
  try
  {
    cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_8UC1);//TYPE_8UC1
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  
  if (is_img_sent == 1)
  {  
  int c = 0;
  for(int i=0; i<rows_; ++i) {
    for(int j=0; j<cols_; j++) {
      // make it visible
      img.push_back(cv_ptr->image.data[c] * 3);
      c = c + 1;
    }
  }
  is_img_rec = 1;
  is_img_sent = 0;
  }
}
  
  
  
void PlotMatlab::callback_fbfeats(const tactile_servo_msgs::ContsFeats& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg.control_features.size() == 1)
  {
    fx_weiss = msg.control_features[0].contactForce;
    mx_weiss = msg.control_features[0].zmp_x;
    my_weiss = msg.control_features[0].zmp_y;
    copx_weiss =msg.control_features[0].centerpressure_x;
    copy_weiss=msg.control_features[0].centerpressure_y;
    orient_weiss=msg.control_features[0].contactOrientation;
    cocx_weiss=msg.control_features[0].centerContact_x;
    cocy_weiss=msg.control_features[0].centerContact_y;
    is_feat_fb_rec_ = true;
  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_DEBUG_STREAM("fb feat is not received");
  }
}

void PlotMatlab::callback_desfeats(const tactile_servo_msgs::ContsFeats& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg.control_features.size() == 1)
  {
    fx_weissd = msg.control_features[0].contactForce; 
    mx_weissd = msg.control_features[0].zmp_x;
    my_weissd = msg.control_features[0].zmp_y;
    copx_weissd =msg.control_features[0].centerpressure_x;
    copy_weissd =msg.control_features[0].centerpressure_y;
    orient_weissd =msg.control_features[0].contactOrientation;
    cocx_weissd =msg.control_features[0].centerContact_x;
    cocy_weissd=msg.control_features[0].centerContact_y;
    is_feat_des_rec_ = true;
  }
  else
  {
    ROS_DEBUG_STREAM("fb feat is not received");
    is_feat_des_rec_ = false;
  }
}

void PlotMatlab::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz = msg.wrench.force.z;
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  is_ati_rec_ = true;
}

void PlotMatlab::callback_planfeats(const tactile_servo_msgs::PlanFeats& msg)
{
  num_contours = msg.numContours;
  is_feat_plan_rec_ = true;
}

void PlotMatlab::callback_posenow(const geometry_msgs::PoseStamped& msg)
{
  x = msg.pose.position.x;
  y = msg.pose.position.y;
  z = msg.pose.position.z;
  wx = msg.pose.orientation.x;
  wy = msg.pose.orientation.y;
  wz = msg.pose.orientation.z;
  ww = msg.pose.orientation.w;
  is_pose_now_rec_ = true;
}

void PlotMatlab::callback_posedes(const geometry_msgs::PoseStamped& msg)
{
  
  
    
  xd = msg.pose.position.x;
  yd = msg.pose.position.y;
  zd = msg.pose.position.z;
  wxd = msg.pose.orientation.x;
  wyd = msg.pose.orientation.y;
  wzd = msg.pose.orientation.z;
  wwd = msg.pose.orientation.w;
  is_pose_des_rec_ = true;
}

void PlotMatlab::callback_contact_hist(const geometry_msgs::PointStamped& msg)
{
  xc = msg.point.x;
  yc = msg.point.y;
  zc = msg.point.z;
  is_cont_hist_rec_ = true;
}


void PlotMatlab::send_plot()
{
   if ( is_feat_fb_rec_ && is_feat_des_rec_ && is_pose_des_rec_ )
  {
    tactile_servo_msgs::PlotMatlabImg plot_matl;
    plot_matl.header.frame_id = "weiss_plot_matl";
    plot_matl.header.stamp = ros::Time::now();
    
    plot_matl.fx = fx;
    plot_matl.fy = fy;
    plot_matl.fz = fz;
    plot_matl.fwx = mx;
    plot_matl.fwy = my;
    plot_matl.fwz = mz;
    
    plot_matl.coc_x = cocx_weiss;
    plot_matl.coc_y = cocy_weiss;
    plot_matl.f = fx_weiss;
    plot_matl.zmp_x = mx_weiss;
    plot_matl.zmp_y = my_weiss;
    plot_matl.orient_z = orient_weiss;
    plot_matl.cop_x = copx_weiss;
    plot_matl.cop_y = copy_weiss;
    
    plot_matl.coc_xd = cocx_weissd;
    plot_matl.coc_yd = cocy_weissd;
    plot_matl.fd = fx_weissd;
    plot_matl.zmp_xd = mx_weissd;
    plot_matl.zmp_yd = my_weissd;
    plot_matl.orient_zd = orient_weissd;
    plot_matl.cop_xd = copx_weissd;
    plot_matl.cop_yd = copy_weissd;
    
    plot_matl.num_contours = num_contours;
    
    plot_matl.x_now = x;
    plot_matl.y_now = y;
    plot_matl.z_now = z;
    plot_matl.wx_now = wx;
    plot_matl.wy_now = wy;
    plot_matl.wz_now = wz;
    plot_matl.ww_now = ww;
    
    plot_matl.x_des = xd;
    plot_matl.y_des = yd;
    plot_matl.z_des = zd;
    plot_matl.wx_des = wxd;
    plot_matl.wy_des = wyd;
    plot_matl.wz_des = wzd;
    plot_matl.ww_des = wwd;
    
    plot_matl.x_c = xc;
    plot_matl.y_c = yc;
    plot_matl.z_c = zc;
    
    if (is_img_rec)
    {
      for (int ind = 0; ind < img.size(); ind++)
      {
	plot_matl.img.push_back(img.at(ind));
      }
      is_img_rec = 0;
      img.clear();
    }
    
    if (!init_time)
    {
      test_time_begin = ros::Time::now();
    }
    // correct time
    test_time = ros::Time::now() - test_time_begin;
    
    if(init_time)
    {
      dt = ros::Time::now() - t_old_sec_total;
    }
    
    t_old_sec_total = ros::Time::now();
    
    if(init_time)
    {
      n_cycle = n_cycle + 1;
      plot_matl.dt = dt.toSec();
      test_time2 = dt.toSec() * n_cycle;
      // not correct time as the dt is not stable
      plot_matl.test_time2 = test_time2;
    }
    
    init_time = true;
    
    plot_matl.time = test_time.toSec();
    
    ROS_DEBUG_STREAM("all_recieved");
    pub_plot_matlab.publish(plot_matl);
    is_feat_des_rec_ = false;
    is_feat_fb_rec_ = false;
    is_pose_now_rec_ = false;
    is_pose_des_rec_ = false;
    
    img.clear();
    plot_matl.img.clear();
    is_img_sent = 1;
  }
  
}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "plot_matlab_image");
    ros::NodeHandle n;
    ros::Rate loop_rate(100);
    ROS_INFO("plot_matlab_image");
    PlotMatlab plot_matlab;
    while( ros::ok() )
    {
      plot_matlab.send_plot();
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


