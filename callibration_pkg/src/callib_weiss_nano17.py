#!/usr/bin/env python

import serial, time, sys
import rospy
import message_filters
from message_filters import TimeSynchronizer, Subscriber
from message_filters import TimeSynchronizer, Subscriber
from tactile_servo_msgs.msg import ContsFeats 
from tactile_servo_msgs.msg import OneContFeats 

from std_msgs.msg import String # type of massage in ROS
from std_msgs.msg import Header # type of massage in ROS

from geometry_msgs.msg import WrenchStamped

from tactile_servo_msgs.msg import CalibWeissNano # type of massage in ROS


def callback(weiss, nano):
  assert weiss.header.stamp == nano.header.stamp
  #Fill message data in ros image format using std_msgs
  #print "got weiss qnd nano Info"
  
  msg.header.frame_id = "Sensor_calibration" #frame where is located the sensor
  print("header=", msg.header.frame_id)
  now = rospy.get_rostime()   #get ros time
  msg.header.stamp = now # time
  print("fb_feats_size=", len(weiss.control_features))
  if ( len(weiss.control_features) > 0):
    msg.contactForce=weiss.control_features[0].contactForce
    msg.zmp_x=weiss.control_features[0].zmp_x
    msg.zmp_y=weiss.control_features[0].zmp_y
    msg.nano=nano.wrench
    pub.publish(msg)

def pub_sensor_calibration():
    global msg
    msg = CalibWeissNano()
    global pub
    rospy.init_node('calib_weiss', anonymous=True)
    rospy.loginfo("Calibration publishing.")

    pub = rospy.Publisher('/calib_weiss', CalibWeissNano, queue_size=1)
    weiss_sub = message_filters.Subscriber('/fb_feats', ContsFeats)
    nano_sub = message_filters.Subscriber('/nano17ft/finger1', WrenchStamped)
	
    ts = message_filters.ApproximateTimeSynchronizer([weiss_sub, nano_sub], 1,0.1)#the last parameter defines the delay (in seconds) with which messages can be synchronized
    ts.registerCallback(callback)
    
    #tss =  message_filters.TimeSynchronizer( message_filters.Subscriber('/fb_feats', ContsFeats),  message_filters.Subscriber('/nano17ft/finger1', WrenchStamped), 1)
    #tss.registerCallback(callback)
    
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
      rospy.loginfo("spinning")
      rospy.spin()
    rospy.loginfo("spinning2")


if __name__ == '__main__':
    try:
        pub_sensor_calibration()
    except rospy.ROSInterruptException: pass
    rospy.loginfo("Calibration publishing terminated.")


# ser.open()

# ser.isOpen()
