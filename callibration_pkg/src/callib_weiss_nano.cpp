#include <ros/ros.h>
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
#include "tactile_servo_msgs/CalibWeissNano.h"
#include <geometry_msgs/WrenchStamped.h>

class CalibWeisNano

{
public:
  CalibWeisNano();
  ~CalibWeisNano();
  ros::Subscriber force;
  void callback_force(const geometry_msgs::WrenchStamped& msg);
  ros::Subscriber sub_feats_fb;
  void callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg);
  ros::NodeHandle nh;
  ros::Publisher pub_calib;
  float fx, fy, fz, mx, my, mz;
  float fx_weiss, mx_weiss, my_weiss;
    float copx_weiss, copy_weiss, orient_weiss;
float cocx_weiss, cocy_weiss;
  bool is_feat_fb_rec_;
};

CalibWeisNano::CalibWeisNano()
{
  force = nh.subscribe("/nano17ft/finger1", 2, &CalibWeisNano::callback_force, this);
  sub_feats_fb = nh.subscribe("/fb_feats", 2,  &CalibWeisNano::callback_fbfeats,this);
  
  pub_calib = nh.advertise<tactile_servo_msgs::CalibWeissNano>("/callb_weiss_nano", 2);
  is_feat_fb_rec_ = false;
}
CalibWeisNano::~CalibWeisNano()
{}

void CalibWeisNano::callback_fbfeats(const tactile_servo_msgs::ContsFeatsConstPtr& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
  if (msg->control_features.size() == 1)
  {
    fx_weiss = std::abs(  msg->control_features[0].contactForce );
    mx_weiss = msg->control_features[0].zmp_x;
    my_weiss = msg->control_features[0].zmp_y;
    copx_weiss =msg->control_features[0].centerpressure_x;
    copy_weiss=msg->control_features[0].centerpressure_y;
    orient_weiss=msg->control_features[0].contactOrientation;
    cocx_weiss=msg->control_features[0].centerContact_x;
    cocy_weiss=msg->control_features[0].centerContact_y;
    is_feat_fb_rec_ = true;
  }
  else
  {
    is_feat_fb_rec_ = false;
    ROS_DEBUG_STREAM("fb feat is not received");
  }
}

void CalibWeisNano::callback_force(const geometry_msgs::WrenchStamped& msg)
{
//  ROS_FATAL_STREAM("fb feat size = " << msg->control_features.size());
 
  fx = msg.wrench.force.x;
  fy = msg.wrench.force.y;
  fz =  std::abs( msg.wrench.force.z );
  mx = msg.wrench.torque.x;
  my = msg.wrench.torque.y;
  mz = msg.wrench.torque.z;
  
  if ( is_feat_fb_rec_ == true)
  {
    tactile_servo_msgs::CalibWeissNano calib;
    calib.header.frame_id = "weiss_callib";
    calib.header.stamp = ros::Time::now();
    calib.nano.force.x = fx;
    calib.nano.force.y = fy;
    calib.nano.force.z = fz;
    calib.nano.torque.x = mx;
    calib.nano.torque.y = my;
    calib.contactForce = fx_weiss;
    calib.zmp_x = mx_weiss;
    calib.zmp_y = my_weiss;
    calib.cop_x = copx_weiss;
    calib.cop_y = copy_weiss;
    calib.coc_x = cocx_weiss;
    calib.coc_y = cocy_weiss;
    calib.orient_z = orient_weiss;
    ROS_DEBUG_STREAM("fb_feats_recieved");
    pub_calib.publish(calib);
  }

}
int main(int argc, char** argv)
{
    ros::init(argc, argv, "weiss_calib_cpp");
    ros::NodeHandle n;
    ros::Rate loop_rate(25);
    ROS_INFO("weiss_calib_cpp");
    CalibWeisNano weiss_callib;
    while( ros::ok() )
    {
      ros::spinOnce();
      loop_rate.sleep();
    }
    return 0;
}


