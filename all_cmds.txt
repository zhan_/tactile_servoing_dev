load param for servoing

real sensor on handpc sudo chmod 666 /dev/ttyACM0

roslaunch tactile_servo_config all_params.launch 
  <rosparam file="$(find tactile_servo_config)/arm_hand/init_arm_pose_one.yaml" command="load"/>
  <rosparam file="$(find tactile_servo_config)/pid/servo_pid_palm.yaml" command="load"/>
  <rosparam file="$(find tactile_servo_config)/select_matrix/selec_features_matrix.yaml" command="load"/>
  <rosparam file="$(find tactile_servo_config)/sensors/tactile_arrays_conf.yaml" command="load"/>


real sensor
roslaunch real_tactile_image write_sensor_params_real_image.launch
200 hz

img_proc
rosrun tactile_image tactile_image_processing_node
100 hz (be carefull should be lower than ros tactile image 
but can be lower, usually 150 Hz)

time filtering of features /fb_feats_avg
rosrun tactile_image time_filter_feats_node
125 hz, but we take an average of 5 samples (10) defined in params


des_feats
rosrun servo_planner des_feats_cmd
200 hz

vis_feats
rosrun tactile_image vis_feats_tactile_image_node
50 hz

servo_control to run in cartesian space
roslaunch tact_contr_clean tact_contr_clean_node.launch
50 hz
<launch>
loads the pid params
	<group ns="/servo_pid/">
		<node pkg="tact_contr_clean" type="tact_contr_clean_node" name="tact_contr_clean" output="screen"> 
		</node>
	</group>
</launch>


to control in joint space the wrj1 wrj2 and elbow rotate
roslaunch tact_contr_clean tact_contr_clean_node_wrj2_control_gazebo.launch
roslaunch tact_contr_clean tact_contr_clean_node_wrj2_control_real_hand.launch

select_what to servo
rosrun tact_contr_clean select_servo_client 0 0 0 0 0 1
select configuration due to singularity
rosservice call /select_configuration "choose_configuration: 0"
begin controller
rosrun tact_contr_clean srv_client_servo_pose_start_node
    call /begin_tactile_servo_real "begin_control: 1" 

===============================================================
line follow
rosrun tact_contr_clean line_follow_node
rosservice call /begin_tactile_servo_real "begin_control: 1"
to test it run:

roslaunch tact_contr_clean tact_contr_clean_node.launch
rosrun show_markers show_markers_node
rosrun contact_points contact_points_node
rosrun callibration_pkg callib_weiss_nano_node
rosrun tact_contr_clean line_follow_node
===============================================================


show markers
rosrun show_markers show_markers_node

reconfigure dynamic
rosrun rqt_reconfigure rqt_reconfigure

callibration weiss force topic
rosrun callibration_pkg callib_weiss_nano_node





=========== fake =============================================
if to test fake feats
rosrun cv_features_extractor publish_fake_feats_to_test_jacob

plan_feats
rosrun servo_planner plan_feats_cmd
200 hz
===============================================================

================== init pose =========================================
load joint vals of init, run node to move arm to init pose $init_experiment pose_arm_init
roslaunch init_experiment load_params_init_pose.launch
<launch>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/arm_joint_names_initialization.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/hand_joint_names_initialization.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/joint_vals_arm_init.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/name_controller_types_simulation.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/joint_vals_arm_init2.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/joint_vals_hand_init.yaml" command="load"/>
    	<rosparam file="$(find tactile_servo_config)/arm_hand/object_to_spawn.yaml" command="load"/>
	<node pkg="init_experiment" type ="pose_arm_init" name = "pose_arm_init" output="screen">  </node>
</launch>



calls srv of init pose arm
rosrun init_experiment init_pose_client2

calls srv of init hand
rosrun init_experiment init_pose_fingers_client
============================================================================

==========================spawn object=============================
<rosparam file="$(find tactile_servo_config)/arm_hand/object_to_spawn.yaml" command="load"/>
[cylinder_servo, sphere, bar, corner, corner_2]
press 5 to delete
rosrun init_experiment spawn_object_now 

=====================================================================


====================aux===================================
to start controller
rosservice call /start_servo_controller_service_cart \"begin_controller: true\"
======================================================

0.00055556 stiffness
