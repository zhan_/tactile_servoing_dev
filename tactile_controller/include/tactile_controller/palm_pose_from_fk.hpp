#include <ros/ros.h>

// JA_now - get joint angles using
/// subscriber /joint_state
#include <control_msgs/JointControllerState.h> // for the real hand <sr_robot_msgs/
#include "sensor_msgs/JointState.h"

// POSE_cur - FK - get FK using
/// services arm_kinematicso1
#include <moveit_msgs/GetKinematicSolverInfo.h>
#include <moveit_msgs/GetPositionFK.h>

// to send commands and read joint states
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>



class CartTactCOntr
{
public:
    CartTactCOntr();
    ~CartTactCOntr();
    ros::NodeHandle nh_;
    // JA_now - get joint angles
    // one callback for many subscribers
    std::string controller_type_;
    std::vector<std::string> arm_hand_joints_names;
    bool is_joint_vals_rec;
    std::vector<bool> is_rec_jnt_val;
    std::vector<double> joint_reads;
    std::vector<ros::Subscriber> arm_hand_joint_vals_sub;
    void cb_joint_state(const control_msgs::JointControllerStateConstPtr &msg_, int id_);
    // POSE_cur - FK - get FK using
    ros::ServiceClient fk_client;
    ros::ServiceClient query_client_fk;
    void getFK();
    moveit_msgs::GetPositionFK::Request gpfk_req;
    moveit_msgs::GetPositionFK::Response gpfk_res;
    moveit_msgs::GetKinematicSolverInfo::Request request;
    moveit_msgs::GetKinematicSolverInfo::Response response;
    std::string frame_names_FK[6];
    geometry_msgs::PoseStamped X_now, X_now_check_RPY;

    ros::Publisher pose_now_marker_pub;
    
    ros::Publisher pose_now_pub;
    ros::Publisher pose_des_pub;

private:
  ros::NodeHandle nh_private;
  std::string joint_states_name;
  
  void callback(sensor_msgs::JointState msg)
  
  msg.position[6]

};


