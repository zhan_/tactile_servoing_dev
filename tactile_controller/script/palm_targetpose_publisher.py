#!/usr/bin/env python
import roslib;roslib.load_manifest('kinematics_tools')
import rospy
import math
import tf
from geometry_msgs.msg import Vector3,Quaternion,Pose,PoseStamped

if __name__ == '__main__':
  args = rospy.myargv()
  if len(args) > 1:
    tf_prefix="/"+args[1]
    node_name=args[1]
  else:
    tf_prefix=""
    node_name=""
  rospy.init_node('tf_palm_publisher'+node_name)
  listener = tf.TransformListener()
  tip_pos_pub=rospy.Publisher('/move_arm_6D/pose/', PoseStamped) 
  rate = rospy.Rate(10.0)
  while not rospy.is_shutdown():
    try:
      listener.waitForTransform('/shadowarm_base', '/target_palm',rospy.Time(),rospy.Duration(4.0))
      try:
        (trans,rot) = listener.lookupTransform('/shadowarm_base', '/target_palm', rospy.Time(0))
      except (tf.LookupException, tf.ConnectivityException):
        print "could not transform from base to palm"
        continue 
        
    except (tf.Exception):
      print "No transform from base to palm"
      continue 
    
    myposestamped = PoseStamped()
    mypose = Pose(Vector3(trans[0],trans[1],trans[2]),Quaternion(rot[0],rot[1],rot[2],rot[3]))
    myposestamped.pose=mypose
    myposestamped.header.frame_id="/shadowarm_base"
    myposestamped.header.stamp=rospy.Time.now()
    tip_pos_pub.publish(myposestamped)

    rate.sleep()

