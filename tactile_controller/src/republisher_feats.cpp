#include <ros/ros.h>

#include <std_msgs/Float64.h> 

// FEATS_sensor - get features using
/// subscribers /des_feats and /fb_feats
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"
class CartTactCOntr
{
public:
    CartTactCOntr();
    ~CartTactCOntr();
    ros::NodeHandle nh_;
/////////////////////////////////////////////////////////////////////////////////
////////////////Subscribers Features/////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////     
    // FEATS_sensor - get features using
    ros::Subscriber des_feats_sub;
    ros::Subscriber fb_feats_sub;
    void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    float   copx_fb,
            copy_fb,
            force_fb,
            cocx_fb,
            cocy_fb,
            orientz_fb;
    float   copx_des,
            copy_des,
            force_des,
            cocx_des,
            cocy_des,
            orientz_des;
	    ros::Publisher fb_feats_pub[6];
	    ros::Publisher des_feats_pub[6];
    void pub_res();
 

};
CartTactCOntr::CartTactCOntr(){
  
/////////////////////////////////////////////////////////////////////////////////
////////////////Subscribers Feats/////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////// 
    des_feats_sub = nh_.subscribe("/des_feats", 1,  &CartTactCOntr::cb_des_feats_sub,this);
    fb_feats_sub = nh_.subscribe("/fb_feats", 1,  &CartTactCOntr::cb_fb_feats_sub,this);

/////////////////////////////////////////////////////////////////////////////////
////////////////Publish /////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////     
   	    ros::Publisher fb_feats_pub[6];

    wrj1_pub = nh_.advertise<std_msgs::Float64>("/sh_wrj1_mixed_position_velocity_controller/command", 1);
    wrj2_pub = nh_.advertise<std_msgs::Float64>("/sh_wrj2_mixed_position_velocity_controller/command", 1);

    // dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor
    // eigen
    J <<    1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0,
            0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 1;
    J_state_art <<  1, 0, 0, 0, 0, 0,
                    0, 1, 0, 0, 0, 0,
                    0, 0, 1, 0, 0, 0,
                    0, 1, 0, 0, 0, 0,
                    1, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 1;
    S_Matr_force << 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0;
    S_Matr_force_copx_copy << 1, 0, 0, 0, 0, 0,
                            0, 1, 0, 0, 0, 0,
                            0, 0, 1, 0, 0, 0,
                            0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0;
    S_Matr_orient <<   0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 1;
    S_Matr_orient_coc <<   0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0,
                        0, 0, 0, 1, 0, 0,
                        0, 0, 0, 0, 1, 0,
                        0, 0, 0, 0, 0, 1;
    S_Matr_all <<       1, 0, 0, 0, 0, 0,
                        0, 1, 0, 0, 0, 0,
                        0, 0, 1, 0, 0, 0,
                        0, 0, 0, 1, 0, 0,
                        0, 0, 0, 0, 1, 0,
                        0, 0, 0, 0, 0, 1;
    //  the gains in cart space
    Kp_(0) = 1.0;  Kd_(0) = 0.0;  Ki_(0) = 0.0;    // Translation x
    Kp_(1) = 1.0;  Kd_(1) = 0.0;  Ki_(1) = 0.0;    // Translation y
    Kp_(2) = 0.0005;  Kd_(2) = 0.0;  Ki_(2) = 0.0;    // Translation z
    Kp_(3) = 1.0;  Kd_(3) = 0.0;  Ki_(3) = 0.0;    // Rotation x
    Kp_(4) = 1.0;  Kd_(4) = 0.0;  Ki_(4) = 0.0;    // Rotation y
    Kp_(5) = 1.0;  Kd_(5) = 0.0;  Ki_(5) = 0.0;    // Rotation z
    dt_vector(0) = 0.0;
    dt_vector(1) = 0.0;
    dt_vector(2) = 0.0;
    dt_vector(3) = 0.0;
    dt_vector(4) = 0.0;
    dt_vector(5) = 0.0;

    service = nh_.advertiseService("start_servo_controller_service_cart", &CartTactCOntr::start_servo_control, this);
/////////////////////////////////////////////////////////////////////////////////
////////////////PID set/////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////  
    begin_servo_controller = false;
    is_feature_rec = false;
    // set pid features
    f = boost::bind(&CartTactCOntr::cb_dynamic_reconf, this, _1, _2);
    set_pid_feats_server.setCallback(f);
    Pcpx = 1; Icpx = 0; Dcpx = 0; Deadcpx = 0;
    pid_integral_old = 0;
    pid_err_old = 0;
    feats_err_I << 0,0,0,0,0,0;
}
CartTactCOntr::~CartTactCOntr(){}

void CartTactCOntr::cb_joint_state_wrj1(const  sr_robot_msgs::JointControllerStateConstPtr& msg_){

       wj1_joint_val_now  = msg_->process_value;
//    std::cout<<" joint val now = "<< wj1_joint_val_now <<std::endl;
   is_joint_val_rec_wrj1 = true;

   return;

}
void CartTactCOntr::cb_joint_state_wrj2(const  sr_robot_msgs::JointControllerStateConstPtr& msg_){

       wj2_joint_val_now  = msg_->process_value;
//    std::cout<<" joint val now = "<< wj2_joint_val_now <<std::endl;
   is_joint_val_rec_wrj2 = true;

   return;

}
/////////////////////////////////////////////////////////////////////////////////
////////////////Deasired&Fb Feats/////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////  
// FEATS_sensor - get features using
void CartTactCOntr::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_des = msg_->control_features[0].centerpressure_x;
        copy_des = msg_->control_features[0].centerpressure_y;
        force_des = msg_->control_features[0].contactForce;
        cocx_des = msg_->control_features[0].centerContact_x;
        cocy_des = msg_->control_features[0].centerContact_y;
        orientz_des = msg_->control_features[0].contactOrientation;
    }
}

void CartTactCOntr::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_fb = msg_->control_features[0].centerpressure_x;
        copy_fb = msg_->control_features[0].centerpressure_y;
        force_fb = msg_->control_features[0].contactForce;
        cocx_fb = msg_->control_features[0].centerContact_x;
        cocy_fb = msg_->control_features[0].centerContact_y;
        orientz_fb = msg_->control_features[0].contactOrientation;
        is_feature_rec = true;
    }
}
/////////////////////////////////////////////////////////////////////////////////
////////////////Transformations/////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////// 
// dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor
// eigen library
void CartTactCOntr::matrix_operations(){
    // Servo loop time step
    // Calculate the dt between servo cycles.
    dt = ros::Time::now().toSec() - last_time_;
    last_time_ = ros::Time::now().toSec();
    dt_vector(0) = dt;
    dt_vector(1) = dt;
    dt_vector(2) = dt;
    dt_vector(3) = dt;
    dt_vector(4) = dt;
    dt_vector(5) = dt;


    // Get the current features vector
    feats_des(0) = copx_des;
    feats_des(1) = copy_des;
    feats_des(2) = force_des;
    feats_des(3) = cocx_des-copx_des;
    feats_des(4) = cocy_des-copy_des;
    feats_des(5) = orientz_des;
//     std::cout<<"feats_des(3)="<<feats_des(3)<<std::endl;
//     std::cout<<"feats_des(4)="<<feats_des(4)<<std::endl;


    // Get the fb features vector
    feats_fb(0) = copx_fb;
    feats_fb(1) = copy_fb;
    feats_fb(2) = force_fb;
    feats_fb(3) = cocx_fb-copx_fb;
    feats_fb(4) = cocy_fb-copy_fb;
    feats_fb(5) = orientz_fb;
//     std::cout<<"feats_fb(3)="<<feats_fb(3)<<std::endl;

    feats_err_P = feats_des - feats_fb;
    // Calculate proportional contribution to command
    feats_err_KP = Kp_.asDiagonal()*feats_err_P;

    // Calculate the integral of the position error
    feats_err_I = feats_err_I + dt*feats_err_P;
    // Calculate integral contribution to command
    feats_err_KI = Ki_.asDiagonal() * feats_err_I;

    // TODO Limit i_term so that the limit is meaningful in the output
    //    i_term = std::max( gains.i_min_, std::min( i_term, gains.i_max_) );
    //    if (feats_err_KI.array()>i_max.Array()){
    //    }
    // Calculate the derivative error
   if (dt > 0.0)
   {
       feats_err_D = (feats_err_P - feats_err_P_last) / dt;
       feats_err_P_last = feats_err_P;
   }
   // Calculate derivative contribution to command
   feats_err_KD = Kd_.asDiagonal() * feats_err_D;
   // Compute the command
   feats_cmd_ = feats_err_KP + feats_err_KI + feats_err_KD;
   feats_cmd_ = feats_err_KP;
   // [Xerr] = J [dF]
   // xerr wrt sensor frame
   xerr = J*feats_cmd_;
//////////////////////////////////////////////////////////////
////////selection matrix/////////////////////////////////////   
/////////////////////////////////////////////////
/// error in feature space mapped to error in cartesian space
/// xerr is error in cartesian space
/// feats_cmd_ error in feature space
/// [F] = [selection]*[xerr]
/// 6x1 vector to store selected features
   
S_Matr_force <<servo_copx, 0, 0, 0, 0, 0,
                0, servo_copy, 0, 0, 0, 0,
                0, 0, servo_force, 0, 0, 0,
                0, 0, 0, servo_cocx, 0, 0,
                0, 0, 0, 0, servo_cocy, 0,
                0, 0, 0, 0, 0, servo_orient;

   selected_feature = S_Matr_force*xerr; 
 
/// 3x1 vector to store only changes in position

   
//    selected_feature(0); //copx 
//    selected_feature(1);
//    selected_feature(2); //pressure
//    selected_feature(3);
//    selected_feature(4); 
//    selected_feature(5); // orient
//       
   isready = true;

}
///////////////////////////////////////////////////////////
///////////////publish/////////////////////////////
///////////////////////////////////////////////////////////  
void CartTactCOntr::pub_res(){
  std_msgs::Float64 command_wrj1;
  std_msgs::Float64 command_wrj2;
  
  if ((is_feature_rec)&&(isready)){
    command_wrj1.data =  selected_feature(0);
//     command_wrj1.data =  selected_feature(2);
//     command_wrj1.data =  selected_feature(3);
//     
    if (command_wrj1.data > 0.42) command_wrj1.data = 0.42;
    if (command_wrj1.data < -0.69) command_wrj1.data = -0.69;
    
    wrj1_pub.publish(command_wrj1);
    
    command_wrj2.data =  selected_feature(1);
//     command_wrj2.data =  selected_feature(4);
//     command_wrj2.data =  selected_feature(5);
    
    if (command_wrj2.data > 0.15) command_wrj2.data = 0.15;
    if (command_wrj2.data < -0.28) command_wrj2.data = -0.28;
    wrj2_pub.publish(command_wrj2);
    
    isready = false;
    
    //        begin_servo_controller = false;
    is_feature_rec = false;
    //}
  }
    else{
        command_wrj1.data = wj1_joint_val_now;
        wrj1_pub.publish(command_wrj1);
        command_wrj2.data = wj2_joint_val_now;
        wrj2_pub.publish(command_wrj2);
    }
}
///////////////////////////////////////////////////////////
///////////////// set pid gains/////////////////////////////
///////////////////////////////////////////////////////////
void CartTactCOntr::cb_dynamic_reconf(tactile_controller::feats_pid_params_realConfig &config, uint32_t level){
  
    Deadcpx = config.Deadcpx;
    Kp_(0) = config.Pcpx;Kd_(0) = config.Dcpx;Ki_(0) = config.Icpx;
    Kp_(1) = config.Pcpy;Kd_(1) = config.Dcpy;Ki_(1) = config.Icpy;
    Kp_(2) = config.Pfz; Kd_(2) = config.Dfz; Ki_(2) = config.Ifz;
    Kp_(3) = config.Pccx;Kd_(3) = config.Dccx;Ki_(3) = config.Iccx;
    Kp_(4) = config.Pccy;Kd_(4) = config.Dccy;Ki_(4) = config.Iccy;
    Kp_(5) = config.Por; Kd_(5) = config.Dor; Ki_(5) = config.Ior;

    servo_copx = config.servo_copx;
    servo_copy = config.servo_copy;
    servo_force = config.servo_force;
    servo_cocx = config.servo_cocx;
    servo_cocy = config.servo_cocy;
    servo_orient = config.servo_orient;
}

/// service to start motion
bool CartTactCOntr::start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req, tactile_servo_srvs::start_servo_controller::Response &res){
 begin_servo_controller = req.begin_controller;
    return true;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "cart_tact_contr");
    ros::NodeHandle n;

    ros::Rate loop_rate(100);
    CartTactCOntr CartTactCOntr1;

    while( ros::ok() ){
//        CartTactCOntr1.print();
        CartTactCOntr1.matrix_operations();
        CartTactCOntr1.pub_res();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
