#ifndef TACTILE_HEADER_HPP_
#define TACTILE_HEADER_HPP_

/// some useful libraries
#include <iostream>
#include <string>
#include <cmath>    
#include <stdio.h>    	

/// ros libraries needed and msgs types used
#include "ros/ros.h"
#include <urdf/model.h>

#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/JointState.h"											

/// kdl libraries for robot kinematics, not all of those are used
#include <boost/scoped_ptr.hpp>
#include <kdl/chain.hpp> 
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp> 
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/chainiksolver.hpp> 										//solvers
#include <kdl/chainfksolver.hpp> 
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp> 
#include <kdl_parser/kdl_parser.hpp>    								// to get the URDF chain

/// own msgs created needed
#include <tactile_controller/servo.h>	
#include <cv_features_extractor/featuresControl.h>	

/// helps to spped up the coding 
using namespace KDL;
using namespace std;
using namespace urdf;
#endif
