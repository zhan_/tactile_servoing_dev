/**
  * Node:tactile_control.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	
  * Topic: 
  * Topic Msg Type: 
 */

/// some useful libraries
#include <iostream>
#include <string>
#include <cmath>
#include <stdio.h>

/// ros libraries needed and msgs types used
#include "ros/ros.h"
#include <urdf/model.h>

#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/JointState.h"

/// kdl libraries for robot kinematics, not all of those are used
#include <boost/scoped_ptr.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/chainiksolver.hpp> 										//solvers
#include <kdl/chainfksolver.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl_parser/kdl_parser.hpp>    								// to get the URDF chain

/// own msgs created needed
#include <tactile_controller/servo.h>
#include <cv_features_extractor/featuresControl.h>

/// helps to spped up the coding
using namespace KDL;
using namespace std;
using namespace urdf;


/*************************
 * Global variables
 ************************/


/// Positions, velocities and errors
float q1, q2, v1, v2, e1, e2, M1, M2, M3, M4, pan_real, tilt_real;

/// Features variables of actual state
float f_force, f_pressure, f_orientation, f_offset_x, f_offset_y;
string contact;

const float pan_limit = 1.57;											/// pan joint limit in both directions expressed in radians
const float tilt_limit = 1.57;											/// tilt joint limit in both directions expressed in radians


class Tactile {
public:
  Tactile(ros::NodeHandle &n);
  bool init();

private:
  float pos_val;
  ros::NodeHandle nh_private_;
  /// Subscribers
  ros::Subscriber sub_features, sub_joints_state_virtual, sub_joints_state_real;

/// Hear joints state "/tilt_pan/joint_states" and store the data
void virtual_joints_stateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{ /// get for the joints
  q1=msg->position[0]; 												//q1 angle in radians   -pi rad and pi rad
  q1 *= (180 /pi); 													//radians to deg
  q2=msg->position[1]; 												//q2 angle in radians   -pi rad and pi rad
  q2 *= (180 /pi); 													//radians to deg
  v1=msg->velocity[0]; 												//v1 angle in meters/seconds
  v2=msg->velocity[1]; 												//v2 angle in meters/seconds
  e1=msg->effort[0]; 													//e1 get actual effort applied in joint 1
  e2=msg->effort[1]; 													//e2 get actual effort applied in joint 2
  ROS_INFO("Tactile Controller: I heard the joint states... ");
  gotJointStates=1;
}

void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
  pan_real = msg->position[0];
  tilt_real = msg->position[1];
  ROS_INFO("Tactile Controller: I heard the real joint states... ");
}

/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features f
void featuresCallback(const cv_features_extractor::featuresControl::ConstPtr& featuresC)
{
  ros::Time now = ros::Time::now();
  ROS_INFO("Tactile Controller: I heard the features... ");
  if ( featuresC->contactForce.size() > 0 )
  {	/// Extract features from topic tilt_pan/features. Main componentes = [f,p,alfa,x,y]
    f_force = featuresC->contactForce[0];
    f_pressure = featuresC->contactPressure[0];
    f_orientation = featuresC->contactOrientation[0]; //	f_orientation=abs(f_orientation);
    f_offset_x = featuresC->contactOffset_x[0];
    f_offset_y = featuresC->contactOffset_y[0];
    gotFeatures=1;
    cout << f_force<< endl;
  }
}

  /// Publishers
  ros::Publisher pub_m1_pos,pub_m2_pos,pub_joint1_pos, pub_joint2_pos, pub_for_plot;
  tactile_controller::servo servo_orientation_plot;

        /// Desired Feature States
        float d_force, d_pressure, d_orientation, d_offset_x, d_offset_y;
        /// Error output between desired and actual state
        float error_force, error_pressure, error_orientation, error_offset_x, error_offset_y;

        /// To send the motor commands
        std_msgs::Float64 pan_joint_position_, tilt_joint_position_;

        string root_name, finger_base_name, tip_name;
        urdf::Model robot_model;
        KDL::Tree tree;
        KDL::Chain chain;
        KDL::JntArray q_in, q_out;   									/// joints positions input and output
        KDL::Frame cartpos;          									/// the actual position and orientation in cartesian space
        KDL::Rotation d_rotRPY;  									    /// the desired rotation built from Roll-Pitch-Yaw angles
        KDL::Rotation d_rotEZYX; 									    /// the desired rotation built from Euler Z-Y-X angles
        KDL::JntArray joint_min, joint_max;								/// the joints limits
        string contactType, planning;

        unsigned int num_joints;
        unsigned int nj; 			 									/// the number of joints in the tree
        unsigned int ns; 			 									/// the number of segments in the tree
        float V[6][1];
        int F[6][1];

        bool loadModel(const std::string xml);  						/// to check model parameters
        bool readJoints(urdf::Model &robot_model);        				/// to check joint data
        bool kinematics_status;											/// to check FK solve
        bool gotFeatures, gotJointStates;								/// in callbacks
        void contactState();											/// to map feature space to twist
        void featureError();											/// to get difference between desired and actual features
        void feature_spaceController();									/// to apply gains
        void printScreen();												/// to print in scrren tactile information

};


tactile_controller::servo servo_alfa;

/*************************
 * Global variables
 ************************/
bool gotFeatures, gotJointStates;

/// To send the motor commands 
std_msgs::Float64 torque1;
std_msgs::Float64 torque2;
std_msgs::Float64 control_actual;
std_msgs::Float64 control_desired;

/// Positions, velocities and errors
float q1, q2, v1, v2, e1, e2, M1, M2, M3, M4, pan_real, tilt_real; 

/// Features variables of actual state
float f_force, f_pressure, f_orientation, f_offset_x, f_offset_y;
string contact;

const float pan_limit = 1.57;											/// pan joint limit in both directions expressed in radians
const float tilt_limit = 1.57;											/// tilt joint limit in both directions expressed in radians

float pos_val ;

Tactile::Tactile(): nh_private ("~") {
}
		
		
		
		
/****************************************************************************************
 * Callback functions that execute each time this node subscribes to the topic .../state
 ***************************************************************************************/
 
/// Hear joints state "/tilt_pan/joint_states" and store the data
void joints_stateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	/// get for the joints
	q1=msg->position[0]; 												//q1 angle in radians   -pi rad and pi rad
	q1 *= (180 /M_PI); 													//radians to deg
	q2=msg->position[1]; 												//q2 angle in radians   -pi rad and pi rad
	q2 *= (180 /M_PI); 													//radians to deg
	v1=msg->velocity[0]; 												//v1 angle in meters/seconds
	v2=msg->velocity[1]; 												//v2 angle in meters/seconds
	e1=msg->effort[0]; 													//e1 get actual effort applied in joint 1
	e2=msg->effort[1]; 													//e2 get actual effort applied in joint 2
	//ROS_DEBUG("Tactile Controller: I heard the joint states... ");
	gotJointStates=1;
} 

void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
	pan_real = msg->position[0];
	tilt_real = msg->position[1];
}

/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features f
void featuresCallback(const cv_features_extractor::featuresControl::ConstPtr& featuresC) 
{
	if ( featuresC->contactForce.size() > 0 ) {
	/// Extract features from topic tilt_pan/features
	/// Main componentes = [f,p,alfa,x,y]		
	f_force = featuresC->contactForce[0];	
	f_pressure = featuresC->contactPressure[0];	
	f_orientation = featuresC->contactOrientation[0];
//	f_orientation=abs(f_orientation);
	f_offset_x = featuresC->contactOffset_x[0];	
	f_offset_y = featuresC->contactOffset_y[0];	
	
	/// Redundant data 
	//contact = features->contactType;
	
	gotFeatures=1;
	ROS_DEBUG("Tactile Controller: I heard the features... ");
	} 
} 













bool Tactile::init() {

	/// Get URDF XML from parameter server
    string urdf_xml, full_urdf_xml;
    nh_private.param("urdf_xml",urdf_xml,std::string("robot_description"));
    nh_private.searchParam(urdf_xml,full_urdf_xml);
    ROS_DEBUG("Reading xml file from parameter server");


    /// Check if loaded and store parameters 
    string params;
    if (!nh_private.getParam(full_urdf_xml, params)) {
		ROS_FATAL("Tactile Controller: Could not load the xml from parameter server: %s", urdf_xml.c_str());
		return false;
    }
    
    /// Get Root and Tip From Parameter Service
//	if (!nh_private.getParam("link1", root_name)) {
//		ROS_ERROR("No joint given in namespace: '%s')", nh_private.getNamespace().c_str());
//		ROS_FATAL("Tactile Controller: No root name found on parameter server: %s",urdf_xml.c_str());
//		return false;
//	}
	

	
//	if (!nh_private.getParam("link3", tip_name)) {
//	ROS_FATAL("Tactile Controller: No tip name found on parameter server");
//	return false;
//	}
//	cout<<root_name<<"  "<<tip_name<<endl;
	
	
    /// Load and Read Robot Models and form chains and trees
	if (!loadModel(params)) {
		return false;
	}
    
    nj = chain.getNrOfJoints();	 ns = chain.getNrOfSegments();
	ROS_DEBUG("CHAIN--> Joints:%d, Ind. Joints:%d, Segments:%d",nj,nj,ns);
  
    /// Create joint array
    q_in = JntArray(nj);
	q_out = JntArray(nj);
    
	/// Get actual joint states in radians
	q_in(1) = q1 * (M_PI/180);  q_in(2) = q2 * (M_PI/180);  

	/// Create solver based on kinematic chain
	ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);   										//Forward position solver  
	ChainIkSolverVel_pinv iksolvervel = ChainIkSolverVel_pinv(chain);												//Inverse velocity solver
	ChainIkSolverPos_NR iksolverpos = ChainIkSolverPos_NR(chain,fksolver,iksolvervel, NUM_ITERATIONS_IK, IK_EPS);   //Inverse position solver, Maximum 100 iterations, stop at accuracy 1e-6

	/// Calculate forward position kinematics  
	kinematics_status = fksolver.JntToCart(q_in,cartpos);
    
    if(kinematics_status>=0){		
        //  cout <<endl<< cartpos <<std::endl;
        ROS_DEBUG("Tactile Controller: Kinematic Solver OK");
    }else{
		ROS_FATAL("Tactile Controller: Could not calculate forward kinematics ");
    }
	
	/// Get desired frame (rot,pos) from features -> V = TM * DF 
	contactState ();	
	
    /// The position and orientation of the object wrt to the sensor frame  
    //cout<<"Input Angle: "<<endl;  cin >> angle;    float angle=10;
 
    float roll = V[3][0] * (M_PI/180); float pitch = V[5][1] * (M_PI/180);  float yaw = V[4][1] * (M_PI/180);
    float eZ = V[3][0] * (M_PI/180); float eY = V[5][0] * (M_PI/180);  float eX = V[4][0] * (M_PI/180);

    KDL::Vector d_pos(V[0][0], V[1][0], V[2][0]);					    //X-Y-Z are initialized with the given values 
    d_rotRPY = KDL::Rotation::RPY(roll, pitch, yaw );  			        //Roll-Pitch-Yaw angles
    d_rotEZYX = KDL::Rotation::EulerZYX(eZ, eY, eX);                    //Euler Z-Y-X angles

  
    /// The actual orientation of end-effector frame expressed as Rotation type to then operate
    float rotTemp[9]={};
    unsigned int acc=1;
    for (int i = 0; i<3; i++) {
	  for (int j = 0; j<3; j++) {
		rotTemp[acc] = cartpos.M(i,j);
		++acc;
	  }
    }
    KDL::Rotation roteef( rotTemp[1], rotTemp[2], rotTemp[3], rotTemp[4], rotTemp[5], rotTemp[6], rotTemp[7], rotTemp[8], rotTemp[9]);

  
    /// FINAL POSITION AND ORIENTATION: Put frame wrt base -> Create the frame q + dF 
    KDL::Vector pos = d_pos + cartpos.p;
    //cout << "POSITION: " <<d_pos<< endl;
    KDL::Rotation rot = roteef * d_rotRPY;
    //cout << "ROTATION LAST LINK:" << endl << roteef <<endl;
    //cout << "ROTATION SENSOR:" << endl << d_rotRPY <<endl;
    //cout << "ROTATION PRODUCT:" << endl << rot <<endl;
    KDL::Frame F_dest(rot,pos);											
    //cout<<"Matrix: "<<endl<< F_dest <<endl;
	
	/// Get joint positions in joint space
    iksolverpos.CartToJnt(q_in,F_dest,q_out);

    double q2[3] = {};
    cout << "IK (deg):" << endl;
    for (unsigned int j = 0; j < q_out.rows(); j++)  {
		q2[j] = (q_out(1, j) * (180/M_PI)) ;
		//cout << "  "<< q2[j];
    }
    cout << endl;
     
    /// Verifi joint limits
    if (q_out(1,1) > pan_limit) {
		//ROS_INFO("Position exced pan joint limit", q_out(1,1) );
		q_out(1,1) = 0;
	}
    
    if (q_out(1,1) > tilt_limit) {
		//ROS_INFO("Position exced pan tilt limit", q_out(1,2) );
		q_out(1,2) = 0;
    }
    float kp = 0.01;
    float control_term =  kp * error_orientation * (M_PI/180); // in radians 0.02
    pos_val += control_term;
     
    cout << "                        DEG       " << "RADIANS" << endl;  
    cout << "error:         " << error_orientation << "      ,       "<< error_orientation*(M_PI/180) << endl;
    cout << "desired angle: " << f_orientation <<     "      ,       "<< f_orientation*(M_PI/180) <<endl ;
    cout << "actual  angle: " << pan_real*(180/M_PI)<<  "      ,       "<< pan_real << endl ;
    cout << "step:          " << control_term  * (180/M_PI)<< "         ,       " <<  control_term  << endl;
    cout << "out:           " << pos_val<<endl;
    if(pos_val<-1) pos_val = -1;
    if(pos_val>1) pos_val = 1;
    
    
    torque1.data = pos_val;

    servo_alfa.desired_orientation =  0;
    servo_alfa.actual_orientation = f_orientation;
    cout << "out:           " << pos_val<<endl;
    /// Apply controller, see ROS control description and yaml for PID details
     //torque1.data = q_out(1,1);		
    torque2.data = 0;	//-6.28318531
     
    // torque2.data = q_out(1,2);
    // featureController ();
    // printScreen();
    
}









/** @brief Check and load the urdf robot model as xml, chain and tree  */
bool Tactile::loadModel(const std::string xml) {
    if (!robot_model.initString(xml)) {
        ROS_FATAL("Could not initialize robot model");
        return -1;
    }
    if (!kdl_parser::treeFromString(xml, tree)) {
        ROS_ERROR("Could not initialize tree object");
        return false;
    }
    if (!tree.getChain("world", "link3", chain)) {
        ROS_ERROR("Could not initialize chain object");
        return false;
    }
    
 //   if (!readJoints(robot_model)) {
  //      ROS_FATAL("Could not read information about the joints");
  //      return false;
   // }
    
    return true;
}

/** @brief Read the joints of the urdf model  */
bool Tactile::readJoints(urdf::Model &robot_model) {
    num_joints = 0;
    
    /// get joint maxs and mins
    boost::shared_ptr<const urdf::Link> link = robot_model.getLink(tip_name);
    boost::shared_ptr<const urdf::Joint> joint;

	urdf::Vector3 length;

    while (link && link->name != root_name) {
        joint = robot_model.getJoint(link->parent_joint->name);
        if (!joint) {
            ROS_ERROR("Could not find joint: %s",link->parent_joint->name.c_str());
         //   return false;
        }
        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED) {
            ROS_DEBUG( "adding joint: [%s]", joint->name.c_str() );
            num_joints++;
        }
        link = robot_model.getLink(link->getParent()->name);
    }

    joint_min.resize(num_joints);
    joint_max.resize(num_joints);
    //info.joint_names.resize(num_joints);
    //info.link_names.resize(num_joints);
    //info.limits.resize(num_joints);

    link = robot_model.getLink(tip_name);
    unsigned int i = 0;
    while (link && i < num_joints) {
        joint = robot_model.getJoint(link->parent_joint->name);
        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED) {
            ROS_DEBUG( "getting bounds for joint: [%s]", joint->name.c_str() );

            float lower, upper;
            int hasLimits;
            if ( joint->type != urdf::Joint::CONTINUOUS ) {
                lower = joint->limits->lower;
                upper = joint->limits->upper;
                hasLimits = 1;
            } else {
                lower = -M_PI;
                upper = M_PI;
                hasLimits = 0;
            }
            int index = num_joints - i -1;
            joint_min.data[index] = lower;
            joint_max.data[index] = upper;
            //info.joint_names[index] = joint->name;
            //info.link_names[index] = link->name;
            //info.limits[index].joint_name = joint->name;
            //info.limits[index].has_position_limits = hasLimits;
            //info.limits[index].min_position = lower;
            //info.limits[index].max_position = upper;
            i++;
        }
        link = robot_model.getLink(link->getParent()->name);
    }
   // return true;
}









/// Contact Selector Matrix = (TactileMatrix wich accomodates data in vector) * (Feature error vector)
void Tactile::contactState ()
{

/// get selection matrix
/**************************
 *  Selection Matrices
 **************************/
   // cout<<"Contact Type: " << contactType<<endl;
	contactType = "edge"; 	planning = "orientation";
	
	/// Create an empty matrix to store the Contact Selector Matrix * Feature Error
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				V[row][col] = 0;
				F[row][col] = 0;
			}
		}	
	int S[6][6] = {};
	
/**        --- Force Servoing Matrix Selector ---
 * @brief : Control tilt with wx, wy  and force with vz */

	if(contactType == "push"  && planning == "force" ) {
	   S[2][2] = 1;   S[3][3] = 1 ;   S[4][4] = 1 ;
	}


/**        --- Orientation Servoing Matrix Selector ---				
 * @brief : Control orientation wz */
 
	if(contactType == "edge" && planning == "orientation" ) {
		cout << "edge and orientation" << endl;
		S[5][5] = 1;	
	}		


/**    --- Force and Orientation Servoing Matrix Selector ---
 * @brief :  Control force with vz and orientation wz */
	if(contactType == "edge" && planning == "force&orientation" ) {
		S[2][2] = 1; S[5][5] = 1;
	}
	
		
/**       --- Position Servoing Matrix Selector 2D ---
* @brief : control rolling (tilt) wx, wy  */
	if(planning == "offset" ) {
		S[3][3] = 1; S[4][4] = 1;
	}
	
	
	featureError ();
	
	/// Rearrange data depending on robot kinematics, equivalent to multiplicate by an accomodation matrix
	/// Feature error vector for tilt pan:
	F[0][0]=error_offset_x; F[1][0]=error_offset_y; F[2][0]=error_force; 
	F[3][0]=error_offset_x; F[4][0]=error_offset_y; F[5][0]=error_orientation; 

	
/// Selection Matrix	
cout<<"Selection Matrix:"<<endl;			
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 6; col++) {
			//	cout << S[row][col]<<" ";
			}
		//	cout<<endl;
		}	
		
/// Feature Matrix
cout<<"Feature Matrix:"<<endl;
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
		//		cout << F[row][col];
			}
		//	cout<<endl;
		}		
			
				
/// Multiplicate selection matrix "S" by feature error depending of redundant data  V = S*DF
cout<<"Twist"<<endl;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				for (int inner = 0; inner < 6; inner++) {
					V[row][col] += S[row][inner] * F[inner][col];
				}
				cout << V[row][col] << "  ";
			}
			cout << endl;
		}			

}	
/// The feature error
void Tactile::featureError ()
{
	error_force = f_force - d_force;
	error_pressure = f_pressure - d_pressure;
	error_offset_x = f_offset_x - d_offset_x;
	error_offset_y = f_offset_y - d_offset_y;
	error_orientation = f_orientation ; // error in degrees --> f_desired=0 => 0-f_orientation
	//error_orientation = f_orientation - (pan_real*(180/pi)); // error in degrees 
	//cout << "ERROR DEG: " << error_orientation << endl;
	//- d_orientation   ... -q1 in simulation instead of pan_real;

}

/// Print data in screen
void Tactile::printScreen ()
{
  	cout<<"       STATES"<<endl;	
	cout<<"q1:       "<<q1<<endl; cout<<"q2:       "<<q2<<endl;	
	cout<<"v1:       "<<v1<<endl; cout<<"v2:       "<<v2<<endl;	
	cout<<"e1:       "<<e1<<endl; cout<<"e2:       "<<e2<<endl;	
	cout<<"      FEATURES"<<endl;
	//cout<<"Force:       "<<f_force<<endl;
	//cout<<"Pressure:    "<<f_pressure<<endl;
	cout<<"Orientation: "<<endl;
	cout<<"Actual: "<<q1<<endl;
	cout<<"Desired: "<<f_orientation<<endl;
	//cout<<"Offset x:    "<<f_offset_x<<endl;
	//cout<<"Offset y:    "<<f_offset_y<<endl;
}


/// The controller in feature space => gives torques to motors
void Tactile::feature_spaceController ()
{
		
	float kp = 1; 
	float d_stiffness = 3;
	
	
	/// Compute dF
	error_orientation = kp*(f_orientation-q1);
}



/*******************************
 * 			Main
 ********************************/
   
int main(int argc, char **argv)
{
  std::cout << argv[0] << std::endl;
  ros::init(argc, argv, "tactile_controller");							/// Initialize Ros systen 
  ros::NodeHandle n;													/// Create node
  ROS_INFO("FEATURE CONTROLLER NODE");
    
  /// Create the suscriber to the simulated joint angles position, velocity and effort
  ros::Subscriber sub_joints_state = n.subscribe("/tilt_pan/joint_states", 1000, joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to the real joint angles position, velocity and effort
  ros::Subscriber sub_joints_state_real = n.subscribe("/real/joint_states", 1000, real_joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to image features
  ros::Subscriber sub_features = n.subscribe("tilt_pan/Features/controlFeatures", 1000, featuresCallback);
  ros::spinOnce();
    
  /// Create the publishers for advertise joint positions
  ros::Publisher pub_joint1_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint1_position_controller/command", 100);
  ros::Publisher pub_joint2_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint2_position_controller/command", 100);
  
  ros::Publisher pub_m1_pos = n.advertise<std_msgs::Float64>("/head_pan_joint/command", 100);   
  ros::Publisher pub_m2_pos = n.advertise<std_msgs::Float64>("/head_tilt_joint/command", 100);  
    
  /// to store plots
  ros::Publisher pub_desired = n.advertise<std_msgs::Float64>("tactileContorller/desiredFeature", 100);   	
  ros::Publisher pub_actual = n.advertise<std_msgs::Float64>("tactileController/actualFeature", 100);    
  ros::Publisher pub_orientation = n.advertise<tactile_controller::servo>("PLOT/pan_tilt/servo_orientation", 100);  
  
  ros::Rate loop_rate(100);												/// Loop at 100Hz until the node is shut down
  
  
  int count = 0;
  while (ros::ok())
  {
		
	Tactile k;
    if (k.init()<0) {
        ROS_ERROR("Could not initialize tactile controller node");
        return -1;
   } 
	  /// pan
		  pub_m1_pos.publish(torque1); 
		  pub_joint1_pos.publish(torque1);
	  /// tilt
	      pub_m2_pos.publish(torque2);	
	      pub_joint2_pos.publish(torque2);
	      
	      servo_alfa.header.stamp = ros::Time::now(); 
	      pub_orientation.publish(servo_alfa);
	  //ROS_DEBUG("Joints States Published ... ");
	  ros::spinOnce();													/// Do this
      loop_rate.sleep();												/// Wait until it is time for another interaction
      ++count;
      
  }
  return 0;
}
