#ifndef TACTILE_HPP_
#define TACTILE_HPP_

/*************************
 * Global variables
 ************************/


/// Positions, velocities and errors
float q1, q2, v1, v2, e1, e2, M1, M2, M3, M4, pan_real, tilt_real; 

/// Features variables of actual state
float f_force, f_pressure, f_orientation, f_offset_x, f_offset_y;
string contact;

const float pan_limit = 1.57;											/// pan joint limit in both directions expressed in radians
const float tilt_limit = 1.57;											/// tilt joint limit in both directions expressed in radians


class Tactile {
public:
  Tactile(ros::NodeHandle &n);
  bool init();
   
private:
  float pos_val;
  ros::NodeHandle nh_private_;  
  /// Subscribers
  ros::Subscriber sub_features, sub_joints_state_virtual, sub_joints_state_real; 

/// Hear joints state "/tilt_pan/joint_states" and store the data
void virtual_joints_stateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{ /// get for the joints
  q1=msg->position[0]; 												//q1 angle in radians   -pi rad and pi rad
  q1 *= (180 /pi); 													//radians to deg
  q2=msg->position[1]; 												//q2 angle in radians   -pi rad and pi rad
  q2 *= (180 /pi); 													//radians to deg
  v1=msg->velocity[0]; 												//v1 angle in meters/seconds
  v2=msg->velocity[1]; 												//v2 angle in meters/seconds
  e1=msg->effort[0]; 													//e1 get actual effort applied in joint 1
  e2=msg->effort[1]; 													//e2 get actual effort applied in joint 2
  ROS_INFO("Tactile Controller: I heard the joint states... ");
  gotJointStates=1;
} 

void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
  pan_real = msg->position[0];
  tilt_real = msg->position[1];
  ROS_INFO("Tactile Controller: I heard the real joint states... ");
}  
      
/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features f
void featuresCallback(const cv_features_extractor::featuresControl::ConstPtr& featuresC) 
{
  ros::Time now = ros::Time::now();
  ROS_INFO("Tactile Controller: I heard the features... ");
  if ( featuresC->contactForce.size() > 0 ) 
  {	/// Extract features from topic tilt_pan/features. Main componentes = [f,p,alfa,x,y]	 
    f_force = featuresC->contactForce[0];	
	f_pressure = featuresC->contactPressure[0];	
	f_orientation = featuresC->contactOrientation[0]; //	f_orientation=abs(f_orientation);
	f_offset_x = featuresC->contactOffset_x[0];	
	f_offset_y = featuresC->contactOffset_y[0];	
	gotFeatures=1;
	cout << f_force<< endl;
  } 
} 
        
  /// Publishers
  ros::Publisher pub_m1_pos,pub_m2_pos,pub_joint1_pos, pub_joint2_pos, pub_for_plot;
  tactile_controller::servo servo_orientation_plot;
  
        /// Desired Feature States
		float d_force, d_pressure, d_orientation, d_offset_x, d_offset_y;	
		/// Error output between desired and actual state
		float error_force, error_pressure, error_orientation, error_offset_x, error_offset_y;

        /// To send the motor commands 
		std_msgs::Float64 pan_joint_position_, tilt_joint_position_;

        string root_name, finger_base_name, tip_name;         
        urdf::Model robot_model;
		KDL::Tree tree;  
		KDL::Chain chain; 				
        KDL::JntArray q_in, q_out;   									/// joints positions input and output 
		KDL::Frame cartpos;          									/// the actual position and orientation in cartesian space
		KDL::Rotation d_rotRPY;  									    /// the desired rotation built from Roll-Pitch-Yaw angles
		KDL::Rotation d_rotEZYX; 									    /// the desired rotation built from Euler Z-Y-X angles
		KDL::JntArray joint_min, joint_max;								/// the joints limits
		string contactType, planning; 
		
		unsigned int num_joints;  
        unsigned int nj; 			 									/// the number of joints in the tree
        unsigned int ns; 			 									/// the number of segments in the tree
        float V[6][1];
        int F[6][1];		
        
        bool loadModel(const std::string xml);  						/// to check model parameters
        bool readJoints(urdf::Model &robot_model);        				/// to check joint data
        bool kinematics_status;											/// to check FK solve                   
        bool gotFeatures, gotJointStates;								/// in callbacks
        void contactState();											/// to map feature space to twist
        void featureError();											/// to get difference between desired and actual features
        void feature_spaceController();									/// to apply gains
        void printScreen();												/// to print in scrren tactile information
        
};
#endif
