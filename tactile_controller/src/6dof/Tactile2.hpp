#ifndef TACTILE_HPP_
#define TACTILE_HPP_
#define NUM_ITERATIONS_IK 100
#define IK_EPS	1e-6

/// ROS LIBRARIES
#include <urdf/model.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/JointState.h"
/// to get the URDF chain
#include <kdl_parser/kdl_parser.hpp>

/// UTILS LIBRARIES
#include <iostream>
#include <string>
#include <cmath>        												// std::abs

/// KDL LIBRARIES
#include <boost/scoped_ptr.hpp>
#include <kdl/chain.hpp> //
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>  //
#include <kdl/frames.hpp>
#include <kdl/frames_io.hpp> //
#include <kdl/jacobian.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/chainfksolver.hpp>  //
#include <kdl/frames_io.hpp>
#include <stdio.h>
#include <kdl/chainiksolver.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <tactile_controller/servo.h>	
#include <cv_features_extractor/featuresControl.h>	
using namespace KDL;
using namespace std;
using namespace urdf;

class Tactile {
    public:
        Tactile();
        bool init();
		

    private:
        ros::NodeHandle n, nh_private;  
        void control_force();
        void control_orientation();
        void control_offset();
        float tilt_val, pan_val, prev_pan_val,prev_f_orientation, prev_tilt_val ;
        /// Desired Feature States
		float d_force, d_pressure, d_orientation, d_offset_x, d_offset_y;	
		/// Error output between desired and actual state
		float error_force, error_pressure, error_orientation, error_offset_x, error_offset_y;

        
        string root_name, finger_base_name, tip_name;         
        urdf::Model robot_model;
		KDL::Tree tree;  
		KDL::Chain chain; 				
        KDL::JntArray q_in, q_out;   									/// joints positions input and output 
		KDL::Frame cartpos;          									/// the actual position and orientation in cartesian space
		KDL::Rotation d_rotRPY;  									    /// the desired rotation built from Roll-Pitch-Yaw angles
		KDL::Rotation d_rotEZYX; 									    /// the desired rotation built from Euler Z-Y-X angles
		KDL::JntArray joint_min, joint_max;								/// the joints limits
		string contactType, planning; 
		
		unsigned int num_joints;  
        unsigned int nj; 			 									/// the number of joints in the tree
        unsigned int ns; 			 									/// the number of segments in the tree
        float V[6][1];
        int F[6][1];		
        
        bool loadModel(const std::string xml);  						/// to check model parameters
        bool readJoints(urdf::Model &robot_model);        				/// to check joint data
        bool kinematics_status;											/// to check FK solve                   
        void contactState();											/// to map feature space to twist
        
        void featureError();											/// to get difference between desired and actual features
        void feature_spaceController();									/// to apply gains
        void printScreen();												/// to print in scrren tactile information
        
};
#endif
