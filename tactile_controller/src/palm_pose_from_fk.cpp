#include <ros/ros.h>

// JA_now - get joint angles using
/// subscriber /joint_state
#include <control_msgs/JointControllerState.h> // for the real hand <sr_robot_msgs/

// POSE_cur - FK - get FK using
/// services arm_kinematicso1
#include <moveit_msgs/GetKinematicSolverInfo.h>
#include <moveit_msgs/GetPositionIK.h>
#include <moveit_msgs/GetPositionFK.h>
// FEATS_sensor - get features using
/// subscribers /des_feats and /fb_feats
#include "tactile_servo_msgs/ContsFeats.h"
#include "tactile_servo_msgs/OneContFeats.h"

// dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor
// eigen library
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Dense>
// for transformations
#include <tf/tf.h>
#include <tf/transform_listener.h>

// J - using eigen create jacobian 6x6
/// eigen
// SM - selection matrix 6x6
// dFEATS_cart - J*SM*dFEATS - calculate the change in cart space for IK
// POSE_des =  POSE_cur+dFEATS_cart

/// tf package RPY and Position
///
// JA_des  = IK of  POSE_des
/// services arm_kinematics
// to send commands and read joint states
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>
// MOVE - send JA_des
/// publish /sa or sh _ joint name / controller name
// SHOW_MARKERS - rviz markers

/// publish /markers
#include <visualization_msgs/Marker.h>

/// tune pid param by dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <tactile_controller/feats_pid_paramsConfig.h>

// services to start movements
#include "tactile_servo_srvs/start_servo_controller.h"

class CartTactCOntr
{
public:
    CartTactCOntr();
    ~CartTactCOntr();
    ros::NodeHandle nh_;
    // JA_now - get joint angles
    // one callback for many subscribers
    std::string controller_type;
    std::vector<std::string> arm_hand_joints_names;
    bool is_joint_vals_rec;
    std::vector<bool> is_rec_jnt_val;
    std::vector<double> joint_reads;
    std::vector<ros::Subscriber> arm_hand_joint_vals_sub;
    void cb_joint_state(const control_msgs::JointControllerStateConstPtr &msg_, int id_);
    // POSE_cur - FK - get FK using
    ros::ServiceClient fk_client;
    ros::ServiceClient query_client_fk;
    void getFK();
    moveit_msgs::GetPositionFK::Request gpfk_req;
    moveit_msgs::GetPositionFK::Response gpfk_res;
    moveit_msgs::GetKinematicSolverInfo::Request request;
    moveit_msgs::GetKinematicSolverInfo::Response response;
    std::string frame_names_FK[6];
    geometry_msgs::PoseStamped X_now, X_now_check_RPY;

    // FEATS_sensor - get features using
    ros::Subscriber des_feats_sub;
    ros::Subscriber fb_feats_sub;
    void cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    void cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_);
    float   copx_fb,
            copy_fb,
            force_fb,
            cocx_fb,
            cocy_fb,
            orientz_fb;
    float   copx_des,
            copy_des,
            force_des,
            cocx_des,
            cocy_des,
            orientz_des;

    // dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor

    // Define the joint/cart vector types accordingly (using a fixed
    // size to avoid dynamic allocations and make the code realtime safe).
    typedef Eigen::Matrix<double, 6, 1>  JointVector;
    typedef Eigen::Matrix<double, 6, 1>  FeatursVector;
    typedef Eigen::Matrix<double, 3, 1>  Cart3Vector;
    typedef Eigen::Matrix<double, 6, 1>  Cart6Vector;
    typedef Eigen::Matrix<double, 6, 6>  JacobianMatrix;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    // Tip initial pose, pose now, pose desired
    Cart6Vector     x0_,
                    x,
                    xd,
    xd_palm;
    // Cart error,velocity
    Cart6Vector
    xerr,
    xdot,
    xerr_world,
    xerr_palm;
    // joint vals now, joint vals desired, joint velocities
    JointVector     q,
                    qdes,
                    qdot;
    // Features desired, fb
    FeatursVector
    feats_fb, feats_des,
    feats_err_P, feats_err_KP,
    feats_err_I, feats_err_KI,
    feats_err_D, feats_err_P_last, feats_err_KD,
    feats_cmd_,
    dt_vector;
    // Features desired, fb
    FeatursVector   feats_state_art_fb,
                    feats_state_art_des,
                    feats_state_art_err;
    // Jacobian
    JacobianMatrix  J, J_state_art,S_Matr_all, S_Matr_force, S_Matr_force_copx_copy, S_Matr_orient, S_Matr_orient_coc;
    // The controller parameters gains
    Cart6Vector  Kp_;
    Cart6Vector  Kd_;
    Cart6Vector  Ki_;
    Cart6Vector  i_max;
    Cart6Vector  i_min;
    double       last_time_;
    double       dt;

    geometry_msgs::PoseStamped X_des, X_des2, X_des3, X_err_palm, X_err_frame, X_err_wrt_base,
    X_weiss_wrt_base;

    void matrix_operations();
    // IK
    // JA_des  = IK of  POSE_des
    /// services arm_kinematics
    moveit_msgs::GetPositionIK::Request  gpik_req;
    moveit_msgs::GetPositionIK::Response gpik_res;
    moveit_msgs::GetKinematicSolverInfo::Request request_ik;
    moveit_msgs::GetKinematicSolverInfo::Response response_ik;
    void getIK();
    // send command
    ros::Publisher pub[6];
    std::vector<std_msgs::Float64> command;
    std::vector<float> joints_commands;
    std::vector<std::string> joints_names;
    ros::ServiceClient ik_client;
    ros::ServiceClient query_client_ik;
    // publish to joints
    std::vector<ros::Publisher> Publishers_joints;
    void pub_res();
    bool isready;



    // SHOW_MARKERS - rviz markers
    visualization_msgs::Marker create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                                          std::string frame_name_,
                                                          std::vector<float> color_,
                                                          u_int32_t shape_,
                                                          std::vector<float> scale_);
    void markers_pub();

    // transform error in sensor frame to error in palm frame (end_effector)
    tf::TransformListener tf_listener;
    // transform error in sensor frame to error in palm frame (end_effector)
    geometry_msgs::PoseStamped convert_pose(geometry_msgs::PoseStamped& pose_in_);


    /// tune pid param by dynamic reconfigure
    void cb_dynamic_reconf(tactile_controller::feats_pid_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<tactile_controller::feats_pid_paramsConfig> set_pid_feats_server;
    dynamic_reconfigure::Server<tactile_controller::feats_pid_paramsConfig> :: CallbackType f;
    double  Pcpx,
            Icpx,
            Dcpx,
            Deadcpx;
    double pid_out;
    double pid_out_old;
    double pid_dt;
    double pid_integral_old;
    double pid_err_old;
    // debug
    void print();

    ros::Publisher pose_now_marker_pub;
    ros::Publisher pose_now_check_rpy_marker_pub;
    ros::Publisher pose_des_marker_pub;
    ros::Publisher pose_des2_marker_pub;
    ros::Publisher pose_des3_marker_pub;
    ros::Publisher pose_now_y_marker_pub;
    ros::Publisher pose_now_z_marker_pub;


    // start movement service
    bool begin_servo_controller;
    ros::ServiceServer service;
    bool start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req,
                             tactile_servo_srvs::start_servo_controller::Response &res);

    int servo_copx;
    int servo_copy;
    int servo_force;
    int servo_cocx;
    int servo_cocy;
    int servo_orient;
    bool is_feature_rec;
    
    ros::Publisher pose_now_pub;
    ros::Publisher pose_des_pub;

	std::vector<double> joint_commands_last_good;
	bool is_there_at_least_one_succes;
};
CartTactCOntr::CartTactCOntr(){
    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);

    // read joints
    controller_type = "_position_controller";
    controller_type = static_cast<std::string> (my_list[0]["controller_type"]);

    std::string joint_names_str[6]={"sa_sr", "sa_ss", "sa_es",
                                    "sa_er", "sh_wrj2","sh_wrj1"};
    for (int i = 0; i < 6; ++i){
        arm_hand_joints_names.push_back(joint_names_str[i] );
    }
    float joints_reads_arr[6] = {0,0,0,0,0,0};
    for (int i = 0; i < 6; ++i){
        joint_reads.push_back(joints_reads_arr[i] );
    }
    for(int j = 0; j < arm_hand_joints_names.size(); ++j){
        std::string topic_name_sub = "/"+arm_hand_joints_names[j]+controller_type+"/state";
        arm_hand_joint_vals_sub.push_back(nh_.subscribe<control_msgs::JointControllerState>(
                                              topic_name_sub, 1,
                                              boost::bind(&CartTactCOntr::cb_joint_state,
                                                          this, _1, j) ) );
    }
    is_joint_vals_rec = false;
    for (int i = 0; i < 6; ++i){
        is_rec_jnt_val.push_back(false);
    }

    // POSE_cur - FK - get FK using
    // prepare services
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk");
    query_client_fk = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_fk_solver_info");
    fk_client = nh_.serviceClient<moveit_msgs::GetPositionFK>("shadow_right_arm_kinematics/get_fk");
    frame_names_FK[0] = "ShoulderJRotate";
    frame_names_FK[1] = "ShoulderJSwing";
    frame_names_FK[2] = "ElbowJSwing";
    frame_names_FK[3] = "ElbowJRotate";
    frame_names_FK[4] = "WRJ2";
    frame_names_FK[5] = "WRJ1";
    // FEATS_sensor - get features using
    int tactile_sensor_num_ = 1;
    std::string tactile_sensor_num_str;
    std::stringstream out;
    out << tactile_sensor_num_;
    tactile_sensor_num_str = out.str();
    des_feats_sub = nh_.subscribe("/des_feats", 1,  &CartTactCOntr::cb_des_feats_sub,this);
    fb_feats_sub = nh_.subscribe("/fb_feats", 1,  &CartTactCOntr::cb_fb_feats_sub,this);
    // dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor
    // eigen
    J <<    1, 0, 0, 0, 0, 0,
            0, 1, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0,
            0, 0, 0, 1, 0, 0,
            0, 0, 0, 0, 1, 0,
            0, 0, 0, 0, 0, 1;
    J_state_art <<  1, 0, 0, 0, 0, 0,
                    0, 1, 0, 0, 0, 0,
                    0, 0, 1, 0, 0, 0,
                    0, 1, 0, 0, 0, 0,
                    1, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 1;
    S_Matr_force << 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 1, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0;
    S_Matr_force_copx_copy << 1, 0, 0, 0, 0, 0,
                            0, 1, 0, 0, 0, 0,
                            0, 0, 1, 0, 0, 0,
                            0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0;
    S_Matr_orient <<   0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 0,
                                0, 0, 0, 0, 0, 1;
    S_Matr_orient_coc <<   0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0,
                        0, 0, 0, 1, 0, 0,
                        0, 0, 0, 0, 1, 0,
                        0, 0, 0, 0, 0, 1;
    S_Matr_all <<       1, 0, 0, 0, 0, 0,
                        0, 1, 0, 0, 0, 0,
                        0, 0, 1, 0, 0, 0,
                        0, 0, 0, 1, 0, 0,
                        0, 0, 0, 0, 1, 0,
                        0, 0, 0, 0, 0, 1;
    //  the gains in cart space
    Kp_(0) = 1.0;  Kd_(0) = 0.0;  Ki_(0) = 0.0;    // Translation x
    Kp_(1) = 1.0;  Kd_(1) = 0.0;  Ki_(1) = 0.0;    // Translation y
    Kp_(2) = 0.0005;  Kd_(2) = 0.0;  Ki_(2) = 0.0;    // Translation z
    Kp_(3) = 1.0;  Kd_(3) = 0.0;  Ki_(3) = 0.0;    // Rotation x
    Kp_(4) = 1.0;  Kd_(4) = 0.0;  Ki_(4) = 0.0;    // Rotation y
    Kp_(5) = 1.0;  Kd_(5) = 0.0;  Ki_(5) = 0.0;    // Rotation z
    dt_vector(0) = 0.0;
    dt_vector(1) = 0.0;
    dt_vector(2) = 0.0;
    dt_vector(3) = 0.0;
    dt_vector(4) = 0.0;
    dt_vector(5) = 0.0;
///IK
    //    {"ShoulderJRotate", "ShoulderJSwing", "ElbowJSwing", "ElbowJRotate", "WRJ2", "WRJ1"};
    // prepare services
    //    ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
    //    ros::service::waitForService("shadow_right_arm_kinematics/get_ik");
    //    ros::ServiceClient query_client = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    // prepare services
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik");
    service = nh_.advertiseService("start_servo_controller_service_cart", &CartTactCOntr::start_servo_control, this);
    query_client_ik = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    ik_client = nh_.serviceClient<moveit_msgs::GetPositionIK>("shadow_right_arm_kinematics/get_ik");
    controller_type = "_position_controller";

    float joints_commands_arr[6] = {0,0,0,0,0,0};
    for (int i = 0; i < 6; ++i){
        joints_commands.push_back(joints_commands_arr[i] );
    }

    for(int i=0;i<6;i++){
        std::string topicname="/"+joint_names_str[i]+controller_type+"/command";
        this->Publishers_joints.push_back( nh_.advertise<std_msgs::Float64>(topicname, 1));
    }

    begin_servo_controller = false;
    is_feature_rec = false;
    // set pid features
    f = boost::bind(&CartTactCOntr::cb_dynamic_reconf, this, _1, _2);
    set_pid_feats_server.setCallback(f);
    Pcpx = 1; Icpx = 0; Dcpx = 0; Deadcpx = 0;
    pid_integral_old = 0;
    pid_err_old = 0;


    // SHOW_MARKERS - rviz markers
    pose_now_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_now", 0 );
    pose_now_check_rpy_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_check_RPY_now", 0 );
    pose_des_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_des", 0 );
    pose_des2_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_des2", 0 );
    pose_des3_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_des3", 0 );
    pose_now_y_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_now_y", 0 );
    pose_now_z_marker_pub =  nh_.advertise<visualization_msgs::Marker>("marker_pose_now_z", 0 );
    pose_now_pub = nh_.advertise<geometry_msgs::PoseStamped>("pose_now",10);
    pose_des_pub = nh_.advertise<geometry_msgs::PoseStamped>("pose_des",10);


    feats_err_I << 0,0,0,0,0,0;
	is_there_at_least_one_succes = false;
}
CartTactCOntr::~CartTactCOntr(){}

// joint vals now
void CartTactCOntr::cb_joint_state(const control_msgs::JointControllerStateConstPtr &msg_, int id_){
    joint_reads.at(id_) = msg_->process_value;
    is_rec_jnt_val.at(id_) = true;
    int counter_to_verify_readings = 0;
    for (int i = 0; i < 6; ++i){
//        std::cout<<"is_rec_jnt_val.at "<< i << "is " <<is_rec_jnt_val.at(i)<<std::endl;
        if (is_rec_jnt_val.at(i) ){
            counter_to_verify_readings = counter_to_verify_readings + 1;
        }
    }
    if (counter_to_verify_readings == 6){
        is_joint_vals_rec = true;
        counter_to_verify_readings = 0;
        for (int i = 0; i < 6; ++i){
            is_rec_jnt_val.at(i) = false;
        }
    }

    /*
    std::cout<<"joint name at id "<<id_<<std::endl;
    std::cout<<"has name = "<<arm_hand_joints_names.at(id_)<<std::endl;
    std::cout<<"and value = "<<joint_reads.at(id_)<<std::endl;*/
}

// FK
// get current pose of Palm (end-effector)
// wrt base_frame (shadowarm_vase)
void CartTactCOntr::getFK(){
    // get info
    query_client_fk.call(request,response);
    if(query_client_fk.call(request,response))
    {
      for(unsigned int i=0; i < response.kinematic_solver_info.joint_names.size(); i++)
      {
//        ROS_INFO("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
      }
    }
    else
    {
      ROS_INFO("Could not call query service");
      ros::shutdown();
      exit(1);
    }
    //FK
    // fill request

    gpfk_req.header.frame_id = "shadowarm_base";
    gpfk_req.fk_link_names.push_back( "palm" );
    gpfk_req.robot_state.joint_state.header.stamp = ros::Time::now();
    gpfk_req.robot_state.joint_state.header.frame_id = "smth";
    gpfk_req.robot_state.joint_state.name.assign(frame_names_FK,frame_names_FK + sizeof(frame_names_FK) / sizeof(frame_names_FK[0])  );

    gpfk_req.robot_state.joint_state.position = joint_reads;

    fk_client.call(gpfk_req, gpfk_res);
    if(gpfk_res.error_code.val == gpfk_res.error_code.SUCCESS){
        float smth = gpfk_res.pose_stamped.at(0).pose.position.x;
        X_now = gpfk_res.pose_stamped.at(0);
//        ROS_INFO(" x = %f y = %f  z = %f",
//                 X_now.pose.position.x, X_now.pose.position.y,X_now.pose.position.z);
//        ROS_INFO(" qx = %f qy = %f  qz = %f  qw = %f",
//                 X_now.pose.orientation.x, X_now.pose.orientation.y,
//                 X_now.pose.orientation.z, X_now.pose.orientation.w);
//        ROS_INFO(" header %s",
//                 X_now.header.frame_id.c_str());
    }
}
// FEATS_sensor - get features using
void CartTactCOntr::cb_des_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_des = msg_->control_features[0].centerpressure_x;
        copy_des = msg_->control_features[0].centerpressure_y;
        force_des = msg_->control_features[0].contactForce;
        cocx_des = msg_->control_features[0].centerContact_x;
        cocy_des = msg_->control_features[0].centerContact_y;
        orientz_des = msg_->control_features[0].contactOrientation;

        /* coordinates of COP/COC in world =
        / sequence number of the  COP/COC pixel in x or y
        / *
        / physical size of a sensor in x or y
        / /
        / number of cells in x(y)*/
        /* copx_des = copx_des*size_x / cells_x;
        copy_des = copy_des*size_y / cells_y;
        cocx_des = cocx_des*size_x / cells_x;
        cocy_des = cocy_des*size_y / cells_y; */
    }
}

void CartTactCOntr::cb_fb_feats_sub(const tactile_servo_msgs::ContsFeatsConstPtr& msg_){
    if (msg_->control_features.size() == 1){
        copx_fb = msg_->control_features[0].centerpressure_x;
        copy_fb = msg_->control_features[0].centerpressure_y;
        force_fb = msg_->control_features[0].contactForce;
        cocx_fb = msg_->control_features[0].centerContact_x;
        cocy_fb = msg_->control_features[0].centerContact_y;
        orientz_fb = msg_->control_features[0].contactOrientation;
        is_feature_rec = true;
    }
}

// dFEATS_sensor - substruct feats, put in matrix form 6x1 - wrt sensor
// eigen library
void CartTactCOntr::matrix_operations(){
    // Servo loop time step
    // Calculate the dt between servo cycles.
    dt = ros::Time::now().toSec() - last_time_;
    last_time_ = ros::Time::now().toSec();
    dt_vector(0) = dt;
    dt_vector(1) = dt;
    dt_vector(2) = dt;
    dt_vector(3) = dt;
    dt_vector(4) = dt;
    dt_vector(5) = dt;

    // Get the current joint positions
    // is done in std::vector <float> joint_reads
    q(0) = joint_reads.at(0);
    q(1) = joint_reads.at(1);
    q(2) = joint_reads.at(2);
    q(3) = joint_reads.at(3);
    q(4) = joint_reads.at(4);
    q(5) = joint_reads.at(5);

    // Fill up the current cartesian pose, which is
    // expressed wrt base_frame (shadowarm_vase)
    // is done in geometry_msgs::pose X_now
    // but is uses quartenion
    x(0) = X_now.pose.position.x;
    x(1) = X_now.pose.position.y;
    x(2) = X_now.pose.position.z;
    tf::Quaternion q_q2rpy_base2palm;
    q_q2rpy_base2palm.setX(X_now.pose.orientation.x);
    q_q2rpy_base2palm.setY(X_now.pose.orientation.y);
    q_q2rpy_base2palm.setZ(X_now.pose.orientation.z);
    q_q2rpy_base2palm.setW(X_now.pose.orientation.w);
    tf::Matrix3x3 rot_matr_base2palm(q_q2rpy_base2palm);
    double roll_base2palm_now, pitch_base2palm_now, yaw_base2palm_now;
    rot_matr_base2palm.getRPY(roll_base2palm_now, pitch_base2palm_now, yaw_base2palm_now);
    x(3) = roll_base2palm_now;
    x(4) = pitch_base2palm_now;
    x(5) = yaw_base2palm_now;

    // Get the current features vector
    feats_des(0) = copx_des;
    feats_des(1) = copy_des;
    feats_des(2) = force_des;
    feats_des(3) = cocx_des-copx_des;
    feats_des(4) = cocy_des-copy_des;
    feats_des(5) = orientz_des;
std::cout<<"feats_fb="<<feats_des<<std::endl;
    // Get the fb features vector
    feats_fb(0) = copx_fb;
    feats_fb(1) = copy_fb;
    feats_fb(2) = force_fb;
    feats_fb(3) = cocx_fb-copx_fb;
    feats_fb(4) = cocy_fb-copy_fb;
    feats_fb(5) = orientz_fb;
std::cout<<"feats_fb="<<feats_fb<<std::endl;
    // Calculate proportional error
    feats_err_P = feats_des - feats_fb;
    // Calculate proportional contribution to command
    feats_err_KP = Kp_.asDiagonal()*feats_err_P;

    std::cout<<"feats_err_KP="<<feats_err_KP<<std::endl;
     // Calculate the integral of the position error
    feats_err_I = feats_err_I + dt*feats_err_P;
    // Calculate integral contribution to command
    feats_err_KI = Ki_.asDiagonal() * feats_err_I;

    // TODO Limit i_term so that the limit is meaningful in the output
    //    i_term = std::max( gains.i_min_, std::min( i_term, gains.i_max_) );
    //    if (feats_err_KI.array()>i_max.Array()){
    //    }
    // Calculate the derivative error
   if (dt > 0.0)
   {
       feats_err_D = (feats_err_P - feats_err_P_last) / dt;
       feats_err_P_last = feats_err_P;
   }
   // Calculate derivative contribution to command
   feats_err_KD = Kd_.asDiagonal() * feats_err_D;
   // Compute the command
   feats_cmd_ = feats_err_KP + feats_err_KI + feats_err_KD;

   // [Xerr] = J [dF]
   // xerr wrt sensor frame
   xerr = J*feats_cmd_;
//   for (int i = 0; i < 3; ++i){
//       if (xerr(i) < -0.0020){
//           xerr(i) = -0.0020;
//       }
//       if (xerr(i) > 0.0020){
//           xerr(i) = 0.002;
//       }
//   }
//   for (int i = 3; i < 5; ++i){
//       if (xerr(i) < -0.2){
//           xerr(i) = -0.2;
//       }
//       if (xerr(i) > 0.2){
//           xerr(i) = 0.2;
//       }
//   }


   // xerr wrt base frame
/// get transformation of position from base to palm
/// listen transform listener
/// void tf::TransformListener::lookupTransform
/// (std::string &W, std::string &A, ros::Time &time, StampedTransform &transform)
/// The transform returned is WAT, that is, the pose of A in W's coordinate system.
/// This transform can also be used to take points in A's coordinate system and
///  convert them to W. Typically, the transformXXX
/// functions will be used for this task, though.

///////////////////////////
/// Force begin ///////////
/// [T'] = [T] + [Rot1]*[Rot2][F]
/// [T]
/// actually transformation from base to palm is
/// X_now.pose.position and x (0), x (1), x (2)
/// [T] = transfrom_vector_position_base_palm
   Eigen::Vector3d transfrom_vector_position_base_palm;
   transfrom_vector_position_base_palm(0) = x (0);
   transfrom_vector_position_base_palm(1) = x (1);
   transfrom_vector_position_base_palm(2) = x (2);
/// Rot1
/// X_now.pose.rotation and eigen x (3), x (4), x (5) and
/// tf::Quaternion q_q2rpy_base2palm and tf::Matrix3x3 rot_matr_base2palm
   Eigen::Matrix3d eig_rot_base2palm;
   tf::Vector3  row0 =  rot_matr_base2palm.getRow(0);
   tf::Vector3  row1 =  rot_matr_base2palm.getRow(1);
   tf::Vector3  row2 =  rot_matr_base2palm.getRow(2);

   eig_rot_base2palm(0,0) = row0.getX();
   eig_rot_base2palm(0,1) = row0.getY();
   eig_rot_base2palm(0,2) = row0.getZ();
   eig_rot_base2palm(1,0) = row1.getX();
   eig_rot_base2palm(1,1) = row1.getY();
   eig_rot_base2palm(1,2) = row1.getZ();
   eig_rot_base2palm(2,0) = row2[0];
   eig_rot_base2palm(2,1) = row2[1];
   eig_rot_base2palm(2,2) = row2[2];

/// Rot2
/// Rot2 is rotation from palm to weiss sensor
/// which is never changed
   tf::Quaternion q_q2rot_palm2weiss;
   q_q2rot_palm2weiss.setX(-0.5);
   q_q2rot_palm2weiss.setY(0.5);
   q_q2rot_palm2weiss.setZ(-0.5);
   q_q2rot_palm2weiss.setW(-0.5);
   tf::Matrix3x3 rot_matr_palm2weiss(q_q2rot_palm2weiss);
   double rollpalm2weiss, pitchpalm2weiss, yawpalm2weiss;
   rot_matr_palm2weiss.getRPY(rollpalm2weiss, pitchpalm2weiss, yawpalm2weiss);
   //   std::cout << "rollpalm2weiss = " <<rollpalm2weiss<< std::endl;
   //   std::cout << "pitchpalm2weiss = " <<pitchpalm2weiss<< std::endl;
   //   std::cout << "yawpalm2weiss = " <<yawpalm2weiss<< std::endl;
   Eigen::Matrix3d eig_rot_palm2weiss;
   row0 =  rot_matr_palm2weiss.getRow(0);
   row1 =  rot_matr_palm2weiss.getRow(1);
   row2 =  rot_matr_palm2weiss.getRow(2);
   eig_rot_palm2weiss(0,0) = row0[0];
   eig_rot_palm2weiss(0,1) = row0[1];
   eig_rot_palm2weiss(0,2) = row0[2];
   eig_rot_palm2weiss(1,0) = row1[0];
   eig_rot_palm2weiss(1,1) = row1[1];
   eig_rot_palm2weiss(1,2) = row1[2];
   eig_rot_palm2weiss(2,0) = row2[0];
   eig_rot_palm2weiss(2,1) = row2[1];
   eig_rot_palm2weiss(2,2) = row2[2];
/// [F]
/// error in feature space mapped to error in cartesian space
/// xerr is error in cartesian space
/// feats_cmd_ error in feature space
/// [F] = [selection]*[xerr]
///   6x1 vector to store selected features
   FeatursVector selected_feature;

   //selection matrix
//S_Matr_force[0][0] = (double) servo_copx;
//S_Matr_force[1][1] = (double)servo_copy;
//S_Matr_force[2][2] =(double) servo_force;
//S_Matr_force[3][3 ]= (double)servo_cocx;
//S_Matr_force[4][4 ]= (double)servo_cocy;
//S_Matr_force[5][5] = (double)servo_orient;

S_Matr_force <<servo_copx, 0, 0, 0, 0, 0,
                0, servo_copy, 0, 0, 0, 0,
                0, 0, servo_force, 0, 0, 0,
                0, 0, 0, servo_cocx, 0, 0,
                0, 0, 0, 0, servo_cocy, 0,
                0, 0, 0, 0, 0, servo_orient;

   selected_feature = S_Matr_force*xerr; // S_Matr_force S_Matr_orient  S_Matr_force_copx_copy

   //           std::cout << "selected_feature" << std::endl;
   //           std::cout << selected_feature   << std::endl;
   //           std::cout << "selected_feature" << std::endl;
/// 3x1 vector to store only changes in position
/// [F]
   Eigen::Vector3d decopled_error_in_cart_position;
   decopled_error_in_cart_position(0) = selected_feature(0);
   decopled_error_in_cart_position(1) = selected_feature(1);
   decopled_error_in_cart_position(2) = selected_feature(2);
/// [T']
/// [T'] = [T] + [rot1]*[rot2]*[F]
/// [T'] = [T] + [rot1]*[rot2]*[COPx]
/// [T'] = [T] + [rot1]*[rot2]*[COPy]
/// eig_rot_base2weiss = eig_rot_base2palm*eig_rot_palm2weiss

   Eigen::Matrix3d eig_rot_base2weiss;
   eig_rot_base2weiss = eig_rot_base2palm*eig_rot_palm2weiss;
   Eigen::Vector3d X_pos_force = transfrom_vector_position_base_palm  +
           eig_rot_base2weiss*decopled_error_in_cart_position;
///  geometry::msgs X_des_pose
/// also for markers
/// position changed according to applied force or COPx or COPy
   X_des.pose.position.x = X_pos_force(0);
   X_des.pose.position.y = X_pos_force(1);
   X_des.pose.position.z = X_pos_force(2);
/// orienttion should be same for Force COPx COPy
/// for orient COC anc COCy change
/// [rot_des] = [rot_delta]^-1 * rot_base2palm_now
/// rot2 depends on the current features for rotation
   Eigen::Vector3d decopled_error_in_cart_orientation;
   double roll_des, pitch_des, yaw_des;
   // palm: around X - COCx
   //              Y - orientation
   //              Z - COCy
   // selected_feature(3) - COCx
   // selected_feature(4) - COCy
   // selected_feature(5) - orientation
   decopled_error_in_cart_orientation(0) = selected_feature(3);
   decopled_error_in_cart_orientation(1) = selected_feature(5);
   decopled_error_in_cart_orientation(2) = selected_feature(4);
   std::cout<< "selected_feature" << selected_feature<< std::endl;
   tf::Transform transform_des_orient;
   transform_des_orient.setOrigin( tf::Vector3(X_des.pose.position.x, X_des.pose.position.y, X_des.pose.position.z) );
   tf::Quaternion q_rpy2q_des_orient;
   q_rpy2q_des_orient.setRPY(decopled_error_in_cart_orientation(0), decopled_error_in_cart_orientation(1), decopled_error_in_cart_orientation(2));
   tf::Matrix3x3 rot_matr_weiss2weiss_des(q_rpy2q_des_orient);
   tf::Matrix3x3 rot_matr_weiss2weiss_des_inverse = rot_matr_weiss2weiss_des.transpose();
   std::cout<< "rot_matr_weiss2weiss_des_inverse" << rot_matr_weiss2weiss_des_inverse.getRow(0)<< std::endl;

   tf::Matrix3x3 rot_matr_base2palm_des;
   rot_matr_base2palm.operator *=(rot_matr_weiss2weiss_des_inverse);
   rot_matr_base2palm_des.operator =(rot_matr_base2palm);
   tf::Quaternion q_base2palm_des;
   rot_matr_base2palm_des.getRotation(q_base2palm_des);

   X_des.pose.orientation.x = q_base2palm_des.getX();
   X_des.pose.orientation.y = q_base2palm_des.getY();
   X_des.pose.orientation.z = q_base2palm_des.getZ();
   X_des.pose.orientation.w = q_base2palm_des.getW();
   xd(0) = X_des.pose.position.x;
   xd(1) = X_des.pose.position.y;
   xd(2) = X_des.pose.position.z;
   xd(3) = roll_des;
   xd(4) = pitch_des;
   xd(5) = yaw_des;

   Eigen::Matrix3d eig_rot_matr_weiss2weiss_des_inverse;
   row0 =  rot_matr_weiss2weiss_des_inverse.getRow(0);
   row1 =  rot_matr_weiss2weiss_des_inverse.getRow(1);
   row2 =  rot_matr_weiss2weiss_des_inverse.getRow(2);
   eig_rot_matr_weiss2weiss_des_inverse(0,0) = row0[0];
   eig_rot_matr_weiss2weiss_des_inverse(0,1) = row0[1];
   eig_rot_matr_weiss2weiss_des_inverse(0,2) = row0[2];
   eig_rot_matr_weiss2weiss_des_inverse(1,0) = row1[0];
   eig_rot_matr_weiss2weiss_des_inverse(1,1) = row1[1];
   eig_rot_matr_weiss2weiss_des_inverse(1,2) = row1[2];
   eig_rot_matr_weiss2weiss_des_inverse(2,0) = row2[0];
   eig_rot_matr_weiss2weiss_des_inverse(2,1) = row2[1];
   eig_rot_matr_weiss2weiss_des_inverse(2,2) = row2[2];
////         /////////////
////Force end/////////////
//////////////////////////

/////////////////////////
///// COP ///////////////

   // just set selection matrix

///// end COP /////////
///////////////////////

/////////////////////////
///// orientation ///////

   // just set selection matrix

///// end COP /////////
///////////////////////

/*
   //std::cout<<"X_des2.pose.orientation.x ="<<X_des2.pose.orientation.x<<std::endl;
   //std::cout<<"X_des2.pose.orientation.y ="<<X_des2.pose.orientation.y<<std::endl;
   //std::cout<<"X_des2.pose.orientation.z ="<<X_des2.pose.orientation.z<<std::endl;
   //std::cout<<"X_des2.pose.orientation.w ="<<X_des2.pose.orientation.w<<std::endl;
   //      std::cout << "x" << std::endl;
   //      std::cout << x   << std::endl;
   //      std::cout << "x" << std::endl;
   //      std::cout << "Xnow quartenions" << std::cout <<  X_now.pose.orientation.x  <<
   //                   X_now.pose.orientation.y  << X_now.pose.orientation.z  <<
   //                   X_now.pose.orientation.w  << "Xnow quartenions" << std::endl;
   //      std::cout << "xerr" << std::endl;
   //      std::cout  << xerr << std::endl;
   //      std::cout << "xerr" << std::endl;
   //      std::cout << "eig_rot_base2palm" << std::endl;
   //      std::cout  << eig_rot_base2palm << std::endl;
   //      std::cout << "eig_rot_base2palm" << std::endl;
   //      std::cout << "eig_rot_palm2weiss" << std::endl;
   //      std::cout  << eig_rot_palm2weiss << std::endl;
   //      std::cout << "eig_rot_palm2weiss" << std::endl;

   //      std::cout << "decopled_error_in_cart_position" << std::endl;
   //      std::cout  << decopled_error_in_cart_position << std::endl;
   //      std::cout << "decopled_error_in_cart_position" << std::endl;

   //      std::cout << "decopled_error_in_cart_position" << std::endl;
   //      std::cout  << decopled_error_in_cart_position << std::endl;
   //      std::cout << "decopled_error_in_cart_position" << std::endl;
   //      std::cout << "X_pos_force" << std::endl;
   //      std::cout  << X_pos_force << std::endl;
   //      std::cout << "X_pos_force" << std::endl;
   //      std::cout << "xd" << std::endl;
   //      std::cout << xd   << std::endl;
   //      std::cout << "xd" << std::endl;
   //      std::cout << "X_des quartenions" << std::cout <<  X_des.pose.orientation.x  <<
   //                   X_des.pose.orientation.y  << X_des.pose.orientation.z  <<
   //                   X_des.pose.orientation.w  << "X_des quartenions" << std::endl;



   // std::cout << J << std::endl;
   //   std::cout << "xerr" << std::endl;
   //   std::cout  << xerr << std::endl;
   //   std::cout << "xerr" << std::endl;
   //   std::cout << "xerr_palm" << std::endl;
   //   std::cout  << xerr_palm << std::endl;
   //   std::cout << "xerr_palm" << std::endl;
   //   std::cout << "xd" << std::endl;
   //   std::cout  << xd << std::endl;
   //   std::cout << "xd" << std::endl;

   //    computeCartError(x,xd,xerr)
   //    119   // Calculate a Cartesian restoring force.
   //    120   kin_.computeCartError(x,xd,xerr);
   //    121   F = - Kp_.asDiagonal() * xerr - Kd_.asDiagonal() * xdot;
   //    122
   //    123   // Convert the force into a set of joint torques.
   //    124   tau = J.transpose() * F;
*/
}
/////////////////////////
/// IK  ////////////////
void CartTactCOntr::getIK(){
    //++FILL REQUEST
    // define the service messages
    // gpik_req.timeout = ros::Duration(5.0);
    gpik_req.ik_request.timeout = ros::Duration(5.0);

    gpik_req.ik_request.ik_link_name = "palm";
    gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
    gpik_req.ik_request.pose_stamped.pose.position.x = X_des.pose.position.x;// x;// 0.526;//0.36513;
    gpik_req.ik_request.pose_stamped.pose.position.y = X_des.pose.position.y;//
    gpik_req.ik_request.pose_stamped.pose.position.z = X_des.pose.position.z;//1.0913;

    //pos is not relevant with the used ik_solver but set it anyway to identity
    gpik_req.ik_request.pose_stamped.pose.orientation.x = X_des.pose.orientation.x;//0.70701;
    gpik_req.ik_request.pose_stamped.pose.orientation.y = X_des.pose.orientation.y;//-0.023199;
    gpik_req.ik_request.pose_stamped.pose.orientation.z = X_des.pose.orientation.z;//0.70671;
    gpik_req.ik_request.pose_stamped.pose.orientation.w = X_des.pose.orientation.w;//0.012863;
    gpik_req.ik_request.robot_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    //    gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    gpik_req.ik_request.robot_state.joint_state.name = response.kinematic_solver_info.joint_names;
    if(ik_client.call(gpik_req, gpik_res))
    {
        if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
        {
            std_msgs::Float64 message;
            for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
            {
                ROS_INFO("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
                //                ROS_INFO("we publish to %s",pub[i].getTopic().c_str());
                joints_commands.at(i) = (gpik_res.solution.joint_state.position[i]);
                message.data= (double)gpik_res.solution.joint_state.position[i];

             }
            isready = true;
            for (int i = 0; i < 6; ++i){
                if (joints_commands.at(i) > 5.0) {
                    isready = false;
                }
                if (joints_commands.at(i) < -5.0) {
                    isready = false;
                }
            }


            //            std::cout<< "soluton for joint name " << gpik_res.solution.joint_state.name.at(0)<< std::endl;
            //            std::cout<< "soluton for " <<gpik_res.solution.joint_state.position.at(0)<<std::endl;
        }
        else
        {
            ROS_INFO("Inverse kinematics failed");
            std::cout << "error = "<< gpik_res.error_code.val <<std::endl;
        }
    }
    else{
        ROS_INFO("Inverse kinematics service call failed %d",gpik_res.error_code.val);
    }
}

void CartTactCOntr::pub_res(){
    std_msgs::Float64 single_command;
    
    X_des.header.stamp = ros::Time::now();
    X_des.header.frame_id = "desired_pose";
    X_now.header.stamp = ros::Time::now();
    pose_now_pub.publish(X_now);
    pose_des_pub.publish(X_des);
    
     if ((is_feature_rec)&&(isready) && (begin_servo_controller)){
         for (int i = 0; i < this->Publishers_joints.size();++i){
             std::cout<<" joints_commands = "<<joints_commands.at(i)<<std::endl;
         }
         for (int i = 0; i < this->Publishers_joints.size();++i){
             single_command.data = joints_commands.at(i);
		joint_commands_last_good.push_back(joints_commands.at(i));
             this->Publishers_joints[i].publish(single_command);
         }
// 
// 	    
         isready = false;
// 
        begin_servo_controller = false;
         is_feature_rec = false;
	is_there_at_least_one_succes = true;
     }
	if (is_there_at_least_one_succes){
		for (int i = 0; i < this->Publishers_joints.size();++i){
             	single_command.data = joint_commands_last_good.at(i);
             	this->Publishers_joints[i].publish(single_command);
         }

	}
}

// set pid gains
void CartTactCOntr::cb_dynamic_reconf(tactile_controller::feats_pid_paramsConfig &config, uint32_t level){
    Pcpx = config.Pcpx;
    Icpx = config.Icpx;
    Dcpx = config.Dcpx;
    Deadcpx = config.Deadcpx;
    Kp_(0) = config.Pcpx;Kd_(0) = config.Dcpx;Ki_(0) = config.Icpx;
    Kp_(1) = config.Pcpy;Kd_(1) = config.Dcpy;Ki_(1) = config.Icpy;
    Kp_(2) = config.Pfz; Kd_(2) = config.Dfz; Ki_(2) = config.Ifz;
    Kp_(3) = config.Pccx;Kd_(3) = config.Dccx;Ki_(3) = config.Iccx;
    Kp_(4) = config.Pccy;Kd_(4) = config.Dccy;Ki_(4) = config.Iccy;
    Kp_(5) = config.Por; Kd_(5) = config.Dor; Ki_(5) = config.Ior;

    servo_copx = config.servo_copx;
    servo_copy = config.servo_copy;
    servo_force = config.servo_force;
    servo_cocx = config.servo_cocx;
    servo_cocy = config.servo_cocy;
    servo_orient = config.servo_orient;
}

// SHOW_MARKERS - rviz markers
void CartTactCOntr::markers_pub(){
    std::vector<float> color_now, color_des;
    std::vector<float> scale_default;
    static const float red[] = {255.0,0.0,0.0};
    static const float green[] = {0.0,255.0,0.0};
    static const float def_scales[] = {0.1,0.01,0.01};
    color_now.assign(red, red + sizeof(red) / sizeof(red[0]) );
    color_des.assign(green, green + sizeof(green) / sizeof(green[0]) );
    scale_default.assign(def_scales, def_scales + sizeof(def_scales) / sizeof(def_scales[0]));

    //  marker current pose
    visualization_msgs::Marker pose_now_marker = create_marker(X_now,
                                                               "shadowarm_base",
                                                              color_now,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_now_marker_pub.publish(pose_now_marker);



    //  marker des pose
    visualization_msgs::Marker pose_des_marker = create_marker(X_des,
                                                               "shadowarm_base",
                                                              color_des,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_des_marker_pub.publish(pose_des_marker);
/// suplementaty axis
    /// second arrow of des position
    /// geometry::msgs type for markers second arrow
    /// position is the same
    /// rot1*rot2
    tf::Quaternion q_q2rpy_base2palm;
    q_q2rpy_base2palm.setX(X_des.pose.orientation.x);
    q_q2rpy_base2palm.setY(X_des.pose.orientation.y);
    q_q2rpy_base2palm.setZ(X_des.pose.orientation.z);
    q_q2rpy_base2palm.setW(X_des.pose.orientation.w);
    tf::Matrix3x3 rot_matr_base2palm(q_q2rpy_base2palm);
    double roll_base2palm, pitch_base2palm, yaw_base2palm;
    rot_matr_base2palm.getRPY(roll_base2palm, pitch_base2palm, yaw_base2palm);
    X_des2.pose.position.x = X_des.pose.position.x;
    X_des2.pose.position.y = X_des.pose.position.y;
    X_des2.pose.position.z = X_des.pose.position.z;
    // rot1*rot2
    // rot1 = rot from base to palm now rot2 = 0 0 90
    tf::Transform transform2;
    transform2.setOrigin( tf::Vector3(X_des2.pose.position.x, X_des2.pose.position.y, X_des2.pose.position.z) );
    tf::Quaternion q_rpy2q2;
    q_rpy2q2.setRPY(roll_base2palm, pitch_base2palm, yaw_base2palm);
    tf::Matrix3x3 rot_matr_base2palm_original(q_rpy2q2);
    tf::Transform transform3;
    transform3.setOrigin( tf::Vector3(X_des2.pose.position.x, X_des2.pose.position.y, X_des2.pose.position.z) );
    tf::Quaternion q_rpy2q_suplement;
    q_rpy2q_suplement.setRPY(0, 3.13, 1.57);
    tf::Matrix3x3 rot_matr_base2palm_suplement(q_rpy2q_suplement);
    tf::Matrix3x3 rot_matr_base2palm_suplement_final;
    rot_matr_base2palm_suplement_final = rot_matr_base2palm_original*rot_matr_base2palm_suplement;
    double row_suplem, pitch_suplem, yaw_sulplem;
    rot_matr_base2palm_suplement_final.getRPY(row_suplem, pitch_suplem, yaw_sulplem);
    tf::Quaternion q_rpy2q_now_suplem;
    q_rpy2q_now_suplem.setRPY(row_suplem, pitch_suplem, yaw_sulplem);
    tf::Transform transform_now_suplem;
    transform_now_suplem.setOrigin( tf::Vector3(X_des2.pose.position.x, X_des2.pose.position.y, X_des2.pose.position.z) );
    transform_now_suplem.setRotation(q_rpy2q_now_suplem);
    X_des2.pose.orientation.x = q_rpy2q_now_suplem.getX();
    X_des2.pose.orientation.y = q_rpy2q_now_suplem.getY();
    X_des2.pose.orientation.z = q_rpy2q_now_suplem.getZ();
    X_des2.pose.orientation.w = q_rpy2q_now_suplem.getW();
    /// second arrow of des position
    /// geometry::msgs type for markers second arrow
    /// position is the same
    /// rot1*rot2
    X_des3.pose.position.x = X_des.pose.position.x;
    X_des3.pose.position.y = X_des.pose.position.y;
    X_des3.pose.position.z = X_des.pose.position.z;
    // rot1*rot2
    // rot1 = rot from base to palm now rot2 = 0 0 90
    tf::Transform transform3_des3;
    transform3_des3.setOrigin( tf::Vector3(X_des3.pose.position.x, X_des3.pose.position.y, X_des3.pose.position.z) );
    tf::Quaternion q_rpy2q_suplement_2;
    q_rpy2q_suplement_2.setRPY(0, 1.57, 0);
    tf::Matrix3x3 rot_matr_base2palm_suplement_2(q_rpy2q_suplement_2);
    tf::Matrix3x3 rot_matr_base2palm_suplement_2_final;
    rot_matr_base2palm_suplement_2_final = rot_matr_base2palm_original*rot_matr_base2palm_suplement_2;
    double row_suplem2, pitch_suplem2, yaw_sulplem2;
    rot_matr_base2palm_suplement_2_final.getRPY(row_suplem2, pitch_suplem2, yaw_sulplem2);
    tf::Quaternion q_rpy2q_now_suplem2;
    q_rpy2q_now_suplem2.setRPY(row_suplem2, pitch_suplem2, yaw_sulplem2);
    tf::Transform transform_now_suplem2;
    transform_now_suplem2.setOrigin( tf::Vector3(X_des3.pose.position.x, X_des3.pose.position.y, X_des3.pose.position.z) );
    transform_now_suplem2.setRotation(q_rpy2q_now_suplem2);
    X_des3.pose.orientation.x = q_rpy2q_now_suplem2.getX();
    X_des3.pose.orientation.y = q_rpy2q_now_suplem2.getY();
    X_des3.pose.orientation.z = q_rpy2q_now_suplem2.getZ();
    X_des3.pose.orientation.w = q_rpy2q_now_suplem2.getW();

    //  marker des pose second arrow for orient

    visualization_msgs::Marker pose_des2_marker = create_marker(X_des2,
                                                               "shadowarm_base",
                                                              color_des,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_des2_marker_pub.publish(pose_des2_marker);

    visualization_msgs::Marker pose_des3_marker = create_marker(X_des3,
                                                               "shadowarm_base",
                                                              color_des,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_des3_marker_pub.publish(pose_des3_marker);

    //  marker descheck RPY
    X_now_check_RPY.pose.position.x = x(0);
    X_now_check_RPY.pose.position.y = x(1);
    X_now_check_RPY.pose.position.z = x(2);
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(X_now_check_RPY.pose.position.x, X_now_check_RPY.pose.position.y, X_now_check_RPY.pose.position.z) );
    tf::Quaternion q_rpy2q;
    q_rpy2q.setRPY(x(3), x(4), x(5));
    transform.setRotation(q_rpy2q);
    X_now_check_RPY.pose.orientation.x = q_rpy2q.getX();
    X_now_check_RPY.pose.orientation.y = q_rpy2q.getY();
    X_now_check_RPY.pose.orientation.z = q_rpy2q.getZ();
    X_now_check_RPY.pose.orientation.w = q_rpy2q.getW();

    visualization_msgs::Marker pose_now_check_RPY_marker = create_marker(X_now_check_RPY,
                                                               "shadowarm_base",
                                                              color_now,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_now_check_rpy_marker_pub.publish(pose_now_check_RPY_marker);
    ///////////////////////////
    ///  marker current pose///
    ///      y arrow        ///
    geometry_msgs::PoseStamped X_now_y;
    X_now_y = X_now;
    tf::Quaternion q_q2rpy_now_y;
    q_q2rpy_now_y.setX(X_now_y.pose.orientation.x);
    q_q2rpy_now_y.setY(X_now_y.pose.orientation.y);
    q_q2rpy_now_y.setZ(X_now_y.pose.orientation.z);
    q_q2rpy_now_y.setW(X_now_y.pose.orientation.w);
    // q2rpy
    tf::Matrix3x3 m_now_y(q_q2rpy_now_y);
    double roll, pitch, yaw;
    m_now_y.getRPY(roll, pitch, yaw);
    // rpy2q
    tf::Transform transform_now_y;
    transform_now_y.setOrigin( tf::Vector3(X_now_y.pose.position.x, X_now_y.pose.position.y, X_now_y.pose.position.z) );
    tf::Quaternion q_rpy2q_now_y;
    q_rpy2q_now_y.setRPY(roll + 1.57, pitch+1.57, yaw);
    transform_now_y.setRotation(q_rpy2q_now_y);
    X_now_y.pose.orientation.x = q_rpy2q_now_y.getX();
    X_now_y.pose.orientation.y = q_rpy2q_now_y.getY();
    X_now_y.pose.orientation.z = q_rpy2q_now_y.getZ();
    X_now_y.pose.orientation.w = q_rpy2q_now_y.getW();

    visualization_msgs::Marker pose_now_y_marker = create_marker(X_now_y,
                                                               "shadowarm_base",
                                                              color_now,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_now_y_marker_pub.publish(pose_now_y_marker);
    ///      z arrow        ///
    geometry_msgs::PoseStamped X_now_z;
    X_now_z = X_now;
    tf::Quaternion q_q2rpy_now_z;
    q_q2rpy_now_z.setX(X_now_z.pose.orientation.x);
    q_q2rpy_now_z.setY(X_now_z.pose.orientation.y);
    q_q2rpy_now_z.setZ(X_now_z.pose.orientation.z);
    q_q2rpy_now_z.setW(X_now_z.pose.orientation.w);
    // q2rpy
    tf::Matrix3x3 m_now_z(q_q2rpy_now_z);
    m_now_z.getRPY(roll, pitch, yaw);
    // rpy2q
    tf::Transform transform_now_z;
    transform_now_z.setOrigin( tf::Vector3(X_now_z.pose.position.x, X_now_z.pose.position.y, X_now_z.pose.position.z) );
    tf::Quaternion q_rpy2q_now_z;
    q_rpy2q_now_z.setRPY(roll, pitch+1.57, yaw+1.57);
    transform_now_z.setRotation(q_rpy2q_now_z);
    X_now_z.pose.orientation.x = q_rpy2q_now_z.getX();
    X_now_z.pose.orientation.y = q_rpy2q_now_z.getY();
    X_now_z.pose.orientation.z = q_rpy2q_now_z.getZ();
    X_now_z.pose.orientation.w = q_rpy2q_now_z.getW();

    visualization_msgs::Marker pose_now_z_marker = create_marker(X_now_z,
                                                               "shadowarm_base",
                                                              color_now,
                                                              visualization_msgs::Marker::ARROW,
                                                              scale_default);
    pose_now_z_marker_pub.publish(pose_now_z_marker);
    ///      arrows       ///
    ///////////////////////////
}
// SHOW_MARKERS - rviz markers
visualization_msgs::Marker CartTactCOntr::create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                                      std::string frame_name_,
                                                      std::vector<float> color_,
                                                      u_int32_t shape_,
                                                      std::vector<float> scale_){
    u_int32_t shape = visualization_msgs::Marker::ARROW;
    shape = shape_;
//    shape = visualization_msgs::Marker::SPHERE;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/"+frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "orientations";
    marker.id = 0;
    marker.type = shape;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = point_to_viz_.pose.position.x;
    marker.pose.position.y = point_to_viz_.pose.position.y;
    marker.pose.position.z = point_to_viz_.pose.position.z;

    marker.pose.orientation.x = point_to_viz_.pose.orientation.x;
    marker.pose.orientation.y = point_to_viz_.pose.orientation.y;
    marker.pose.orientation.z = point_to_viz_.pose.orientation.z;
    marker.pose.orientation.w = point_to_viz_.pose.orientation.w;

    marker.scale.x = scale_.at(0);
    marker.scale.y = scale_.at(1);
    marker.scale.z = scale_.at(2);
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = color_.at(0);
    marker.color.g = color_.at(1);
    marker.color.b = color_.at(2);

    return marker;
}


// transform error in sensor frame to error in palm frame (end_effector)
geometry_msgs::PoseStamped CartTactCOntr::convert_pose(geometry_msgs::PoseStamped& pose_in_){
    geometry_msgs::PoseStamped pose_out;
    tf_listener.waitForTransform("shadowarm_base", ros::Time(0), "weiss",ros::Time(0), "world", ros::Duration(10));


    try{
        tf_listener.transformPose("shadowarm_base", pose_in_, pose_out);
        ROS_INFO("position at weiss: (%.4f, %.4f. %.4f) -----> at shadowarm_base: (%.4f, %.4f, %.4f) at time %.4f",
                 pose_in_.pose.position.x, pose_in_.pose.position.y, pose_in_.pose.position.z,
                 pose_out.pose.position.x, pose_out.pose.position.y, pose_out.pose.position.z, pose_out.header.stamp.toSec());
        ROS_INFO("orientation at weiss: (%.4f, %.4f, %.4f, %.4f,) -----> at shadowarm_base: (%.4f, %.4f, %.4f, %.4f) at time %.4f",
                 pose_in_.pose.orientation.x, pose_in_.pose.orientation.y, pose_in_.pose.orientation.z,pose_in_.pose.orientation.w ,
                 pose_out.pose.orientation.x, pose_out.pose.orientation.y, pose_out.pose.orientation.z, pose_out.pose.orientation.w, pose_out.header.stamp.toSec());

        return pose_out;
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return pose_in_;
    }
}

void CartTactCOntr::print(){
//    std::cout<<" begin "<<std::endl;
//    for (int i = 0; i < 6; ++i){
//        std::cout<<"joint reads "<<joint_reads.at(i)<<std::endl;
//    }
//    std::cout<<" end "<<std::endl;
    std::cout<<" copx_fb = "<< copx_fb <<std::endl;
    std::cout<<" orientz_des = "<< orientz_des <<std::endl;
    ROS_INFO(" x = %f y = %f  z = %f",
             X_now.pose.position.x, X_now.pose.position.y,X_now.pose.position.z);
    ROS_INFO(" qx = %f qy = %f  qz = %f  qw = %f",
             X_now.pose.orientation.x, X_now.pose.orientation.y,
             X_now.pose.orientation.z, X_now.pose.orientation.w);
    ROS_INFO(" header %s",
             X_now.header.frame_id.c_str());
}

/// service to start motion
bool CartTactCOntr::start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req, tactile_servo_srvs::start_servo_controller::Response &res){
 //begin_servo_controller = req.begin_controller;
	begin_servo_controller = true;
    return true;
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "cart_tact_contr");
    ros::NodeHandle n;

    ros::Rate loop_rate(100);
    CartTactCOntr CartTactCOntr1;

    while( ros::ok() ){
//        CartTactCOntr1.print();
        CartTactCOntr1.getFK();
        CartTactCOntr1.markers_pub();
        CartTactCOntr1.matrix_operations();
        CartTactCOntr1.getIK();
        CartTactCOntr1.pub_res();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
/*
   //map [Xerr] to end effector frame
//    X_err_world = AdTgs*Xerr
   X_err_frame.pose.position.x = xerr(0);
   X_err_frame.pose.position.y = xerr(1);
   X_err_frame.pose.position.z = xerr(2);
   tf::Transform transform_x_err;
   transform_x_err.setOrigin( tf::Vector3(X_err_frame.pose.position.x, X_err_frame.pose.position.y, X_err_frame.pose.position.z) );
   tf::Quaternion q_rpy2q_xerr;
   q_rpy2q_xerr.setRPY(xerr(3), xerr(4), xerr(5));
   transform_x_err.setRotation(q_rpy2q_xerr);
   X_err_frame.pose.orientation.x = q_rpy2q_xerr.getX();
   X_err_frame.pose.orientation.y = q_rpy2q_xerr.getY();
   X_err_frame.pose.orientation.z = q_rpy2q_xerr.getZ();
   X_err_frame.pose.orientation.w = q_rpy2q_xerr.getW();
   X_err_frame.header.frame_id = "weiss";
   X_err_frame.header.stamp = ros::Time();
    // error (contact frame) wrt base of shadow arm
   X_err_palm = convert_pose(X_err_frame);

   // rewrite X_err_frame into matrix eigen format
   xerr_palm(0) = X_err_palm.pose.position.x;
   xerr_palm(1) = X_err_palm.pose.position.y;
   xerr_palm(2) = X_err_palm.pose.position.z;
   tf::Quaternion q_q2rpy_xerr;
   q_q2rpy_xerr.setX(X_err_palm.pose.orientation.x);
   q_q2rpy_xerr.setY(X_err_palm.pose.orientation.y);
   q_q2rpy_xerr.setZ(X_err_palm.pose.orientation.z);
   q_q2rpy_xerr.setW(X_err_palm.pose.orientation.w);
   tf::Matrix3x3 m_xerr(q_q2rpy_xerr);
   double roll_xerr, pitch_xerr, yaw_xerr;
   m_xerr.getRPY(roll_xerr, pitch_xerr, yaw_xerr);
   xerr_palm(3) = roll_xerr;
   xerr_palm(4) = pitch_xerr;
   xerr_palm(5) = yaw_xerr; */

/*
 *


   tf_listener.waitForTransform("shadowarm_base", ros::Time(0), "palm",ros::Time(0), "world", ros::Duration(10));

   tf::StampedTransform transform_base_palm;
   //from world to weiss
   try{
       tf_listener.lookupTransform("/shadowarm_base", "/palm",
                                ros::Time(0), transform_base_palm);
   }
   catch (tf::TransformException ex){
       ROS_ERROR("%s",ex.what());
       ros::Duration(1.0).sleep();
   }
   std::cout<<"transformation frame name = "<<transform.frame_id_<<std::endl;
//   getOrigin ()
//       Return the origin vector translation.


   std::cout<<transform.getOrigin().getX()<<std::endl;
   std::cout<<transform.getOrigin().getY()<<std::endl;
   transform.getRotation().getAngle();
*/
