#ifndef PPS_HEADER_HPP_
#define PPS_HEADER_HPP_
// helps to keep more stable the data because the data arrives with high peaks, neverthless when filtering data this may be removed
#define MIN_PRESSURE_TH 1
#define	MIN_FORCE_TH 500
#define MAX_PRESSURE_TH 20
#define MAX_FORCE_TH 6000
#endif
