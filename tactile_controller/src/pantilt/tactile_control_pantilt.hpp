#ifndef TACTILE_CONTROL_PANTILT_HPP_
#define TACTILE_CONTROL_PANTILT_HPP_
#define ERROR_PRESSURE	0.1					// to stabilize close to the desired value
#define ERROR_FORCE	10			  
#include "tactile_header_pantilt.hpp" 
#include "pantilt.hpp" 
#include "pps.hpp" 

class Tactile_pantilt {
  public:
    Tactile_pantilt();
    bool init();

  private:
    ros::NodeHandle n, nh_private;  
      
    float tilt_val, pan_val, prev_pan_val,prev_f_orientation, prev_tilt_val ;	
	// tactile pressure
	float control_pressure_ (float ap, float dp, float x, float y)
	{
	  float f_pressure = ap;
	  float d_pressure = dp;
	  float error_pressure = d_pressure - f_pressure;
	  float f_offset_x = x;
	  float f_offset_y = y;
	  
	  if (f_pressure > MIN_PRESSURE_TH) 
		{ // if force more than threshold, then keep a contact force 
		  float kp = 0.1;
		  
		  if (abs(d_pressure - f_pressure) < ERROR_PRESSURE ) 
		  { 
			float error_pressure = 0;
		  }
		  else  
		  { 
			float error_pressure = d_pressure - f_pressure;
		  }  
		  
		  float control_term =  -kp * error_pressure * (M_PI/180); // the control term is added in radians

		  if(f_offset_y>0) tilt_val += control_term;
		  if(f_offset_y<0) tilt_val -= control_term;
		  /// check limits
		  if(tilt_val<-TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
		  if(tilt_val>TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
		} 
	  prev_tilt_val = tilt_val;
	  return tilt_val;
	}
	
	// tactile force	
    float control_force_ (float af, float df, float x, float y)
    {
	  float f_force = af;
	  float d_force = df;
	  float error_force = d_force - f_force;
	  float f_offset_x = x;
	  float f_offset_y = y;
	  	  
	  if (f_force > MIN_FORCE_TH) 
      { // if force more than threshold, then keep a contact force d1500
        float kp = 0.0003;
        if (abs(d_force - f_force) < ERROR_FORCE )
        { 
          float error_force = 0;
        }
        else  
        { 
          float error_force = d_force - f_force;
        }    
		float control_term =  -kp * error_force * (M_PI/180); 
        if(f_offset_y>0) tilt_val += control_term;
        if(f_offset_y<0) tilt_val -= control_term;
        if(tilt_val<-TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
        if(tilt_val>TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
        return tilt_val;
	    }
      }
        
        
    // tactile orientation   
	float control_orientation_ (float ac_o, float d_o)
	{
	  float f_orientation = ac_o;
	  float d_orientation = d_o; 
	  float error_orientation = f_orientation - d_orientation;
	  if (abs(f_orientation) > 100) f_orientation = prev_f_orientation;
	  float kp = 0.1;
	  float control_term = kp * error_orientation * (M_PI/180);
	  pan_val += control_term;
	  if(pan_val<-PAN_JOINT_LIMIT) pan_val = prev_pan_val;
	  if(pan_val>PAN_JOINT_LIMIT)  pan_val = prev_pan_val;
	  prev_pan_val = pan_val;
	  prev_f_orientation = f_orientation;
	  return pan_val;
	}
		
	// c	
	float control_offset_x_ (float x, float d_x)
	{
	  float f_offset_x = x;
	  float d_offset_x = d_x;
	  float error = d_offset_x - f_offset_x;
	  float kp1 = 0.1;
      float control_term1 =  kp1 * (error) * (M_PI/180); 
	  pan_val += control_term1;
	  if(pan_val<-PAN_JOINT_LIMIT) pan_val = prev_pan_val;
	  if(pan_val>PAN_JOINT_LIMIT)  pan_val = prev_pan_val;
	  prev_pan_val = pan_val;
	  return pan_val;
    }    
    
    
    float control_offset_y_ (float y, float d_y)
	{
	  float f_offset_y = y;
	  float d_offset_y = d_y;
	  float error = d_offset_y - f_offset_y;
      float kp2 = 0.15;
      float control_term2 =  -kp2 * (error) * (M_PI/180); 
	  tilt_val += control_term2;    
	  if(tilt_val<-TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
	  if(tilt_val>TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
      prev_tilt_val = tilt_val; 
      return tilt_val;
    }
    
    
    float balance_two_contacts_ (float ap, float ap2)
	{
	  float f_pressure = ap;
	  float f_pressure_2 = ap2;

	  float error_pressure = f_pressure_2 - f_pressure;
	 
	  
	  if (f_pressure > MIN_PRESSURE_TH) 
		{ // if force more than threshold, then keep a contact force 
		  float kp = 0.1;
		  
		  if (abs(error_pressure) < ERROR_PRESSURE ) 
		  { 
			float error_pressure = 0;
		  }
		  else  
		  { 
			float error_pressure = f_pressure_2 - f_pressure; // here I get error of pressure btw two points, but
			// pantilt does not move. also is it possible to add  f_pressure_2 to the massge so that we can plot, you need to tune the parameters with the plot that is the easiest way
			// just modify the servo message for the second. ok. and how are you? can we do test of the corner servo? let me finish zhanat 
			// I have the orientations check.
			// I can not call you.wait i will turn on the ceellphone
		  }  
		  
		  float control_term =  -kp * error_pressure * (M_PI/180); // the control term is added in radians


		  tilt_val += control_term;
		   
		  /// check limits
		  if(tilt_val<-TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
		  if(tilt_val>TILT_JOINT_LIMIT) tilt_val = prev_tilt_val;
		} 
	  prev_tilt_val = tilt_val;
	  return tilt_val;
	}   

        
};
#endif
