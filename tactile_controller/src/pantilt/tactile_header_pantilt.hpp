#ifndef TACTILE_HEADER_PANTILT_HPP_
#define TACTILE_HEADER_PANTILT_HPP_

/// ROS LIBRARIES
#include <urdf/model.h>
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include "sensor_msgs/JointState.h"

/// UTILS LIBRARIES
#include <iostream>
#include <string>
#include <cmath>        												// std::abs

#include <boost/scoped_ptr.hpp>
#include <stdio.h>

//#include <tactile_control_pan_tilt/servo.h>	
//#include "cv_features_extractor/featuresControl.h"	
//#include "cv_features_extractor/featuresPlanning.h"	
#include <tactile_servo_msgs/servo.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/featuresPlanning.h>



using std::cout;
using std::string;
#endif
