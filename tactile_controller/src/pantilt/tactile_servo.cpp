/**
  * Node:tactile_servo.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	Subscribe to controlling features and to pan tilt dynamixels joint states
  * 	and publish the motor commands to the pantilt setup (position data used by the dynamixels manager). 
  *     It also publish servo output data for plotting as topics (rqt_gui) 
 */
#include "tactile_header_pantilt.hpp"  
#include "tactile_control_pantilt.hpp"  // the feature controller class 

tactile_servo_msgs::servo servo_state;

/*************************
 * Global variables
 ************************/
 
/// To send the motor commands 
std_msgs::Float64 torque1;
std_msgs::Float64 torque2;

/// Callbacks
/// Positions, velocities and errors
float pan_real, tilt_real; 

/// Features variables of actual state
float f_force, f_pressure, f_pressure_2, f_orientation, f_offset_x, f_offset_y, f_orient_corner;
bool gotFeatures;

///for planing
string contactType_temp;

  int counter1, counter2, counter2_2; 
  
Tactile_pantilt::Tactile_pantilt(): nh_private ("~") 
{
}
		
void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
	pan_real = msg->position[0];
	tilt_real = msg->position[1];
}

/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features [f,p,alfa,x,y]	
void controlCallback(const tactile_servo_msgs::featuresControl::ConstPtr& featuresC) 
{

  float aux, f_acc, p_acc, p_acc_2;   

  if ( featuresC->contactForce.size() > 0 ) 
  { 
	float f_temp = featuresC->contactForce[0];	//store in temporal variable	
	  if (f_temp < MAX_FORCE_TH)  //verify peaks
	  { // average filter it (may improve by another filter)
		f_acc += f_temp ;
		counter1++;
		float avg_val = 2; // the number of total samples

		if (counter1 >= avg_val) 
		{
		  f_force = f_acc / counter1; 
		  counter1 = 0;
		}
	 } 
	 // here also check size of offset x and y arrays and if equal two then extract [0] and [1]. why I need to check the size?. If you try to extrac [0] and [1]
	 // and your array has only one element you will have an error when executing . not in debugging but while executing. ok. if I want to plot here these two CoP just
	 f_offset_x = featuresC->contactOffset_x[0];	// here you have already extract the first one
	 
	 f_offset_y = featuresC->contactOffset_y[0];	// oh, great, I see now. There is also message type for FeatureForPlannin - we do not use them now? for corners It will be needed
	 // ok. Galo, I will work in this now. ok let me know if you find troubles. )))) I am a big trouble. I should have learned programming before.:) you will got it soon. The mail with your
	 // grades should arrive within 2 days. ok zhanta thank you very much!!! nono. Galo, at 20.15 I go back to home. Please, do corner to control )) if possible, then tomorrow we servo the corner
	 // pk i will try it tonight and tomorrow morning otherwise is very slow communication. ok
	 //f_offset_x = featuresC->contactOffset_x[1];
	 //f_offset_y = featuresC->contactOffset_x[1];
	 
	 float o_temp = featuresC->contactOrientation[0];
	 if(o_temp < 90 && o_temp > -90)  f_orientation = o_temp; //servo just in 180 degrees since it is  equivalent to servo 360 degrees
     //f_orient_corner = featuresC->contactOrientation_corner[0] - 90; 
	 
	 float p_temp = featuresC->contactPressure[0];
	// float p_temp_2 = featuresC->contactPressure[1];	
	 /// For two contact points
	/// check the size of contact pressure array , if size is 2 we extract [0] [1]
	/// For two contact points
	//cout << " size of pressure vector"<< featuresC->contactPressure.size() << std::endl;
	 //cout << "  pressure 2"<< featuresC->contactPressure[1] << std::endl;
	 
	 if (p_temp < MAX_PRESSURE_TH) 
	 {
	   p_acc += p_temp ;
	   counter2++;
	   float avg_val = 2;
	   if (counter2 >= avg_val) 
	   {
		 f_pressure = p_acc / counter2;
		 counter2 = 0;
		 p_acc = 0;
	   }
	 }
	 f_pressure = featuresC->contactPressure[0];
	 //cout << " size pressure " << featuresC->contactPressure.size()<< std::endl;
	 f_pressure_2 = featuresC->contactPressure[1];	 //if (featuresC->contactPressure.size() == 2)
	 //~ if (p_temp_2 < MAX_PRESSURE_TH) 
	 //~ {
	   //~ p_acc_2 += p_temp_2 ;
	   //~ counter2_2++;
	   //~ float avg_val = 2;
	   //~ if (counter2_2 >= avg_val) 
	   //~ {
		 //~ f_pressure_2 = p_acc / counter2_2;
		 //~ counter2_2 = 0;
		 //~ p_acc_2 = 0;
	   //~ }
	 //~ }
	 gotFeatures = true;
	// cout << "  f_pressure_2  "<< f_pressure_2 << std::endl;
  }  
} 


/// Hear msg type from topic "tilt_pan/Features/planningFeatures" and store the features for plan tactile control
void planingCallback(const tactile_servo_msgs::featuresPlanning::ConstPtr& featuresP) 
{
	if ( featuresP->contactsNo > 0 ) {	
		//int contacts_number = featuresP->contactsNo;	
	//	for	(unsigned int i = 0 ; i < featuresP->contactsNo ; i++ ) 
	//	{
		 // contactType_temp[0] = featuresP->contactTypes[i];	
	//	}
		//featuresUpdated=true;
		//ROS_DEBUG("Tactile Planner: I heard the features... ");
		//contactType_temp = featuresP->contactTypes[0];
		
	} 
} 

bool Tactile_pantilt::init() {
  if (gotFeatures)
  {
	// this should come from the high level planner  check this program
	// --------------------------------------------------
    string servoTask = "two_points_balancing"; 
    //string servoTask;
    //if (contactType_temp == "small_point" ) servoTask ="pressure";
    //if (contactType_temp == "large_point" ) servoTask ="force";
    float d_pressure =  13;
    float d_force = 2600;
    float d_orientation = 0; 
    float d_offset_x = 0;
    float d_offset_y = 0;
	//-------------------------------------------------
    
    if (servoTask=="pressure")      
    {
	// cout << "doing pressure control " << std::endl;	
	  float tilt = control_pressure_(f_pressure, d_pressure, f_offset_x, f_offset_y );
//	  cout << "tilt is " << tilt << std::endl;
      if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	}
    else if (servoTask=="force")   
    {
	//	cout << "doing force " << std::endl;
	  float tilt = control_force_(f_force, d_force, f_offset_x, f_offset_y );
	//  cout << "tilt is " << tilt << std::endl;
	  if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	}
    else if (servoTask=="orientation")  
    {
	  float pan = control_orientation_(f_orientation, d_orientation);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
    else if (servoTask=="offset_x")  
    {
	  float pan = control_offset_x_(f_offset_x, d_offset_x);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
	else if (servoTask=="offset_y")  
    {
	  float tilt = control_offset_y_(f_offset_y, d_offset_y);
      if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	}
	else if (servoTask=="pressure_orientation")  
    {
	  float tilt = control_pressure_(f_pressure, d_pressure, f_offset_x, f_offset_y );
      if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	  float pan = control_orientation_(f_orientation, d_orientation);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
    else if (servoTask=="force_orientation")  
    {
	  float tilt = control_force_(f_force, d_force, f_offset_x, f_offset_y );
	  if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	  float pan = control_orientation_(f_orientation, d_orientation);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
    else if (servoTask=="offset_xy")  
    {
	  float pan = control_offset_x_(f_offset_x, d_offset_x);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	  float tilt = control_offset_y_(f_offset_y, d_offset_y);
      if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	}
	else if (servoTask=="two_points_orient")
	{
	  float pan = control_orientation_(f_orientation, d_orientation);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
	else if (servoTask=="two_points_balancing")
	{
	  cout << "doing two point control " << std::endl;	
	  float tilt = balance_two_contacts_(f_pressure, f_pressure_2);
//	  cout << "tilt is " << tilt << std::endl;
      if ( abs(tilt) < TILT_JOINT_LIMIT) torque2.data = tilt;
	}
	 else if (servoTask=="orientation_corner")  
    {
	  float pan = control_orientation_(f_orient_corner, d_orientation);
      if ( abs(pan) < PAN_JOINT_LIMIT) torque1.data = pan;
	}
		
	/// For tow contact points need to add function and logic in tactile_control_pantilt.hpp
	
	// so here I need to use contactOffset_y[0] and contactOffset_y[1]? kind of.
	
    ///for publish plot data 
    // you can do it here. but it is better to do it in the feature extractor. you can create new image with just cop
    // if you do it here then you will need to add libraries. ok.
    servo_state.actual_orientation = f_orientation; 
    servo_state.actual_pressure = f_pressure; 
    servo_state.actual_force = f_force; 
    servo_state.actual_offset_x = f_offset_x;  
    servo_state.actual_offset_y = f_offset_y;
    servo_state.actual_pressure2 = f_pressure_2;
    
    servo_state.desired_orientation = d_orientation; 
    servo_state.desired_pressure =  d_pressure;
    servo_state.desired_force =  d_force;
    servo_state.desired_offset_x =  d_offset_x ;  
    servo_state.desired_offset_y =  d_offset_y; 
    gotFeatures = false;
  }

}



/*******************************
 * 			Main
 ********************************/
   
int main(int argc, char **argv)
{
  std::cout << argv[0] << std::endl;
  ros::init(argc, argv, "tactile_controller_pan_tilt");					/// Initialize Ros systen 
  ros::NodeHandle n;													/// Create node
  ROS_INFO("SERVOING PAN TILT");

  
  /// Create the suscriber to the real joint angles position, velocity and effort
  ros::Subscriber sub_joints_state_real = n.subscribe("/real/joint_states", 1000, real_joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to image features
  ros::Subscriber sub_featuresC = n.subscribe("tilt_pan/Features/controlFeatures", 1000, controlCallback);
  ros::spinOnce();
  
  /// Create the suscriber to image features
  ros::Subscriber sub_featuresP = n.subscribe("tilt_pan/Features/planningFeatures", 1000, planingCallback);
  ros::spinOnce();
    
  /// Create the publishers for advertise joint positions
  ros::Publisher pub_m1_pos = n.advertise<std_msgs::Float64>("/head_pan_joint/command", 100);   
  ros::Publisher pub_m2_pos = n.advertise<std_msgs::Float64>("/head_tilt_joint/command", 100);  
    
  /// to store plots 
  ros::Publisher pub_servo_state = n.advertise<tactile_servo_msgs::servo>("pan_tilt/servo", 100);   
  ros::Rate loop_rate(100);												

  int count = 0;
  while (ros::ok())
  {	
	Tactile_pantilt k;
    if (k.init()<0) 
    {
        ROS_ERROR("Could not initialize tactile controller node");
        return -1;
    } 
    pub_m1_pos.publish(torque1); 
    pub_m2_pos.publish(torque2);	
    servo_state.header.stamp = ros::Time::now(); 
    pub_servo_state.publish(servo_state);
	ros::spinOnce();											
    loop_rate.sleep();												
    ++count;      
  }
  return 0;
}
