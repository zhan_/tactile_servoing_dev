#ifndef PANTILT_HEADER_HPP_
#define PANTILT_HEADER_HPP_
#define TILT_JOINT_LIMIT 1					// tilt joint limit in both directions expressed in radians
#define PAN_JOINT_LIMIT	1.57        		// pan joint limit in both directions expressed in radians
#endif
