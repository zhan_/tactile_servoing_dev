/**
  * Node:tactile_image_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	Subscribe to collision message from gazebo simualation
  * 	and publish the ros fotmated tactile image
  * Topic: ros_tactile_image
  * Topic Msg Type: http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
 */

#include "Tactile.hpp"
#include <boost/bind.hpp>

//Tactile::Tactile(ros::NodeHandle &n): nh_private_("~") 
//{
//}
		
//bool Tactile::init()
Tactile::Tactile(ros::NodeHandle &n): nh_private_(n)
  {
  cout << "loop" << endl;
  /// Create the publishers for advertise joint positions and to plot the controller variables (desride vs actual)
  pub_joint1_pos = 
    nh_private_.advertise<std_msgs::Float64>("/tilt_pan/joint1_position_controller/command", 100);
  pub_joint2_pos = 
    nh_private_.advertise<std_msgs::Float64>("/tilt_pan/joint2_position_controller/command", 100);
  pub_m1_pos = 
    nh_private_.advertise<std_msgs::Float64>("/head_pan_joint/command", 100);   
  pub_m2_pos = 
    nh_private_.advertise<std_msgs::Float64>("/head_tilt_joint/command", 100);
  pub_for_plot = 
    nh_private_.advertise<tactile_controller::servo>("PLOT/pan_tilt/servo_orientation", 100); 		 
  
  /// suscribe to contact image features for control
  sub_features = 
    nh_private_.subscribe("tilt_pan/Features/controlFeatures", 100, &Tactile::featuresCallback, this);
  
  servo_orientation_plot.desired_orientation =  0;
  servo_orientation_plot.actual_orientation = f_orientation;
  servo_orientation_plot.header.stamp = ros::Time::now(); 
  pub_for_plot.publish(servo_orientation_plot);		
  
  /// Get URDF XML from parameter server
  string urdf_xml, full_urdf_xml;
  nh_private_.param("urdf_xml",urdf_xml,std::string("robot_description"));
  nh_private_.searchParam(urdf_xml,full_urdf_xml);
  ROS_DEBUG("Reading xml file from parameter server");

  /// Check if loaded and store parameters 
  string params;
  if (!nh_private_.getParam(full_urdf_xml, params))
  {
	ROS_FATAL("Tactile Controller: Could not load the xml from parameter server: %s", urdf_xml.c_str());
  }
    
  /// Get Root and Tip From Parameter Service
//	if (!nh_private_.getParam("link1", root_name)) {
//		ROS_ERROR("No joint given in namespace: '%s')", nh_private.getNamespace().c_str());
//		ROS_FATAL("Tactile Controller: No root name found on parameter server: %s",urdf_xml.c_str());
//		return false;
//	}

//	if (!nh_private.getParam("link3", tip_name)) {
//	ROS_FATAL("Tactile Controller: No tip name found on parameter server");
//	return false;
//	}
//	cout<<root_name<<"  "<<tip_name<<endl;

  /// Load and Read Robot Models and form chains and trees
  if (!loadModel(params)) 
  {
  ROS_FATAL("Tactile Controller: Could not load acquire parameters from parameter server: %s", urdf_xml.c_str());
  }  
    
  nj = chain.getNrOfJoints();	 ns = chain.getNrOfSegments();
  //ROS_DEBUG("CHAIN--> Joints:%d, Ind. Joints:%d, Segments:%d",nj,nj,ns);
  
  /// Create joint array
  q_in = JntArray(nj);
  q_out = JntArray(nj);
    
  /// Get actual joint states in radians 
  sub_joints_state_virtual = 
    nh_private_.subscribe("/tilt_pan/joint_states", 1000, &Tactile::virtual_joints_stateCallback, this);	//simulated
    sub_joints_state_real = 
    nh_private_.subscribe("/real/joint_states", 1000, &Tactile::real_joints_stateCallback, this); 			//real setup	
  q_in(1) = q1 * (M_PI/180);  q_in(2) = q2 * (M_PI/180); //to degrees 

  /// Create solver based on kinematic chain
  ChainFkSolverPos_recursive fksolver = ChainFkSolverPos_recursive(chain);   										//Forward position solver  
  ChainIkSolverVel_pinv iksolvervel = ChainIkSolverVel_pinv(chain);												//Inverse velocity solver
  ChainIkSolverPos_NR iksolverpos = ChainIkSolverPos_NR(chain,fksolver,iksolvervel, NUM_ITERATIONS_IK, IK_EPS);   //Inverse position solver, Maximum 100 iterations, stop at accuracy 1e-6

  /// Calculate forward position kinematics  
  kinematics_status = fksolver.JntToCart(q_in,cartpos);
    
  if(kinematics_status>=0)
  {		
        //  cout <<endl<< cartpos <<std::endl;
        //ROS_DEBUG("Tactile Controller: Kinematic Solver OK");
  }
   else
  {
		ROS_FATAL("Tactile Controller: Could not calculate forward kinematics ");
  }
	
  /// Get desired frame (rot,pos) from features -> V = TM * DF 
  contactState ();	
	
  /// The position and orientation of the object wrt to the sensor frame  
    //cout<<"Input Angle: "<<endl;  cin >> angle;    float angle=10;
 
  float roll = V[3][0] * (pi/180); float pitch = V[5][1] * (pi/180);  float yaw = V[4][1] * (pi/180);
  float eZ = V[3][0] * (pi/180); float eY = V[5][0] * (pi/180);  float eX = V[4][0] * (pi/180);

  KDL::Vector d_pos(V[0][0], V[1][0], V[2][0]);					    //X-Y-Z are initialized with the given values 
  d_rotRPY = KDL::Rotation::RPY(roll, pitch, yaw );  			        //Roll-Pitch-Yaw angles
  d_rotEZYX = KDL::Rotation::EulerZYX(eZ, eY, eX);                    //Euler Z-Y-X angles

  /// The actual orientation of end-effector frame expressed as Rotation type to then operate
  float rotTemp[9]={};
  unsigned int acc=1;
  for (int i = 0; i<3; i++) 
  {
    for (int j = 0; j<3; j++) 
    {
	  rotTemp[acc] = cartpos.M(i,j);
	  ++acc;
	}
  }
  KDL::Rotation roteef( rotTemp[1], rotTemp[2], rotTemp[3], rotTemp[4], rotTemp[5], rotTemp[6], rotTemp[7], rotTemp[8], rotTemp[9]);
    
  /// FINAL POSITION AND ORIENTATION: Put frame wrt base -> Create the frame q + dF 
  KDL::Vector pos = d_pos + cartpos.p;
    //cout << "POSITION: " <<d_pos<< endl;
  KDL::Rotation rot = roteef * d_rotRPY;
    //cout << "ROTATION LAST LINK:" << endl << roteef <<endl;
    //cout << "ROTATION SENSOR:" << endl << d_rotRPY <<endl;
    //cout << "ROTATION PRODUCT:" << endl << rot <<endl;
  KDL::Frame F_dest(rot,pos);											
    //cout<<"Matrix: "<<endl<< F_dest <<endl;
	
  /// Get joint positions in joint space
  iksolverpos.CartToJnt(q_in,F_dest,q_out);

  double q2[3] = {};
  cout << "IK (deg):" << endl;
  for (unsigned int j = 0; j < q_out.rows(); j++)  
  {
    q2[j] = (q_out(1, j) * (180/pi)) ;
    //cout << "  "<< q2[j];
  }
    cout << endl;
     
  /// Verifi joint limits
  if (q_out(1,1) > pan_limit) 
  {
		ROS_INFO("Position exced pan joint limit", q_out(1,1) );
		q_out(1,1) = 0;
  }
    
  if (q_out(1,1) > tilt_limit) 
  {
		ROS_INFO("Position exced pan tilt limit", q_out(1,2) );
		q_out(1,2) = 0;
  }
    
  float kp = 0.01;
  float control_term =  kp * error_orientation * (pi/180); // in radians 0.02
  pos_val += control_term;
     
    cout << "                        DEG       " << "RADIANS" << endl;  
    cout << "error:         " << error_orientation << "      ,       "<< error_orientation*(pi/180) << endl;
    cout << "desired angle: " << f_orientation <<     "      ,       "<< f_orientation*(pi/180) <<endl ;
    cout << "actual  angle: " << pan_real*(180/pi)<<  "      ,       "<< pan_real << endl ;
    cout << "step:          " << control_term  * (180/pi)<< "         ,       " <<  control_term  << endl;
    cout << "out:           " << pos_val<<endl;
  if(pos_val<-1) pos_val = -1;
  if(pos_val>1) pos_val = 1;
    
    
  pan_joint_position_.data = pos_val;

    
  cout << "out:           " << pos_val<<endl;
    /// Apply controller, see ROS control description and yaml for PID details
     //pan_joint_position_.data = q_out(1,1);		
  tilt_joint_position_.data = 0;	//-6.28318531
     
    // tilt_joint_position_.data = q_out(1,2);
    // featureController ();
    // printScreen();
    
     /// pan
  pub_m1_pos.publish(pan_joint_position_); 
  pub_joint1_pos.publish(pan_joint_position_);
			/// tilt
  pub_m2_pos.publish(tilt_joint_position_);	
  pub_joint2_pos.publish(tilt_joint_position_);

}




/** @brief Check and load the urdf robot model as xml, chain and tree  */
bool Tactile::loadModel(const std::string xml) {
    if (!robot_model.initString(xml)) {
        ROS_FATAL("Could not initialize robot model");
        return -1;
    }
    if (!kdl_parser::treeFromString(xml, tree)) {
        ROS_ERROR("Could not initialize tree object");
        return false;
    }
    if (!tree.getChain("world", "link3", chain)) {
        ROS_ERROR("Could not initialize chain object");
        return false;
    }
    
 //   if (!readJoints(robot_model)) {
  //      ROS_FATAL("Could not read information about the joints");
  //      return false;
   // }
    
    return true;
}

/** @brief Read the joints of the urdf model  */
bool Tactile::readJoints(urdf::Model &robot_model) {
    num_joints = 0;
    
    /// get joint maxs and mins
    boost::shared_ptr<const urdf::Link> link = robot_model.getLink(tip_name);
    boost::shared_ptr<const urdf::Joint> joint;

	urdf::Vector3 length;

    while (link && link->name != root_name) {
        joint = robot_model.getJoint(link->parent_joint->name);
        if (!joint) {
            ROS_ERROR("Could not find joint: %s",link->parent_joint->name.c_str());
         //   return false;
        }
        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED) {
            ROS_DEBUG( "adding joint: [%s]", joint->name.c_str() );
            num_joints++;
        }
        link = robot_model.getLink(link->getParent()->name);
    }

    joint_min.resize(num_joints);
    joint_max.resize(num_joints);
    //info.joint_names.resize(num_joints);
    //info.link_names.resize(num_joints);
    //info.limits.resize(num_joints);

    link = robot_model.getLink(tip_name);
    unsigned int i = 0;
    while (link && i < num_joints) {
        joint = robot_model.getJoint(link->parent_joint->name);
        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED) {
            ROS_DEBUG( "getting bounds for joint: [%s]", joint->name.c_str() );

            float lower, upper;
            int hasLimits;
            if ( joint->type != urdf::Joint::CONTINUOUS ) {
                lower = joint->limits->lower;
                upper = joint->limits->upper;
                hasLimits = 1;
            } else {
                lower = -M_PI;
                upper = M_PI;
                hasLimits = 0;
            }
            int index = num_joints - i -1;
            joint_min.data[index] = lower;
            joint_max.data[index] = upper;
            //info.joint_names[index] = joint->name;
            //info.link_names[index] = link->name;
            //info.limits[index].joint_name = joint->name;
            //info.limits[index].has_position_limits = hasLimits;
            //info.limits[index].min_position = lower;
            //info.limits[index].max_position = upper;
            i++;
        }
        link = robot_model.getLink(link->getParent()->name);
    }
   // return true;
}









/// Contact Selector Matrix = (TactileMatrix wich accomodates data in vector) * (Feature error vector)
void Tactile::contactState ()
{

/// get selection matrix
/**************************
 *  Selection Matrices
 **************************/
   // cout<<"Contact Type: " << contactType<<endl;
	contactType = "edge"; 	planning = "orientation";
	
	/// Create an empty matrix to store the Contact Selector Matrix * Feature Error
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				V[row][col] = 0;
				F[row][col] = 0;
			}
		}	
	int S[6][6] = {};
	
/**        --- Force Servoing Matrix Selector ---
 * @brief : Control tilt with wx, wy  and force with vz */

	if(contactType == "push"  && planning == "force" ) {
	   S[2][2] = 1;   S[3][3] = 1 ;   S[4][4] = 1 ;
	}


/**        --- Orientation Servoing Matrix Selector ---				
 * @brief : Control orientation wz */
 
	if(contactType == "edge" && planning == "orientation" ) {
		cout << "edge and orientation" << endl;
		S[5][5] = 1;	
	}		


/**    --- Force and Orientation Servoing Matrix Selector ---
 * @brief :  Control force with vz and orientation wz */
	if(contactType == "edge" && planning == "force&orientation" ) {
		S[2][2] = 1; S[5][5] = 1;
	}
	
		
/**       --- Position Servoing Matrix Selector 2D ---
* @brief : control rolling (tilt) wx, wy  */
	if(planning == "offset" ) {
		S[3][3] = 1; S[4][4] = 1;
	}
	
	
	featureError ();
	
	/// Rearrange data depending on robot kinematics, equivalent to multiplicate by an accomodation matrix
	/// Feature error vector for tilt pan:
	F[0][0]=error_offset_x; F[1][0]=error_offset_y; F[2][0]=error_force; 
	F[3][0]=error_offset_x; F[4][0]=error_offset_y; F[5][0]=error_orientation; 

	
/// Selection Matrix	
cout<<"Selection Matrix:"<<endl;			
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 6; col++) {
			//	cout << S[row][col]<<" ";
			}
		//	cout<<endl;
		}	
		
/// Feature Matrix
cout<<"Feature Matrix:"<<endl;
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
		//		cout << F[row][col];
			}
		//	cout<<endl;
		}		
			
				
/// Multiplicate selection matrix "S" by feature error depending of redundant data  V = S*DF
cout<<"Twist"<<endl;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				for (int inner = 0; inner < 6; inner++) {
					V[row][col] += S[row][inner] * F[inner][col];
				}
				cout << V[row][col] << "  ";
			}
			cout << endl;
		}			

}	
/// The feature error
void Tactile::featureError ()
{
	error_force = f_force - d_force;
	error_pressure = f_pressure - d_pressure;
	error_offset_x = f_offset_x - d_offset_x;
	error_offset_y = f_offset_y - d_offset_y;
	error_orientation = f_orientation ; // error in degrees --> f_desired=0 => 0-f_orientation
	//error_orientation = f_orientation - (pan_real*(180/pi)); // error in degrees 
	//cout << "ERROR DEG: " << error_orientation << endl;
	//- d_orientation   ... -q1 in simulation instead of pan_real;

}

/// Print data in screen
void Tactile::printScreen ()
{
  	cout<<"       STATES"<<endl;	
	cout<<"q1:       "<<q1<<endl; cout<<"q2:       "<<q2<<endl;	
	cout<<"v1:       "<<v1<<endl; cout<<"v2:       "<<v2<<endl;	
	cout<<"e1:       "<<e1<<endl; cout<<"e2:       "<<e2<<endl;	
	cout<<"      FEATURES"<<endl;
	//cout<<"Force:       "<<f_force<<endl;
	//cout<<"Pressure:    "<<f_pressure<<endl;
	cout<<"Orientation: "<<endl;
	cout<<"Actual: "<<q1<<endl;
	cout<<"Desired: "<<f_orientation<<endl;
	//cout<<"Offset x:    "<<f_offset_x<<endl;
	//cout<<"Offset y:    "<<f_offset_y<<endl;
}


/// The controller in feature space => gives torques to motors
void Tactile::feature_spaceController ()
{
		
	float kp = 1; 
	float d_stiffness = 3;
	
	
	/// Compute dF
	error_orientation = kp*(f_orientation-q1);
}



/*******************************
 * 			Main
 ********************************/
   
int main(int argc, char **argv)
{
  ros::init(argc, argv, "tactile_controller");							
  ros::NodeHandle node;
  ROS_INFO("FEATURE CONTROLLER NODE");  
  ros::Rate loop_rate(100);												
  while (ros::ok())
  {		
  Tactile k(node);
  ros::spinOnce();	
  loop_rate.sleep();										    
  }
   return 0;
}


