/**
  * Node:tactile_image_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	Subscribe to collision message from gazebo simualation
  * 	and publish the ros fotmated tactile image
  * Topic: ros_tactile_image
  * Topic Msg Type: http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
 */

#include "Tactile2.hpp"
tactile_controller::servo servo_alfa;

/*************************
 * Global variables
 ************************/
bool gotFeatures, gotJointStates;

/// To send the motor commands 
std_msgs::Float64 torque1;
std_msgs::Float64 torque2;
std_msgs::Float64 control_actual;
std_msgs::Float64 control_desired;

/// Positions, velocities and errors
float q1, q2, v1, v2, e1, e2, M1, M2, M3, M4, pan_real, tilt_real; 

/// Features variables of actual state
float f_force, f_pressure, f_orientation, f_offset_x, f_offset_y;
string contact;

const float pan_limit = 1.57;											/// pan joint limit in both directions expressed in radians
const float tilt_limit = 1.57;											/// tilt joint limit in both directions expressed in radians
float const pi = 3.14159265359;


Tactile::Tactile(): nh_private ("~") {
}
		
		
		
		
/****************************************************************************************
 * Callback functions that execute each time this node subscribes to the topic .../state
 ***************************************************************************************/

void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
	pan_real = msg->position[0];
	tilt_real = msg->position[1];
}

/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features f
void featuresCallback(const cv_features_extractor::featuresControl::ConstPtr& featuresC) 
{
	if ( featuresC->contactForce.size() > 0 ) {
	f_orientation = featuresC->contactOrientation[0];
	gotFeatures = 1;
	} 
} 


bool Tactile::init() 
{
  if (gotFeatures) error_orientation = f_orientation;  // 30-
  control_orientation(); 
}



void Tactile::control_orientation() 
{

	if (abs(f_orientation) > 100) f_orientation = prev_f_orientation;
	float kp = 0.01;
    float control_term = kp * error_orientation * (pi/180); // in radians 0.02
    pan_val += control_term;
      
    if(pan_val<-1.5) pan_val = prev_pan_val;
    if(pan_val>1.5)  pan_val = prev_pan_val;
    torque1.data = pan_val;
    prev_pan_val = pan_val;
    prev_f_orientation = f_orientation;
    servo_alfa.actual_orientation = f_orientation;  
}


/*******************************
 * 			Main
 ********************************/
   
int main(int argc, char **argv)
{
  std::cout << argv[0] << std::endl;
  ros::init(argc, argv, "tactile_controller");							/// Initialize Ros systen 
  ros::NodeHandle n;													/// Create node
  ROS_INFO("TEST ORIENTATION NODE");
    
  
  /// Create the suscriber to the real joint angles position, velocity and effort
  ros::Subscriber sub_joints_state_real = n.subscribe("/real/joint_states", 1000, real_joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to image features
  ros::Subscriber sub_features = n.subscribe("tilt_pan/Features/controlFeatures", 1000, featuresCallback);
  ros::spinOnce();
    
  /// Create the publishers for advertise joint positions
  ros::Publisher pub_joint1_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint1_position_controller/command", 100);
  ros::Publisher pub_joint2_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint2_position_controller/command", 100);
  
  ros::Publisher pub_m1_pos = n.advertise<std_msgs::Float64>("/head_pan_joint/command", 100);   
  ros::Publisher pub_m2_pos = n.advertise<std_msgs::Float64>("/head_tilt_joint/command", 100);  
    
  /// to store plots
  ros::Publisher pub_desired = n.advertise<std_msgs::Float64>("tactileContorller/desiredFeature", 100);   	
  ros::Publisher pub_actual = n.advertise<std_msgs::Float64>("tactileController/actualFeature", 100);    
  ros::Publisher pub_force = n.advertise<tactile_controller::servo>("pan_tilt/servo", 100);  
  
  ros::Rate loop_rate(100);												/// Loop at 100Hz until the node is shut down
  
  torque1.data = 0;
  pub_joint1_pos.publish(torque1);
  torque2.data = 0;
  pub_joint2_pos.publish(torque2);
  servo_alfa.desired_orientation =  0;  //servo to this angle
  
  int count = 0;
  while (ros::ok())
  {
		
	Tactile k;
    if (k.init()<0) {
        ROS_ERROR("Could not initialize tactile controller node");
        return -1;
   } 
	  /// pan
		  pub_m1_pos.publish(torque1); 
		  pub_joint1_pos.publish(torque1);
	  /// tilt
	      pub_m2_pos.publish(torque2);	
	      pub_joint2_pos.publish(torque2);
	      
	      servo_alfa.header.stamp = ros::Time::now(); 
	      pub_force.publish(servo_alfa);
	  //ROS_DEBUG("Joints States Published ... ");
	  ros::spinOnce();													/// Do this
      loop_rate.sleep();												/// Wait until it is time for another interaction
      ++count;
      
  }
  return 0;
}
