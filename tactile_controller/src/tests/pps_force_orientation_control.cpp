/**
  * Node:tactile_image_extractor.cpp  
  * Version: 1.1 
  * Date: 2014-04-16
  * Description:
  * 	Subscribe to collision message from gazebo simualation
  * 	and publish the ros fotmated tactile image
  * Topic: ros_tactile_image
  * Topic Msg Type: http://docs.ros.org/api/sensor_msgs/html/msg/Image.html
 */

#include "Tactile2.hpp"
tactile_controller::servo servo_alfa;

/*************************
 * Global variables
 ************************/
bool gotFeatures, gotJointStates;

/// To send the motor commands 
std_msgs::Float64 torque1;
std_msgs::Float64 torque2;
std_msgs::Float64 control_actual;
std_msgs::Float64 control_desired;

/// Positions, velocities and errors
float q1, q2, v1, v2, e1, e2, M1, M2, M3, M4, pan_real, tilt_real; 

/// Features variables of actual state
float f_force, f_pressure, f_orientation, f_offset_x, f_offset_y;
string contact;

const float pan_limit = 1.57;											/// pan joint limit in both directions expressed in radians
const float tilt_limit = 1.57;											/// tilt joint limit in both directions expressed in radians
float const pi = 3.14159265359;


Tactile::Tactile(): nh_private ("~") {
}
		
		
		
		
/****************************************************************************************
 * Callback functions that execute each time this node subscribes to the topic .../state
 ***************************************************************************************/
 
/// Hear joints state "/tilt_pan/joint_states" and store the data
void joints_stateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
	/// get for the joints
	q1=msg->position[0]; 												//q1 angle in radians   -pi rad and pi rad
	q1 *= (180 /pi); 													//radians to deg
	q2=msg->position[1]; 												//q2 angle in radians   -pi rad and pi rad
	q2 *= (180 /pi); 													//radians to deg
	v1=msg->velocity[0]; 												//v1 angle in meters/seconds
	v2=msg->velocity[1]; 												//v2 angle in meters/seconds
	e1=msg->effort[0]; 													//e1 get actual effort applied in joint 1
	e2=msg->effort[1]; 													//e2 get actual effort applied in joint 2
	//ROS_DEBUG("Tactile Controller: I heard the joint states... ");
	gotJointStates=1;
} 

void real_joints_stateCallback (const sensor_msgs::JointState::ConstPtr& msg)
{
	pan_real = msg->position[0];
	tilt_real = msg->position[1];
}

/// Hear msg type from topi "tilt_pan/Features/controlFeatures" and store the features f
void featuresCallback(const cv_features_extractor::featuresControl::ConstPtr& featuresC) 
{
	if ( featuresC->contactForce.size() > 0 ) {
	/// Extract features from topic tilt_pan/features
	/// Main componentes = [f,p,alfa,x,y]		
	f_force = featuresC->contactForce[0];	
	f_pressure = featuresC->contactPressure[0];	
	f_orientation = featuresC->contactOrientation[0];
//	f_orientation=abs(f_orientation);
	f_offset_x = featuresC->contactOffset_x[0];	
	f_offset_y = featuresC->contactOffset_y[0];	
	
	/// Redundant data 
	//contact = features->contactType;
	
	gotFeatures=1;
	//ROS_DEBUG("Tactile Controller: I heard the features... ");
	} 
} 


bool Tactile::init() {

	/// Get desired frame (rot,pos) from features -> V = TM * DF 
	contactState ();	    
    control_force();
    //control_orientation();
    
    

    
}

void Tactile::control_force() 
{
  if (f_force > 5000) f_force = 5000;
  if (!gotFeatures) 
  { // if force less than a treshold, then keep the tilt in cero position
    //torque2.data = prev_tilt_val; 
    //d_force = 0;
    //f_force = 0;
  }
  else
  { // if force more than threshold, then keep a contact force 
    d_force = 2000;
    float kp = 0.0004;//0.0003  0.0001
  
    if (abs(d_force - f_force) < 0.1 ) //since force data from tactile sensors is very unstable
    { 
      error_force = 0;
    }
    else  
    { 
      error_force = d_force - f_force;
    }
        
    float control_term =  -kp * error_force * (pi/180); // the control term is added in radians
    
    if(f_offset_y>0) tilt_val += control_term;
    if(f_offset_y<0) tilt_val -= control_term;
    cout<<"C"<<control_term<<endl;
    
    // check limits
    if(tilt_val<-1) tilt_val = prev_tilt_val;
    if(tilt_val>1) tilt_val = prev_tilt_val;
    // data to publish the position not the torque
    //if(f_offset_y>0) torque2.data = tilt_val;  // if I press in the right, then move tilt to the right
    //if(f_offset_y<0) torque2.data = -tilt_val; // viceversa
    torque2.data = tilt_val;
  }
  
  prev_tilt_val = tilt_val;
  //for publish plot data
  servo_alfa.desired_force =  d_force;
  servo_alfa.actual_force = f_force;
  gotFeatures = 0;
  
}


void Tactile::control_orientation() 
{
	torque2.data = 0.5;//-1
	if (f_force < 20) 
	{
	  f_orientation = 0;	
	}
	else
	{
	  if (abs(f_orientation) > 100) f_orientation = prev_f_orientation;
	  float kp = 0.01;
      float control_term =  kp * error_orientation * (pi/180); // in radians 0.02
      pan_val += control_term;
      
      if(pan_val<-1.5) pan_val = prev_pan_val;
      if(pan_val>1.5)  pan_val = prev_pan_val;
      torque1.data = pan_val;
	}
	
    prev_pan_val = pan_val;
    prev_f_orientation = f_orientation;
    servo_alfa.desired_orientation =  0;  //servo to this angle
    servo_alfa.actual_orientation = f_orientation;  
}


/// Contact Selector Matrix = (TactileMatrix wich accomodates data in vector) * (Feature error vector)
void Tactile::contactState ()
{

/// get selection matrix
/**************************
 *  Selection Matrices
 **************************/
   // cout<<"Contact Type: " << contactType<<endl;
	contactType = "push"; 	planning = "force";
	
	/// Create an empty matrix to store the Contact Selector Matrix * Feature Error
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				V[row][col] = 0;
				F[row][col] = 0;
			}
		}	
	int S[6][6] = {};
	
/**        --- Force Servoing Matrix Selector ---
 * @brief : Control tilt with wx, wy  and force with vz */

	if(contactType == "push"  && planning == "force" ) {
	   S[2][2] = 1;   S[3][3] = 1 ;   S[4][4] = 1 ;
	}


/**        --- Orientation Servoing Matrix Selector ---				
 * @brief : Control orientation wz */
 
	if(contactType == "edge" && planning == "orientation" ) {
		cout << "edge and orientation" << endl;
		S[5][5] = 1;	
	}		


/**    --- Force and Orientation Servoing Matrix Selector ---
 * @brief :  Control force with vz and orientation wz */
	if(contactType == "edge" && planning == "force&orientation" ) {
		S[2][2] = 1; S[5][5] = 1;
	}
	
		
/**       --- Position Servoing Matrix Selector 2D ---
* @brief : control rolling (tilt) wx, wy  */
	if(planning == "offset" ) {
		S[3][3] = 1; S[4][4] = 1;
	}
	
	
	featureError ();
	
	/// Rearrange data depending on robot kinematics, equivalent to multiplicate by an accomodation matrix
	/// Feature error vector for tilt pan:
	F[0][0]=error_offset_x; F[1][0]=error_offset_y; F[2][0]=error_force; 
	F[3][0]=error_offset_x; F[4][0]=error_offset_y; F[5][0]=error_orientation; 

	
/// Selection Matrix	
//cout<<"Selection Matrix:"<<endl;			
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 6; col++) {
			//	cout << S[row][col]<<" ";
			}
		//	cout<<endl;
		}	
		
/// Feature Matrix
//cout<<"Feature Matrix:"<<endl;
	for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
		//		cout << F[row][col];
			}
		//	cout<<endl;
		}		
			
				
/// Multiplicate selection matrix "S" by feature error depending of redundant data  V = S*DF
//cout<<"Twist"<<endl;
		for (int row = 0; row < 6; row++) {
			for (int col = 0; col < 1; col++) {
				for (int inner = 0; inner < 6; inner++) {
					V[row][col] += S[row][inner] * F[inner][col];
				}
				//cout << V[row][col] << "  ";
			}
			cout << endl;
		}			

}	
/// The feature error
void Tactile::featureError ()
{
	error_force = f_force - d_force;
	error_pressure = f_pressure - d_pressure;
	error_offset_x = f_offset_x - d_offset_x;
	error_offset_y = f_offset_y - d_offset_y;
	error_orientation = f_orientation ; // error in degrees --> f_desired=0 => 0-f_orientation
	//error_orientation = f_orientation - (pan_real*(180/pi)); // error in degrees 
	//cout << "ERROR DEG: " << error_orientation << endl;
	//- d_orientation   ... -q1 in simulation instead of pan_real;

}



/*******************************
 * 			Main
 ********************************/
   
int main(int argc, char **argv)
{
  std::cout << argv[0] << std::endl;
  ros::init(argc, argv, "tactile_controller");							/// Initialize Ros systen 
  ros::NodeHandle n;													/// Create node
  ROS_INFO("FEATURE CONTROLLER NODE");
    
  /// Create the suscriber to the simulated joint angles position, velocity and effort
  ros::Subscriber sub_joints_state = n.subscribe("/tilt_pan/joint_states", 1000, joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to the real joint angles position, velocity and effort
  ros::Subscriber sub_joints_state_real = n.subscribe("/real/joint_states", 1000, real_joints_stateCallback);
  ros::spinOnce();
  
  /// Create the suscriber to image features
  ros::Subscriber sub_features = n.subscribe("tilt_pan/Features/controlFeatures", 1000, featuresCallback);
  ros::spinOnce();
    
  /// Create the publishers for advertise joint positions
  ros::Publisher pub_joint1_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint1_position_controller/command", 100);
  ros::Publisher pub_joint2_pos = n.advertise<std_msgs::Float64>("/tilt_pan/joint2_position_controller/command", 100);
  
  ros::Publisher pub_m1_pos = n.advertise<std_msgs::Float64>("/head_pan_joint/command", 100);   
  ros::Publisher pub_m2_pos = n.advertise<std_msgs::Float64>("/head_tilt_joint/command", 100);  
    
  /// to store plots
  ros::Publisher pub_desired = n.advertise<std_msgs::Float64>("tactileContorller/desiredFeature", 100);   	
  ros::Publisher pub_actual = n.advertise<std_msgs::Float64>("tactileController/actualFeature", 100);    
  ros::Publisher pub_force = n.advertise<tactile_controller::servo>("pan_tilt/servo", 100);  
  
  ros::Rate loop_rate(100);												/// Loop at 100Hz until the node is shut down
  
  torque2.data = 0;
  pub_joint2_pos.publish(torque2);
  int count = 0;
  while (ros::ok())
  {
		
	Tactile k;
    if (k.init()<0) {
        ROS_ERROR("Could not initialize tactile controller node");
        return -1;
   } 
	  /// pan
		  pub_m1_pos.publish(torque1); 
		  pub_joint1_pos.publish(torque1);
	  /// tilt
	      pub_m2_pos.publish(torque2);	
	      pub_joint2_pos.publish(torque2);
	      
	      servo_alfa.header.stamp = ros::Time::now(); 
	      pub_force.publish(servo_alfa);
	  //ROS_DEBUG("Joints States Published ... ");
	  ros::spinOnce();													/// Do this
      loop_rate.sleep();												/// Wait until it is time for another interaction
      ++count;
      
  }
  return 0;
}
