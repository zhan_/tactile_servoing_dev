#include <ros/ros.h>
#include <string>

//messages
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>

#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/servo_hand.h>

// begin to servo
#include "tactile_servo_srvs/start_servo_controller.h"


/**sdfsf
sf
sdf
*/

#include <ros/ros.h>
#include <string>

//messages
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>

#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/servo_hand.h>

// begin to servo
#include "tactile_servo_srvs/start_servo_controller.h"

#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <moveit_msgs/GetKinematicSolverInfo.h>
#include <moveit_msgs/GetPositionIK.h>
#include <moveit_msgs/GetPositionFK.h>
#include <ros/package.h>


moveit_msgs::GetPositionIK::Request  gpik_req;
moveit_msgs::GetPositionIK::Response gpik_res;

moveit_msgs::GetKinematicSolverInfo::Request request;
moveit_msgs::GetKinematicSolverInfo::Response response;



/// Class that subscribes to /control_features
/// subscribe to /desired_feature
/// publishes /sh_[joint_name]_position_controller/command
/// <limit lower="${-30/180*M_PI}" upper="${10/180*M_PI}" -0.52 to 0.174


class tactileController
{
public:
    tactileController(int joint_to_control_, int tactile_sensor_num_);
    virtual ~tactileController();

    ros::NodeHandle nh_;


    void publish_command();
    void cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_joint_state(const control_msgs::JointControllerStateConstPtr& msg_);
    void set_pid();
//rrbot
    void cb_rrbot_joint_state(const control_msgs::JointControllerStateConstPtr& msg_);

    ros::Publisher pub_command_wrj2;
    ros::Publisher pub_command_wrj1;
    ros::Publisher pub_command_sa_er;
    ros::Publisher pub_command_sa_es;
    ros::Publisher pub_command_sa_sr;
    ros::Publisher pub_command_sa_ss;




    ros::Subscriber sub_contr_features;
    ros::Subscriber sub_des_features;
    ros::Subscriber sub_joint_state;
//rrbot
    ros::Subscriber sub_rrbot_joint_state;

    // publish topics to plot
    ros::Publisher variables_to_plot;

    // joint number to control
    int joint_to_control;

    // string names joints and controller
    std::string controller_type;
    std::string hand_jointnames[24];
//rrbot
    std::string rrbot_jointname;

    // string names topic to sub
    std::string topic_name_sub_c;
    std::string topic_name_sub_p;
    std::string topic_name_sub_d;

    // string names topic to pub
    std::string topic_name_pub_c;



    // control variable

    //tactile_servo_msgs::featuresControl feeback_feature;
    //tactile_servo_msgs::featuresControl des_feature;

    double error_orient_feature;
    double feedback_orient_feature;
    double des_orient_feature;
    double joint_val_now;
//rrbot
    double rrbot_joint_val_now;

    // allow pub
    bool allow_pub_orient;
    bool is_feedback_orient_rec;
    bool is_des_orient_rec;
    bool is_joint_val_rec;

    //rrbot
    bool is_rrbot_joint_val_rec;

    double pid_k_gain;
    double pid_integ_gain;
    double pid_deriv_gain;
    double pid_out;
    double pid_out_old;
    double pid_dt;
    double pid_der;
    double pid_integral;
    double pid_integral_old;
    double pid_err_old;

    moveit_msgs::GetPositionIK::Request  gpik_req;
    moveit_msgs::GetPositionIK::Response gpik_res;

    moveit_msgs::GetKinematicSolverInfo::Request request;
    moveit_msgs::GetKinematicSolverInfo::Response response;


    ros::ServiceClient query_client;
    ros::ServiceClient query_client_fk;
    ros::ServiceClient ik_client;


    int moveIK6D(float x, float y, float z, float qw, float qx, float qy, float qz);
    int moveIK();
};

tactileController::tactileController(int joint_to_control_, int tactile_sensor_num_){

    controller_type = "_position_controller";
    std::string hand_jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                                "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                                "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                                  "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};
    joint_to_control = 23;
    if (joint_to_control_ == 1){
        joint_to_control = 23;
    }

    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);
    // get topic names of control features for a given sensor
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 9th pose");
    topic_name_sub_c = static_cast<std::string> (my_list[tactile_sensor_num_]["control_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    topic_name_sub_p = static_cast<std::string> (my_list[tactile_sensor_num_]["planning_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["desired_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    topic_name_sub_d = static_cast<std::string> (my_list[tactile_sensor_num_]["desired_topic_name"]);

    topic_name_pub_c = hand_jointnames[joint_to_control] + controller_type ;

    //rrbot
    rrbot_jointname = "rrbot/joint3_position_controller/state";

    sub_contr_features = nh_.subscribe("/"+topic_name_sub_c, 1, &tactileController::cb_feature_c, this);
    sub_des_features = nh_.subscribe("/"+topic_name_sub_d, 1, &tactileController::cb_feature_d, this);
    sub_joint_state = nh_.subscribe("/"+topic_name_pub_c + "/state", 1,  &tactileController::cb_joint_state,this);
    //rrbot
    sub_rrbot_joint_state = nh_.subscribe("/"+rrbot_jointname,1,&tactileController::cb_rrbot_joint_state,this);


    // publish topics to plot
    variables_to_plot = nh_.advertise<tactile_servo_msgs::servo_hand>("servo_hand", 1);


    pub_command_wrj2 = nh_.advertise<std_msgs::Float64>(topic_name_pub_c + "/command", 1);
    pub_command_wrj1 = nh_.advertise<std_msgs::Float64>(hand_jointnames[joint_to_control-1] + controller_type + "/command", 1);
    pub_command_sa_er = nh_.advertise<std_msgs::Float64>(hand_jointnames[joint_to_control-5] + controller_type + "/command", 1);
    pub_command_sa_er = nh_.advertise<std_msgs::Float64>(hand_jointnames[joint_to_control-4] + controller_type + "/command", 1);
    pub_command_sa_er = nh_.advertise<std_msgs::Float64>(hand_jointnames[joint_to_control-2] + controller_type + "/command", 1);
    pub_command_sa_er = nh_.advertise<std_msgs::Float64>(hand_jointnames[joint_to_control-3] + controller_type + "/command", 1);


    pid_integral_old = 0;
    pid_err_old = 0;
    pid_k_gain = 0.1;
    pid_deriv_gain = 0;
    pid_integ_gain = 0;
    pid_dt = double(1.0/50.0);


    // prepare services
    // prepare services
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk");

    ros::ServiceClient query_client = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::ServiceClient query_client_fk = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::ServiceClient ik_client;
    ik_client = nh_.serviceClient<moveit_msgs::GetPositionIK>("shadow_right_arm_kinematics/get_ik");
    query_client_fk = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    query_client = nh_.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");


}
tactileController::~tactileController(){

}


int tactileController::moveIK(){


    //++ INVOKE SERVICE GET_IK_SOLVER_INFO, to get joint information:
    query_client.call(request,response);
    // get possible computable joints
    if(query_client.call(request,response))
    {
      for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
      {
        ROS_INFO("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
      }
    }
    else
    {
      ROS_INFO("Could not call query service");
      ros::shutdown();
      exit(1);
    }
    query_client_fk.call(request,response);


    //++FILL REQUEST
    // define the service messages
//    gpik_req.timeout = ros::Duration(5.0);
    /* gpik_req.ik_request.timeout = ros::Duration(5.0);
    gpik_req.ik_request.ik_link_name = "palm";
    gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
   gpik_req.ik_request.pose_stamped.pose.position.x = 0.526;//0.36513;
    gpik_req.ik_request.pose_stamped.pose.position.y = -0.134;//
    gpik_req.ik_request.pose_stamped.pose.position.z = 0.230;//1.0913;

    //pos is not relevant with the used ik_solver but set it anyway to identity
    gpik_req.ik_request.pose_stamped.pose.orientation.x = 0.66;//0.70701;
    gpik_req.ik_request.pose_stamped.pose.orientation.y = 0.26;//-0.023199;
    gpik_req.ik_request.pose_stamped.pose.orientation.z = 0.667;//0.70671;
    gpik_req.ik_request.pose_stamped.pose.orientation.w = 0.312;//0.012863;
    gpik_req.ik_request.robot_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
//    gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    gpik_req.ik_request.robot_state.joint_state.name = response.kinematic_solver_info.joint_names;
*/
    //+ INVOKE SERVICE WITH REQUEST AND TREAT RESPONSE LATER:
    ik_client.call(gpik_req,gpik_res);

    if(ik_client.call(gpik_req, gpik_res))
    {
        if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
        {
            std_msgs::Float64 message;
            for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
            {
                ROS_INFO("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
//                ROS_INFO("we publish to %s",pub[i].getTopic().c_str());
                message.data= (double)gpik_res.solution.joint_state.position[i];
                std::cout << "(double)gpik_res.solution.joint_state.position at "<<i<<"is ="<<(double)gpik_res.solution.joint_state.position[i] <<std::endl;
//                pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
            }
            std::cout<< "soluton for joint name " <<gpik_res.solution.joint_state.name.at(0)<< std::endl;
            std::cout<< "soluton for " <<gpik_res.solution.joint_state.position.at(0)<<std::endl;
            return 0;
        }
        else{
            ROS_INFO("Inverse kinematics failed");
            std::cout << "error = "<< gpik_res.error_code.val <<std::endl;
            return -1;
        }
    }
    else
        ROS_INFO("Inverse kinematics service call failed %d",gpik_res.error_code.val);
    return -1;
}


int tactileController::moveIK6D(float x, float y, float z, float qw, float qx, float qy, float qz)
{
  gpik_req.ik_request.pose_stamped.pose.position.x = x;
  gpik_req.ik_request.pose_stamped.pose.position.y = y;
  gpik_req.ik_request.pose_stamped.pose.position.z = z;
  gpik_req.ik_request.pose_stamped.pose.orientation.x = qx;
  gpik_req.ik_request.pose_stamped.pose.orientation.y = qy;
  gpik_req.ik_request.pose_stamped.pose.orientation.z = qz;
  gpik_req.ik_request.pose_stamped.pose.orientation.w = qw;
  return moveIK();
}

void tactileController::cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_){

    error_orient_feature = 0;

    if (msg_->contactOrientation.size() > 0){
        std::cout<<"feedback orientation msg = "<< msg_->contactOrientation.at(0)  <<std::endl;

        feedback_orient_feature = msg_->contactOrientation.at(0)* M_PI / 180;
        std::cout<<"feedback orientation = "<< feedback_orient_feature * 180 / M_PI  <<std::endl;
        is_feedback_orient_rec = true;
    }
    else{
        is_feedback_orient_rec = false;
        std::cout<<" empty feedback"<<std::endl;

    }
    return;
}


void tactileController::cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_){
    if(msg_->contactOrientation.size() > 0){
        des_orient_feature = msg_->contactOrientation.at(0) * M_PI / 180;
        std::cout<<"desired orientation = "<< des_orient_feature * 180 / M_PI <<std::endl;
        is_des_orient_rec = true;
    }
    else{
        is_feedback_orient_rec = false;
        std::cout<<" empty desired"<<std::endl;
    }

    return;
}

void tactileController::cb_joint_state(const control_msgs::JointControllerStateConstPtr& msg_){

   joint_val_now = msg_->process_value;
   std::cout<<" joint val now = "<< joint_val_now <<std::endl;
   is_joint_val_rec = true;
   return;

}
void tactileController::cb_rrbot_joint_state(const control_msgs::JointControllerStateConstPtr& msg_){

   rrbot_joint_val_now = msg_->process_value;
   //std::cout<<" rrbot_joint val now in callback = "<< rrbot_joint_val_now <<std::endl;
   is_rrbot_joint_val_rec = true;
   return;

}

void tactileController::set_pid(){
    //use
    return;
}



void tactileController::publish_command (){
    std_msgs::Float64 command;
    command.data = 0;

    if (is_des_orient_rec && is_feedback_orient_rec && is_joint_val_rec){
        //if (is_rrbot_joint_val_rec && is_joint_val_rec){

            // error
            error_orient_feature = feedback_orient_feature-des_orient_feature;
            std::cout<<" feedback_orient_feature = "<< feedback_orient_feature <<std::endl;
            std::cout<<" des_orient_feature = "<< des_orient_feature <<std::endl;
            std::cout<<" error_orient_feature = "<< error_orient_feature <<std::endl;

            /*
             * in case of just rrbot
             * error = Orient_des - (rrbot_val - hand_val)
             * P_out = error*0.1
             * Command = hand_val - P_out
             */
//            error_orient_feature = 1.57 - (rrbot_joint_val_now - joint_val_now) ;
//            std::cout<<" rrbot_joint_val_now = "<< rrbot_joint_val_now <<std::endl;
//            std::cout<<" joint_val_now = "<< joint_val_now <<std::endl;
//            std::cout<<" error_orient_feature = "<< error_orient_feature <<std::endl;

            // integral

            pid_integral = pid_integral_old + error_orient_feature;
            // derivative
            pid_der = error_orient_feature - pid_err_old;


            double temp_int_part = (pid_integ_gain*pid_integral*pid_dt);
            //        std::cout<<" pid_integ_gain = "<< pid_integ_gain <<std::endl;
            //        std::cout<<" pid_integral = "<< pid_integral <<std::endl;
            //        std::cout<<" pid_dt = "<< pid_dt <<std::endl;
            //        std::cout<<" temp_int_part = "<< temp_int_part <<std::endl;

            double temp_der_part = (pid_deriv_gain*pid_der/pid_dt);
            //        std::cout<<" temp_der_part = "<< temp_der_part <<std::endl;
            //        std::cout<<" pid_deriv_gain = "<< pid_deriv_gain <<std::endl;
            //        std::cout<<" pid_der = "<< pid_der <<std::endl;

            double temp_sum_int_der = (temp_der_part + temp_int_part );
            //        std::cout<<" temp_sum_int_der = "<< temp_sum_int_der <<std::endl;

            // out
            pid_out = pid_k_gain*error_orient_feature + temp_sum_int_der;//
            //+ (pid_integ_gain*pid_integral*pid_dt) + (pid_deriv_gain*pid_der/pid_dt);
            // joint val + pid_out
            std::cout<<" pid out = "<< pid_out <<std::endl;

            //std::cout<<"desired orientation = "<< des_orient_feature  <<std::endl;
            //std::cout<<"feedback orientation = "<< feedback_orient_feature  <<std::endl;

            command.data = joint_val_now - pid_out;
            //command.data = joint_val_now;
            pub_command_wrj2.publish(command);
            pid_integral_old = pid_integral;
            pid_err_old = error_orient_feature;
            is_des_orient_rec = false;
            is_feedback_orient_rec = false;
            is_joint_val_rec = false;


            // publish topics to plot
            tactile_servo_msgs::servo_hand vars_to_pub;
            vars_to_pub.header.frame_id = "servo_variables";
            vars_to_pub.header.stamp = ros::Time::now();
            vars_to_pub.actual_orientation = feedback_orient_feature;
            vars_to_pub.desired_orientation = des_orient_feature;
            //rrbot
            // in case when I send only position of rrbot I must track the difference
            // between joint values of the hand and the rrbot (mutual orientation)

//          vars_to_pub.actual_orientation = (rrbot_joint_val_now - joint_val_now);// rrbot_joint_val_now;
//          vars_to_pub.desired_orientation = 1.5708;
            vars_to_pub.error_orientation = error_orient_feature;
            vars_to_pub.hand_orientation = joint_val_now;
            vars_to_pub.obj_real_orientation = rrbot_joint_val_now;

            variables_to_plot.publish(vars_to_pub);

            /**
example 1
        error = setpoint - PV;
         // track error over time, scaled to the timer interval
        integral = integral + (error * Dt);
         // determine the amount of change from the last time checked
        derivative = (error - preError) / Dt;
         // calculate how much to drive the output in order to get to the
         // desired setpoint.
        output = (Kp * error) + (Ki * integral) + (Kd * derivative);
         // remember the error for the next time around.
        preError = error;

example 2
        output = Kp * err + (Ki * integral * dt) + (Kd * der /dt);
        where
        Kp = Proptional Constant.
        Ki = Integral Constant.
        Kd = Derivative Constant.
        err = Expected Output - Actual Output ie. error;
        integral  = integral from previous loop + err; ( i.e. integral error )
        der  = err - err from previous loop; ( i.e. differential error)
        dt = execution time of loop.
*/

            /* In the shadow robot. PID implementation.
    error_position = joint_state_->position_ - command_;
    bool in_deadband = hysteresis_deadband.is_in_deadband(command_, error_position, position_deadband);

    sr_deadband::HysteresisDeadband<double> hysteresis_deadband;
    @return true if the demand is in the deadband
    in sr_utilities/sr_deadband.hpp and sr_math_uthils.hpp


    if (in_deadband)
    error_position = 0.0;
  commanded_effort = pid_controller_position_->computeCommand(-error_position, period);


    boost::scoped_ptr<control_toolbox::Pid> pid_controller_position_;       Internal PID controller for the position loop.

          */


      /* how to use ros::Time::now()
          * void MyControllerClass::update()
          39 {
          40   double desired_pos = init_pos_ + 15 * sin(ros::Time::now().toSec());
          41   double current_pos = joint_state_->position_;
          42   joint_state_->commanded_effort_ = -10 * (current_pos - desired_pos);
          43 }
      */

    }
    else{
        command.data = joint_val_now;
        pub_command_wrj2.publish(command);

    }

    return;
}

bool begin_servo_controller;


/// we start controller when we want by calling the
/// service "start_servo_controller_service"
/// callback function
/// changes the global variable to true.

bool start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req, tactile_servo_srvs::start_servo_controller::Response &res){
 begin_servo_controller = true;
    return true;
}



int main(int argc, char** argv)
{
    //init the ros node
    ros::init(argc, argv, "tactile_controller");
    ros::NodeHandle nh;



    // prepare services
    // prepare services
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk");

    ros::ServiceClient query_client = nh.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::ServiceClient query_client_fk = nh.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::ServiceClient ik_client;
    ik_client = nh.serviceClient<moveit_msgs::GetPositionIK>("shadow_right_arm_kinematics/get_ik");

    //++ INVOKE SERVICE GET_IK_SOLVER_INFO, to get joint information:
    query_client.call(request,response);
    // get possible computable joints
    if(query_client.call(request,response))
    {
      for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
      {
        ROS_INFO("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
      }
    }
    else
    {
      ROS_INFO("Could not call query service");
      ros::shutdown();
      exit(1);
    }
    query_client_fk.call(request,response);


    //++FILL REQUEST
    // define the service messages
//    gpik_req.timeout = ros::Duration(5.0);
    gpik_req.ik_request.timeout = ros::Duration(5.0);

    gpik_req.ik_request.ik_link_name = "palm";



    gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
    gpik_req.ik_request.pose_stamped.pose.position.x = 0.526;//0.36513;
    gpik_req.ik_request.pose_stamped.pose.position.y = -0.134;//
    gpik_req.ik_request.pose_stamped.pose.position.z = 0.230;//1.0913;

    //pos is not relevant with the used ik_solver but set it anyway to identity
    gpik_req.ik_request.pose_stamped.pose.orientation.x = 0.66;//0.70701;
    gpik_req.ik_request.pose_stamped.pose.orientation.y = 0.26;//-0.023199;
    gpik_req.ik_request.pose_stamped.pose.orientation.z = 0.667;//0.70671;
    gpik_req.ik_request.pose_stamped.pose.orientation.w = 0.312;//0.012863;
    gpik_req.ik_request.robot_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
//    gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    gpik_req.ik_request.robot_state.joint_state.name = response.kinematic_solver_info.joint_names;

    //+ INVOKE SERVICE WITH REQUEST AND TREAT RESPONSE LATER:
    ik_client.call(gpik_req,gpik_res);

    if(ik_client.call(gpik_req, gpik_res))
    {
        if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
        {
            std_msgs::Float64 message;
            for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
            {
                ROS_INFO("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
//                ROS_INFO("we publish to %s",pub[i].getTopic().c_str());
                message.data= (double)gpik_res.solution.joint_state.position[i];
                std::cout << "(double)gpik_res.solution.joint_state.position at "<<i<<"is ="<<(double)gpik_res.solution.joint_state.position[i] <<std::endl;
//                pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
            }
            std::cout<< "soluton for joint name " <<gpik_res.solution.joint_state.name.at(0)<< std::endl;
            std::cout<< "soluton for " <<gpik_res.solution.joint_state.position.at(0)<<std::endl;
        }
        else
            ROS_INFO("Inverse kinematics failed");
        std::cout << "error = "<< gpik_res.error_code.val <<std::endl;

    }
    else
        ROS_INFO("Inverse kinematics service call failed %d",gpik_res.error_code.val);

    ros::spinOnce();
    // I can write service server here
    // while loop will stop when service client
    // will send a request to start controller

//    ros::ServiceServer service = nh.advertiseService("start_servo_controller_service", start_servo_control);
//    ros::spinOnce();

//    while(!begin_servo_controller){
//        ROS_INFO("not started servo");
//        ros::Duration(1).sleep();
//        ros::spinOnce();
//    }


    ROS_INFO("SERVO started ");

    tactileController weiss_wrist(1,0);
    ros::spinOnce();

    ros::Rate loop_rate(10);

    while( ros::ok() ){
//        weiss_wrist.publish_command();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
