/// for cartesian control using kdl
///
#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <moveit_msgs/GetKinematicSolverInfo.h>
#include <moveit_msgs/GetPositionIK.h>
#include <moveit_msgs/GetPositionFK.h>
#include <ros/package.h>

// to send commands and read joint states
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>

// dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include <tactile_controller/cartesian_practise_paramsConfig.h>

// for transformations
#include <geometry_msgs/PointStamped.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <visualization_msgs/Marker.h>

// begin to servo
#include "tactile_servo_srvs/start_servo_controller.h"

class CartesianPractise
{
public:
    CartesianPractise();
    virtual ~CartesianPractise();
    ros::NodeHandle nh;

    moveit_msgs::GetPositionIK::Request  gpik_req;
    moveit_msgs::GetPositionIK::Response gpik_res;

    moveit_msgs::GetKinematicSolverInfo::Request request;
    moveit_msgs::GetKinematicSolverInfo::Response response;

    ros::Publisher pub[6];
    visualization_msgs::Marker create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                                                std::string frame_name_, float color_);
    void get_frame_pose();
    void viz2();
    void viz();
    void getIK();
    void print();
    void cb_dynamic_reconf(tactile_controller::cartesian_practise_paramsConfig &config, uint32_t level);
    dynamic_reconfigure::Server<tactile_controller::cartesian_practise_paramsConfig> change_pose_server;
    dynamic_reconfigure::Server<tactile_controller::cartesian_practise_paramsConfig> :: CallbackType f;

    // for IK service
    float x, y, z, qx, qy, qz, qw, tx, ty,tz;

    // for rviz marker current pose
    float xx, yy, zz, qxx, qyy, qzz, qww, txx, tyy,tzz;
    // for rviz marker des pose
    float x_world, y_world, z_world, qx_world, qy_world, qz_world, qw_world;
    // publish joint commands
    std::vector<std_msgs::Float64> command;
    std::vector<float> joints_commands;
    std::vector<std::string> joints_names;



    ros::ServiceClient ik_client;
    ros::ServiceClient query_client;
    ros::ServiceClient query_client_fk;

    // for RVIZ
    ros::Publisher vis_pub_des_cart_pose;
    ros::Publisher vis_pub_des_cart_pose_des;

    // publish to joints
    std::vector<ros::Publisher> Publishers_joints;

    tf::TransformListener tf_listener;

    geometry_msgs::PoseStamped convert_pose(geometry_msgs::PoseStamped& pose_in_);

    std::string controller_type;

    void pub_res();

    bool isready;
    bool begin_servo_controller;

    ros::ServiceServer service;

    bool start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req,
                             tactile_servo_srvs::start_servo_controller::Response &res);
};


CartesianPractise::CartesianPractise(){
    f = boost::bind(&CartesianPractise::cb_dynamic_reconf, this, _1, _2);
    change_pose_server.setCallback(f);


    // prepare services
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_ik");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk_solver_info");
    ros::service::waitForService("shadow_right_arm_kinematics/get_fk");

    service = nh.advertiseService("start_servo_controller_service_cart", &CartesianPractise::start_servo_control, this);


    query_client = nh.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
    query_client_fk = nh.serviceClient<moveit_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");

    ik_client = nh.serviceClient<moveit_msgs::GetPositionIK>("shadow_right_arm_kinematics/get_ik");


    vis_pub_des_cart_pose = nh.advertise<visualization_msgs::Marker>( "visualization_marker_cur_pose", 0 );
    vis_pub_des_cart_pose_des = nh.advertise<visualization_msgs::Marker>( "visualization_marker_des_pose", 0 );
    x = 0.526;
    y = -0.134;
    z = 0.230;
    qx = 0.66;
    qy = 0.26;
    qz = 0.667;
    qw = 0.312;
/*
    x = 0.366;
    y = -0.14;
    z = 1.09115;
    qx = 0.707;
    qy = -0.14;
    qz = 1.09115;
    qw = 0.01745;
*/
    xx = 0.526;
    yy = -0.134;
    zz = 0.230;
    qxx = 0.66;
    qyy = 0.26;
    qzz = 0.667;
    qww = 0.312;
    controller_type = "_position_controller";

    std::string joint_names_str[6]={"sa_sr", "sa_ss", "sa_es",
                                    "sa_er", "sh_wrj2","sh_wrj1"};


    for (int i = 0; i < 6; ++i){
        joints_names.push_back(joint_names_str[i] );
    }

//    std::vector<float> matrix;
//    for (int i=0; i < cells_x * cells_y; ++i){
//        matrix.push_back(0.0);
//    }

    float joints_commands_arr[6] = {0,0,0,0,0,0};
    for (int i = 0; i < 6; ++i){
        joints_commands.push_back(joints_commands_arr[i] );
    }

    for(int i=0;i<6;i++){
        std::string topicname="/"+joint_names_str[i]+controller_type+"/command";
        this->Publishers_joints.push_back( nh.advertise<std_msgs::Float64>(topicname, 1));
    }

    begin_servo_controller = false;


}
CartesianPractise::~CartesianPractise(){}

bool CartesianPractise::start_servo_control(tactile_servo_srvs::start_servo_controller::Request &req, tactile_servo_srvs::start_servo_controller::Response &res){
 begin_servo_controller = true;
    return true;
}

//    ros::ServiceServer service = nh.advertiseService("start_servo_controller_service", start_servo_control);
//    ros::spinOnce();

//    while(!begin_servo_controller){
//        ROS_INFO("not started servo");
//        ros::Duration(1).sleep();
//        ros::spinOnce();
//    }



void CartesianPractise::cb_dynamic_reconf(tactile_controller::cartesian_practise_paramsConfig &config, uint32_t level){
//    x = config.x;
//    y = config.y;
//    z = config.z;
//    qz = config.qz;
//    qx = config.qx;
//    qy = config.qy;
//    qw = config.qw;

    geometry_msgs::PoseStamped des_pose_set;
    des_pose_set.header.frame_id = "world";
    des_pose_set.header.stamp = ros::Time();
    des_pose_set.pose.position.x = config.x;
    des_pose_set.pose.position.y = config.y;
    des_pose_set.pose.position.z = config.z;


    tf::Transform transform;
    transform.setOrigin( tf::Vector3(des_pose_set.pose.position.x, des_pose_set.pose.position.y, des_pose_set.pose.position.z) );
    tf::Quaternion q;
    q.setRPY(config.qx, config.qy, config.qz);
    transform.setRotation(q);
    des_pose_set.pose.orientation.x = q.getX();
    des_pose_set.pose.orientation.y = q.getY();
    des_pose_set.pose.orientation.z = q.getZ();
    des_pose_set.pose.orientation.w = q.getW();



    x_world = des_pose_set.pose.position.x;
    y_world = des_pose_set.pose.position.y;
    z_world = des_pose_set.pose.position.z;
    qx_world = des_pose_set.pose.orientation.x;
    qy_world = des_pose_set.pose.orientation.y;
    qz_world = des_pose_set.pose.orientation.z;
    qw_world = des_pose_set.pose.orientation.w;

    geometry_msgs::PoseStamped des_pose_wrt_base_set = convert_pose(des_pose_set);

    x = des_pose_wrt_base_set.pose.position.x;
    y = des_pose_wrt_base_set.pose.position.y;
    z = des_pose_wrt_base_set.pose.position.z;
    qx = des_pose_wrt_base_set.pose.orientation.x;
    qy = des_pose_wrt_base_set.pose.orientation.y;
    qz = des_pose_wrt_base_set.pose.orientation.z;
    qw = des_pose_wrt_base_set.pose.orientation.w;


    ROS_INFO("x:%f", x);
}

void CartesianPractise::print(){
    std::cout<<"For IK Solver"<<std::endl;
    std::cout<<"x="<<x<<std::endl;
    std::cout<<"y="<<y<<std::endl;
    std::cout<<"z="<<z<<std::endl;
    std::cout<<"qx="<<qx<<std::endl;
    std::cout<<"qy="<<qy<<std::endl;
    std::cout<<"qz="<<qz<<std::endl;
    std::cout<<"qw="<<qw<<std::endl;

//    std::cout<<" command.at(0).data"<<  command.at(0).data << std::endl;
    for (int i = 0; i < 6; ++i){
        std::cout<<" joints_commands at"<< i<< std::endl;
        std::cout<<" = " <<joints_commands.at(i) <<  std::endl;
    }
}

void CartesianPractise::getIK(){
    // FK
    //++ INVOKE SERVICE GET_IK_SOLVER_INFO, to get joint information:
    query_client.call(request,response);
    // get possible computable joints
    if(query_client.call(request,response))
    {
      for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
      {
//        ROS_INFO("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
      }
    }
    else
    {
      ROS_INFO("Could not call query service");
      ros::shutdown();
      exit(1);
    }
    query_client_fk.call(request,response);
    // IK
    //++FILL REQUEST
    // define the service messages
//    gpik_req.timeout = ros::Duration(5.0);
    gpik_req.ik_request.timeout = ros::Duration(5.0);

    gpik_req.ik_request.ik_link_name = "palm";
    gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
    gpik_req.ik_request.pose_stamped.pose.position.x = x;// 0.526;//0.36513;
    gpik_req.ik_request.pose_stamped.pose.position.y = y;//
    gpik_req.ik_request.pose_stamped.pose.position.z = z;//1.0913;

    //pos is not relevant with the used ik_solver but set it anyway to identity
    gpik_req.ik_request.pose_stamped.pose.orientation.x = qx;//0.70701;
    gpik_req.ik_request.pose_stamped.pose.orientation.y = qy;//-0.023199;
    gpik_req.ik_request.pose_stamped.pose.orientation.z = qz;//0.70671;
    gpik_req.ik_request.pose_stamped.pose.orientation.w = qw;//0.012863;
    gpik_req.ik_request.robot_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
//    gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    gpik_req.ik_request.robot_state.joint_state.name = response.kinematic_solver_info.joint_names;

    //+ INVOKE SERVICE WITH REQUEST AND TREAT RESPONSE LATER:
    ik_client.call(gpik_req,gpik_res);

    if(ik_client.call(gpik_req, gpik_res))
    {
        if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
        {
            std_msgs::Float64 message;
            for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
            {
                ROS_INFO("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
//                ROS_INFO("we publish to %s",pub[i].getTopic().c_str());
                joints_commands.at(i) = (gpik_res.solution.joint_state.position[i]);
//                joints_commands_arr[i] = joints_commands.at(i);
                message.data= (double)gpik_res.solution.joint_state.position[i];
                isready = true;
                //std::cout << "(double)gpik_res.solution.joint_state.position at "<<i<<"is ="<<(double)gpik_res.solution.joint_state.position[i] <<std::endl;
//                pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
            }
//            std::cout<< "soluton for joint name " << gpik_res.solution.joint_state.name.at(0)<< std::endl;
//            std::cout<< "soluton for " <<gpik_res.solution.joint_state.position.at(0)<<std::endl;
        }
        else
            ROS_INFO("Inverse kinematics failed");
        std::cout << "error = "<< gpik_res.error_code.val <<std::endl;

    }
    else
        ROS_INFO("Inverse kinematics service call failed %d",gpik_res.error_code.val);
    ros::spinOnce();
}


visualization_msgs::Marker CartesianPractise::create_marker(geometry_msgs::PoseStamped& point_to_viz_,
                                                            std::string frame_name_, float color_){
    // draw in RVIZ
    u_int32_t shape = visualization_msgs::Marker::ARROW;
    //shape = visualization_msgs::Marker::SPHERE;
    visualization_msgs::Marker marker;
    marker.header.frame_id = "/"+frame_name_;
    marker.header.stamp = ros::Time();
    marker.ns = "orientations";
    marker.id = 0;
    marker.type = shape;
    marker.action = visualization_msgs::Marker::ADD;
    marker.pose.position.x = point_to_viz_.pose.position.x;
    marker.pose.position.y = point_to_viz_.pose.position.y;
    marker.pose.position.z = point_to_viz_.pose.position.z;

    marker.pose.orientation.x = point_to_viz_.pose.orientation.x;
    marker.pose.orientation.y = point_to_viz_.pose.orientation.y;
    marker.pose.orientation.z = point_to_viz_.pose.orientation.z;
    marker.pose.orientation.w = point_to_viz_.pose.orientation.w;

//    marker.pose.orientation.x = point_to_viz_.pose.orientation.x;
//    marker.pose.orientation.y = point_to_viz_.pose.orientation.y;
//    marker.pose.orientation.z = point_to_viz_.pose.orientation.z;
//    marker.pose.orientation.w = point_to_viz_.pose.orientation.w;
    marker.scale.x = 0.5;
    marker.scale.y = 0.01;
    marker.scale.z = 0.01;
    marker.color.a = 1.0; // Don't forget to set the alpha!
    marker.color.r = color_;
    marker.color.g = 1.0;
    marker.color.b = 0.0;

    return marker;
}


void CartesianPractise::viz(){

    geometry_msgs::PoseStamped des_pose;
    des_pose.header.frame_id = "world";
    des_pose.header.stamp = ros::Time();
    des_pose.pose.position.x = x_world;
    des_pose.pose.position.y = y_world;
    des_pose.pose.position.z = z_world;

    des_pose.pose.orientation.x = qx_world;
    des_pose.pose.orientation.y = qy_world;
    des_pose.pose.orientation.z = qz_world;
    des_pose.pose.orientation.w = qw_world;

    std::cout<<" For des pose viz marker "<<x_world<<std::endl;
    std::cout<<"des pose x = "<<x_world<<std::endl;
    std::cout<<"des pose y = "<<y_world<<std::endl;
    std::cout<<"des pose z = "<<z_world<<std::endl;
    std::cout<<"des pose qx_world = "<<qx_world<<std::endl;
    std::cout<<"des pose qy_world = "<<qy_world<<std::endl;
    std::cout<<"des pose qz_world = "<<qz_world<<std::endl;
    std::cout<<"des pose qw_world = "<<qw_world<<std::endl;


    visualization_msgs::Marker marker_des_pose = create_marker(des_pose, "world", 0);

    vis_pub_des_cart_pose_des.publish( marker_des_pose );

}

geometry_msgs::PoseStamped CartesianPractise::convert_pose(geometry_msgs::PoseStamped& pose_in_){
    geometry_msgs::PoseStamped pose_out;
    tf_listener.waitForTransform("shadowarm_base", ros::Time(0), "world",ros::Time(0), "world", ros::Duration(10));


    try{
        tf_listener.transformPose("shadowarm_base", pose_in_, pose_out);
        ROS_INFO("at world: (%.4f, %.4f. %.4f) -----> at shadowarm_base: (%.4f, %.4f, %.4f) at time %.4f",
                 pose_in_.pose.position.x, pose_in_.pose.position.y, pose_in_.pose.position.z,
                 pose_out.pose.position.x, pose_out.pose.position.y, pose_out.pose.position.z, pose_out.header.stamp.toSec());
        return pose_out;
    }
    catch(tf::TransformException& ex){
        ROS_ERROR("Received an exception trying to transform a point from \"/world\" to \"/weiss\": %s", ex.what());
        return pose_in_;
    }


}

void CartesianPractise::viz2(){

    geometry_msgs::PoseStamped des_pose;
    des_pose.header.frame_id = "world";
    des_pose.header.stamp = ros::Time();
    des_pose.pose.position.x = xx;
    des_pose.pose.position.y = yy;
    des_pose.pose.position.z = zz;


    des_pose.pose.orientation.x = qxx;
    des_pose.pose.orientation.y = qyy;
    des_pose.pose.orientation.z = qzz;
    des_pose.pose.orientation.w = qww;

    geometry_msgs::PoseStamped des_pose_wrt_base = convert_pose(des_pose);


/*
    x = des_pose_wrt_base.pose.position.x;
    y = des_pose_wrt_base.pose.position.y;
    z = des_pose_wrt_base.pose.position.z;
    qx = des_pose_wrt_base.pose.orientation.x;
    qy = des_pose_wrt_base.pose.orientation.y;
    qz = des_pose_wrt_base.pose.orientation.z;
    qw = des_pose_wrt_base.pose.orientation.w;

*/

    visualization_msgs::Marker marker_des_pose = create_marker(des_pose, "world", 1.0);

    vis_pub_des_cart_pose.publish( marker_des_pose );

}


void CartesianPractise::get_frame_pose(){
    tf_listener.waitForTransform("world", ros::Time(0), "palm",ros::Time(0), "world", ros::Duration(10));

    tf::StampedTransform transform;
    //from world to weiss
    try{
        tf_listener.lookupTransform("/world", "/palm",
                                 ros::Time(0), transform);
    }
    catch (tf::TransformException ex){
        ROS_ERROR("%s",ex.what());
        ros::Duration(1.0).sleep();
    }
    std::cout<<"transformation frame name = "<<transform.frame_id_<<std::endl;
    std::cout<<"transformation frame name = "<<transform.child_frame_id_<<std::endl;

    xx = transform.getOrigin().x();
    yy = transform.getOrigin().y();
    zz = transform.getOrigin().z();

    qxx = transform.getRotation().getX();
    qyy = transform.getRotation().getY();
    qzz = transform.getRotation().getZ();
    qww = transform.getRotation().getW();
    std::cout<<"current pose viz marker = "<<std::endl;
    std::cout<<"curent pose xx = "<<xx<<std::endl;
    std::cout<<"curent pose yy = "<<yy<<std::endl;
    std::cout<<"curent pose zz = "<<zz<<std::endl;
    std::cout<<"curent pose qxx = "<<qxx<<std::endl;
    std::cout<<"curent pose qyy = "<<qyy<<std::endl;
    std::cout<<"curent pose qzz = "<<qzz<<std::endl;
    std::cout<<"curent pose qww = "<<qww<<std::endl;
    std::cout<<"get axis "<<std::endl;
    std::cout<<"curent pose rot x = "<<transform.getRotation().getAxis().getX()<<std::endl;
    std::cout<<"curent pose rot y = "<<transform.getRotation().getAxis().getY()<<std::endl;
    std::cout<<"curent pose rot z = "<<transform.getRotation().getAxis().getZ()<<std::endl;
    tf::Quaternion q;
    q.setX(qxx);
    q.setY(qyy);
    q.setZ(qzz);
    q.setW(qww);

    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);
    std::cout<<" quartenion to rpy "<<std::endl;
    std::cout<<" roll = "<<roll<<std::endl;
    std::cout<<" pitch = "<<pitch<<std::endl;
    std::cout<<" yaw = "<<yaw<<std::endl;




}

void CartesianPractise::pub_res(){
    std_msgs::Float64 single_command;

    if ((isready) && (begin_servo_controller)){
//        for (int i = 0; i < this->Publishers_joints.size();++i){
//            std::cout<<" joints_commands_arr = "<<joints_commands_arr[i]<<std::endl;
//        }
        for (int i = 0; i < this->Publishers_joints.size();++i){
            std::cout<<" joints_commands = "<<joints_commands.at(i)<<std::endl;
        }
        for (int i = 0; i < this->Publishers_joints.size();++i){
            single_command.data = joints_commands.at(i);
            this->Publishers_joints[i].publish(single_command);
        }
        isready = false;
        begin_servo_controller = false;
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "practise_cartesian");
    ros::NodeHandle n;

    ros::Rate loop_rate(10);
    CartesianPractise practise1;

    while( ros::ok() ){
        practise1.get_frame_pose();
        practise1.print();
        practise1.getIK();
        practise1.viz();
        practise1.viz2();
        practise1.pub_res();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
/*
 *     //++ INVOKE SERVICE GET_IK_SOLVER_INFO, to get joint information:
    query_client.call(request,response);
    // get possible computable joints
    if(query_client.call(request,response))
    {
      for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
      {
        ROS_INFO("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
      }
    }
    else
    {
      ROS_INFO("Could not call query service");
      ros::shutdown();
      exit(1);
    }
    query_client_fk.call(request,response);

    //++FILL REQUEST
    // define the service messages
    //    gpik_req.timeout = ros::Duration(5.0);
    gpik_req.ik_request.timeout = ros::Duration(5.0);
    gpik_req.ik_request.ik_link_name = "palm";
    gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
    gpik_req.ik_request.pose_stamped.pose.position.x = 0.526;//0.36513;
    gpik_req.ik_request.pose_stamped.pose.position.y = -0.134;//
    gpik_req.ik_request.pose_stamped.pose.position.z = 0.230;//1.0913;
    //pos is not relevant with the used ik_solver but set it anyway to identity
    gpik_req.ik_request.pose_stamped.pose.orientation.x = 0.66;//0.70701;
    gpik_req.ik_request.pose_stamped.pose.orientation.y = 0.26;//-0.023199;
    gpik_req.ik_request.pose_stamped.pose.orientation.z = 0.667;//0.70671;
    gpik_req.ik_request.pose_stamped.pose.orientation.w = 0.312;//0.012863;
    gpik_req.ik_request.robot_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
//    gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
    gpik_req.ik_request.robot_state.joint_state.name = response.kinematic_solver_info.joint_names;

    //+ INVOKE SERVICE WITH REQUEST AND TREAT RESPONSE LATER:
    ik_client.call(gpik_req,gpik_res);

    if(ik_client.call(gpik_req, gpik_res))
    {
        if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
        {
            std_msgs::Float64 message;
            for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
            {
                ROS_INFO("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
//                ROS_INFO("we publish to %s",pub[i].getTopic().c_str());
                message.data= (double)gpik_res.solution.joint_state.position[i];
                std::cout << "(double)gpik_res.solution.joint_state.position at "<<i<<"is ="<<(double)gpik_res.solution.joint_state.position[i] <<std::endl;
//                pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
            }
            std::cout<< "soluton for joint name " <<gpik_res.solution.joint_state.name.at(0)<< std::endl;
            std::cout<< "soluton for " <<gpik_res.solution.joint_state.position.at(0)<<std::endl;
        }
        else
            ROS_INFO("Inverse kinematics failed");
        std::cout << "error = "<< gpik_res.error_code.val <<std::endl;

    }
    else
        ROS_INFO("Inverse kinematics service call failed %d",gpik_res.error_code.val);

    ros::spinOnce();
    // I can write service server here
    // while loop will stop when service client
    // will send a request to start controller

//    ros::ServiceServer service = nh.advertiseService("start_servo_controller_service", start_servo_control);
//    ros::spinOnce();

//    while(!begin_servo_controller){
//        ROS_INFO("not started servo");
//        ros::Duration(1).sleep();
//        ros::spinOnce();
//    }


*/

/*
 * int moveIK()
{
    //seed is not relevant with the used ik_solver but set it anyway
    for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
    {
        gpik_req.ik_request.ik_seed_state.joint_state.position[i] = 0.5;
    }

  if(ik_client.call(gpik_req, gpik_res))
  {
    if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
    {
      std_msgs::Float64 message;
      for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
      {
        ROS_DEBUG("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
        if(pub[i]!=NULL)
        {
        if((gpik_res.solution.joint_state.name[i].compare("WRJ1") && gpik_res.solution.joint_state.name[i].compare("WRJ2")) || !no_wrist)
        {
                ROS_DEBUG("we publish to %s",pub[i].getTopic().c_str());
                message.data= (double)gpik_res.solution.joint_state.position[i];
                pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
        }
       }
      }
      ros::spinOnce();
      return 0;
    }
    else
      ROS_WARN("Inverse kinematics failed");
    return -1;
  }
  else
    ROS_ERROR("Inverse kinematics service call failed %d",gpik_res.error_code.val);
  return -1;
}

int moveIK6D(float x, float y, float z, float qw, float qx, float qy, float qz)
{
  gpik_req.ik_request.pose_stamped.pose.position.x = x;
  gpik_req.ik_request.pose_stamped.pose.position.y = y;
  gpik_req.ik_request.pose_stamped.pose.position.z = z;
  gpik_req.ik_request.pose_stamped.pose.orientation.x = qx;
  gpik_req.ik_request.pose_stamped.pose.orientation.y = qy;
  gpik_req.ik_request.pose_stamped.pose.orientation.z = qz;
  gpik_req.ik_request.pose_stamped.pose.orientation.w = qw;
  return moveIK();
}

void callbackPose6D(const geometry_msgs::PoseStamped& msg)
{
    if(moveIK6D(msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,
msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z)==0)
        ROS_INFO("Moving to x:%f y:%f z:%f qw:%f qx:%f qy:%f qz:%f",msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
    else
        ROS_WARN("Cannot move to x:%f y:%f z:%f qw:%f qx:%f qy:%f qz:%f",msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
}
*/
