#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <kinematics_msgs/GetKinematicSolverInfo.h>
#include <kinematics_msgs/GetPositionIK.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseStamped.h>
#include <map>
#include <string>

#include <std_msgs/Float64.h>
#include <pr2_mechanism_msgs/ListControllers.h>

std::map<std::string,std::string> jointControllerMap;
std::map<std::string,unsigned int> jointPubIdxMap;
ros::Publisher pub[6];
ros::ServiceClient ik_client;
bool no_wrist;
kinematics_msgs::GetPositionIK::Request  gpik_req;
kinematics_msgs::GetPositionIK::Response gpik_res;

kinematics_msgs::GetKinematicSolverInfo::Request request;
kinematics_msgs::GetKinematicSolverInfo::Response response;

//above your main


int moveIK();

int moveIK6D(float x, float y, float z, float qw, float qx, float qy, float qz)
{
  gpik_req.ik_request.pose_stamped.pose.position.x = x;
  gpik_req.ik_request.pose_stamped.pose.position.y = y;
  gpik_req.ik_request.pose_stamped.pose.position.z = z;
  gpik_req.ik_request.pose_stamped.pose.orientation.x = qx;
  gpik_req.ik_request.pose_stamped.pose.orientation.y = qy;
  gpik_req.ik_request.pose_stamped.pose.orientation.z = qz;
  gpik_req.ik_request.pose_stamped.pose.orientation.w = qw;
  return moveIK();
}
// get IK for x,y,z
int moveIK3D(float x, float y, float z)
{
  gpik_req.ik_request.pose_stamped.pose.position.x = x;
  gpik_req.ik_request.pose_stamped.pose.position.y = y;
  gpik_req.ik_request.pose_stamped.pose.position.z = z;
  //orientation set to 0 (maybe set it to always accessible val)0.626, 0.260, 0.666, 0.312
  gpik_req.ik_request.pose_stamped.pose.orientation.x = 0.5;
  gpik_req.ik_request.pose_stamped.pose.orientation.y = 0.5;
  gpik_req.ik_request.pose_stamped.pose.orientation.z = 0.5;
  gpik_req.ik_request.pose_stamped.pose.orientation.w = 0.5;
  return moveIK();
}

// process IK send joint pos to joint pos controllers
int moveIK()
{
	//seed is not relevant with the used ik_solver but set it anyway
	for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
	{
		gpik_req.ik_request.ik_seed_state.joint_state.position[i] = 0.5;
	}
          
  if(ik_client.call(gpik_req, gpik_res))
  {
    if(gpik_res.error_code.val == gpik_res.error_code.SUCCESS)
    {
      std_msgs::Float64 message;
      for(unsigned int i=0; i < gpik_res.solution.joint_state.name.size(); i ++)
      {
        ROS_DEBUG("Joint: %s %f",gpik_res.solution.joint_state.name[i].c_str(),gpik_res.solution.joint_state.position[i]);
        if(pub[i]!=NULL)
        {
		if((gpik_res.solution.joint_state.name[i].compare("WRJ1") && gpik_res.solution.joint_state.name[i].compare("WRJ2")) || !no_wrist)
		{ 
	        	ROS_DEBUG("we publish to %s",pub[i].getTopic().c_str());
	       		message.data= (double)gpik_res.solution.joint_state.position[i];
	  	        pub[jointPubIdxMap[gpik_res.solution.joint_state.name[i]]].publish(message);
		}
       }
      }
      ros::spinOnce();
      return 0;
    }
    else
      ROS_WARN("Inverse kinematics failed");
    return -1;
  }
  else
    ROS_ERROR("Inverse kinematics service call failed %d",gpik_res.error_code.val);
  return -1;
}

// get IK for Vector3 in_vec and send joint pos to joint pos controllers
int moveIK3D(tf::Vector3 in_vec)
{
  return moveIK3D(in_vec.x(), in_vec.y(),in_vec.z());
}

void callback(const geometry_msgs::Vector3& msg)
{	
    tf::Vector3 vec(msg.x,msg.y,msg.z);
    
    if(moveIK3D(vec)==0)
	ROS_INFO("Moving to x:%f y:%f z:%f",vec.x(),vec.y(),vec.z());
    else
        ROS_INFO("Cannot move to x:%f y:%f z:%f",vec.x(),vec.y(),vec.z());
}

void callbackPose3D(const geometry_msgs::PoseStamped& msg)
{	
    
    tf::Vector3 vec(msg.pose.position.x,msg.pose.position.y,msg.pose.position.z);
    
    if(moveIK3D(vec)==0)
	ROS_INFO("Moving to x:%f y:%f z:%f",vec.x(),vec.y(),vec.z());
    else
        ROS_INFO("Cannot move to x:%f y:%f z:%f",vec.x(),vec.y(),vec.z());
}

void callbackPose6D(const geometry_msgs::PoseStamped& msg)
{
    if(moveIK6D(msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,
msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z)==0)
        ROS_INFO("Moving to x:%f y:%f z:%f qw:%f qx:%f qy:%f qz:%f",msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
    else
        ROS_WARN("Cannot move to x:%f y:%f z:%f qw:%f qx:%f qy:%f qz:%f",msg.pose.position.x,msg.pose.position.y,msg.pose.position.z,msg.pose.orientation.w,msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z);
}

int main(int argc, char **argv){

  no_wrist=false;
  if(argc>1)
  {
	std::string no_wrist_in=argv[1];
	ROS_INFO("Received :%s",no_wrist_in.c_str());
	if(!no_wrist_in.compare("no_wrist"))
		no_wrist=true;
  }	  	

  ros::init (argc, argv, "arm_IK_listener");
  ros::NodeHandle rh;

  if(no_wrist)
	ROS_WARN("Not publishing Wrist joints");
 
  
  
  // prepare services
  ros::service::waitForService("shadow_right_arm_kinematics/get_ik_solver_info");
  ros::service::waitForService("shadow_right_arm_kinematics/get_ik");

  ros::ServiceClient query_client = rh.serviceClient<kinematics_msgs::GetKinematicSolverInfo>("shadow_right_arm_kinematics/get_ik_solver_info");
  ik_client = rh.serviceClient<kinematics_msgs::GetPositionIK>("shadow_right_arm_kinematics/get_ik");

  ros::service::waitForService("/sr_controller_manager/list_controllers");
  ROS_INFO("connected to sr_controller_manager");
  ros::ServiceClient controller_list_client = rh.serviceClient<pr2_mechanism_msgs::ListControllers>("/sr_controller_manager/list_controllers");
  ROS_INFO("got service response");
  // init treated joint (to be modified to get more generic behaviour)
  std::string controlled_joint_name;
  std::vector<std::string> joint_name;
  
  joint_name.push_back("ElbowJRotate");
  joint_name.push_back("ElbowJSwing");
  joint_name.push_back("ShoulderJSwing");
  joint_name.push_back("ShoulderJRotate");
  joint_name.push_back("WRJ1");
  joint_name.push_back("WRJ2");


  // init jointControllerMapping
  pr2_mechanism_msgs::ListControllers controller_list;
  controller_list_client.call(controller_list);
  for (unsigned int i=0;i<controller_list.response.controllers.size() ;i++ )
  {
    if (rh.getParam("/"+controller_list.response.controllers[i]+"/joint", controlled_joint_name))
    {
      ROS_INFO("controller %d:%s controls joint %s\n",i,controller_list.response.controllers[i].c_str(),controlled_joint_name.c_str());
      jointControllerMap[controlled_joint_name]= controller_list.response.controllers[i] ;
    }
  }

  // get possible computable joints
  if(query_client.call(request,response))
  {
    for(unsigned int i=0; i< response.kinematic_solver_info.joint_names.size(); i++)
    {
      ROS_DEBUG("Joint: %d %s",i,response.kinematic_solver_info.joint_names[i].c_str());
    }
  }
  else
  {
    ROS_ERROR("Could not call query service");
    ros::shutdown();
    exit(1);
  }
  
  //TODO: verify treated joint MATCH computable joints

  // define the service messages
  gpik_req.timeout = ros::Duration(5.0);
  gpik_req.ik_request.ik_link_name = "palm";
  gpik_req.ik_request.pose_stamped.header.frame_id = "shadowarm_base";
  gpik_req.ik_request.pose_stamped.pose.position.x = 0.526;
  gpik_req.ik_request.pose_stamped.pose.position.y = -0.134;
  gpik_req.ik_request.pose_stamped.pose.position.z = 0.230;

  //pos is not relevant with the used ik_solver but set it anyway to identity
  gpik_req.ik_request.pose_stamped.pose.orientation.x = 0.66;
  gpik_req.ik_request.pose_stamped.pose.orientation.y = 0.26;
  gpik_req.ik_request.pose_stamped.pose.orientation.z = 0.667;
  gpik_req.ik_request.pose_stamped.pose.orientation.w = 0.312;
  gpik_req.ik_request.ik_seed_state.joint_state.position.resize(response.kinematic_solver_info.joint_names.size());
  gpik_req.ik_request.ik_seed_state.joint_state.name = response.kinematic_solver_info.joint_names;


  // prepare the publishers
  for(unsigned int i=0; i < joint_name.size(); i ++)
  {
		std::string controller_name=jointControllerMap[joint_name[i]];
  	if(controller_name.compare("")!=0)
	    pub[i] = rh.advertise<std_msgs::Float64>("/"+jointControllerMap[joint_name[i]]+"/command", 2);
	  else
	  {
	  	ROS_WARN("Could not find a controller for joint %s",joint_name[i].c_str());
	  }
    jointPubIdxMap[joint_name[i]]=i;
  }

  ros::spinOnce();
  sleep(1); // this is required otherwise publishers are not ready for first messages to be sent


  ros::Subscriber sub=rh.subscribe("/move_arm_3D/position",2, callback);
  ros::Subscriber sub2=rh.subscribe("/move_arm_3D/pose",2, callbackPose3D);
  ros::Subscriber sub3=rh.subscribe("/move_arm_6D/pose",2, callbackPose6D);

  while( ros::ok() )
  {
    ros::spin();
  }
  //ros::shutdown();
}



/* For the emacs weenies in the crowd.
Local Variables:
   c-basic-offset: 2
End:
*/
