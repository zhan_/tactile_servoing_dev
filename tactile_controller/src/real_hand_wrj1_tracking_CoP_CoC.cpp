#include <ros/ros.h>
#include <string>

//messages
#include <std_msgs/Float64.h>
#include <control_msgs/JointControllerState.h>
#include "sr_robot_msgs/JointControllerState.h"

#include <tactile_servo_msgs/featuresPlanning.h>
#include <tactile_servo_msgs/featuresControl.h>
#include <tactile_servo_msgs/servo_hand.h>


class tactileController
{
public:
    tactileController(int joint_to_control_, int tactile_sensor_num_);
    virtual ~tactileController();

    ros::NodeHandle nh_;


    void publish_command();
    void cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_);
    void cb_joint_state(const sr_robot_msgs::JointControllerStateConstPtr& msg_);
    void set_pid();
//rrbot

    ros::Publisher pub_command;
    ros::Subscriber sub_contr_features;
    ros::Subscriber sub_des_features;
    ros::Subscriber sub_joint_state;
//rrbot

    // publish topics to plot
    ros::Publisher variables_to_plot;

    // joint number to control
    int joint_to_control;

    // string names joints and controller
    std::string controller_type;
    std::string hand_jointnames[24];
//rrbot
    std::string rrbot_jointname;

    // string names topic to sub
    std::string topic_name_sub_c;
    std::string topic_name_sub_p;
    std::string topic_name_sub_d;

    // string names topic to pub
    std::string topic_name_pub_c;



    // control variable

    //tactile_servo_msgs::featuresControl feeback_feature;
    //tactile_servo_msgs::featuresControl des_feature;

    double error_orient_feature;
    double feedback_orient_feature;
    double des_orient_feature;
    double joint_val_now;
//rrbot
    double rrbot_joint_val_now;

    // allow pub
    bool allow_pub_orient;
    bool is_feedback_orient_rec;
    bool is_des_orient_rec;
    bool is_joint_val_rec;

    //rrbot
    bool is_rrbot_joint_val_rec;

    double pid_k_gain;
    double pid_integ_gain;
    double pid_deriv_gain;
    double pid_out;
    double pid_out_old;
    double pid_dt;
    double pid_der;
    double pid_integral;
    double pid_integral_old;
    double pid_err_old;



};tactileController::tactileController(int joint_to_control_, int tactile_sensor_num_){

    controller_type = "_mixed_position_velocity_controller";
    std::string hand_jointnames[24]={"sh_ffj0","sh_ffj3","sh_ffj4","sh_lfj0","sh_lfj3","sh_lfj4",
                                "sh_lfj5","sh_mfj0","sh_mfj3","sh_mfj4","sh_rfj0","sh_rfj3",
                                "sh_rfj4","sh_thj1","sh_thj2","sh_thj3","sh_thj4","sh_thj5",
                                  "sa_er",  "sa_es",  "sa_ss",  "sa_sr","sh_wrj1","sh_wrj2"};
    joint_to_control = 23;
    if (joint_to_control_ == 1){
        joint_to_control = 23;
    }

    XmlRpc::XmlRpcValue my_list;
    nh_.getParam("/tactile_arrays_param", my_list);
    // get topic names of control features for a given sensor
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["control_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 9th pose");
    topic_name_sub_c = static_cast<std::string> (my_list[tactile_sensor_num_]["control_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["planning_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    topic_name_sub_p = static_cast<std::string> (my_list[tactile_sensor_num_]["planning_topic_name"]);
    ROS_ASSERT_MSG(my_list[tactile_sensor_num_]["desired_topic_name"].getType() == XmlRpc::XmlRpcValue::TypeString, "Test string at 10th pose");
    topic_name_sub_d = static_cast<std::string> (my_list[tactile_sensor_num_]["desired_topic_name"]);

    topic_name_pub_c = hand_jointnames[joint_to_control] + controller_type ;

    //rrbot
    rrbot_jointname = "rrbot/joint3_position_controller/state";

    sub_contr_features = nh_.subscribe("/"+topic_name_sub_c, 1, &tactileController::cb_feature_c, this);
    sub_des_features = nh_.subscribe("/"+topic_name_sub_d, 1, &tactileController::cb_feature_d, this);
    sub_joint_state = nh_.subscribe("/"+topic_name_pub_c + "/state", 1,  &tactileController::cb_joint_state,this);
    //rrbot


    // publish topics to plot
    variables_to_plot = nh_.advertise<tactile_servo_msgs::servo_hand>("servo_hand", 1);


    pub_command = nh_.advertise<std_msgs::Float64>(topic_name_pub_c + "/command", 1);

    pid_integral_old = 0;
    pid_err_old = 0;
    pid_k_gain = 1;
    pid_deriv_gain = 0;
    pid_integ_gain = 0;
    pid_dt = double(1.0/50.0);

}
tactileController::~tactileController(){

}void tactileController::cb_feature_c(const tactile_servo_msgs::featuresControlConstPtr& msg_){

    error_orient_feature = 0;

    if (msg_->contactOrientation.size() > 0){
        std::cout<<"feedback orientation msg = "<< msg_->contactOrientation.at(0)  <<std::endl;

        feedback_orient_feature = msg_->contactOrientation.at(0)* M_PI / 180;
        std::cout<<"feedback orientation = "<< feedback_orient_feature * 180 / M_PI  <<std::endl;
        is_feedback_orient_rec = true;
    }
    else{
        is_feedback_orient_rec = false;
        std::cout<<" empty feedback"<<std::endl;

    }
    return;
}


void tactileController::cb_feature_d(const tactile_servo_msgs::featuresControlConstPtr& msg_){
    if(msg_->contactOrientation.size() > 0){
        des_orient_feature = msg_->contactOrientation.at(0) * M_PI / 180;
        std::cout<<"desired orientation = "<< des_orient_feature * 180 / M_PI <<std::endl;
        is_des_orient_rec = true;
    }
    else{
        is_feedback_orient_rec = false;
        std::cout<<" empty desired"<<std::endl;
    }

    return;
}

void tactileController::cb_joint_state(const  sr_robot_msgs::JointControllerStateConstPtr& msg_){

   joint_val_now = msg_->process_value;
   std::cout<<" joint val now = "<< joint_val_now <<std::endl;
   is_joint_val_rec = true;
   return;

}

void tactileController::publish_command (){
    std_msgs::Float64 command;
    command.data = 0;

    if (is_des_orient_rec && is_feedback_orient_rec && is_joint_val_rec){
     // error
          error_orient_feature = feedback_orient_feature-des_orient_feature;
          pid_out = pid_k_gain*error_orient_feature;
          command.data = joint_val_now + pid_out;
          if (command.data > 0.15) command.data = 0.15;
          if (command.data < -0.28) command.data = -0.28;
          pub_command.publish(command);
            pid_integral_old = pid_integral;
            pid_err_old = error_orient_feature;
            is_des_orient_rec = false;
            is_feedback_orient_rec = false;
            is_joint_val_rec = false;
            // publish topics to plot
            tactile_servo_msgs::servo_hand vars_to_pub;
            vars_to_pub.header.frame_id = "servo_variables";
            vars_to_pub.header.stamp = ros::Time::now();
            
vars_to_pub.actual_orientation = feedback_orient_feature;
            vars_to_pub.desired_orientation = des_orient_feature;
            vars_to_pub.error_orientation = error_orient_feature;
            vars_to_pub.hand_orientation = joint_val_now;
            vars_to_pub.obj_real_orientation = 0;

            variables_to_plot.publish(vars_to_pub);
  }
    else{
        command.data = joint_val_now;
        pub_command.publish(command);

    }

    return;
}
int main(int argc, char** argv)
{
    //init the ros node
    ros::init(argc, argv, "tactile_controller");
    ros::NodeHandle nh;

    ros::spinOnce();
    
    ROS_INFO("SERVO started ");

    tactileController weiss_wrist(1,0);
    ros::spinOnce();

    ros::Rate loop_rate(10);

    while( ros::ok() ){
        weiss_wrist.publish_command();
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
